#!/bin/bash
  
#
# This file is used to automatically stop ATS daemon from within Jenkins build system.
#

PID=`ps -ef | grep ats-core | grep -v "grep" | awk '{print $2}'`
if [ -z "$PID" ]
then
        exit 0
else
        kill -9 $PID
fi