DROP TABLE if exists prescreen_event;
DROP TABLE if exists submission_event;
DROP TABLE if exists client_submission_event;
DROP TABLE if exists interview_event;
DROP TABLE if exists placement_event;

DROP TABLE if exists user_job_order;
DROP TABLE if exists job_order_candidate;
DROP TABLE if exists job_order;
DROP TABLE if exists shortlist_candidate;
DROP TABLE if exists shortlist;
DROP TABLE if exists candidate;

DROP TABLE IF EXISTS eod_report_source;
DROP TABLE IF EXISTS eod_report;

DROP TABLE if exists users_role;
DROP TABLE if exists role_permission;
DROP TABLE if exists sources;
DROP TABLE if exists role;
DROP TABLE if exists users;
DROP TABLE if exists permission;

DROP TABLE if exists end_client;
DROP TABLE if exists client;
DROP TABLE if exists team;
DROP TABLE if exists users_group;

DROP TYPE if exists workflow_stage;
DROP TYPE if exists rate;
DROP TYPE if exists source_category;

CREATE TYPE workflow_stage AS ENUM ('prescreen', 'submission', 'presentation', 'interview', 'placement');
CREATE TYPE rate AS ENUM ('perhour', 'perannum');
CREATE TYPE source_category AS ENUM ('source', 'activity', 'leads');

CREATE TABLE users_group (
	id SERIAL,
	PRIMARY KEY (id)
);

CREATE TABLE team (
	id SERIAL,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(160),
	email VARCHAR(256) NOT NULL,
	users_group_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT users_group_id_fk
		FOREIGN KEY (users_group_id)
		REFERENCES users_group (id)
);

ALTER SEQUENCE team_id_seq RESTART WITH 1000;

CREATE INDEX team_users_group_id_idx ON team (users_group_id);

CREATE TABLE users (
	id SERIAL,
	firstname VARCHAR(256) NOT NULL,
	lastname VARCHAR(256) NOT NULL,
	username VARCHAR(128) NOT NULL,
	password VARCHAR(128) NOT NULL,
	email VARCHAR(256) NOT NULL,
	enabled BOOLEAN DEFAULT TRUE NOT NULL,
	team_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT team_id_fk
		FOREIGN KEY (team_id)
		REFERENCES team (id),	
	CONSTRAINT uni_name_index
		UNIQUE (username),
	CONSTRAINT enabled_check
		CHECK (enabled IN ( 'Y', 'N' ))
);

ALTER SEQUENCE users_id_seq RESTART WITH 1000;

CREATE INDEX users_username_idx ON users (username);
CREATE INDEX users_team_id_idx ON users (team_id);

CREATE TABLE client (
	id SERIAL,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(160),
	team_id INTEGER NOT NULL,
	is_default BOOLEAN NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT team_id_fk
		FOREIGN KEY (team_id)
		REFERENCES team (id)
);

ALTER SEQUENCE client_id_seq RESTART WITH 1000;
CREATE UNIQUE INDEX client_team_id_is_default_uni ON client (team_id)
WHERE is_default = TRUE;

CREATE INDEX client_team_id_idx ON client (team_id);

CREATE TABLE end_client (
	id SERIAL,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(160),
	client_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT client_id_fk
		FOREIGN KEY (client_id)
		REFERENCES client (id)
);

ALTER SEQUENCE end_client_id_seq RESTART WITH 1000;
CREATE INDEX end_client_client_id_idx ON end_client (client_id);

CREATE TABLE role (
	id SERIAL,
	name varchar(15) NOT NULL,
	description VARCHAR(160),
	PRIMARY KEY (id),
	CONSTRAINT role_name_unq
        UNIQUE (name)
);

CREATE INDEX role_name_idx ON role (name);

CREATE TABLE users_role (
	id SERIAL,
	users_id INTEGER NOT NULL,
	role_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT users_role_users_id_role_id_unq
        UNIQUE (users_id,role_id),
    CONSTRAINT users_id_fk
		FOREIGN KEY (users_id)
		REFERENCES users(id),
	CONSTRAINT role_id_fk
		FOREIGN KEY (role_id)
		REFERENCES role(id)
);

CREATE INDEX users_role_users_id_idx ON users_role (users_id);
CREATE INDEX users_role_role_id_idx ON users_role (role_id);

CREATE TABLE permission (
	id SERIAL,
	name varchar(80) NOT NULL,
	description VARCHAR(160),
	PRIMARY KEY (id),
	CONSTRAINT permission_name_unq
        UNIQUE (name)
);

CREATE INDEX permission_name_idx ON permission (name);

CREATE TABLE role_permission (
	id SERIAL,
	role_id INTEGER NOT NULL,
	permission_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT role_permission_role_id_permission_id_unq
        UNIQUE (role_id,permission_id),
    CONSTRAINT role_id_fk
		FOREIGN KEY (role_id)
		REFERENCES role(id),
	CONSTRAINT permission_id_fk
		FOREIGN KEY (permission_id)
		REFERENCES permission(id)
);

CREATE INDEX role_permission_role_id_idx ON role_permission (role_id);
CREATE INDEX role_permission_permission_id_idx ON role_permission (permission_id);

CREATE TABLE sources (
	id SERIAL,
	name VARCHAR(160) NOT NULL,
	category source_category DEFAULT 'source',
	description VARCHAR(160),
	index INTEGER NOT NULL,
	PRIMARY KEY (id)
);

ALTER SEQUENCE sources_id_seq RESTART WITH 1000;

------

CREATE TABLE shortlist (
	id SERIAL,
	name VARCHAR(32) NOT NULL,
	users_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT users_id_fk
		FOREIGN KEY (users_id)
		REFERENCES users(id),
	-- DAXTRA shortlist name is unique by name
	-- regardless of the user who created it.
	CONSTRAINT shortlist_name_unq
        UNIQUE (name)
);

CREATE INDEX shortlist_name_idx ON shortlist (name);
CREATE INDEX shortlist_users_id_fk_idx ON shortlist (users_id);

CREATE TABLE candidate (
	id SERIAL,
	candidate_id VARCHAR(15) NOT NULL,
	source VARCHAR(128),
	firstname VARCHAR(25) NOT NULL,
	lastname VARCHAR(25) NOT NULL,
	fullname VARCHAR(50) NOT NULL,
	create_date timestamp DEFAULT now() NOT NULL,
	update_date timestamp DEFAULT now() NOT NULL,
	address_city VARCHAR(35), -- possibly not null
	address_country VARCHAR(35), -- possibly not null
	address_latitude NUMERIC, -- possibly not null
	address_longitude NUMERIC, -- possibly not null
	contact_mobile VARCHAR(20), --field for mobile phone
	contact_phone VARCHAR(20), --field for telephone
	contact_email VARCHAR(128),
	employment_current_title VARCHAR(50), -- possibly not null
	employment_current_employer VARCHAR(50), -- possibly not null
	hrxml TEXT,
	PRIMARY KEY (id),
	CONSTRAINT candidate_id_unq
        UNIQUE (candidate_id)
);

CREATE INDEX candidate_id_idx ON candidate (candidate_id);

CREATE TABLE shortlist_candidate (
	id SERIAL,
	shortlist_id INTEGER NOT NULL,
	candidate_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT shortlist_id_fk
		FOREIGN KEY (shortlist_id)
		REFERENCES shortlist(id),
	CONSTRAINT candidate_id_fk
		FOREIGN KEY (candidate_id)
		REFERENCES candidate(id),
	CONSTRAINT shortlist_id_candidate_id_unq
        UNIQUE (shortlist_id,candidate_id)
);

CREATE INDEX shortlist_candidate_shorlist_id_fk_idx ON shortlist_candidate (shortlist_id);
CREATE INDEX shortlist_candidate_candidate_id_fk_idx ON shortlist_candidate (candidate_id);

CREATE TABLE job_order (
	id SERIAL,
	jo_number VARCHAR(20) NOT NULL,
	jo_date timestamp NOT NULL,
	date_created timestamp DEFAULT now() NOT NULL,
	title VARCHAR(50) NOT NULL,
	location VARCHAR(30),
	zip_code VARCHAR(30),
	division VARCHAR(30),
	start_date timestamp NOT NULL,
	end_date timestamp NOT NULL,
	bill_rate_start DOUBLE PRECISION NOT NULL,
	bill_rate_end DOUBLE PRECISION NOT NULL,
	bill_period_rate rate NOT NULL,
	pay_rate_start DOUBLE PRECISION NOT NULL,
	pay_rate_end DOUBLE PRECISION NOT NULL,
	pay_period_rate rate NOT NULL,
	openings VARCHAR(30),
	max_subs VARCHAR(30),
	status VARCHAR(30),
	mime_type VARCHAR(255),
	file_name VARCHAR(255),
	jo_file bytea,
	latest_stage workflow_stage, -- nullable during create.
	end_client_id INTEGER NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT end_client_id_fk
		FOREIGN KEY (end_client_id)
		REFERENCES end_client(id)
);

CREATE INDEX job_order_jo_number_title_idx ON job_order (jo_number, title);
CREATE INDEX job_order_title_idx ON job_order (title);

CREATE TABLE job_order_candidate (
	id SERIAL,
	job_order_id INTEGER NOT NULL,
	candidate_id INTEGER NOT NULL,
	users_id INTEGER NOT NULL, -- owner of job order candidate
	last_workflow workflow_stage,
	import_date DATE NOT NULL DEFAULT NOW(), 
	pay_rate_start DOUBLE PRECISION NOT NULL,
	pay_rate_end DOUBLE PRECISION NOT NULL,
	pay_period_rate rate NOT NULL,
	event_remarks VARCHAR (250),
	PRIMARY KEY (id),
	CONSTRAINT job_order_id_fk
		FOREIGN KEY (job_order_id)
		REFERENCES job_order(id),
	CONSTRAINT candidate_id_fk
		FOREIGN KEY (candidate_id)
		REFERENCES candidate(id),
	CONSTRAINT users_id_fk
		FOREIGN KEY (users_id)
		REFERENCES users(id),
	CONSTRAINT job_order_id_candidate_id_users_id_unq
        UNIQUE (job_order_id,candidate_id,users_id)
);

CREATE INDEX job_order_candidate_job_order_id_idx ON job_order_candidate (job_order_id);
CREATE INDEX job_order_candidate_candidate_id_idx ON job_order_candidate (candidate_id);
CREATE INDEX job_order_candidate_users_id_idx ON job_order_candidate (users_id);

CREATE TABLE user_job_order (
	id SERIAL,
	job_order_id INTEGER NOT NULL,
	users_id INTEGER NOT NULL,
	primary_user BOOLEAN DEFAULT TRUE NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT job_order_id_fk
		FOREIGN KEY (job_order_id)
		REFERENCES job_order(id),
	CONSTRAINT users_id_fk
		FOREIGN KEY (users_id)
		REFERENCES users(id),
	CONSTRAINT users_id_job_order_id_unq
        UNIQUE (users_id,job_order_id)
);

CREATE UNIQUE INDEX one_primary_owner_per_job_order_unq ON user_job_order (job_order_id) WHERE primary_user;
CREATE INDEX user_job_order_job_order_id_idx ON user_job_order (job_order_id);
CREATE INDEX user_job_order_users_id_idx ON user_job_order (users_id);

CREATE TABLE prescreen_event (
	id SERIAL,
	sys_datetime TIMESTAMPTZ DEFAULT now() NOT NULL,
	txn_date DATE NOT NULL,
	job_order_candidate_id INTEGER NOT NULL,
	remarks VARCHAR(256),
	PRIMARY KEY (id),
	CONSTRAINT job_order_candidate_id_fk
		FOREIGN KEY (job_order_candidate_id)
		REFERENCES job_order_candidate(id),
	CONSTRAINT prescreen_event_job_order_candidate_id_unq
    	UNIQUE (job_order_candidate_id)
);

CREATE INDEX prescreen_event_job_order_candidate_id_fk_idx ON prescreen_event (job_order_candidate_id);

CREATE TABLE submission_event (
	id SERIAL,
	sys_datetime TIMESTAMPTZ DEFAULT now() NOT NULL,
	txn_date DATE NOT NULL,
	job_order_candidate_id INTEGER NOT NULL,
	remarks VARCHAR(256),
	PRIMARY KEY (id),
	CONSTRAINT job_order_candidate_id_fk
		FOREIGN KEY (job_order_candidate_id)
		REFERENCES job_order_candidate(id),
	CONSTRAINT submission_event_job_order_candidate_id_unq
    	UNIQUE (job_order_candidate_id)
);

CREATE INDEX submission_event_job_order_candidate_id_fk_idx ON submission_event (job_order_candidate_id);

CREATE TABLE client_submission_event (
	id SERIAL,
	sys_datetime TIMESTAMPTZ DEFAULT now() NOT NULL,
	txn_date DATE NOT NULL,
	job_order_candidate_id INTEGER NOT NULL,
	remarks VARCHAR(256),
	PRIMARY KEY (id),
	CONSTRAINT job_order_candidate_id_fk
		FOREIGN KEY (job_order_candidate_id)
		REFERENCES job_order_candidate(id),
	CONSTRAINT client_submission_event_job_order_candidate_id_unq
    	UNIQUE (job_order_candidate_id)
);

CREATE INDEX client_submission_event_job_order_candidate_id_fk_idx ON client_submission_event (job_order_candidate_id);

CREATE TABLE interview_event (
	id SERIAL,
	sys_datetime TIMESTAMPTZ DEFAULT now() NOT NULL,
	txn_date DATE NOT NULL,
	job_order_candidate_id INTEGER NOT NULL,
	remarks VARCHAR(256),
	PRIMARY KEY (id),
	CONSTRAINT job_order_candidate_id_fk
		FOREIGN KEY (job_order_candidate_id)
		REFERENCES job_order_candidate(id),
	CONSTRAINT interview_event_job_order_candidate_id_unq
    	UNIQUE (job_order_candidate_id)
);

CREATE INDEX interview_event_job_order_candidate_id_fk_idx ON interview_event (job_order_candidate_id);

CREATE TABLE placement_event (
	id SERIAL,
	sys_datetime TIMESTAMPTZ DEFAULT now() NOT NULL,
	txn_date DATE NOT NULL,
	job_order_candidate_id INTEGER NOT NULL,
	remarks VARCHAR(256),
	PRIMARY KEY (id),
	CONSTRAINT job_order_candidate_id_fk
		FOREIGN KEY (job_order_candidate_id)
		REFERENCES job_order_candidate(id),
	CONSTRAINT placement_event_job_order_candidate_id_unq
    	UNIQUE (job_order_candidate_id)
);

CREATE INDEX placement_event_job_order_candidate_id_fk_idx ON placement_event (job_order_candidate_id);

-- Eod Report --

CREATE TABLE eod_report (
	id SERIAL,
	users_id INTEGER NOT NULL,
	report_date DATE DEFAULT now() NOT NULL,
	create_date TIMESTAMP DEFAULT now() NOT NULL,
	update_date TIMESTAMP DEFAULT now() NOT NULL,
	comment TEXT,
	report_html TEXT,
	PRIMARY KEY (id),
	CONSTRAINT users_id_fk
		FOREIGN KEY (users_id)
		REFERENCES users(id),
	CONSTRAINT users_id_report_date_unq
		UNIQUE (users_id,report_date)
);

CREATE INDEX eod_report_users_id_fk_idx ON eod_report (users_id);
ALTER SEQUENCE eod_report_id_seq RESTART WITH 1000;

CREATE TABLE eod_report_source (
	id SERIAL,
	eod_report_id INTEGER NOT NULL,
	sources_id INTEGER NOT NULL,
	source_views INTEGER,
	source_leads INTEGER,
	leads INTEGER,
	activity INTEGER,
	PRIMARY KEY (id),
	CONSTRAINT eod_report_id_fk
		FOREIGN KEY (eod_report_id)
		REFERENCES eod_report(id),
	CONSTRAINT sources_id_fk
		FOREIGN KEY (sources_id)
		REFERENCES sources(id)
);

CREATE INDEX eod_report_source_eod_report_id_fk_idx ON eod_report_source (eod_report_id);
CREATE INDEX eod_report_source_sources_id_fk_idx ON eod_report_source (sources_id);
ALTER SEQUENCE eod_report_source_id_seq RESTART WITH 1000;