INSERT INTO permission (id, name, description) VALUES (24, 'app.workflow.edit', 'Workflow Edit Permission');
INSERT INTO permission (id, name, description) VALUES (25, 'menu.workflow.edit', 'Workflow Menu Edit Permission');
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 24);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 25);