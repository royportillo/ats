ALTER TABLE prescreen_event ADD COLUMN remarks VARCHAR (256);
ALTER TABLE submission_event ADD COLUMN remarks VARCHAR (256);
ALTER TABLE client_submission_event ADD COLUMN remarks VARCHAR (256);
ALTER TABLE interview_event ADD COLUMN remarks VARCHAR (256);
ALTER TABLE placement_event ADD COLUMN remarks VARCHAR (256);