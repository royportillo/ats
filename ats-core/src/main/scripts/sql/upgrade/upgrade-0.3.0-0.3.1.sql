INSERT INTO team (id, name, users_group_id) VALUES (3, 'Jackie', 1);
INSERT INTO team (id, name, users_group_id) VALUES (4, 'VMS', 1);
INSERT INTO team (id, name, users_group_id) VALUES (5, 'Tom Lee', 1);

INSERT INTO client (id, name, team_id, is_default) VALUES (4, 'APR', 3, TRUE);
INSERT INTO client (id, name, team_id, is_default) VALUES (5, 'CoreTechs', 4, TRUE);
INSERT INTO client (id, name, team_id, is_default) VALUES (6, 'HEPCO', 5, TRUE);

INSERT INTO users (id, username, password, enabled, team_id) VALUES (10, 'aoperio','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 4);
INSERT INTO users (id, username, password, enabled, team_id) VALUES (11, 'imarquez','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 5);
INSERT INTO users (id, username, password, enabled, team_id) VALUES (12, 'jcalderon','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 3);

--CoreTechs
INSERT INTO end_client (id, name, client_id) VALUES (10, 'Sutter Health', 5);
INSERT INTO end_client (id, name, client_id) VALUES (11, 'SuperValu', 5);
INSERT INTO end_client (id, name, client_id) VALUES (12, 'Silicon Valley Bank', 5);
INSERT INTO end_client (id, name, client_id) VALUES (13, 'Intuitive Surgical Inc.', 5);
INSERT INTO end_client (id, name, client_id) VALUES (14, 'Caesars', 5);
INSERT INTO end_client (id, name, client_id) VALUES (15, 'Tesla', 5);
INSERT INTO end_client (id, name, client_id) VALUES (16, 'Bridgewater Associates', 5);

--HEPCO
INSERT INTO end_client (id, name, client_id) VALUES (17, 'New York City Transit - MTA', 6);
INSERT INTO end_client (id, name, client_id) VALUES (18, 'Northrop Grumman Corporation', 6);
INSERT INTO end_client (id, name, client_id) VALUES (19, 'PSE&G', 6);
INSERT INTO end_client (id, name, client_id) VALUES (20, 'Maser Consulting', 6);
INSERT INTO end_client (id, name, client_id) VALUES (21, 'C6I Services', 6);
INSERT INTO end_client (id, name, client_id) VALUES (22, 'Jacobs Technology', 6);
INSERT INTO end_client (id, name, client_id) VALUES (23, 'BAE Systems', 6);

INSERT INTO end_client (id, name, client_id) VALUES (24, 'Ametek', 4);
INSERT INTO end_client (id, name, client_id) VALUES (25, 'Eaton', 4);
INSERT INTO end_client (id, name, client_id) VALUES (26, 'AQMD', 4);

INSERT INTO users_role (users_id, role_id) VALUES (10, 2);
INSERT INTO users_role (users_id, role_id) VALUES (11, 2);
INSERT INTO users_role (users_id, role_id) VALUES (12, 2);