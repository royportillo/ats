ALTER TABLE users ADD COLUMN email VARCHAR(256) NOT NULL DEFAULT '';
ALTER TABLE users ALTER COLUMN email SET NOT NULL;

ALTER TABLE team ADD COLUMN email VARCHAR(256) NOT NULL DEFAULT '';
ALTER TABLE team ALTER COLUMN email SET NOT NULL;

CREATE TYPE source_category AS ENUM ('source', 'activity', 'leads');
ALTER TABLE sources ADD COLUMN category source_category DEFAULT 'source';