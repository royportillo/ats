ALTER TABLE job_order_candidate ADD COLUMN import_date DATE;
UPDATE job_order_candidate SET import_date = '2021-01-01';
ALTER TABLE job_order_candidate ALTER COLUMN import_date SET NOT NULL;
ALTER TABLE job_order_candidate ALTER COLUMN import_date SET DEFAULT NOW();