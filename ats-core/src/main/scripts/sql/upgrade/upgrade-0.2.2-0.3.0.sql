ALTER TABLE job_order DROP CONSTRAINT IF EXISTS job_order_number_unq;
ALTER TABLE job_order_candidate DROP CONSTRAINT IF EXISTS job_order_id_candidate_id_unq;
ALTER TABLE job_order_candidate ADD CONSTRAINT job_order_id_candidate_id_users_id_unq UNIQUE (job_order_id,candidate_id,users_id);