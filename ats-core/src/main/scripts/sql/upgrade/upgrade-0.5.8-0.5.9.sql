ALTER TABLE eod_report DROP CONSTRAINT IF EXISTS report_date_unq;

ALTER TABLE eod_report ADD CONSTRAINT users_id_report_date_unq UNIQUE (users_id,report_date);