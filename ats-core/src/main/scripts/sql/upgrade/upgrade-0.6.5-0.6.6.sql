INSERT INTO permission (id, name, description) VALUES (26, 'app.reports-generation.view', 'Reports Generation View Permission');
INSERT INTO permission (id, name, description) VALUES (27, 'menu.reports-generation', 'Reports Generation Menu Permission');

INSERT INTO role_permission (role_id, permission_id) VALUES (1, 26);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 27);