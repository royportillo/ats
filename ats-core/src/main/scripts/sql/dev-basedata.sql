INSERT INTO users_group (id) VALUES (1);

INSERT INTO team (id, name, email, description, users_group_id) VALUES (1, 'Team A', 'team_a@24hrpo.com', 'Team A Description', 1);
INSERT INTO team (id, name, email, description, users_group_id) VALUES (2, 'Team B', 'team_b@24hrpo.com', 'Team B Description', 1);

INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (1, 'Recsys', 'Admin', 'recsys@24hrpo.com', 'admin','{bcrypt}$2a$10$GckCmrpY03RilGkPOQ2AS.NGeOmFChG8W8vHitdkkCV7evMe8ZwdS', true, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (2, 'Dann', 'Hilario', 'dhilario@24hrpo.com', 'dhilario','{bcrypt}$2a$10$ANpH5jaJE8Bs5EVuuM6Km.ZxN7p89eodAUquU/FjfJDYzsjhtU/MK', true, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (3, 'Guest', 'User 0', 'guest0@24hrpo.com', 'guest','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 2);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (4, 'Disabled', 'User', 'disabled_user@24hrpo.com', 'guest0','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', false, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (5, 'Guest', 'User 1', 'guest1@24hrpo.com', 'guest1','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (6, 'Guest', 'User 2', 'guest2@24hrpo.com', 'guest2','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (7, 'Guest', 'User 3', 'guest3@24hrpo.com', 'guest3','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (8, 'Guest', 'User 4', 'guest4@24hrpo.com', 'guest4','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (9, 'Guest', 'User 5', 'guest5@24hrpo.com', 'guest5','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 1);
INSERT INTO users (id, firstname, lastname, email, username, password, enabled, team_id) VALUES (10, 'Guest', 'User 6', 'guest6@24hrpo.com', 'guest6','{bcrypt}$2a$10$zhhnMs3vFAdhbzCrsRKcCOgCULHN4GGolwvrkJQLNXEy1r6xahbu.', true, 1);


INSERT INTO client (id, name, description, team_id, is_default) VALUES (1, 'Client A', 'Client A Description', 1, FALSE);
INSERT INTO client (id, name, description, team_id, is_default) VALUES (2, 'Client B', 'Client B Description', 1, TRUE);
INSERT INTO client (id, name, description, team_id, is_default) VALUES (3, 'Client C', 'Client C Description', 2, TRUE);
INSERT INTO end_client (id, name, description, client_id) VALUES (1, 'End Client A', 'End Client A Description', 1);
INSERT INTO end_client (id, name, description, client_id) VALUES (2, 'End Client B', 'End Client B Description', 1);
INSERT INTO end_client (id, name, description, client_id) VALUES (3, 'End Client C', 'End Client C Description', 1);
INSERT INTO end_client (id, name, description, client_id) VALUES (4, 'End Client D', 'End Client D Description', 2);
INSERT INTO end_client (id, name, description, client_id) VALUES (5, 'End Client E', 'End Client E Description', 2);
INSERT INTO end_client (id, name, description, client_id) VALUES (6, 'End Client F', 'End Client F Description', 2);
INSERT INTO end_client (id, name, description, client_id) VALUES (7, 'End Client G', 'End Client G Description', 3);
INSERT INTO end_client (id, name, description, client_id) VALUES (8, 'End Client H', 'End Client H Description', 3);
INSERT INTO end_client (id, name, description, client_id) VALUES (9, 'End Client I', 'End Client I Description', 3);

INSERT INTO role (id, name, description) VALUES (1, 'ADMIN', 'Admin Role');
INSERT INTO role (id, name, description) VALUES (2, 'USER', 'User Role');

-- add admin and user role to admin user to test multiple roles in one user.
INSERT INTO users_role (users_id, role_id) VALUES (1, 1);
INSERT INTO users_role (users_id, role_id) VALUES (2, 1);
INSERT INTO users_role (users_id, role_id) VALUES (2, 2);
INSERT INTO users_role (users_id, role_id) VALUES (3, 2);
INSERT INTO users_role (users_id, role_id) VALUES (4, 2);
INSERT INTO users_role (users_id, role_id) VALUES (5, 2);
INSERT INTO users_role (users_id, role_id) VALUES (6, 2);
INSERT INTO users_role (users_id, role_id) VALUES (7, 2);
INSERT INTO users_role (users_id, role_id) VALUES (8, 2);
INSERT INTO users_role (users_id, role_id) VALUES (9, 2);

INSERT INTO permission (id, name, description) VALUES (1, 'menu.dashboard', 'Dashboard Permission');
INSERT INTO permission (id, name, description) VALUES (2, 'menu.joborder', 'JO Permission');
INSERT INTO permission (id, name, description) VALUES (3, 'menu.prescreen', 'Prescreen Permission');
INSERT INTO permission (id, name, description) VALUES (4, 'menu.submission', ' Submission Permission');
INSERT INTO permission (id, name, description) VALUES (5, 'menu.presentation', 'Presentation Permission');
INSERT INTO permission (id, name, description) VALUES (6, 'menu.interview', 'Interview Permission');
INSERT INTO permission (id, name, description) VALUES (7, 'menu.placement', 'Placement Permission');
INSERT INTO permission (id, name, description) VALUES (8, 'app.dashboard.view', 'Dashboard Permission');
INSERT INTO permission (id, name, description) VALUES (9, 'app.shortlist.find', 'Shortlist Permission');
INSERT INTO permission (id, name, description) VALUES (10, 'app.candidate.view', 'Candidate View Permission');
INSERT INTO permission (id, name, description) VALUES (11, 'app.joborder.edit', 'JO Edit Permission');
INSERT INTO permission (id, name, description) VALUES (12, 'app.workflow.view', 'Workflow View Permission');
INSERT INTO permission (id, name, description) VALUES (13, 'menu.eodreport', 'EOD Report Permission');
INSERT INTO permission (id, name, description) VALUES (14, 'app.users.edit', 'Users Edit Permission');
INSERT INTO permission (id, name, description) VALUES (15, 'menu.users', 'Users Permission');
INSERT INTO permission (id, name, description) VALUES (16, 'app.teams.edit', 'Teams Edit Permission');
INSERT INTO permission (id, name, description) VALUES (17, 'menu.teams', 'Teams Permission');
INSERT INTO permission (id, name, description) VALUES (18, 'app.clients.edit', 'Clients Edit Permission');
INSERT INTO permission (id, name, description) VALUES (19, 'menu.clients', 'Clients Permission');
INSERT INTO permission (id, name, description) VALUES (20, 'app.endclients.edit', 'End Clients Edit Permission');
INSERT INTO permission (id, name, description) VALUES (21, 'menu.endclients', 'End Clients Permission');
INSERT INTO permission (id, name, description) VALUES (22, 'app.sources.edit', 'Sources Edit Permission');
INSERT INTO permission (id, name, description) VALUES (23, 'menu.sources', 'Sources Permission');
INSERT INTO permission (id, name, description) VALUES (24, 'app.workflow.edit', 'Workflow Edit Permission');
INSERT INTO permission (id, name, description) VALUES (25, 'menu.workflow.edit', 'Workflow Menu Edit Permission');
INSERT INTO permission (id, name, description) VALUES (26, 'app.reports-generation.view', 'Reports Generation View Permission');
INSERT INTO permission (id, name, description) VALUES (27, 'menu.reports-generation', 'Reports Generation Menu Permission');


-- admin only permission for now
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 14);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 15);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 16);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 17);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 18);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 19);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 20);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 21);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 22);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 23);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 24);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 25);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 26);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 27);

-- user permissions
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 1);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 2);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 3);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 4);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 5);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 6);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 7);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 8);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 9);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 10);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 11);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 12);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 13);

-- sources
INSERT INTO sources (id, name, category, description, index) VALUES (1, 'Daxtra - CB', 'source', 'Daxtra - Career Builder', 0);
INSERT INTO sources (id, name, category, description, index) VALUES (2, 'Daxtra - M', 'source', 'Daxtra - Monster', 1);
INSERT INTO sources (id, name, category, description, index) VALUES (3, 'Daxtra - TP', 'source', 'Daxtra - Talent Pool', 2);
INSERT INTO sources (id, name, category, description, index) VALUES (4, 'CB', 'source', 'Career Builder', 3);
INSERT INTO sources (id, name, category, description, index) VALUES (5, 'Monster', 'source', 'Monster', 4);
INSERT INTO sources (id, name, category, description, index) VALUES (6, 'Dice', 'source', 'Dice', 5);
INSERT INTO sources (id, name, category, description, index) VALUES (7, 'LinkedIn', 'source', 'LinkedIn', 6);
INSERT INTO sources (id, name, category, description, index) VALUES (8, 'Zip JP', 'source', 'Zip JP', 7);
INSERT INTO sources (id, name, category, description, index) VALUES (9, 'Zip RES', 'source', 'Zip Res', 8);
INSERT INTO sources (id, name, category, description, index) VALUES (10, 'Indeed JP', 'source', 'Indeed JP', 9);
INSERT INTO sources (id, name, category, description, index) VALUES (11, 'Indeed RES', 'source', 'Indeed RES', 10);
INSERT INTO sources (id, name, category, description, index) VALUES (12, 'Others', 'source', 'Others', 11);
INSERT INTO sources (id, name, category, description, index) VALUES (13, 'Pipeline Leads', 'leads', 'Activity', 12);
INSERT INTO sources (id, name, category, description, index) VALUES (14, 'Dials', 'activity', 'Activity', 13);
INSERT INTO sources (id, name, category, description, index) VALUES (15, 'Emails', 'activity', 'Activity', 14);
INSERT INTO sources (id, name, category, description, index) VALUES (16,'SMS', 'activity', 'Activity', 15);
INSERT INTO sources (id, name, category, description, index) VALUES (17, 'Connects', 'activity', 'Activity', 16);


