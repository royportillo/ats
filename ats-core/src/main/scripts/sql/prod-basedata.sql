-- TODO: production base data for organization e.g. group, team, client, end_client.

INSERT INTO users_group (id) VALUES (1);

INSERT INTO team (id, name, description, users_group_id) VALUES (1, 'Team A', 'Team A Description', 1);

INSERT INTO users (id, firstname, lastname, username, password, enabled, team_id) VALUES (1, 'Recsys', 'Admin', 'admin','{bcrypt}$2a$10$GckCmrpY03RilGkPOQ2AS.NGeOmFChG8W8vHitdkkCV7evMe8ZwdS', true, 1);

INSERT INTO role (id, name, description) VALUES (1, 'ADMIN', 'Admin Role');
INSERT INTO role (id, name, description) VALUES (2, 'RECRUITER', 'Recruiter Role');

INSERT INTO users_role (users_id, role_id) VALUES (1, 1);

INSERT INTO permission (id, name, description) VALUES (1, 'menu.dashboard', 'Dashboard Permission');
INSERT INTO permission (id, name, description) VALUES (2, 'menu.joborder', 'JO Permission');
INSERT INTO permission (id, name, description) VALUES (3, 'menu.prescreen', 'Prescreen Permission');
INSERT INTO permission (id, name, description) VALUES (4, 'menu.submission', ' Submission Permission');
INSERT INTO permission (id, name, description) VALUES (5, 'menu.presentation', 'Presentation Permission');
INSERT INTO permission (id, name, description) VALUES (6, 'menu.interview', 'Interview Permission');
INSERT INTO permission (id, name, description) VALUES (7, 'menu.placement', 'Placement Permission');
INSERT INTO permission (id, name, description) VALUES (8, 'app.dashboard.view', 'Dashboard Permission');
INSERT INTO permission (id, name, description) VALUES (9, 'app.shortlist.find', 'Shortlist Permission');
INSERT INTO permission (id, name, description) VALUES (10, 'app.candidate.view', 'Candidate View Permission');
INSERT INTO permission (id, name, description) VALUES (11, 'app.joborder.edit', 'JO Edit Permission');
INSERT INTO permission (id, name, description) VALUES (12, 'app.workflow.view', 'Workflow View Permission');
INSERT INTO permission (id, name, description) VALUES (13, 'menu.eodreport', 'EOD Report Permission');
INSERT INTO permission (id, name, description) VALUES (14, 'app.users.edit', 'Users Edit Permission');
INSERT INTO permission (id, name, description) VALUES (15, 'menu.users', 'Users Permission');
INSERT INTO permission (id, name, description) VALUES (16, 'app.teams.edit', 'Teams Edit Permission');
INSERT INTO permission (id, name, description) VALUES (17, 'menu.teams', 'Teams Permission');
INSERT INTO permission (id, name, description) VALUES (18, 'app.clients.edit', 'Clients Edit Permission');
INSERT INTO permission (id, name, description) VALUES (19, 'menu.clients', 'Clients Permission');
INSERT INTO permission (id, name, description) VALUES (20, 'app.endclients.edit', 'End Clients Edit Permission');
INSERT INTO permission (id, name, description) VALUES (21, 'menu.endclients', 'End Clients Permission');

-- admin only permission for now
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 14);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 15);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 16);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 17);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 18);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 19);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 20);
INSERT INTO role_permission (role_id, permission_id) VALUES (1, 21);

-- recruiter permissions
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 1);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 2);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 3);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 4);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 5);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 6);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 7);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 8);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 9);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 10);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 11);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 12);
INSERT INTO role_permission (role_id, permission_id) VALUES (2, 13);