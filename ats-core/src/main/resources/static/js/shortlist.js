$(document).ready(function() {
    var table = $('#shortlist-candidate-table').DataTable({
        "dom" : '<"top">rt<"bottom"flp><"clear">',
        "searching" : false,
        "info" : false,
        //Define a default sorting behavior ("order" : []) to allow disable sorting on first column (the checkbox)
        "order" : [],
        "columnDefs" : [ 
            { "targets" : 0, "orderable" : false },
            { "targets" : "_all", "orderable" : true}
        ]
    });

    $("#searchForm").on("submit", function() {
        var search = $("#search").prop("disabled", true);
        search.html("Searching...");
    });

    //Get rows available to the table
    var rows = table.rows({"search" : "applied"}).nodes();
    //Set initial selected rows text
    $("#selected-rows").text($(".check-row:checked", rows).length);

    $("#check-all").change(function() {
        $(".check-row", rows).prop("checked", this.checked).change();
    });
    
    $(".check-row", rows).change(function() {
        var isCheckedAll = $(".check-row", rows).not(":checked").length === 0;
        var isCheckedNone = $(".check-row:checked", rows).length === 0;
        
        $("#check-all").prop("checked", isCheckedAll);
        $("#import-selected").prop("disabled", isCheckedNone);
        $("#selected-rows").text($(".check-row:checked", rows).length);
    });

    $("#import-selected").click(function(event) {
        $(".check-row:checked", rows).each(function() {
            $("<input />").attr({
                "type" : "hidden",
                "name" : "candidateid",
                "value" : $(this).val()
            }).appendTo($("#import"));
        });
    });
});