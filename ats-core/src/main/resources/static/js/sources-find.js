$(document).ready(function(){
    var sourcesTable = $('#find-source').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "columnDefs" : [
            {"width" : "10%", "targets" : 3}
        ],
        "searching" : true
    });

    $("#source-search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#sourcename").val();
        var value2 = $("#sourceDescription").val();
        sourcesTable.column(1).search(value1, true, false).column(2).search(value2, true, false).draw();
    });

    $("#source-reset").on("click", function(event) {
        event.preventDefault();
        sourcesTable.column(1).search('', true, false).column(2).search('', true, false).draw();
    });
});