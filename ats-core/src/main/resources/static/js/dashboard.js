$(document).ready(function() {

	var columns = [
		{"data" : "transactionDate"}, //change to workflow transaction date
		{"data" : "firstName"},
		{"data" : "lastName"},
		{"data" : "endClientName"},
		{"data" : "jobOrderTitle"}
	];

	var columnsJo = [
		{"data" : "joDate"},
		{"data" : "number"},
		{"data" : "title"},
		{"data" : "location"},
	];

	var tableDom = '<"top">rt<"bottom"flp><"clear">';

	var selectedPeriod = $("input[name=periodOptions]:checked", "#candidateDetails").val();

	var selectedPeriodJo = $("input[name=periodOptionsJo]:checked", "#jobOrderDetails").val();

	var prescreenTable = $("#prescreen-candidate-view").DataTable({
		"dom" : tableDom,
        "searching" : false,
        "info" : false,
		"ajax" : {
			"url" : "dashboard/candidates?workflow=prescreen&period=" + selectedPeriod,
			"dataSrc" : ""
		},
		"columns" : columns
	});

	var submissionTable = $("#submit-candidate-view").DataTable({
		"dom" : tableDom,
        "searching" : false,
        "info" : false,
		"ajax" : {
			"url" : "dashboard/candidates?workflow=submission&period=" + selectedPeriod,
			"dataSrc" : ""
		},
		"columns" : columns
	});

	var presentationTable = $("#present-candidate-view").DataTable({
		"dom" : tableDom,
        "searching" : false,
        "info" : false,
		"ajax" : {
			"url" : "dashboard/candidates?workflow=presentation&period=" + selectedPeriod,
			"dataSrc" : ""
		},
		"columns" : columns
	});

	var interviewTable = $("#interview-candidate-view").DataTable({
		"dom" : tableDom,
        "searching" : false,
        "info" : false,
		"ajax" : {
			"url" : "dashboard/candidates?workflow=interview&period=" + selectedPeriod,
			"dataSrc" : ""
		},
		"columns" : columns
	});

	var placementTable = $("#placement-candidate-view").DataTable({
		"dom" : tableDom,
        "searching" : false,
        "info" : false,
		"ajax" : {
			"url" : "dashboard/candidates?workflow=placement&period=" + selectedPeriod,
			"dataSrc" : ""
		},
		"columns" : columns
	});

	var jobOrderTable = $("#joborder-view").DataTable({
		"dom" : tableDom,
        "searching" : false,
        "info" : false,
		"ajax" : {
			"url" : "dashboard/joborders?period=" + selectedPeriodJo,
			"dataSrc" : ""
		},
		"columns" : columnsJo
	});

	$("input[name=periodOptions]").on("change", function(e) {
		selectedPeriod = $("input[name=periodOptions]:checked", "#candidateDetails").val();

		prescreenTable.ajax.url("dashboard/candidates?workflow=prescreen&period=" + selectedPeriod);
		submissionTable.ajax.url("dashboard/candidates?workflow=submission&period=" + selectedPeriod);
		presentationTable.ajax.url("dashboard/candidates?workflow=presentation&period=" + selectedPeriod);
		interviewTable.ajax.url("dashboard/candidates?workflow=interview&period=" + selectedPeriod);
		placementTable.ajax.url("dashboard/candidates?workflow=placement&period=" + selectedPeriod);

		prescreenTable.ajax.reload();
		submissionTable.ajax.reload();
		presentationTable.ajax.reload();
		interviewTable.ajax.reload();
		placementTable.ajax.reload();
	});

	$("input[name=periodOptionsJo]").on("change", function(e) {
		selectedPeriodJo = $("input[name=periodOptionsJo]:checked", "#jobOrderDetails").val();

		jobOrderTable.ajax.url("dashboard/joborders?period=" + selectedPeriodJo);

		jobOrderTable.ajax.reload();
	});

});