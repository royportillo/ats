$(document).ready(function() {
    
    $("#clientId").on("change", function() {
        $.ajax({
            type: "GET",
            url: "/joborder/endclients",
            success: function(data) {
                console.log(data);
                var endClientSelect = $("#endClient\\.id").empty();
                
                data.forEach(function(endClient) {
                    $("<option>").text(endClient.name).attr({
                        "value" : endClient.id
                    })
                    .appendTo(endClientSelect);
                });

            }
        });
    });

    if (!$("#jo-date").data("has-candidates")) {
        joDate = $("#jo-date").datepicker();
    }

    dateFrom = $("#start-date").datepicker().on("change", function() {
        var dateSelected = new Date(Date.parse($(this).val()));
        var futureDate = new Date(dateSelected);
        futureDate.setMonth(dateSelected.getMonth() + 3);

        dateTo.datepicker("option", "minDate", dateSelected);
        dateTo.datepicker("setDate", futureDate);

        $(this).datepicker("option", "maxDate", futureDate);
    });

    dateTo = $("#end-date").datepicker().on("change", function() {
        dateFrom.datepicker("option", "maxDate", new Date(Date.parse($(this).val())));
    });
    
    $("#location").autocomplete({
        minLength : 2,
        source : function(request, response) {
            response($.map(states, function(obj, key) {
                
                var city = obj.city.toUpperCase();

                if (city.indexOf(request.term.toUpperCase()) != -1) {
                    return {
                        label : obj.city + ", " + obj.stateCode,
                        value : obj.city + ", " + obj.stateCode
                    }
                } else {
                    return null;
                }
            }));
        },
        focus : function(event, ui) {
            event.preventDefault();
        },
        select : function(event, ui) {
            event.preventDefault();
            $("#location").val(ui.item.label);
        }
    });

    
    //Bill rate and pay rate input behavior
    billFrom = $("#bill-rate-from").on("input", function(event) {
        var value = Number($(this).val());
        billTo.val(value);
    });

    billTo = $("#bill-rate-to")

    payFrom = $("#pay-rate-from").on("input", function(){
        var value = Number($(this).val());
        payTo.val(value);
    });

    payTo = $("#pay-rate-to");
    
});