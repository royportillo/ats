$(document).ready(function() {
    var clientsTable = $('#find-clients').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "columnDefs" : [
            {"width" : "10%", "targets" : 2},
            {"width" : "10%", "targets" : 3}
        ],
        "searching" : true
    });

    $("#client-search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#clientname").val();
        var value2 = $("#clientDescription").val();
        clientsTable.column(0).search(value1, true, false).column(1).search(value2, true, false).draw();
    });

    $("#client-reset").on("click", function(event) {
        event.preventDefault();
        clientsTable.column(0).search('', true, false).column(1).search('', true, false).draw();
    });
});