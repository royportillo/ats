$(document).ready(function(){
    var endClientsTable = $('#endclients-table').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "searching" : true
    });

    $("#endclients-search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#endclientname").val();
        var value2 = $("#endclientDescription").val();
        endClientsTable.column(0).search(value1, true, false).column(1).search(value2, true, false).draw();
    });

    $("#endclients-reset").on("click", function(event) {
        event.preventDefault();
        endClientsTable.column(0).search('', true, false).column(1).search('', true, false).draw();
    });
});