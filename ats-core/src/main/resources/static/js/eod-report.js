$(document).ready(function() {

    $.ajax({
        method: "POST",
        url: "/eod-report/preview",
        data: $("#activity-form").serialize(),
        success: function(data) {
            $("#preview").contents().find("html").html(data);
        },
        error: function(xhr, status, error) {
            var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
            + '\nError Message: "' + xhr.statusText + '"';

            alert(errorMessage);
        }
    });

    $("#generate-preview").on("click", function(e) {
        e.preventDefault();

        $.ajax({
            method: "POST",
            url: "/eod-report/preview",
            data: $("#activity-form").serialize(),
            success: function(data) {
                $("#preview").contents().find("html").html(data);
            },
            error: function(xhr, status, error) {
                var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
                + '\nError Message: "' + xhr.statusText + '"';
    
                alert(errorMessage);
            }
        });
    });

    $("#send-report").on("click", function(e) {
        e.preventDefault();

        var confirmSend = confirm("This will send the End of Day Report. Confirm send?");
        
        if(confirmSend === true) {
            $("#send-status").text("Sending...");
            $.ajax({
                method: "POST",
                url: "/eod-report/send",
                data: $("#activity-form").serialize(),
                success: function(data) {
                    alert("End of Day Report sent successfully.");
                    $("#send-status").html("");
                    location.reload();
                },
                error: function(xhr, status, error) {
                    var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
                    + '\nError Message: "' + xhr.statusText + '"';
                    $("#send-status").text("");
                    alert(errorMessage);
                }
            });
        }
    });

    $("#comment-box").on("input", function(e) {

        var charlength = $(this).val().length;
        $("#charlimit").text(5000 - charlength);
    });
});