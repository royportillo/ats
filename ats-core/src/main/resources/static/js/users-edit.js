$(document).ready(function() {

    $("#roles").val($("#roles-select option").map(function (){
        return this.value + ":" + this.text;
    }).get().join(","));

    $("#available-move-one").on("click", function(event) {
        event.preventDefault();

        $("#available-select option:selected").appendTo("#roles-select");
        $("#available-select option:selected").remove();
    });

    $("#available-move-all").on("click", function(event) {
        event.preventDefault();

        $("#available-select option").appendTo("#roles-select");
        $("#available-select option").remove();
    });

    $("#selected-move-one").on("click", function(event) {
        event.preventDefault();

        $("#roles-select option:selected").appendTo("#available-select");
        $("#roles-select option:selected").remove();
    });

    $("#selected-move-all").on("click", function(event) {
        event.preventDefault();

        $("#roles-select option").appendTo("#available-select");
        $("#roles-select option").remove();
    });

    $(".button-move").on("click", function(event) {
        $("#roles").val($("#roles-select option").map(function (){
            return this.value + ":" + this.text;
        }).get().join(","));
    });
});