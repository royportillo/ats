$(document).ready(function() {
    var table = $('#find-teams').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "searching" : true
    });

    $("#search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#name").val();
        var value2 = $("#description").val();
        table.column(0).search(value1, true, false).column(1).search(value2, true, false).draw();
    });

    $("#reset").on("click", function(event) {
        event.preventDefault();
        table.column(0).search('', true, false).column(1).search('', true, false).draw();
    });
});