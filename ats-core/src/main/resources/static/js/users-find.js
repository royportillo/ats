$(document).ready(function() {
    var table = $('#find-users').DataTable({
        "dom" : '<"top">rt<"bottom"lp><"clear">',
        "searching" : true
    });

    $("#search").on("click", function(event) {
        event.preventDefault();

        var value1 = $("#firstname").val();
        var value2 = $("#lastname").val();
        table.column(2).search(value1, true, false).column(3).search(value2, true, false).draw();
    });
});