$(document).ready(function() {

	$("#prescreen-modal-button").click(function() {
		var joCandidateId = $("#jocid-prescreen").val();
		var transactionDateVal = $("#prescreen-modal-datepicker").val();
		var payRateFromVal = $("#prescreen-modal-payrate-from").val();
		var payRateToVal = $("#prescreen-modal-payrate-to").val();
		var payPeriodRateVal = $("#prescreen-modal-pay-rate-period").val();
		var eventRemarksVal = $("#prescreen-modal-remarks").val();

		$.ajax({
			method: "POST",
			url: "/workflow/save/prescreen",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: transactionDateVal,
				payRateFrom: payRateFromVal,
				payRateTo: payRateToVal,
				payPeriodRate: payPeriodRateVal,
				eventRemarks: eventRemarksVal
			}),
			contentType: "application/json",
			success: function(data) {
				console.log("Returned: " + data);
				$("#prescreen-modal").modal('hide');
				
				if (payRateFromVal && payRateToVal) {
					$("#payrate-cell-" + joCandidateId).text(
						$("#prescreen-modal-payrate-from").val() + " - " + $("#prescreen-modal-payrate-to").val() + " " +
						($("#prescreen-modal-pay-rate-period").val() === "perhour" ? "$/hour" : "$/year") );
				}
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#submission-modal-button").click(function() {
		var joCandidateId = $("#jocid-submission").val();
		var transactionDateVal = $("#submission-modal-datepicker").val();
		var payRateFromVal = $("#submission-modal-payrate-from").val();
		var payRateToVal = $("#submission-modal-payrate-to").val();
		var payPeriodRateVal = $("#submission-modal-pay-rate-period").val();
		var eventRemarksVal = $("#submission-modal-remarks").val();

		$.ajax({
			method: "POST",
			url: "/workflow/save/submission",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: transactionDateVal,
				payRateFrom: payRateFromVal,
				payRateTo: payRateToVal,
				payPeriodRate: payPeriodRateVal,
				eventRemarks: eventRemarksVal
			}),
			contentType: "application/json",
			success: function(data) {
				console.log("Returned: " + data);

				$("#prescreen-button_" + joCandidateId).addClass("disabled");

				$("#submission-modal").modal('hide');

				if (payRateFromVal && payRateToVal) {
					$("#payrate-cell-" + joCandidateId).text(
						$("#submission-modal-payrate-from").val() + " - " + $("#submission-modal-payrate-to").val() + " " +
						($("#submission-modal-pay-rate-period").val() === "perhour" ? "$/hour" : "$/year") );
				}
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#client-submission-modal-button").click(function() {
		var joCandidateId = $("#jocid-client-submission").val();
		var transactionDateVal = $("#client-submission-modal-datepicker").val();
		var payRateFromVal = $("#client-submission-modal-payrate-from").val();
		var payRateToVal = $("#client-submission-modal-payrate-to").val();
		var payPeriodRateVal = $("#client-submission-modal-pay-rate-period").val();
		var eventRemarksVal = $("#client-submission-modal-remarks").val();

		$.ajax({
			method: "POST",
			url: "/workflow/save/client-submission",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: transactionDateVal,
				payRateFrom: payRateFromVal,
				payRateTo: payRateToVal,
				payPeriodRate: payPeriodRateVal,
				eventRemarks: eventRemarksVal
			}),
			contentType: "application/json",
			success: function(data) {
				console.log("Returned: " + data);

				$("#prescreen-button_" + joCandidateId).addClass("disabled");

				$("#submission-button_" + joCandidateId).addClass("disabled");

				$("#client-submission-modal").modal('hide');

				if (payRateFromVal && payRateToVal) {
					$("#payrate-cell-" + joCandidateId).text(
						$("#client-submission-modal-payrate-from").val() + " - " + $("#client-submission-modal-payrate-to").val() + " " +
						($("#client-submission-modal-pay-rate-period").val() === "perhour" ? "$/hour" : "$/year") );
				}
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#interview-modal-button").click(function() {
		var joCandidateId = $("#jocid-interview").val();
		var transactionDateVal = $("#interview-modal-datepicker").val();
		var payRateFromVal = $("#interview-modal-payrate-from").val();
		var payRateToVal = $("#interview-modal-payrate-to").val();
		var payPeriodRateVal = $("#interview-modal-pay-rate-period").val();
		var eventRemarksVal = $("#interview-modal-remarks").val();
		
		$.ajax({
			method: "POST",
			url: "/workflow/save/interview",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: transactionDateVal,
				payRateFrom: payRateFromVal,
				payRateTo: payRateToVal,
				payPeriodRate: payPeriodRateVal,
				eventRemarks: eventRemarksVal
			}),
			contentType: "application/json",
			success: function(data) {
				console.log("Returned: " + data);

				$("#prescreen-button_" + joCandidateId).addClass("disabled");

				$("#submission-button_" + joCandidateId).addClass("disabled");
				
				$("#client-submission-button_" + joCandidateId).addClass("disabled");

				$("#interview-modal").modal('hide');

				if (payRateFromVal && payRateToVal) {
					$("#payrate-cell-" + joCandidateId).text(
						$("#interview-modal-payrate-from").val() + " - " + $("#interview-modal-payrate-to").val() + " " +
						($("#interview-modal-pay-rate-period").val() === "perhour" ? "$/hour" : "$/year") );
				}
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#placement-modal-button").click(function() {
		var joCandidateId = $("#jocid-placement").val();
		var transactionDateVal = $("#placement-modal-datepicker").val();
		var payRateFromVal = $("#placement-modal-payrate-from").val();
		var payRateToVal = $("#placement-modal-payrate-to").val();
		var payPeriodRateVal = $("#placement-modal-pay-rate-period").val();
		var eventRemarksVal = $("#placement-modal-remarks").val();

		$.ajax({
			method: "POST",
			url: "/workflow/save/placement",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: transactionDateVal,
				payRateFrom: payRateFromVal,
				payRateTo: payRateToVal,
				payPeriodRate: payPeriodRateVal,
				eventRemarks: eventRemarksVal
			}),
			contentType: "application/json",
			success: function(data) {
				console.log("Returned: " + data);

				$("#prescreen-button_" + joCandidateId).addClass("disabled");

				$("#submission-button_" + joCandidateId).addClass("disabled");
				
				$("#client-submission-button_" + joCandidateId).addClass("disabled");
				
				$("#interview-button_" + joCandidateId).addClass("disabled");

				$("#placement-modal").modal('hide');

				if (payRateFromVal && payRateToVal) {
					$("#payrate-cell-" + joCandidateId).text(
						$("#placement-modal-payrate-from").val() + " - " + $("#placement-modal-payrate-to").val() + " " +
						($("#placement-modal-pay-rate-period").val() === "perhour" ? "$/hour" : "$/year") );
				}
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});

	$("#edit-modal-rollback").click(function() {
		var joCandidateId = $("#jocid-edit").val();

		if (confirm("Are you sure you want to rollback this candidate's workflow stages?") === false) {
			return;
		}

		$.ajax({
			method: "DELETE",
			url: "/workflow/delete",
			data: { jocid : joCandidateId },
			success: function(data) {
				console.log("Returned: " + data);

				$("#prescreen-button_" + joCandidateId).removeClass("disabled");

				$("#submission-button_" + joCandidateId).removeClass("disabled");
				
				$("#client-submission-button_" + joCandidateId).removeClass("disabled");
				
				$("#interview-button_" + joCandidateId).removeClass("disabled");

				$("#placement-button_" + joCandidateId).removeClass("disabled");
				
				alert("Successfully rolled back candidate.");
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});

	$(function() {
		$("#prescreen-modal-datepicker").datepicker();
		$("#submission-modal-datepicker").datepicker();
		$("#client-submission-modal-datepicker").datepicker();
		$("#interview-modal-datepicker").datepicker();
		$("#placement-modal-datepicker").datepicker();
	});

	var table = $("#candidate-view").DataTable({
        "dom" : '<"top">rt<"bottom"flp><"clear">',
        "searching" : false,
        "info" : false
	});

	var rows = table.rows({"search" : "applied"}).nodes();
	
	$("[data-toggle='tooltip']").tooltip();

	$(".prescreen-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-prescreen").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[0]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[0].transactionDate));
				}
				$("#prescreen-modal-datepicker").val(date);
				$("#prescreen-modal-payrate-from").val(data.payRateFrom);
				$("#prescreen-modal-payrate-to").val(data.payRateTo);
				$("#prescreen-modal-pay-rate-period").val(data.payPeriodRate);
				$("#prescreen-modal-remarks").val(data.eventRemarks);

				$("#prescreen-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#prescreen-modal-submission-remarks").text(data.events[1].remarks);
				$("#prescreen-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#prescreen-modal-interview-remarks").text(data.events[3].remarks);
				$("#prescreen-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".submission-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-submission").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[1]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[1].transactionDate));
				}
				$("#submission-modal-datepicker").val(date);
				$("#submission-modal-payrate-from").val(data.payRateFrom);
				$("#submission-modal-payrate-to").val(data.payRateTo);
				$("#submission-modal-pay-rate-period").val(data.payPeriodRate);
				$("#submission-modal-remarks").val(data.eventRemarks);

				$("#submission-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#submission-modal-submission-remarks").text(data.events[1].remarks);
				$("#submission-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#submission-modal-interview-remarks").text(data.events[3].remarks);
				$("#submission-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".client-submission-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-client-submission").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[2]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[2].transactionDate));
				}
				$("#client-submission-modal-datepicker").val(date);
				$("#client-submission-modal-payrate-from").val(data.payRateFrom);
				$("#client-submission-modal-payrate-to").val(data.payRateTo);
				$("#client-submission-modal-pay-rate-period").val(data.payPeriodRate);
				$("#client-submission-modal-remarks").val(data.eventRemarks);

				$("#client-submission-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#client-submission-modal-submission-remarks").text(data.events[1].remarks);
				$("#client-submission-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#client-submission-modal-interview-remarks").text(data.events[3].remarks);
				$("#client-submission-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".interview-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-interview").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[3]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[3].transactionDate));
				}
				$("#interview-modal-datepicker").val(date);
				$("#interview-modal-payrate-from").val(data.payRateFrom);
				$("#interview-modal-payrate-to").val(data.payRateTo);
				$("#interview-modal-pay-rate-period").val(data.payPeriodRate);
				$("#interview-modal-remarks").val(data.eventRemarks);

				$("#interview-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#interview-modal-submission-remarks").text(data.events[1].remarks);
				$("#interview-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#interview-modal-interview-remarks").text(data.events[3].remarks);
				$("#interview-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".placement-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-placement").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[4]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[4].transactionDate));
				}
				$("#placement-modal-datepicker").val(date);
				$("#placement-modal-payrate-from").val(data.payRateFrom);
				$("#placement-modal-payrate-to").val(data.payRateTo);
				$("#placement-modal-pay-rate-period").val(data.payPeriodRate);
				$("#placement-modal-remarks").val(data.eventRemarks);

				$("#placement-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#placement-modal-submission-remarks").text(data.events[1].remarks);
				$("#placement-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#placement-modal-interview-remarks").text(data.events[3].remarks);
				$("#placement-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".edit-button", rows).click(function(e) {
		$("#jocid-edit").val(
			$(this).data("jocandidate-id")
		);
	});

	$(".modal").on("hidden.bs.modal", function() {
		$(".cid-input").val("");
		$(".modal-input").val("");
		$("select.modal-input").val("perhour");
	});

});