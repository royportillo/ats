$(document).ready(function() {

	var table = $('#candidate-view').DataTable({
        "dom" : '<"top">rt<"bottom"flp><"clear">',
        "searching" : false,
		"info" : false,
		"order" : [[0, "desc"]],
		"scrollX" : true
	});
	
	var rows = table.rows({"search" : "applied"}).nodes();

    $("#prescreen-modal-button").click(function() {
		var joCandidateId = $("#jocid-prescreen").val();
		$.ajax({
			method: "POST",
			url: "/workflow/save/prescreen",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: $("#prescreen-modal-datepicker").val(),
				payRateFrom: $("#prescreen-modal-payrate-from").val(),
				payRateTo: $("#prescreen-modal-payrate-to").val(),
				payPeriodRate: $("#prescreen-modal-pay-rate-period").val(),
				remarks: $("#prescreen-modal-remarks").val()
			}),
			contentType: "application/json",
			success: function(data) {
                console.log("Returned: " + data);
                
				$("#row_joc_" + joCandidateId).fadeOut("300", function() {
                    var table = $("#candidate-view").DataTable();
                    table.row($(this)).remove().draw();
                });
                
				$("#prescreen-modal").modal('hide');
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#submission-modal-button").click(function() {
		var joCandidateId = $("#jocid-submission").val();
		$.ajax({
			method: "POST",
			url: "/workflow/save/submission",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: $("#submission-modal-datepicker").val(),
				payRateFrom: $("#submission-modal-payrate-from").val(),
				payRateTo: $("#submission-modal-payrate-to").val(),
				payPeriodRate: $("#submission-modal-pay-rate-period").val(),
				remarks: $("#submission-modal-remarks").val()
			}),
			contentType: "application/json",
			success: function(data) {
				console.log("Returned: " + data);

				$("#row_joc_" + joCandidateId).fadeOut("300", function() {
                    var table = $("#candidate-view").DataTable();
                    table.row($(this)).remove().draw();
                });

				$("#submission-modal").modal('hide');
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#client-submission-modal-button").click(function() {
		var joCandidateId = $("#jocid-client-submission").val();
		$.ajax({
			method: "POST",
			url: "/workflow/save/client-submission",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: $("#client-submission-modal-datepicker").val(),
				payRateFrom: $("#client-submission-modal-payrate-from").val(),
				payRateTo: $("#client-submission-modal-payrate-to").val(),
				payPeriodRate: $("#client-submission-modal-pay-rate-period").val(),
				remarks: $("#client-submission-modal-remarks").val()
			}),
			contentType: "application/json",
			success: function(data) {
				console.log("Returned: " + data);

				$("#row_joc_" + joCandidateId).fadeOut("300", function() {
                    var table = $("#candidate-view").DataTable();
                    table.row($(this)).remove().draw();
                });

				$("#client-submission-modal").modal('hide');
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#interview-modal-button").click(function() {
		var joCandidateId = $("#jocid-interview").val();
		$.ajax({
			method: "POST",
			url: "/workflow/save/interview",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: $("#interview-modal-datepicker").val(),
				payRateFrom: $("#interview-modal-payrate-from").val(),
				payRateTo: $("#interview-modal-payrate-to").val(),
				payPeriodRate: $("#interview-modal-pay-rate-period").val(),
				remarks: $("#interview-modal-remarks").val()
			}),
			contentType: "application/json",
			success: function(data) {
                console.log("Returned: " + data);
                
				$("#row_joc_" + joCandidateId).fadeOut("300", function() {
                    var table = $("#candidate-view").DataTable();
                    table.row($(this)).remove().draw();
                });

				$("#interview-modal").modal('hide');
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});
	
	$("#placement-modal-button").click(function() {
        var joCandidateId = $("#jocid-placement").val();
        
		$.ajax({
			method: "POST",
			url: "/workflow/save/placement",
			data: JSON.stringify({
				id: joCandidateId,
				transactionDate: $("#placement-modal-datepicker").val(),
				payRateFrom: $("#placement-modal-payrate-from").val(),
				payRateTo: $("#placement-modal-payrate-to").val(),
				payPeriodRate: $("#placement-modal-pay-rate-period").val(),
				remarks: $("#placement-modal-remarks").val()
			}),
			contentType: "application/json",
			success: function(data) {
                console.log("Returned: " + data);
                
                $("#row_joc_" + joCandidateId).fadeOut("300", function() {
                    var table = $("#candidate-view").DataTable();
                    table.row($(this)).remove().draw();
                });

				$("#placement-modal").modal('hide');
			},
			error: function(xhr, status, error) {
				var errorMessage = 'HTTP Status: "' + xhr.status + '"' 
				+ '\nError Message: "' + xhr.responseText + '"';

				alert(errorMessage);
			}
		}); 
	});

	$(function() {
		$("#prescreen-modal-datepicker").datepicker();
		$("#submission-modal-datepicker").datepicker();
		$("#client-submission-modal-datepicker").datepicker();
		$("#interview-modal-datepicker").datepicker();
        $("#placement-modal-datepicker").datepicker();
        
        $('#workflow-date-from').datepicker();
        $('#workflow-date-to').datepicker();
	});

    $("[data-toggle='tooltip']").tooltip();

	$(".prescreen-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-prescreen").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[0]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[0].transactionDate));
				}
				$("#prescreen-modal-datepicker").val(date);
				$("#prescreen-modal-payrate-from").val(data.payRateFrom);
				$("#prescreen-modal-payrate-to").val(data.payRateTo);
				$("#prescreen-modal-pay-rate-period").val(data.payPeriodRate);
				$("#prescreen-modal-remarks").val(data.eventRemarks);

				$("#prescreen-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#prescreen-modal-submission-remarks").text(data.events[1].remarks);
				$("#prescreen-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#prescreen-modal-interview-remarks").text(data.events[3].remarks);
				$("#prescreen-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".submission-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-submission").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[1]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[1].transactionDate));
				}
				$("#submission-modal-datepicker").val(date);
				$("#submission-modal-payrate-from").val(data.payRateFrom);
				$("#submission-modal-payrate-to").val(data.payRateTo);
				$("#submission-modal-pay-rate-period").val(data.payPeriodRate);
				$("#submission-modal-remarks").val(data.eventRemarks);

				$("#submission-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#submission-modal-submission-remarks").text(data.events[1].remarks);
				$("#submission-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#submission-modal-interview-remarks").text(data.events[3].remarks);
				$("#submission-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".client-submission-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-client-submission").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[2]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[2].transactionDate));
				}
				$("#client-submission-modal-datepicker").val(date);
				$("#client-submission-modal-payrate-from").val(data.payRateFrom);
				$("#client-submission-modal-payrate-to").val(data.payRateTo);
				$("#client-submission-modal-pay-rate-period").val(data.payPeriodRate);
				$("#client-submission-modal-remarks").val(data.eventRemarks);

				$("#client-submission-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#client-submission-modal-submission-remarks").text(data.events[1].remarks);
				$("#client-submission-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#client-submission-modal-interview-remarks").text(data.events[3].remarks);
				$("#client-submission-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".interview-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-interview").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[3]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[3].transactionDate));
				}
				$("#interview-modal-datepicker").val(date);
				$("#interview-modal-payrate-from").val(data.payRateFrom);
				$("#interview-modal-payrate-to").val(data.payRateTo);
				$("#interview-modal-pay-rate-period").val(data.payPeriodRate);
				$("#interview-modal-remarks").val(data.eventRemarks);

				$("#interview-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#interview-modal-submission-remarks").text(data.events[1].remarks);
				$("#interview-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#interview-modal-interview-remarks").text(data.events[3].remarks);
				$("#interview-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".placement-button", rows).click(function(e) {
		var jocId = $(this).data("jocandidate-id");

		$("#jocid-placement").val(
			$(this).data("jocandidate-id")
		);

		$.ajax({
			method: "GET",
			url: "/workflow",
			data: { jocId },
			success: function(data) {
				var date = null;
				if (data.events[4]) {
					date = $.datepicker.formatDate("mm/dd/yy", new Date(data.events[4].transactionDate));
				}
				$("#placement-modal-datepicker").val(date);
				$("#placement-modal-payrate-from").val(data.payRateFrom);
				$("#placement-modal-payrate-to").val(data.payRateTo);
				$("#placement-modal-pay-rate-period").val(data.payPeriodRate);
				$("#placement-modal-remarks").val(data.eventRemarks);

				$("#placement-modal-prescreen-remarks").text(data.events[0].remarks);
				$("#placement-modal-submission-remarks").text(data.events[1].remarks);
				$("#placement-modal-client-submission-remarks").text(data.events[2].remarks);
				$("#placement-modal-interview-remarks").text(data.events[3].remarks);
				$("#placement-modal-placement-remarks").text(data.events[4].remarks);
			}
		});
	});

	$(".modal").on("hidden.bs.modal", function() {
		$(".cid-input").val("");
	});
});