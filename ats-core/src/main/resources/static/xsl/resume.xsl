<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/Resume">
		<div>
			<span> <b>Name: </b></span><br/>
			<xsl:value-of
				select="StructuredXMLResume/ContactInfo/PersonName/FormattedName" />
		</div>
		<br/>
		<div>
			<span> <b>Objective: </b></span> <br/>
			<xsl:value-of select="StructuredXMLResume/Objective" />
		</div>
		<br/>
		<div>
			<span> <b>Executive Summary: </b></span> <br/>
			<xsl:value-of select="StructuredXMLResume/ExecutiveSummary" />
		</div>
	</xsl:template>
</xsl:stylesheet>