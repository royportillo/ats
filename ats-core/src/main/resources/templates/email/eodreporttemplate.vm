<head>
<style>
* {
  font-family: arial, sans-serif;
}

table {
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

thead th {
    background-color: #b0d1ff;
}

#candidate-list {
    margin-top: 20px;
}

#activity {
    width:205.15pt;
    border-collapse:collapse;
}

#activity tr{
    height:15.75pt;
}

#title {
    width:205.15pt;
    border:solid windowtext 1.0pt;
    background:red;
    padding:0in 5.4pt 0in 5.4pt;
    color:white;
}

#table-header {
    background:#0070c0;
    color:white;
}
    
.row-header {
    width:105.95pt;
    border:solid windowtext 1.0pt;
    border-top:none;
    padding:0in 5.4pt 0in 5.4pt;
}

.daxtra {
    background: #d9e2f3;
}

.total-row-header {
    width:155.55pt;
    border:solid windowtext 1.0pt;
    border-top:none;
    padding:0in 5.4pt 0in 5.4pt;
}

.cell {
    width:49.6pt;
    border-top:none;
    border-left:none;
    border-bottom:solid windowtext 1.0pt;
    border-right:solid windowtext 1.0pt;
    padding:0in 0in 0in 0in;
}
</style>
</head>

<body>
<h1>24HRPO End of Day Report #if($isUpdated)(re-sent)#end</h1>
<h3>Recruiter: $name($username) 
#if($email.length() > 0) &lt;$email&gt; #end</h3>
<h4>As of $createDate</h4>

<table style="margin-top: 20px;">
    <thead>
        <tr>
            <th></th>
            <th>For the Day</th>
            <th>For the Week</th>
            <th>For the Month</th>
            <th>Last Month</th>
            <th>L-Last Month</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <th scope="row">Prescreen</th>
            <td><span>
                    $eventCount.prescreenForTheDayCount</span></td>
            <td><span>
                    $eventCount.prescreenForTheWeekCount</span></td>
            <td><span>
                    $eventCount.prescreenForTheMonthCount</span></td>
            <td><span>
                    $eventCount.prescreenLastMonthCount</span></td>
            <td><span>
                    $eventCount.prescreenLastLastMonthCount</span></td>
        </tr>
        <tr>
            <th scope="row">Submit</th>
            <td><span>
                    $eventCount.submissionForTheDayCount</span></td>
            <td><span>
                    $eventCount.submissionForTheWeekCount</span></td>
            <td><span>
                    $eventCount.submissionForTheMonthCount</span></td>
            <td><span>
                    $eventCount.submissionLastMonthCount</span></td>
            <td><span>
                    $eventCount.submissionLastLastMonthCount</span></td>
        </tr>
        <tr>
            <th scope="row">Present</th>
            <td><span>
                    $eventCount.clientSubmissionForTheDayCount</span></td>
            <td><span>
                    $eventCount.clientSubmissionForTheWeekCount</span></td>
            <td><span>
                    $eventCount.clientSubmissionForTheMonthCount</span></td>
            <td><span>
                    $eventCount.clientSubmissionLastMonthCount</span></td>
            <td><span>
                    $eventCount.clientSubmissionLastLastMonthCount</span></td>
        </tr>
        <tr>
            <th scope="row">Interview</th>
            <td><span>
                    $eventCount.interviewForTheDayCount</span></td>
            <td><span>
                    $eventCount.interviewForTheWeekCount</span></td>
            <td><span>
                    $eventCount.interviewForTheMonthCount</span></td>
            <td><span>
                    $eventCount.interviewLastMonthCount</span></td>
            <td><span>
                    $eventCount.interviewLastLastMonthCount</span></td>
        </tr>
        <tr>
            <th scope="row">Placement</th>
            <td><span>
                    $eventCount.placementForTheDayCount</span></td>
            <td><span>
                    $eventCount.placementForTheWeekCount</span></td>
            <td><span>
                    $eventCount.placementForTheMonthCount</span></td>
            <td><span>
                    $eventCount.placementLastMonthCount</span></td>
            <td><span>
                    $eventCount.placementLastLastMonthCount</span></td>
        </tr>

        <tr>
            <th scope="row">Job Order</th>
            <td><span>
                    $eventCount.jobOrderForTheDayCount</span></td>
            <td><span>
                    $eventCount.jobOrderForTheWeekCount</span></td>
            <td><span>
                    $eventCount.jobOrderForTheMonthCount</span></td>
            <td><span>
                    $eventCount.jobOrderLastMonthCount</span></td>
            <td><span>
                    $eventCount.jobOrderLastLastMonthCount</span></td>
        </tr>
    </tbody>
</table>

<h4>Names of candidate/s prescreened:</h4>
<ol>
        #foreach($joCandidate in $prescreened)
                <li>
                $joCandidate.candidate.firstName $joCandidate.candidate.lastName,
                #if($joCandidate.candidate.source) $joCandidate.candidate.source, #else N/A, #end
                $joCandidate.jobOrder.endClient.name,
                $joCandidate.jobOrder.title ($joCandidate.jobOrder.number),
                $decimalFormat.format($joCandidate.jobOrder.billRateStart) - $decimalFormat.format($joCandidate.jobOrder.billRateEnd)
                #if($joCandidate.jobOrder.billPeriodRate == "perhour") /hour, #else /year, #end
                $decimalFormat.format($joCandidate.payRateFrom) - $decimalFormat.format($joCandidate.payRateTo)
                #if($joCandidate.jobOrder.payPeriodRate == "perhour") /hour, #else /year #end
                </li>
        #end
</ol>

<h4>Names of candidate/s submitted:</h4>
<ol>
        #foreach($joCandidate in $submitted)
                <li>
                $joCandidate.candidate.firstName $joCandidate.candidate.lastName,
                #if($joCandidate.candidate.source) $joCandidate.candidate.source, #else N/A, #end
                $joCandidate.jobOrder.endClient.name,
                $joCandidate.jobOrder.title ($joCandidate.jobOrder.number),
                $decimalFormat.format($joCandidate.jobOrder.billRateStart) - $decimalFormat.format($joCandidate.jobOrder.billRateEnd)
                #if($joCandidate.jobOrder.billPeriodRate == "perhour") /hour, #else /year, #end
                $decimalFormat.format($joCandidate.payRateFrom) - $decimalFormat.format($joCandidate.payRateTo)
                #if($joCandidate.jobOrder.payPeriodRate == "perhour") /hour, #else /year, #end
                </li>
        #end
</ol>

<h4>Names of candidate/s presented:</h4>
<ol>
        #foreach($joCandidate in $presented)
                <li>
                $joCandidate.candidate.firstName $joCandidate.candidate.lastName,
                #if($joCandidate.candidate.source) $joCandidate.candidate.source, #else N/A, #end
                $joCandidate.jobOrder.endClient.name,
                $joCandidate.jobOrder.title ($joCandidate.jobOrder.number),
                $decimalFormat.format($joCandidate.jobOrder.billRateStart) - $decimalFormat.format($joCandidate.jobOrder.billRateEnd)
                #if($joCandidate.jobOrder.billPeriodRate == "perhour") /hour, #else /year, #end
                $decimalFormat.format($joCandidate.payRateFrom) - $decimalFormat.format($joCandidate.payRateTo)
                #if($joCandidate.jobOrder.payPeriodRate == "perhour") /hour, #else /year, #end
                </li>
        #end
</ol>

<h4>Names of candidate/s interviewed:</h4>
<ol>
        #foreach($joCandidate in $interviewed)
                <li>
                $joCandidate.candidate.firstName $joCandidate.candidate.lastName,
                #if($joCandidate.candidate.source) $joCandidate.candidate.source, #else N/A, #end
                $joCandidate.jobOrder.endClient.name,
                $joCandidate.jobOrder.title ($joCandidate.jobOrder.number),
                $decimalFormat.format($joCandidate.jobOrder.billRateStart) - $decimalFormat.format($joCandidate.jobOrder.billRateEnd)
                #if($joCandidate.jobOrder.billPeriodRate == "perhour") /hour, #else /year,, #end
                $decimalFormat.format($joCandidate.payRateFrom) - $decimalFormat.format($joCandidate.payRateTo)
                #if($joCandidate.jobOrder.payPeriodRate == "perhour") /hour, #else /year,, #end
                </li>
        #end
</ol>

<h4>Names of candidate/s placed:</h4>
<ol>
        #foreach($joCandidate in $placed)
                <li>
                $joCandidate.candidate.firstName $joCandidate.candidate.lastName,
                #if($joCandidate.candidate.source) $joCandidate.candidate.source, #else N/A, #end
                $joCandidate.jobOrder.endClient.name,
                $joCandidate.jobOrder.title ($joCandidate.jobOrder.number),
                $decimalFormat.format($joCandidate.jobOrder.billRateStart) - $decimalFormat.format($joCandidate.jobOrder.billRateEnd)
                #if($joCandidate.jobOrder.billPeriodRate == "perhour") /hour, #else /year, #end
                $decimalFormat.format($joCandidate.payRateFrom) - $decimalFormat.format($joCandidate.payRateTo)
                #if($joCandidate.jobOrder.payPeriodRate == "perhour") /hour, #else /year, #end
                </li>
        #end
</ol>

<hr/>
<h3>New Job Orders for the day:</h3>
<ol>
        #foreach($jobOrder in $jobOrdersToday)
                <li>
                $jobOrder.joDate, $jobOrder.number, $jobOrder.title, $jobOrder.location
                </li>
        #end
</ol>
<hr/>

<table id="activity" border="0" cellspacing="0" cellpadding="0" width="274" >
    <tbody>

        
        <tr >
            <td id="title" width="274" nowrap="" colspan="3" >
                <p align="center" ><span >Activity Summary Report<u></u><u></u></span></p>
            </td>
        </tr>
        <tr id="table-header" >
            <td class="row-header" width="141" nowrap="" >
                <p><span ><u></u>&nbsp;<u></u></span></p>
            </td>
            <td class="cell" width="66" >
                <p align="center" ><span >Views<u></u><u></u></span></p>
            </td>
            <td class="cell" width="66" nowrap="" >
                <p align="center" ><span >Leads<u></u><u></u></span></p>
            </td>
        </tr>

        #foreach($source in $sources)
            <tr>
                <td class="row-header" width="141" nowrap="" >
                    <p><span >$source.source.name
                        #if($origSources && 
                            (${origSources[$foreach.index].sourceViews} != $source.sourceViews || ${origSources[$foreach.index].sourceLeads} != $source.sourceLeads))
                            (${origSources[$foreach.index].sourceViews}/${origSources[$foreach.index].sourceLeads})
                        #end
                    </span><u></u><u></u></p>
                </td>
                <td class="cell" width="66" >
                    <p align="center" ><span >$source.sourceViews</span><u></u><u></u></p>
                </td>
                <td class="cell" width="66" nowrap="" >
                    <p align="center" ><span >$source.sourceLeads</span><u></u><u></u></p>
                </td>
            </tr>
        #end

        <tr >
            <td class="total-row-header" width="207" nowrap="" colspan="2" >
                <p align="right" ><b><span >Views<u></u><u></u></span></b></p>
            </td>
            <td class="cell" width="66" nowrap="" >
                <p align="center" >$totalViews<u></u><u></u></p>
            </td>
        </tr>
        <tr >
            <td class="total-row-header" width="207" nowrap="" colspan="2" >
                <p><b><span >New Leads</span><u></u><u></u></b></p>
            </td>
            <td class="cell" width="66" nowrap="" >
                <p align="center" >$newLeads<u></u><u></u></p>
            </td>
        </tr>

        #foreach($lead in $leads)
            <tr >
                <td class="total-row-header" width="207" nowrap="" colspan="2" >
                    <p><b><span >$lead.source.name 
                    #if($origLeads && ${origLeads[$foreach.index].leads} != $lead.leads) 
                        (${origLeads[$foreach.index].leads})
                    #end
                    <u></u><u></u></span></b></p>
                </td>
                <td class="cell" width="66" nowrap="" >
                    <p align="center" >$lead.leads<u></u><u></u></p>
                </td>
            </tr>
        #end

        <tr >
            <td class="total-row-header" width="207" nowrap="" colspan="2" >
                <p align="right" ><b><span >Total Leads<u></u><u></u></span></b></p>
            </td>
            <td class="cell" width="66" nowrap="" >
                <p align="center" >$totalLeads<u></u><u></u></p>
            </td>
        </tr>

        #foreach($activity in $activities)
            <tr >
                <td class="total-row-header" width="207" nowrap="" colspan="2" >
                    <p align="right" ><b><span >$activity.source.name
                    #if($origActivities && ${origActivities[$foreach.index].activity} != $activity.activity) 
                        (${origActivities[$foreach.index].activity})
                    #end
                    </span><u></u><u></u></b></p>
                </td>
                <td class="cell" width="66" nowrap="" >
                    <p align="center" >$activity.activity<u></u><u></u></p>
                </td>
            </tr>
        #end
        
    </tbody>
</table>

<hr/>

#set($newLine = "
")

<h3>Comment:</h3>
<hr/>
<p>
    $comment.replaceAll("$newLine", "<br/>")
</p>

</body>