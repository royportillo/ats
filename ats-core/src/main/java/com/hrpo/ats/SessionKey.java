package com.hrpo.ats;

public enum SessionKey {

  USER_TEAM("userTeam"),
  TEAM_CLIENTS("teamClients"),
  SELECTED_CLIENT("selectedClient"),
  SELECTED_DAXTRA_DB("selectedDaxtraDb"),
  USER_FULL_NAME("userFullName");

  private String key;

  SessionKey(String key) {
    this.key = key;
  }

  public String getName() {
    return key;
  }

}
