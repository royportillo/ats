package com.hrpo.ats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import com.hrpo.daxtra.DaxtraApiModuleConfiguration;

/**
 * The main launcher application class.
 */
@SpringBootApplication
@Import(DaxtraApiModuleConfiguration.class)
public class AtsApplication {

  /**
   * Main launcher.
   *
   * @param args console argument
   */
  public static void main(String[] args) {
    SpringApplication.run(AtsApplication.class, args);
  }
}
