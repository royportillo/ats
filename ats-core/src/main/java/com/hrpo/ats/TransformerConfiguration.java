package com.hrpo.ats;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.Source;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.ui.form.ClientForm;
import com.hrpo.ats.ui.form.EndClientForm;
import com.hrpo.ats.ui.form.EodReportForm;
import com.hrpo.ats.ui.form.EodReportSourceForm;
import com.hrpo.ats.ui.form.JobOrderCandidateForm;
import com.hrpo.ats.ui.form.JobOrderForm;
import com.hrpo.ats.ui.form.SourceForm;
import com.hrpo.ats.ui.form.TeamForm;
import com.hrpo.ats.ui.form.UsersForm;
import com.hrpo.ats.ui.transformer.ClientFormTransformer;
import com.hrpo.ats.ui.transformer.ClientTransformer;
import com.hrpo.ats.ui.transformer.EndClientFormTransformer;
import com.hrpo.ats.ui.transformer.EndClientTransformer;
import com.hrpo.ats.ui.transformer.EodReportFormTransformer;
import com.hrpo.ats.ui.transformer.EodReportSourceFormTransformer;
import com.hrpo.ats.ui.transformer.EodReportTransformer;
import com.hrpo.ats.ui.transformer.JobOrderCandidateFormTransformer;
import com.hrpo.ats.ui.transformer.JobOrderCandidateTransformer;
import com.hrpo.ats.ui.transformer.JobOrderFormTransformer;
import com.hrpo.ats.ui.transformer.JobOrderTransformer;
import com.hrpo.ats.ui.transformer.SourceFormTransformer;
import com.hrpo.ats.ui.transformer.SourceTransformer;
import com.hrpo.ats.ui.transformer.TeamFormTransformer;
import com.hrpo.ats.ui.transformer.TeamTransformer;
import com.hrpo.ats.ui.transformer.UserFormTransformer;
import com.hrpo.ats.ui.transformer.UserTransformer;

@Configuration
public class TransformerConfiguration {

  @Bean
  public JobOrderTransformer jobOrderTransformer() {
    
    return new JobOrderTransformer() {
      @Override
      public JobOrder transform(JobOrderForm jobOrderForm) {
        return transformToDto(jobOrderForm);
      }
    };
  }
  
  @Bean
  public JobOrderFormTransformer jobOrderFormTransformer() {
    
    return new JobOrderFormTransformer() {
      @Override
      public JobOrderForm transform(JobOrder jobOrderDto) {
        return transformToForm(jobOrderDto);
      }
    };
  }
  
  @Bean
  public JobOrderCandidateFormTransformer jobOrderCandidateFormTransformer() {
    
    return new JobOrderCandidateFormTransformer() {
      @Override
      public JobOrderCandidateForm transform(JobOrderCandidate dto) {
        return transformToForm(dto);
      }
    };
  }
  
  @Bean
  public JobOrderCandidateTransformer jobOrderCandidateTransformer() {
    
    return new JobOrderCandidateTransformer() {
      @Override
      public JobOrderCandidate transform(JobOrderCandidateForm form) {
        return transformToDto(form);
      }
    };
  }
  
  @Bean
  public UserTransformer userFormTransformer() {
    
    return new UserTransformer() {
      
      @Override
      public User transform(UsersForm usersForm) {
        return transformToDto(usersForm);
      }
    };
  }
  
  @Bean
  public UserFormTransformer userTransformer() {
    return new UserFormTransformer() {
      @Override
      public UsersForm transform(User userDto) {
        return transformToForm(userDto);
      }
    };
  }
  
  @Bean
  public TeamFormTransformer teamFormTransformer() {
    return new TeamFormTransformer() {
      @Override
      public TeamForm transform(Team dto) {
        return transformToForm(dto);
      }
    };
  }
  
  @Bean
  public TeamTransformer teamTransformer() {
    return new TeamTransformer() {
      @Override
      public Team transform(TeamForm form) {
        return transformToDto(form);
      }
    };
  }
  
  @Bean
  public ClientFormTransformer clientFormTransformer() {
    return new ClientFormTransformer() {
      @Override
      public ClientForm transform(Client dto) {
        return transformToForm(dto);
      }
    };
  }
  
  @Bean
  public ClientTransformer clientTransformer() {
    return new ClientTransformer() {
      @Override
      public Client transform(ClientForm form) {
        return transformToDto(form);
      }
    };
  }
  
  @Bean
  public EndClientFormTransformer endClientFormTransformer() {
    return new EndClientFormTransformer() {
      @Override
      public EndClientForm transform(EndClient dto) {
        return transformToForm(dto);
      }
    };
  }
  
  @Bean
  public EndClientTransformer endClientTransformer() {
    return new EndClientTransformer() {
      @Override
      public EndClient transform(EndClientForm form) {
        return transformToDto(form);
      }
    };
  }
  
  @Bean
  public SourceFormTransformer sourceFormTransformer() {
    return new SourceFormTransformer() {
      @Override
      public SourceForm transform(Source dto) {
        return transformToForm(dto);
      }
    };
  }
  
  @Bean
  public SourceTransformer sourceTranformer() {
    return new SourceTransformer() {
      @Override
      public Source transform(SourceForm form) {
        return transformToDto(form);
      }
    };
  }
  
  @Bean
  public EodReportTransformer eodReportTransformer() {
    return new EodReportTransformer() {
      @Override
      public EodReport transform(EodReportForm form) {
        return transformToDto(form);
      }
    };
  }
  
  @Bean
  public EodReportFormTransformer eodReportFormTransformer() {
    return new EodReportFormTransformer() {
      @Override
      public EodReportForm transform(EodReport dto) {
        return transformToForm(dto);
      }
    };
  }
  
  @Bean
  public EodReportSourceFormTransformer eodReportSourceFormTransformer() {
    return new EodReportSourceFormTransformer() {
      @Override
      public EodReportSourceForm transform(EodReportSource dto) {
        return transformToForm(dto);
      }
    };
  }
}
