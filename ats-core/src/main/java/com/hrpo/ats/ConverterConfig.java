package com.hrpo.ats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.hrpo.ats.ui.converter.StringToUserRoleConverter;

@Configuration
public class ConverterConfig implements WebMvcConfigurer {
  
  @Autowired
  private StringToUserRoleConverter converter;
  
  @Override
  public void addFormatters(FormatterRegistry registry) {
    registry.addConverter(converter);
  }

}
