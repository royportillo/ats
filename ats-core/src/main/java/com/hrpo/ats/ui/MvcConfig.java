package com.hrpo.ats.ui;

import java.util.Locale;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/dashboard").setViewName("dashboard");
    registry.addViewController("/").setViewName("index");
    registry.addViewController("/login").setViewName("login");
  }

  /**
   * Custom resource handlers configuration. If an error is encountered here, try setting
   * resource chain to false temporarily.
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(
        "/webjars/**", 
        "/images/**", 
        "/css/**")
    .addResourceLocations(
        "/webjars/", 
        "classpath:/static/images/", 
        "classpath:/static/css/")
      .resourceChain(true); // false during development.
  }

  /**
   * Set locale to US by default.
   *
   * @return the locale resolver
   */
  @Bean
  public LocaleResolver localeResolver() {
    SessionLocaleResolver resolver = new SessionLocaleResolver();
    resolver.setDefaultLocale(Locale.US);
    return resolver;
  }

  /**
   * Sets language parameter to "lang".
   *
   * @return the {@code WebMvcConfigurer}
   */
  @Bean
  public WebMvcConfigurer configurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addInterceptors(InterceptorRegistry registry) {
        LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
        localeInterceptor.setParamName("lang");
        registry.addInterceptor(localeInterceptor);
      }
    };
  }
}