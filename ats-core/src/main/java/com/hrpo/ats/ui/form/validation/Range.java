package com.hrpo.ats.ui.form.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
@Constraint(validatedBy = RangeValidator.class)
public @interface Range {
  
  String message() default "Start value must be lower than end value";
  String from();
  String to();
  
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target(ElementType.TYPE)
  @interface List {
    Range[] value();
  }
}
