package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.Source;
import com.hrpo.ats.ui.form.SourceForm;

public interface SourceFormTransformer extends Transformer<Source, SourceForm> {
  
  SourceForm transform(Source dto);
  
  default SourceForm transformToForm(Source dto) {
    SourceForm form = new SourceForm();
    
    form.setId(dto.getId());
    form.setName(dto.getName());
    form.setCategory(dto.getCategory());
    form.setDescription(dto.getDescription());
    form.setIndex(dto.getIndex());
    
    return form; 
  }

}
