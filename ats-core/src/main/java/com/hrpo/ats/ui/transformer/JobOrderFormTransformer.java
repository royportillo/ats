package com.hrpo.ats.ui.transformer;

import java.util.ArrayList;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.ui.form.JobOrderCandidateForm;
import com.hrpo.ats.ui.form.JobOrderForm;

@FunctionalInterface
public interface JobOrderFormTransformer extends Transformer<JobOrder, JobOrderForm> {

  JobOrderForm transform(JobOrder jobOrder);
  
  default JobOrderForm transformToForm(JobOrder dto) {
    JobOrderForm form = new JobOrderForm();
    form.setId(dto.getId().get());
    form.setNumber(dto.getNumber());
    form.setJoDate(dto.getJoDate());
    form.setDateCreated(dto.getDateCreated());
    form.setTitle(dto.getTitle());
    form.setLocation(dto.getLocation());
    form.setZipCode(dto.getZipCode());
    
    form.setEndClient(dto.getEndClient());
    form.setDivision(dto.getDivision());
    
    form.setStartDate(dto.getStartDate());
    form.setEndDate(dto.getEndDate());
    
    form.setBillRateStart(dto.getBillRateStart());
    form.setBillRateEnd(dto.getBillRateEnd());
    form.setBillPeriodRate(dto.getBillPeriodRate());
    
    form.setPayRateStart(dto.getPayRateStart());
    form.setPayRateEnd(dto.getPayRateEnd());
    form.setPayPeriodRate(dto.getPayPeriodRate());
    
    form.setOpenings(dto.getOpenings());
    form.setMaxSubs(dto.getMaxSubs());
    form.setStatus(dto.getStatus());
    
    form.setHasFile(dto.getHasFile());
    
    var jobOrderCandidates = dto.getJobOrderCandidates();
    var jobOrderCandidateForms = new ArrayList<JobOrderCandidateForm>();
    if (jobOrderCandidates != null) {
      for (JobOrderCandidate joc : jobOrderCandidates) {
        JobOrderCandidateForm jocForm = new JobOrderCandidateForm();
        jocForm.setId(joc.getId());
        jocForm.setJobOrderId(joc.getJobOrder().getId().get());
        jocForm.setCandidateRowId(joc.getCandidate().getId());
        jocForm.setCandidateId(joc.getCandidate().getCandidateId());
        jocForm.setSource(joc.getCandidate().getSource());
        jocForm.setFirstName(joc.getCandidate().getFirstName());
        jocForm.setLastName(joc.getCandidate().getLastName());
        jocForm.setFullName(joc.getCandidate().getFullName());
        jocForm.setMobile(joc.getCandidate().getMobile());
        jocForm.setPhone(joc.getCandidate().getPhone());
        jocForm.setEmail(joc.getCandidate().getEmail());
        
        jocForm.setPayRateFrom(joc.getPayRateFrom());
        jocForm.setPayRateTo(joc.getPayRateTo());
        jocForm.setPayPeriodRate(joc.getPayPeriodRate());
        
        JobOrderCandidate.WorkflowState workflowState = joc.getWorkflowState();
        if (workflowState != null) {
          jocForm.setPrescreen(joc.getWorkflowState().getPrescreenState());
          jocForm.setSubmission(joc.getWorkflowState().getSubmissionState());
          jocForm.setClientSubmission(joc.getWorkflowState().getClientSubmissionState());
          jocForm.setInterview(joc.getWorkflowState().getInterviewState());
          jocForm.setPlacement(joc.getWorkflowState().getPlacementState());
        }
        
        jobOrderCandidateForms.add(jocForm);
      }
      
      form.setJobOrderCandidates(jobOrderCandidateForms);
    }
    
    return form;
  }
}
