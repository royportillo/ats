package com.hrpo.ats.ui;

import java.io.IOException;
import java.io.StringReader;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.Base64;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.hrpo.ats.DaxtraDatabase;
import com.hrpo.ats.SessionKey;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.service.CandidateService;
import com.hrpo.ats.service.XmlToHtmlTransformer;
import com.hrpo.ats.util.DecoderUtils;
import com.hrpo.daxtra.dto.Candidate;

@Controller
@RequestMapping("candidate")
public class CandidateController {

  Logger log = LoggerFactory.getLogger(CandidateController.class);

  @Autowired
  private XmlToHtmlTransformer transformer;

  @Autowired
  private CandidateService candidateService;
  
  @Value("classpath:static/xsl/resume.xsl")
  Resource resourceFile;

  @GetMapping("")
  public String show() {

    return "candidate";
  }

  @RequestMapping(params = {"id"}, method = RequestMethod.GET)
  public String load(Model model, @RequestParam(value = "id") String id, Principal principal, 
      HttpSession session) 
      throws IOException, TransformerException {

    Source xslFromClasspath = new StreamSource(resourceFile.getInputStream());

    String database = ((DaxtraDatabase) 
        session.getAttribute(SessionKey.SELECTED_DAXTRA_DB.getName())).getDatabase();
    
    JobOrderCandidate jobOrderCandidate = 
        candidateService.getJobOrderCandidateHrXmlByCandidateId(id, principal.getName(), database);
    Source xmlFromString = 
        new StreamSource(new StringReader(jobOrderCandidate.getCandidate().getHrXml()));

    // Convert candidate's resume from HRXML to HTML fragment by application of XSL.
    String resumeHtmlFragment = transformer.transform(xslFromClasspath, xmlFromString);

    // Embed candidate's HTML fragment resume into candidate page view.
    model.addAttribute("resumeHtml", resumeHtmlFragment);
    model.addAttribute("joc", jobOrderCandidate);

    return "candidate";
  }

  @RequestMapping(value = "/download", params = {"id"}, method = RequestMethod.GET)
  public ResponseEntity<Resource> downloadRawCv(Model model,
      @RequestParam(value = "id") String candidateId, Principal principal, HttpSession session) {
    
    String database = ((DaxtraDatabase) 
        session.getAttribute(SessionKey.SELECTED_DAXTRA_DB.getName())).getDatabase();
    
    Candidate candidate = candidateService
        .getCandidateRawCvByCandidateId(candidateId, database);
    
    String[] fileType = candidate.getRawCvFileType().split(";");
    String fileExtension = fileType[0];
    String fileMimeType = fileType[1];
    
    byte[] payload = DecoderUtils.decode(candidate.getRawCv(), Base64.getMimeDecoder()::decode);
    
    ByteArrayResource byteResource = new ByteArrayResource(payload);
    
    String fileName = MessageFormat.format("{0}_{1}_{2}.{3}", candidate.getCandidateId(),
        candidate.getFirstName(), candidate.getLastName(), fileExtension);
    
    HttpHeaders header = new HttpHeaders();
    header.add(HttpHeaders.CONTENT_DISPOSITION, 
        "attachment; filename=\"" + fileName + "\"");
    
    return ResponseEntity.ok().contentLength((long) payload.length).headers(header)
        .contentType(MediaType.parseMediaType(fileMimeType))
        .body(byteResource);
  }
  
  @PostMapping("")
  public ResponseEntity<String> setSelectedDaxtraDb(
      @RequestParam(value = "daxtraDbId") DaxtraDatabase daxtraDb, HttpSession session) {
    log.info("Selected Daxtra DB: " + daxtraDb.getName());
    
    session.setAttribute(SessionKey.SELECTED_DAXTRA_DB.getName(), daxtraDb);
    return new ResponseEntity<String>(
        "Set Selected Daxtra OK: " + daxtraDb.getName(), HttpStatus.OK);
  }
}
