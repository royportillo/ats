package com.hrpo.ats.ui;

import java.security.Principal;
import java.time.Clock;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.Dashboard.WorkflowEventCountPortlet;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.exception.JobOrderException;
import com.hrpo.ats.service.DashboardService;
import com.hrpo.ats.service.JobOrderService;
import com.hrpo.ats.ui.form.JobOrderCandidateForm;
import com.hrpo.ats.ui.form.JobOrderForm;
import com.hrpo.ats.ui.transformer.JobOrderCandidateFormTransformer;
import com.hrpo.ats.ui.transformer.JobOrderFormTransformer;


@Controller
@RequestMapping({"dashboard"})
public class DashboardController {

  Logger log = LoggerFactory.getLogger(DashboardController.class);

  @Autowired
  private DashboardService dashboardService;
  
  @Autowired
  private JobOrderService jobOrderService;
  
  @Autowired
  private JobOrderCandidateFormTransformer jocFormTransformer;
  
  @Autowired
  private JobOrderFormTransformer joFormTransformer;

  @GetMapping("")
  public String load(Model model, Principal principal) {
    
    WorkflowEventCountPortlet workflowEventCountPortlet = 
        dashboardService.getWorkflowEventCountPortletData(
            principal.getName(), Clock.systemDefaultZone(), false);

    model.addAttribute("serverDate", LocalTime.now());
    model.addAttribute("eventCount", workflowEventCountPortlet);

    return "dashboard";

  }
  
  @GetMapping(value = "/candidates", produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody List<JobOrderCandidateForm> loadCandidates(Principal principal,
      @RequestParam(value = "workflow") String workflow,
      @RequestParam(value = "period") String period) {
    
    List<JobOrderCandidate> joCandidates = null;
    
    try {
      joCandidates = jobOrderService.getJobOrderCandidatesByWorkflowEvent(
          WorkflowStage.ofName(workflow), principal.getName(), period, Clock.systemDefaultZone(),
          false);
    } catch (IllegalArgumentException iae) {
      throw new ResourceNotFoundException("No such page exists");
    } catch (JobOrderException joe) {
      if (joe.getCause() instanceof IllegalArgumentException) {
        throw new ResourceNotFoundException("Invalid period specified");
      }
    }
    
    List<JobOrderCandidateForm> jocForms = joCandidates.stream()
        .map(jocFormTransformer::transform).collect(Collectors.toList());
    
    return jocForms;
  }
  
  @GetMapping(value = "/joborders", produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody List<JobOrderForm> loadJobOrders(Principal principal,
      @RequestParam(value = "period") String period) {
    
    List<JobOrder> jobOrders = null;
    
    try {
      jobOrders = jobOrderService.getJobOrdersByUserAndPeriod(
          principal.getName(), period, Clock.systemDefaultZone(), false);
    } catch (IllegalArgumentException iae) {
      throw new ResourceNotFoundException("No such page exists");
    } catch (JobOrderException joe) {
      if (joe.getCause() instanceof IllegalArgumentException) {
        throw new ResourceNotFoundException("Invalid period specified");
      }
    }
    
    List<JobOrderForm> joForms = jobOrders.stream()
        .map(joFormTransformer::transform).collect(Collectors.toList());
    
    return joForms;
  }
}