package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.Team;
import com.hrpo.ats.ui.form.TeamForm;

public interface TeamTransformer extends Transformer<TeamForm, Team> {

  Team transform(TeamForm form);
  
  default Team transformToDto(TeamForm form) {
    Team team = new Team();
    
    team.setId(form.getId());
    team.setName(form.getName());
    team.setEmail(form.getEmail());
    team.setDescription(form.getDescription());
    
    return team;
  }
}
