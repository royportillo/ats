package com.hrpo.ats.ui.form;

import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.UserRole;
import com.hrpo.ats.ui.form.validation.FieldMatches;

@FieldMatches(value1 = "password", value2 = "confirmPassword", 
    message = "Passwords do not match.")
public class UsersForm {
  private Integer id;
  
  @NotBlank(message = "First name must not be empty.")
  private String firstName;
  @NotBlank(message = "Last name must not be empty.")
  private String lastName;
  
  @NotBlank(message = "Email address is required.")
  @Email(message = "Please enter a valid email address.")
  private String email;
  
  @NotBlank(message = "Username must not be empty.")
  private String username;
  
  private String password;
  private String confirmPassword;
  
  private Boolean enabled;
  
  private Team team;
  
  @NotEmpty(message = "Selected roles must not be empty.")
  private List<UserRole> roles;
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }
  
  public void setUsername(String username) {
    this.username = username;
  }
  
  public String getPassword() {
    return password;
  }
  
  public void setPassword(String password) {
    this.password = password;
  }
  
  public String getConfirmPassword() {
    return confirmPassword;
  }
  
  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }

  public List<UserRole> getRoles() {
    return roles;
  }

  public void setRoles(List<UserRole> roles) {
    this.roles = roles;
  }
  
}
