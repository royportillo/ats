package com.hrpo.ats.ui;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.ats.exception.JobOrderException;
import com.hrpo.ats.service.JobOrderService;
import com.hrpo.ats.service.WorkflowEventService;
import com.hrpo.ats.ui.form.JobOrderCandidateForm;
import com.hrpo.ats.ui.transformer.JobOrderCandidateFormTransformer;
import com.hrpo.ats.ui.transformer.JobOrderCandidateTransformer;

@Controller
@RequestMapping("workflow")
public class WorkflowController {

  Logger log = LoggerFactory.getLogger(WorkflowController.class);
  
  @Autowired
  private JobOrderService jobOrderService;
  
  @Autowired
  private WorkflowEventService workflowService;
  
  @Autowired
  private JobOrderCandidateFormTransformer jobOrderCandidateFormTransformer;
  
  @Autowired
  private JobOrderCandidateTransformer jobOrderCandidateTransformer;
  
  @RequestMapping(value = "/{page}", method = RequestMethod.GET)
  public String load(Model model, Principal principal, @PathVariable("page") String page) {
    
    List<JobOrderCandidate> jobOrderCandidates;
    
    try {
      jobOrderCandidates = jobOrderService
          .getJobOrderCandidatesByLastWorkflowEvent(WorkflowStage.ofName(page), principal.getName(),
              null, null);
    } catch (IllegalArgumentException iae) {
      throw new ResourceNotFoundException("No such page exists");
    }
    
    List<JobOrderCandidateForm> jobOrderCandidateForms = 
        jobOrderCandidates.stream()
        .map(jobOrderCandidateFormTransformer::transform).collect(Collectors.toList());
    model.addAttribute("jocandidates", jobOrderCandidateForms);
    
    return page;
  }
  
  @RequestMapping(value = "", params = {"jocId"}, method = RequestMethod.GET, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody JobOrderCandidateForm viewEventsByJoCandidate(
      @RequestParam(value = "jocId") Integer jocId) {
    JobOrderCandidateForm form = null;
    try {
      JobOrderCandidate joCandidate = jobOrderService.getJobOrderCandidateById(jocId);
      var workflowEvents = workflowService.getWorkflowEventsByJoCandidateId(jocId);
      
      form = jobOrderCandidateFormTransformer.transformToForm(joCandidate);
      form.setEvents(workflowEvents);
    } catch (JobOrderException joe) {
      if (joe.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException("No such job order candidate");
      }
    }
    return form;
  }
  
  @RequestMapping(value = "/{page}", params = {"dateFrom", "dateTo"}, method = RequestMethod.GET)
  public String viewByDate(Model model, Principal principal, 
      @RequestParam(value = "dateFrom") @DateTimeFormat(pattern = "MM/dd/yyyy") LocalDate fromDate,
      @RequestParam(value = "dateTo") @DateTimeFormat(pattern = "MM/dd/yyyy") LocalDate toDate,
      @PathVariable("page") String page) {
    
    List<JobOrderCandidate> jobOrderCandidates;
    
    try {
      jobOrderCandidates = jobOrderService.getJobOrderCandidatesByLastWorkflowEvent(
          WorkflowStage.ofName(page), principal.getName(), fromDate, toDate);
    } catch (IllegalArgumentException iae) {
      throw new ResourceNotFoundException("No such page exists");
    }
    
    List<JobOrderCandidateForm> jobOrderCandidateForms = 
        jobOrderCandidates.stream()
        .map(jobOrderCandidateFormTransformer::transform).collect(Collectors.toList());
    model.addAttribute("jocandidates", jobOrderCandidateForms);
    
    return page;
  }
  
  @RequestMapping(value = "/delete", params = {"jocid"}, method = RequestMethod.DELETE)
  public ResponseEntity<?> rollback(Model model, Principal principal, 
      @RequestParam(value = "jocid") Integer joCandidateId) {
    
    workflowService.deleteWorkflowEventsByJoCandidateId(joCandidateId);
    return new ResponseEntity<Integer>(joCandidateId, HttpStatus.ACCEPTED);
  }
  
  @RequestMapping(value = "/save/prescreen", method = RequestMethod.POST)
  public ResponseEntity<?> savePrescreenEvent(Model model, 
      @Valid @RequestBody JobOrderCandidateForm jocForm, BindingResult bindingResult) {
    
    if (bindingResult.hasErrors()) {
      List<String> errorMessages = bindingResult.getAllErrors().stream().map(error -> {
        return error.getDefaultMessage();
      }).collect(Collectors.toList());
      return new ResponseEntity<String>(String.join("\n", errorMessages), 
          HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    var joCandidate = jobOrderCandidateTransformer.transform(jocForm);

    AbstractWorkflowEvent event = new PrescreenEvent(); 
    try {
      event = computeEvent(joCandidate, new PrescreenEvent());
    } catch (JobOrderException joe) {
      return new ResponseEntity<String>(joe.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    Integer eventId = 
        workflowService.savePrescreenEvent(event, joCandidate);

    return new ResponseEntity<Integer>(eventId, HttpStatus.OK);
  }

  @RequestMapping(value = "/save/submission", method = RequestMethod.POST)
  public ResponseEntity<?> saveSubmissionEvent(Model model, 
      @Valid @RequestBody JobOrderCandidateForm jocForm, BindingResult bindingResult) {
    
    if (bindingResult.hasErrors()) {
      List<String> errorMessages = bindingResult.getAllErrors().stream().map(error -> {
        return error.getDefaultMessage();
      }).collect(Collectors.toList());
      return new ResponseEntity<String>(String.join("\n", errorMessages), 
          HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    var joCandidate = jobOrderCandidateTransformer.transform(jocForm);

    AbstractWorkflowEvent event;
    try {
      event = computeEvent(joCandidate, new SubmissionEvent());
    } catch (JobOrderException joe) {
      return new ResponseEntity<String>(joe.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    Integer eventId = 
        workflowService.saveSubmissionEvent(event, joCandidate);

    return new ResponseEntity<Integer>(eventId, HttpStatus.OK);
  }

  @RequestMapping(value = "/save/client-submission", method = RequestMethod.POST)
  public ResponseEntity<?> saveClientSubmissionEvent(Model model, 
      @Valid @RequestBody JobOrderCandidateForm jocForm, BindingResult bindingResult) { 
    
    if (bindingResult.hasErrors()) {
      List<String> errorMessages = bindingResult.getAllErrors().stream().map(error -> {
        return error.getDefaultMessage();
      }).collect(Collectors.toList());
      return new ResponseEntity<String>(String.join("\n", errorMessages), 
          HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    var joCandidate = jobOrderCandidateTransformer.transform(jocForm);

    AbstractWorkflowEvent event;
    try {
      event = computeEvent(joCandidate, new ClientSubmissionEvent());
    } catch (JobOrderException joe) {
      return new ResponseEntity<String>(joe.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    Integer eventId = 
        workflowService.saveClientSubmissionEvent(event, joCandidate);

    return new ResponseEntity<Integer>(eventId, HttpStatus.OK);
  }

  @RequestMapping(value = "/save/interview", method = RequestMethod.POST)
  public ResponseEntity<?> saveInterviewEvent(Model model, 
      @Valid @RequestBody JobOrderCandidateForm jocForm, BindingResult bindingResult) { 

    if (bindingResult.hasErrors()) {
      List<String> errorMessages = bindingResult.getAllErrors().stream().map(error -> {
        return error.getDefaultMessage();
      }).collect(Collectors.toList());
      return new ResponseEntity<String>(String.join("\n", errorMessages), 
          HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    var joCandidate = jobOrderCandidateTransformer.transform(jocForm);
    
    AbstractWorkflowEvent event;
    try {
      event = computeEvent(joCandidate, new InterviewEvent());
    } catch (JobOrderException joe) {
      return new ResponseEntity<String>(joe.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    Integer eventId = 
        workflowService.saveInterviewEvent(event, joCandidate);

    return new ResponseEntity<Integer>(eventId, HttpStatus.OK);
  }

  @RequestMapping(value = "/save/placement", method = RequestMethod.POST)
  public ResponseEntity<?> savePlacementEvent(Model model, 
      @Valid @RequestBody JobOrderCandidateForm jocForm, BindingResult bindingResult) { 

    if (bindingResult.hasErrors()) {
      List<String> errorMessages = bindingResult.getAllErrors().stream().map(error -> {
        return error.getDefaultMessage();
      }).collect(Collectors.toList());
      return new ResponseEntity<String>(String.join("\n", errorMessages), 
          HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    var joCandidate = jobOrderCandidateTransformer.transform(jocForm);
    
    AbstractWorkflowEvent event;
    
    try {
      event = computeEvent(joCandidate, new PlacementEvent());
    } catch (JobOrderException joe) {
      return new ResponseEntity<String>(joe.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
    
    Integer eventId = 
        workflowService.savePlacementEvent(event, joCandidate);

    return new ResponseEntity<Integer>(eventId, HttpStatus.OK);
  }
  
  private AbstractWorkflowEvent computeEvent(JobOrderCandidate joCandidate, 
      AbstractWorkflowEvent event) {
    
    LocalDate localDate = joCandidate.getTransactionDate();
    log.info("Transaction date: " + localDate);
    
    LocalDate joDate = jobOrderService
        .getJobOrderCandidateById(joCandidate.getId()).getJobOrder().getJoDate();
    
    if (localDate.isBefore(joDate)) {
      String invalidDateMessage = 
          "Invalid transaction date: Transaction date cannot be earlier than job order date";
      throw new JobOrderException(invalidDateMessage);
    }
    
    event.setRemarks(joCandidate.getEventRemarks());
    event.setTransactionDate(localDate);
    
    return event;
  }
}
