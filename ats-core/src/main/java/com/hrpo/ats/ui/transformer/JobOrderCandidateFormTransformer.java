package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.ui.form.JobOrderCandidateForm;
import com.hrpo.daxtra.dto.Candidate;

public interface JobOrderCandidateFormTransformer extends 
    Transformer<JobOrderCandidate, JobOrderCandidateForm> {
  
  JobOrderCandidateForm transform(JobOrderCandidate jobOrderCandidate);
  
  default JobOrderCandidateForm transformToForm(JobOrderCandidate dto) {
    JobOrderCandidateForm form = new JobOrderCandidateForm();
    form.setId(dto.getId());
    
    form.setJobOrderId(dto.getJobOrder().getId().get());
    form.setJobOrderNumber(dto.getJobOrder().getNumber());
    form.setJobOrderTitle(dto.getJobOrder().getTitle());
    
    form.setJoBillRateFrom(dto.getJobOrder().getBillRateStart());
    form.setJoBillRateTo(dto.getJobOrder().getBillRateEnd());
    form.setJoBillPeriodRate(dto.getJobOrder().getBillPeriodRate());
    form.setJoBillRate(dto.getJobOrder().getBillRateStart().toString() 
        + " - " + dto.getJobOrder().getBillRateEnd() + " " + dto.getJobOrder().getBillPeriodRate());
    
    form.setJoPayRateFrom(dto.getJobOrder().getPayRateStart());
    form.setJoPayRateTo(dto.getJobOrder().getPayRateEnd());
    form.setJoPayPeriodRate(dto.getJobOrder().getPayPeriodRate());
    form.setJoPayRate(dto.getJobOrder().getPayRateStart().toString() 
        + " - " + dto.getJobOrder().getPayRateEnd() + " " + dto.getJobOrder().getPayPeriodRate());
    
    form.setJobOrderLocation(dto.getJobOrder().getLocation());
    form.setEndClientName(dto.getJobOrder().getEndClient().getName());
    form.setClientName(dto.getJobOrder().getEndClient().getClient().getName());
    form.setOwnerName(dto.getOwner().getFirstName() + " " + dto.getOwner().getLastName());
    
    Candidate candidate = dto.getCandidate();
    if (candidate != null) {
      form.setCandidateRowId(dto.getCandidate().getId());
      form.setCandidateId(dto.getCandidate().getCandidateId());
      form.setSource(dto.getCandidate().getSource());
      form.setFirstName(dto.getCandidate().getFirstName());
      form.setLastName(dto.getCandidate().getLastName());
      form.setFullName(dto.getCandidate().getFullName());
      form.setPhone(dto.getCandidate().getPhone());
      form.setMobile(dto.getCandidate().getMobile());
      form.setEmail(dto.getCandidate().getEmail());
    }
    
    form.setTransactionDate(dto.getTransactionDate());
    form.setWorkflowStage(dto.getWorkflowStage().getName());
    
    if (dto.getLastWorkflowStage() != null) {
      form.setLastWorkflowStage(dto.getLastWorkflowStage().getName());
    }
    
    form.setPayRateFrom(dto.getPayRateFrom());
    form.setPayRateTo(dto.getPayRateTo());
    form.setPayPeriodRate(dto.getPayPeriodRate());
    
    form.setEventRemarks(dto.getEventRemarks());
    
    JobOrderCandidate.WorkflowState workflowState = dto.getWorkflowState();
    if (workflowState != null) {
      form.setPrescreen(dto.getWorkflowState().getPrescreenState());
      form.setSubmission(dto.getWorkflowState().getSubmissionState());
      form.setClientSubmission(dto.getWorkflowState().getClientSubmissionState());
      form.setInterview(dto.getWorkflowState().getInterviewState());
      form.setPlacement(dto.getWorkflowState().getPlacementState());
    }
    
    return form;
  }
}
