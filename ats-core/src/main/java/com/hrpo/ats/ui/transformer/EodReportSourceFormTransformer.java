package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.ui.form.EodReportSourceForm;

public interface EodReportSourceFormTransformer 
    extends Transformer<EodReportSource, EodReportSourceForm> {
  
  @Override
  EodReportSourceForm transform(EodReportSource dto);
  
  default EodReportSourceForm transformToForm(EodReportSource dto) {
    EodReportSourceForm form = new EodReportSourceForm();
    
    form.setReportDate(dto.getEodReport().getReportDate());
    
    String recruiterName = String.format("%s %s", dto.getEodReport().getUser().getFirstName(),
        dto.getEodReport().getUser().getLastName());
    form.setRecruiter(recruiterName);
    
    form.setSource(dto.getSource().getName());
    form.setViews(dto.getSourceViews());
    form.setLeads(dto.getSourceLeads());
    form.setOtherLeads(dto.getLeads());
    form.setActivity(dto.getActivity());
    form.setTeam(dto.getEodReport().getUser().getTeam().getName());
    form.setComment(dto.getEodReport().getComment());
    
    return form;
  }

}
