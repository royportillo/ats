package com.hrpo.ats.ui;

import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.exception.EndClientException;
import com.hrpo.ats.service.ClientService;
import com.hrpo.ats.service.EndClientService;
import com.hrpo.ats.ui.form.EndClientForm;
import com.hrpo.ats.ui.transformer.ClientFormTransformer;
import com.hrpo.ats.ui.transformer.EndClientFormTransformer;
import com.hrpo.ats.ui.transformer.EndClientTransformer;

@Controller
@RequestMapping("endclients")
public class EndClientController {

  Logger log = LoggerFactory.getLogger(EndClientController.class);
  
  @Autowired
  EndClientService endClientService;
  
  @Autowired
  ClientService clientService;
  
  @Autowired
  EndClientFormTransformer endClientFormTransformer;
  
  @Autowired
  EndClientTransformer endClientTransformer;
  
  @Autowired
  ClientFormTransformer clientFormTransformer;
  
  @GetMapping("/new")
  public String show(Model model) {
    
    model.addAttribute("endclientsForm", new EndClientForm());
    model.addAttribute("clients", 
        clientService.getAllClients().stream().map(clientFormTransformer::transform)
        .collect(Collectors.toList()));
    
    return "endclients-edit";
  }
  
  @RequestMapping(value = "/edit", params = {"id"}, method = RequestMethod.GET)
  public String edit(@RequestParam(value = "id") Integer id, ModelMap model) {
    
    EndClient endClient = null;
    
    try {
      endClient = endClientService.getEndClientById(id);
    } catch (EndClientException ece) {
      if (ece.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(ece.getMessage());
      }
    }
    
    model.addAttribute("endclientsForm", endClientFormTransformer.transform(endClient));
    model.addAttribute("clients", 
        clientService.getAllClients().stream().map(clientFormTransformer::transform)
        .collect(Collectors.toList()));
    
    return "endclients-edit";
  }
  
  @RequestMapping(value = "/view", params = {"id"}, method = RequestMethod.GET)
  public String view(@RequestParam(value = "id") Integer id, ModelMap model) {
    
    EndClient endClient = null;
    
    try {
      endClient = endClientService.getEndClientById(id);
    } catch (EndClientException ece) {
      if (ece.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(ece.getMessage());
      }
    }
    
    model.addAttribute("endclientsForm", endClientFormTransformer.transform(endClient));
    
    return "endclients-view";
  }
  
  @GetMapping("/find")
  public String findAll(Model model) {
    var endClientsForms = endClientService.getAllEndClients().stream()
        .map(endClientFormTransformer::transform).collect(Collectors.toList());
    
    model.addAttribute("endclientsForms", endClientsForms);
    
    return "endclients-find";
  }
  
  @PostMapping("/save")
  public ModelAndView save(@Valid @ModelAttribute(value = "endclientsForm") 
      EndClientForm endClientForm, BindingResult bindingResult, ModelMap model) {
    
    var clients = clientService.getAllClients().stream().map(clientFormTransformer::transform)
        .collect(Collectors.toList());
    
    if (bindingResult.hasErrors()) {
      model.addAttribute("clients", clients);
      return new ModelAndView("endclients-edit");
    }
    
    Integer endClientId = null;
    try {
      endClientId = endClientService.save(endClientTransformer.transform(endClientForm));
    } catch (EndClientException ece) {
      model.addAttribute("errorMessage", ece.getMessage());
      model.addAttribute("clients", clients);
      return new ModelAndView("endclients-edit");
    }
    
    model.addAttribute("id", endClientId);
    return new ModelAndView("redirect:/endclients/view", model);
  }
}
