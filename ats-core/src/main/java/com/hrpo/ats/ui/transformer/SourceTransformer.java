package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.Source;
import com.hrpo.ats.ui.form.SourceForm;

public interface SourceTransformer extends Transformer<SourceForm, Source> {

  Source transform(SourceForm form);
  
  default Source transformToDto(SourceForm form) {
    Source dto = new Source();
    
    dto.setId(form.getId());
    dto.setName(form.getName());
    dto.setCategory(form.getCategory());
    dto.setDescription(form.getDescription());
    dto.setIndex(form.getIndex());
    
    return dto;
  }
}
