package com.hrpo.ats.ui;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.hrpo.ats.service.EodReportService;
import com.hrpo.ats.service.JobOrderService;
import com.hrpo.ats.ui.form.EodReportSourceForm;
import com.hrpo.ats.ui.form.JobOrderCandidateForm;
import com.hrpo.ats.ui.transformer.EodReportSourceFormTransformer;
import com.hrpo.ats.ui.transformer.JobOrderCandidateFormTransformer;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;

@Controller
@RequestMapping("reports-generation")
public class ReportsGenerationController {
  
  private final Logger log = LoggerFactory.getLogger(ReportsGenerationController.class);
  
  @Autowired
  private JobOrderService jobOrderService;
  
  @Autowired
  private EodReportService eodReportService;
  
  @Autowired
  private JobOrderCandidateFormTransformer jobOrderCandidateFormTransformer;
  
  @Autowired
  private EodReportSourceFormTransformer eodReportFormTransformer;
  
  @GetMapping("")
  public String show(Model model) {
    
    return "reports-generation";
  }
  
  @RequestMapping(value = "", params = {"dateFrom", "dateTo", "type"}, method = RequestMethod.GET, 
      produces = "text/csv")
  public void generateCsv(
      @RequestParam(value = "type") String reportType,
      @RequestParam(value = "dateFrom") @DateTimeFormat(pattern = "MM/dd/yyyy") LocalDate fromDate,
      @RequestParam(value = "dateTo") @DateTimeFormat(pattern = "MM/dd/yyyy") LocalDate toDate,
      @RequestParam(value = "columns") String[] columns,
      HttpServletResponse response) {
    
    
    response.setContentType("text/csv");
    
    if (reportType.equalsIgnoreCase("workflow")) {
      response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"workflow.csv\"");
      
      var joCandidatesForm = jobOrderService.getJobOrderCandidatesByWorkflowDate(fromDate, toDate)
          .stream().map(jobOrderCandidateFormTransformer::transform).collect(Collectors.toList());
      
      writeWorkflow(response, joCandidatesForm, columns);
    } else if (reportType.equalsIgnoreCase("activity")) {
      response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"activity.csv\"");
      
      var eodReportForms = eodReportService.getSourceByReportDate(fromDate, toDate)
          .stream().map(eodReportFormTransformer::transform).collect(Collectors.toList());
      
      writeActivity(response, eodReportForms, columns);
    }
  }
  
  private void writeWorkflow(HttpServletResponse response, 
      List<JobOrderCandidateForm> joCandidates, String... columns) {
    try {
      var strategy = new ColumnPositionMappingStrategy<JobOrderCandidateForm>();
      strategy.setType(JobOrderCandidateForm.class);
      strategy.setColumnMapping(columns);
      
      PrintWriter writer = response.getWriter();
      String header = String.join(",", columns) + System.lineSeparator();
      writer.append(header);
      
      var beanToCsv = new StatefulBeanToCsvBuilder<JobOrderCandidateForm>(writer)
          .withApplyQuotesToAll(false)
          .withMappingStrategy(strategy)
          .withSeparator(',')
          .build();
      
      beanToCsv.write(joCandidates);
    } catch (CsvException | IOException ex) {
      log.info("Error in generating CSV: " + ex.getMessage());
    }
  }
  
  private void writeActivity(HttpServletResponse response, 
      List<EodReportSourceForm> activities, String... columns) {
    try {
      var strategy = new ColumnPositionMappingStrategy<EodReportSourceForm>();
      strategy.setType(EodReportSourceForm.class);
      strategy.setColumnMapping(columns);
      
      PrintWriter writer = response.getWriter();
      String header = String.join(",", columns) + System.lineSeparator();
      writer.append(header);
      
      var beanToCsv = new StatefulBeanToCsvBuilder<EodReportSourceForm>(writer)
          .withApplyQuotesToAll(false)
          .withMappingStrategy(strategy)
          .withSeparator(',')
          .build();
      
      beanToCsv.write(activities);
    } catch (CsvException | IOException ex) {
      log.info("Error in generating CSV: " + ex.getMessage());
    }
  }
  
}
