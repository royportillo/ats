package com.hrpo.ats.ui;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.hrpo.ats.dto.Role;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.UserException;
import com.hrpo.ats.service.RoleService;
import com.hrpo.ats.service.TeamService;
import com.hrpo.ats.service.UserService;
import com.hrpo.ats.ui.form.UsersForm;
import com.hrpo.ats.ui.transformer.UserFormTransformer;
import com.hrpo.ats.ui.transformer.UserTransformer;

@Controller
@RequestMapping("users")
public class UsersController {
  
  Logger log = LoggerFactory.getLogger(UsersController.class);
  
  @Autowired
  private TeamService teamService;
  
  @Autowired
  private RoleService roleService;
  
  @Autowired
  private UserTransformer userTransformer;
  
  @Autowired
  private UserFormTransformer userFormTransformer;
  
  @Autowired
  private UserService userService;
  
  @Autowired
  @Qualifier("usersFormValidator")
  private Validator usersFormValidator;

  @GetMapping("/new")
  public String show(Model model) {
    
    model.addAttribute("usersForm", new UsersForm());
    model.addAttribute("teams", teamService.getAllTeams());
    model.addAttribute("availableRoles", roleService.getAllRoles());
    
    return "users-edit";
  }
  
  @GetMapping("/find")
  public String findAll(Model model) {
    
    List<UsersForm> usersForms = userService.getAllUsersWithRole(false).stream()
        .map(userFormTransformer::transform).collect(Collectors.toList());
    
    model.addAttribute("usersForms", usersForms);
    
    return "users-find";
  }
  
  @RequestMapping(value = "/edit", params = {"id"}, method = RequestMethod.GET)
  public String edit(@RequestParam(value = "id") Integer id, ModelMap model) {
    
    User user = null;
    
    try {
      user = userService.getUserWithRoleById(id);
    } catch (UserException ue) {
      if (ue.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(ue.getMessage());
      }
    }
    
    List<Role> assignedRoles = user.getUserRoles().stream().map(
        userRole -> {
          return userRole.getRole();
        }).collect(Collectors.toList());
    
    List<Role> availableRoles = roleService.getAllRoles();
    availableRoles.removeAll(assignedRoles);
    
    model.addAttribute("teams", teamService.getAllTeams());
    model.addAttribute("availableRoles", availableRoles);
    model.addAttribute("usersForm", userFormTransformer.transform(user));
    
    return "users-edit";
  }
  
  @RequestMapping(value = "/view", params = {"id"}, method = RequestMethod.GET)
  public String view(Model model, @RequestParam(value = "id") String id,
      Principal principal) {
    
    User user = null;
    
    try {
      user = userService
          .getUserWithRoleById(Integer.valueOf(id));
    } catch (UserException ue) {
      if (ue.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(ue.getMessage());
      }
    }
    
    model.addAttribute("usersForm", userFormTransformer.transform(user));

    return "users-view";
  }
  
  @PostMapping("/save")
  public ModelAndView save(@Valid @ModelAttribute(value = "usersForm") UsersForm usersForm,
      BindingResult bindingResult, ModelMap model) {
    
    log.info(usersForm.getPassword());
    
    List<Role> availableRoles = roleService.getAllRoles();
    List<Role> assignedRoles = usersForm.getRoles().stream().map(
        userRole -> {
          return userRole.getRole();
        }).collect(Collectors.toList());
    availableRoles.removeAll(assignedRoles);
    List<Team> teams = teamService.getAllTeams();
    
    usersFormValidator.validate(usersForm, bindingResult);
    
    if (bindingResult.hasErrors()) {
      model.addAttribute("teams", teams);
      model.addAttribute("availableRoles", availableRoles);
      return new ModelAndView("users-edit");
    }
    
    User user = userTransformer.transform(usersForm);
    try {
      Integer userId = userService.saveUser(user);
      user.setId(userId);
    } catch (UserException ue) {
      model.addAttribute("teams", teams);
      model.addAttribute("availableRoles", availableRoles);
      model.addAttribute("errorMessage", ue.getMessage());
      return new ModelAndView("users-edit");
    }
    
    model.addAttribute("id", user.getId());
    return new ModelAndView("redirect:/users/view", model);
  }
}
