package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.User;
import com.hrpo.ats.ui.form.UsersForm;

public interface UserFormTransformer extends Transformer<User, UsersForm> {
  UsersForm transform(User userDto);
  
  default UsersForm transformToForm(User dto) {
    UsersForm form = new UsersForm();
    
    form.setId(dto.getId());
    form.setRoles(dto.getUserRoles());
    form.setFirstName(dto.getFirstName());
    form.setLastName(dto.getLastName());
    form.setEmail(dto.getEmail());
    form.setUsername(dto.getUsername());
    form.setEnabled(dto.isEnabled());
    form.setTeam(dto.getTeam());
    
    return form;
  }
}
