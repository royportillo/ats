package com.hrpo.ats.ui;

import java.security.Principal;
import java.text.DecimalFormat;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hrpo.ats.SessionKey;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.Source;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.EodReportException;
import com.hrpo.ats.service.DashboardService;
import com.hrpo.ats.service.EmailService;
import com.hrpo.ats.service.EodReportEmailTemplate;
import com.hrpo.ats.service.EodReportService;
import com.hrpo.ats.service.JobOrderService;
import com.hrpo.ats.service.SourcesService;
import com.hrpo.ats.service.UserService;
import com.hrpo.ats.ui.form.EodReportForm;
import com.hrpo.ats.ui.transformer.EodReportFormTransformer;
import com.hrpo.ats.ui.transformer.EodReportTransformer;
import com.hrpo.ats.ui.transformer.SourceFormTransformer;

@Controller
@RequestMapping("eod-report")
public class EodReportController {
  
  @Autowired
  EodReportService eodReportService;
  
  @Autowired
  EmailService emailService;
  
  @Autowired
  EodReportEmailTemplate template;
  
  @Autowired
  DashboardService dashboardService;
  
  @Autowired
  JobOrderService jobOrderService;
  
  @Autowired
  SourcesService sourcesService;
  
  @Autowired
  SourceFormTransformer sourceFormTransformer;
  
  @Autowired
  EodReportTransformer eodReportTransformer;
  
  @Autowired
  EodReportFormTransformer eodReportFormTransformer;
  
  @Autowired
  UserService userService;
  
  Logger log = LoggerFactory.getLogger(EodReportController.class);

  @GetMapping("")
  public String load(Model model, Principal principal) {
    
    List<EodReportForm> reportsForms = null;
    
    try {
      reportsForms = eodReportService
          .getByReportDate(LocalDate.now().minusDays(7), LocalDate.now(), principal.getName())
          .stream().map(eodReportFormTransformer::transform).collect(Collectors.toList());
    } catch (EodReportException ere) {
      if (ere.getCause() instanceof EmptyResultDataAccessException) {
        log.info(ere.getMessage());
      }
    }
    
    EodReportForm reportForTheDay = null;
    reportForTheDay = reportsForms.stream()
        .filter(report -> report.getReportDate().isEqual(LocalDate.now())).findAny().orElse(null);
    
    if (reportForTheDay == null) {
      reportForTheDay = new EodReportForm();
      
      List<Source> allSources = sourcesService.getAllSources();
      
      final var sources = allSources
          .stream().filter(sourceForm -> sourceForm.getCategory().equals("source"))
          .map(source -> {
            EodReportSource erSource = new EodReportSource();
            erSource.setSource(source);
            return erSource;
          })
          .collect(Collectors.toList());
      
      final var activities = allSources
          .stream().filter(sourceForm -> sourceForm.getCategory().equals("activity"))
          .map(source -> {
            EodReportSource erSource = new EodReportSource();
            erSource.setSource(source);
            return erSource;
          })
          .collect(Collectors.toList());
      
      final var leads = allSources
          .stream().filter(sourceForm -> sourceForm.getCategory().equals("leads"))
          .map(source -> {
            EodReportSource erSource = new EodReportSource();
            erSource.setSource(source);
            return erSource;
          })
          .collect(Collectors.toList());
      
      reportForTheDay.setSources(sources);
      reportForTheDay.setActivity(activities);
      reportForTheDay.setLeads(leads);
      reportForTheDay.setReportDate(LocalDate.now());
    }
    
    model.addAttribute("serverDate", LocalTime.now());
    model.addAttribute("reports", reportsForms);
    model.addAttribute("eodReportForm", reportForTheDay);
    
    return "eod-report";
  }
  
  @RequestMapping(value = "", params = { "id" }, method = RequestMethod.GET)
  public String findById(Model model, Principal principal, @RequestParam("id") Integer id) {
    
    if (id == null) {
      return "redirect:/eod-report";
    }
    
    List<EodReportForm> reportsForms = null;
    EodReportForm response = null;
    
    try {
      reportsForms = eodReportService
          .getByReportDate(LocalDate.now().minusDays(7), LocalDate.now(), principal.getName())
          .stream().map(eodReportFormTransformer::transform).collect(Collectors.toList());
      
      response = eodReportFormTransformer
          .transform(eodReportService.getByIdAndUsername(id, principal.getName()));
      
      EodReportForm reportForTheDay = null;
      reportForTheDay = reportsForms.stream()
          .filter(report -> report.getReportDate().isEqual(LocalDate.now())).findAny().orElse(null);
      
      if (reportForTheDay == null) {
        reportForTheDay = new EodReportForm();
        reportForTheDay.setReportDate(LocalDate.now());
        
        reportsForms.add(reportForTheDay);
      }
    } catch (EodReportException ere) {
      if (ere.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(ere.getMessage());
      }
    }
    
    model.addAttribute("serverDate", LocalTime.now());
    model.addAttribute("reports", reportsForms);
    model.addAttribute("eodReportForm", response);
    
    return "eod-report";
  }
  
  @RequestMapping(value = "/send", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
      produces = MediaType.TEXT_PLAIN_VALUE)
  public ResponseEntity<String> sendMail(Principal principal, 
      @ModelAttribute("eodReportForm") EodReportForm eodReportForm, HttpSession session) {
    
    Team userTeam = (Team) session.getAttribute(SessionKey.USER_TEAM.getName());
    EodReport report = eodReportTransformer.transform(eodReportForm);
    
    try {
      emailService.saveAndSendEmail(principal.getName(), userTeam, report);
    } catch (EodReportException ere) {
      if (ere.getCause() instanceof DuplicateKeyException) {
        log.error("Error: " + ere.getMessage());
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(ere.getMessage());
      } else {
        log.error("Error: " + ere.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ere.getMessage());
      }
    }
    
    return ResponseEntity.ok("End of day report sent successfully");
  }
  
  @RequestMapping(value = "/preview", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
      produces = MediaType.TEXT_PLAIN_VALUE)
  public @ResponseBody String preview(Principal principal, 
      @ModelAttribute("eodReportForm") EodReportForm eodReportForm) {
    
    final String username = principal.getName();
    final User user = (User) userService.loadUserByUsername(username);
    
    EodReport eodReport = eodReportTransformer.transform(eodReportForm);
    
    Integer totalViews = 0;
    Integer newLeads = 0;
    Integer otherLeads = 0;
    
    for (EodReportSource eodReportSource : eodReport.getEodReportSources()) {
      
      if (eodReportSource.getSource().getCategory().equalsIgnoreCase("source")) {
        totalViews += eodReportSource.getSourceViews();
        newLeads += eodReportSource.getSourceLeads();
      } else if (eodReportSource.getSource().getCategory().equalsIgnoreCase("leads")) {
        otherLeads += eodReportSource.getLeads();
      }
    }
    
    Integer totalLeads = otherLeads + newLeads;
    
    Map<String, Object> context = new HashMap<>();
    
    context.put("totalViews", totalViews.toString());
    context.put("newLeads", newLeads.toString());
    context.put("totalLeads", totalLeads.toString());
    
    var sourceFields = eodReport.getEodReportSources();
    var sources = sourceFields.stream()
        .filter(field -> field.getSource().getCategory().equals("source"))
        .collect(Collectors.toList());
    var activities = sourceFields.stream()
        .filter(field -> field.getSource().getCategory().equals("activity"))
        .collect(Collectors.toList());
    var leads = sourceFields.stream()
        .filter(field -> field.getSource().getCategory().equals("leads"))
        .collect(Collectors.toList());
    
    context.put("sources", sources);
    context.put("activities", activities);
    context.put("leads", leads);
    
    LocalDateTime createDate = LocalDateTime.now();
    Clock reportClock = Clock.systemDefaultZone();
    
    if (eodReport.getId() != null) { //i.e. is already persisted
      EodReport origReport = eodReportService.getByIdAndUsername(eodReport.getId(), username);
      
      createDate = origReport.getCreateDate();
      Instant instant = origReport.getReportDate()
          .atStartOfDay(ZoneId.systemDefault()).toInstant();
      reportClock = Clock.fixed(instant, ZoneId.systemDefault());
      
      var origFields = origReport.getEodReportSources();
      var origSources = origFields.stream()
          .filter(field -> field.getSource().getCategory().equals("source"))
          .collect(Collectors.toList());
      var origActivities = origFields.stream()
          .filter(field -> field.getSource().getCategory().equals("activity"))
          .collect(Collectors.toList());
      var origLeads = origFields.stream()
          .filter(field -> field.getSource().getCategory().equals("leads"))
          .collect(Collectors.toList());
      
      context.put("origSources", origSources);
      context.put("origActivities", origActivities);
      context.put("origLeads", origLeads);
      context.put("isUpdated", true);
    }
    
    context.put("createDate", createDate
        .format(DateTimeFormatter.ofPattern("MMMM d, yyyy hh:mm a")));
    
    context.put("decimalFormat", new DecimalFormat("$###,###.00"));
    context.put("name",  user.getFirstName() + " " + user.getLastName());
    context.put("email", user.getEmail());
    context.put("username", username);
    context.put("comment", eodReport.getComment());
    
    context.put("eventCount", 
        dashboardService.getWorkflowEventCountPortletData(username, reportClock, true));
    context.put("prescreened",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.PRESCREEN, username, "ftd", reportClock, true));
    context.put("submitted",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.SUBMIT, username, "ftd", reportClock, true));
    context.put("presented",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.PRESENTATION, username, "ftd", reportClock, true));
    context.put("interviewed",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.INTERVIEW, username, "ftd", reportClock, true));
    context.put("placed",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.PLACEMENT, username, "ftd", reportClock, true));
    context.put("jobOrdersToday", 
        jobOrderService.getJobOrdersByUserAndPeriod(username, "ftd", reportClock, true));
    
    return template.render(context);
  }
}
