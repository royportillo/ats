package com.hrpo.ats.ui;

import java.util.stream.Collectors;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.exception.TeamException;
import com.hrpo.ats.service.TeamService;
import com.hrpo.ats.ui.form.TeamForm;
import com.hrpo.ats.ui.transformer.TeamFormTransformer;
import com.hrpo.ats.ui.transformer.TeamTransformer;

@Controller
@RequestMapping("teams")
public class TeamController {

  Logger log = LoggerFactory.getLogger(UsersController.class);

  @Autowired
  private TeamService teamService;
  
  @Autowired
  private TeamFormTransformer teamFormTransformer;
  
  @Autowired
  private TeamTransformer teamTransformer;
  
  @GetMapping("/new")
  public String createNew(Model model) {
    
    model.addAttribute("teamsForm", new TeamForm());
    
    return "teams-edit";
  }
  
  @RequestMapping(value = "/edit", params = {"id"}, method = RequestMethod.GET)
  public String edit(@RequestParam(value = "id") Integer id, ModelMap model) {
    
    Team team = null;
    
    try {
      team = teamService.getById(id);
    } catch (TeamException te) {
      if (te.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(te.getMessage());
      }
    }
    
    model.addAttribute("teamsForm", teamFormTransformer.transform(team));
    
    return "teams-edit";
  }
  
  @GetMapping("/find")
  public String findAll(Model model) {
    
    var teamsForms = teamService.getAllTeamsWithUsers().stream()
        .map(teamFormTransformer::transform).collect(Collectors.toList());
    
    model.addAttribute("teamsForms", teamsForms);
    
    return "teams-find";
  }
  
  @RequestMapping(value = "/view", params = {"id"}, method = RequestMethod.GET)
  public String view(@RequestParam(value = "id") Integer id, ModelMap model) {
    
    Team team = null;
    
    try {
      team = teamService.getByIdWithUserAndClients(id, true);
    } catch (TeamException te) {
      if (te.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(te.getMessage());
      }
    }
    
    model.addAttribute("teamsForm", teamFormTransformer.transform(team));
    
    return "teams-view";
  }
  
  @PostMapping("/save")
  public ModelAndView save(@Valid @ModelAttribute(value = "teamsForm") TeamForm teamForm,
      BindingResult bindingResult, ModelMap model) {
    
    if (bindingResult.hasErrors()) {
      return new ModelAndView("teams-edit");
    }
    
    Integer teamId = teamService.saveTeam(teamTransformer.transform(teamForm));
    
    model.addAttribute("id", teamId);
    return new ModelAndView("redirect:/teams/view", model);
  }
}
