package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.ui.form.JobOrderCandidateForm;

public interface JobOrderCandidateTransformer 
    extends Transformer<JobOrderCandidateForm, JobOrderCandidate> {
  
  @Override
  JobOrderCandidate transform(JobOrderCandidateForm form);
  
  default JobOrderCandidate transformToDto(JobOrderCandidateForm form) {
    JobOrderCandidate dto = new JobOrderCandidate();
    
    dto.setId(form.getId());
    dto.setTransactionDate(form.getTransactionDate());
    dto.setPayRateFrom(form.getPayRateFrom());
    dto.setPayRateTo(form.getPayRateTo());
    dto.setPayPeriodRate(form.getPayPeriodRate());
    dto.setEventRemarks(form.getEventRemarks());
    //TODO add future fields as necessary
    
    return dto;
  }
  
}
