package com.hrpo.ats.ui.form;

import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.ui.form.validation.Range;

@Range.List({
    @Range(from = "billRateStart", to = "billRateEnd", 
      message = "Bill rate start value must be less than end value"),
    @Range(from = "payRateStart", to = "payRateEnd", 
      message = "Pay rate start value must be less than end value"),
    @Range(from = "payRateStart", to = "billRateStart", 
      message = "Pay rate start value must be less than bill rate start value"),
    @Range(from = "payRateEnd", to = "billRateStart", 
      message = "Pay rate end value must be less than bill rate start value")
    })
public class JobOrderForm {
  
  private Integer id;
  private List<JobOrderCandidateForm> jobOrderCandidates;
  
  @Pattern(regexp = "^[a-zA-Z0-9]*", message = "JO number can only be alpha numeric.")
  @NotNull(message = "JO Number must not be null.")
  @Size(min = 2, max = 20)
  private String number;
  
  @DateTimeFormat(pattern = "MM/dd/yyyy")
  private LocalDate dateCreated;
  
  @NotNull(message = "Title must not be null.")
  @Size(min = 2, max = 50)
  private String title;
  
  @Size(min = 2, max = 30)
  @NotNull(message = "Location must not be null.")
  private String location;
  
  @NotNull(message = "Zip code must not be null.")
  private String zipCode;
  
  private List<EndClient> endClientList;
  private EndClient endClient;
  private String division;
  
  @DateTimeFormat(pattern = "MM/dd/yyyy")
  @NotNull(message = "JO Date must not be null.")
  private LocalDate joDate;
  
  @DateTimeFormat(pattern = "MM/dd/yyyy")
  @NotNull(message = "Start date must not be null.")
  private LocalDate startDate;
  
  @DateTimeFormat(pattern = "MM/dd/yyyy")
  @NotNull(message = "End date must not be null.")
  private LocalDate endDate;
  
  @NotNull(message = "Must not be null.")
  private Double billRateStart;
  
  @NotNull(message = "Must not be null.")
  private Double billRateEnd;
  
  @NotNull(message = "Must not be null.")
  private String billPeriodRate;
  
  @NotNull(message = "Must not be null.")
  private Double payRateStart;
  
  @NotNull(message = "Must not be null.")
  private Double payRateEnd;
  
  @NotNull(message = "Must not be null.")
  private String payPeriodRate;
  
  @NotNull(message = "Must not be null.")
  @Min(value = 1, message = "Cannot be zero.")
  private String openings;
  
  @NotNull(message = "Must not be null.")
  @Min(value = 1, message = "Cannot be zero.")
  private String maxSubs;
  
  @NotNull(message = "Must not be null.")
  private String status;
  
  private MultipartFile file;
  private Boolean hasFile;
  

  public JobOrderForm() {
    super();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public List<JobOrderCandidateForm> getJobOrderCandidates() {
    return jobOrderCandidates;
  }

  public void setJobOrderCandidates(List<JobOrderCandidateForm> jobOrderCandidates) {
    this.jobOrderCandidates = jobOrderCandidates;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String jobOrderNumber) {
    this.number = jobOrderNumber;
  }
  
  public LocalDate getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDate dateCreated) {
    this.dateCreated = dateCreated;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public List<EndClient> getEndClientList() {
    return endClientList;
  }

  public void setEndClientList(List<EndClient> endClientList) {
    this.endClientList = endClientList;
  }

  public EndClient getEndClient() {
    return endClient;
  }

  public void setEndClient(EndClient endClient) {
    this.endClient = endClient;
  }

  public String getDivision() {
    return division;
  }

  public void setDivision(String division) {
    this.division = division;
  }
  
  public LocalDate getJoDate() {
    return joDate;
  }

  public void setJoDate(LocalDate joDate) {
    this.joDate = joDate;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public Double getBillRateStart() {
    return billRateStart;
  }

  public void setBillRateStart(Double billRateStart) {
    this.billRateStart = billRateStart;
  }

  public Double getBillRateEnd() {
    return billRateEnd;
  }

  public void setBillRateEnd(Double billRateEnd) {
    this.billRateEnd = billRateEnd;
  }

  public String getBillPeriodRate() {
    return billPeriodRate;
  }

  public void setBillPeriodRate(String billPeriodRate) {
    this.billPeriodRate = billPeriodRate;
  }

  public Double getPayRateStart() {
    return payRateStart;
  }

  public void setPayRateStart(Double payRateStart) {
    this.payRateStart = payRateStart;
  }

  public Double getPayRateEnd() {
    return payRateEnd;
  }

  public void setPayRateEnd(Double payRateEnd) {
    this.payRateEnd = payRateEnd;
  }

  public String getPayPeriodRate() {
    return payPeriodRate;
  }

  public void setPayPeriodRate(String payPeriodRate) {
    this.payPeriodRate = payPeriodRate;
  }

  public String getOpenings() {
    return openings;
  }

  public void setOpenings(String openings) {
    this.openings = openings;
  }

  public String getMaxSubs() {
    return maxSubs;
  }

  public void setMaxSubs(String maxSubs) {
    this.maxSubs = maxSubs;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public MultipartFile getFile() {
    return file;
  }

  public void setFile(MultipartFile file) {
    this.file = file;
  }

  public Boolean getHasFile() {
    return hasFile;
  }

  public void setHasFile(Boolean hasFile) {
    this.hasFile = hasFile;
  }
  
}