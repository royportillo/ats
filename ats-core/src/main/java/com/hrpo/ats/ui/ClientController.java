package com.hrpo.ats.ui;

import java.util.stream.Collectors;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.hrpo.ats.SessionKey;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.exception.ClientException;
import com.hrpo.ats.service.ClientService;
import com.hrpo.ats.service.TeamService;
import com.hrpo.ats.ui.form.ClientForm;
import com.hrpo.ats.ui.transformer.ClientFormTransformer;
import com.hrpo.ats.ui.transformer.ClientTransformer;
import com.hrpo.ats.ui.transformer.TeamFormTransformer;

@Controller
@RequestMapping("clients")
public class ClientController {
  
  Logger log = LoggerFactory.getLogger(ClientController.class);
  
  @Autowired
  private ClientService clientService;
  
  @Autowired
  private TeamService teamService;
  
  @Autowired
  private ClientFormTransformer clientFormTransformer;
  
  @Autowired
  private ClientTransformer clientTransformer;
  
  @Autowired
  private TeamFormTransformer teamFormTransformer;
  
  @GetMapping("/new")
  public String createNew(Model model) {
    
    model.addAttribute("clientsForm", new ClientForm());
    model.addAttribute("teams", 
        teamService.getAllTeams().stream()
        .map(teamFormTransformer::transform).collect(Collectors.toList()));
    
    return "clients-edit";
  }
  
  @RequestMapping(value = "/edit", params = {"id"}, method = RequestMethod.GET)
  public String edit(@RequestParam(value = "id") Integer id, ModelMap model) {
    
    Client client = null;
    
    try {
      client = clientService.getClientById(id, false);
    } catch (ClientException ce) {
      if (ce.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(ce.getMessage());
      }
    }
    
    model.addAttribute("clientsForm", clientFormTransformer.transform(client));
    model.addAttribute("teams", 
        teamService.getAllTeams().stream()
        .map(teamFormTransformer::transform).collect(Collectors.toList()));
    
    return "clients-edit";
  }
  
  @RequestMapping(value = "/view", params = {"id"}, method = RequestMethod.GET)
  public String view(@RequestParam(value = "id") Integer id, ModelMap model) {
    
    Client client = null;
    
    try {
      client = clientService.getClientById(id, true);
    } catch (ClientException ce) {
      if (ce.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(ce.getMessage());
      }
    }
    
    model.addAttribute("clientsForm", clientFormTransformer.transform(client));
    
    return "clients-view";
  }
  
  @GetMapping("/find")
  public String findAll(Model model) {
    
    var clientsForms = clientService.getAllClients().stream()
        .map(clientFormTransformer::transform).collect(Collectors.toList());
    
    model.addAttribute("clientsForms", clientsForms);
    
    return "clients-find";
  }
  
  @PostMapping("/save")
  public ModelAndView save(@Valid @ModelAttribute(value = "clientsForm") ClientForm clientForm,
      BindingResult bindingResult, ModelMap model) {
    
    var teams = teamService.getAllTeams().stream()
        .map(teamFormTransformer::transform).collect(Collectors.toList());
    
    if (bindingResult.hasErrors()) {
      model.addAttribute("teams", teams);
      return new ModelAndView("clients-edit");
    }
    
    Integer clientId = null;
    try {
      clientId = clientService.save(clientTransformer.transform(clientForm));
    } catch (ClientException ce) {
      if (ce.getCause() instanceof DuplicateKeyException) {
        model.addAttribute("errorMessage", ce.getMessage());
        model.addAttribute("teams", teams);
        return new ModelAndView("clients-edit");
      }
    }
    
    model.addAttribute("id", clientId);
    return new ModelAndView("redirect:/clients/view", model);
  }
  
  @PostMapping("")
  public ResponseEntity<String> setSelectedClient(
      @ModelAttribute Client client, HttpSession session) {
    log.info("Selected Client: " + client.getClientId());
    
    session.setAttribute(SessionKey.SELECTED_CLIENT.getName(), client);
    return new ResponseEntity<String>(
        "Set Selected Client OK: " + client.getClientId(), HttpStatus.OK);
  }
}
