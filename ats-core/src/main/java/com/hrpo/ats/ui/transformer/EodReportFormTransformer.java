package com.hrpo.ats.ui.transformer;

import java.util.stream.Collectors;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.ui.form.EodReportForm;

public interface EodReportFormTransformer extends Transformer<EodReport, EodReportForm> {

  @Override
  EodReportForm transform(EodReport dto);
  
  default EodReportForm transformToForm(EodReport dto) {
    
    EodReportForm form = new EodReportForm();
    
    form.setId(dto.getId());
    form.setReportDate(dto.getReportDate());
    form.setCreateDate(dto.getCreateDate());
    form.setUpdateDate(dto.getUpdateDate());
    form.setUserName(dto.getUser().getUsername());
    form.setFirstName(dto.getUser().getFirstName());
    form.setLastName(dto.getUser().getLastName());
    form.setComment(dto.getComment());
    
    var sources = dto.getEodReportSources().stream()
        .filter(eodReportSource -> eodReportSource
            .getSource().getCategory().equalsIgnoreCase("source"))
        .collect(Collectors.toList());
    
    var leads = dto.getEodReportSources().stream()
        .filter(eodReportSource -> eodReportSource
            .getSource().getCategory().equalsIgnoreCase("leads"))
        .collect(Collectors.toList());
    
    var activity = dto.getEodReportSources().stream()
        .filter(eodReportSource -> eodReportSource
            .getSource().getCategory().equalsIgnoreCase("activity"))
        .collect(Collectors.toList());
    
    form.setSources(sources);
    form.setLeads(leads);
    form.setActivity(activity);
    
    return form;
  }
  
}
