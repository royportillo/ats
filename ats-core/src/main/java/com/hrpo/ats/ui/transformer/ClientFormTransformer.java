package com.hrpo.ats.ui.transformer;

import java.util.ArrayList;
import java.util.List;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.ui.form.ClientForm;
import com.hrpo.ats.ui.form.EndClientForm;
import com.hrpo.ats.ui.form.TeamForm;

public interface ClientFormTransformer extends Transformer<Client, ClientForm> {

  @Override
  ClientForm transform(Client dto);
  
  default ClientForm transformToForm(Client dto) {
    ClientForm form = new ClientForm();
    
    form.setId(dto.getClientId());
    form.setName(dto.getName());
    form.setDescription(dto.getDescription());
    
    Team team = dto.getTeam();
    TeamForm teamForm = new TeamForm();
    teamForm.setId(team.getId());
    teamForm.setName(team.getName());
    teamForm.setDescription(team.getDescription());
    
    form.setTeam(teamForm);
    form.setIsDefault(dto.getIsDefault());
    
    var endClients = dto.getEndClients();
    List<EndClientForm> endClientForms = new ArrayList<>();
    if (endClients != null) {
      for (EndClient endClient : endClients) {
        EndClientForm endClientForm = new EndClientForm();
        
        endClientForm.setId(endClient.getId());
        endClientForm.setName(endClient.getName());
        endClientForm.setDescription(endClient.getDescription());
        
        endClientForms.add(endClientForm);
      }
    }
    form.setEndClients(endClientForms);
    
    return form;
  }
}
