package com.hrpo.ats.ui.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.ui.form.EodReportForm;

public interface EodReportTransformer extends Transformer<EodReportForm, EodReport> {
  
  EodReport transform(EodReportForm form);
  
  default EodReport transformToDto(EodReportForm form) {
    
    EodReport dto = new EodReport();
    
    dto.setId(form.getId());
    dto.setReportDate(form.getReportDate());
    dto.setCreateDate(form.getCreateDate());
    dto.setUpdateDate(form.getUpdateDate());
    dto.setComment(form.getComment());
    
    User user = new User();
    user.setFirstName(form.getFirstName());
    user.setLastName(form.getLastName());
    user.setUsername(form.getUserName());
    
    dto.setUser(user);
    
    List<EodReportSource> eodReportSources = new ArrayList<>();
    
    eodReportSources.addAll(form.getSources());
    eodReportSources.addAll(form.getLeads());
    eodReportSources.addAll(form.getActivity());
    
    eodReportSources = eodReportSources.stream().map(eodReportSource -> {
      
      eodReportSource.setSourceLeads(
          (eodReportSource.getSourceLeads() != null) ? eodReportSource.getSourceLeads() : 0);
      
      eodReportSource.setSourceViews(
          (eodReportSource.getSourceViews() != null) ? eodReportSource.getSourceViews() : 0);
      
      eodReportSource.setLeads(
          (eodReportSource.getLeads() != null) ? eodReportSource.getLeads() : 0);
      
      eodReportSource.setActivity(
          (eodReportSource.getActivity() != null) ? eodReportSource.getActivity() : 0);
      
      return eodReportSource;
    }).collect(Collectors.toList());
    
    dto.setEodReportSources(eodReportSources);
    
    return dto;
  }
  
}
