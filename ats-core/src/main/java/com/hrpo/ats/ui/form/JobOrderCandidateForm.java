package com.hrpo.ats.ui.form;

import java.time.LocalDate;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.ui.form.validation.Range;

@Range(from = "payRateFrom", to = "payRateTo", 
    message = "Pay rate start value must be less than end value")
public class JobOrderCandidateForm {
  
  private Integer id;
  private Boolean isSelected;

  private Double payRateFrom;
  private Double payRateTo;
  private String payPeriodRate;
  private String eventRemarks;
  
  private Integer jobOrderId;
  private String jobOrderNumber;
  private String jobOrderTitle;
  
  private Double joBillRateFrom;
  private Double joBillRateTo;
  private String joBillPeriodRate;
  private String joBillRate;
  
  private Double joPayRateFrom;
  private Double joPayRateTo;
  private String joPayPeriodRate;
  private String joPayRate;
  
  private String jobOrderLocation;
  private String endClientName;
  private String clientName;
  private String ownerName;
  
  private Integer candidateRowId;
  private String candidateId;
  private String source;
  private String firstName;
  private String lastName;
  private String fullName;
  private String phone;
  private String mobile;
  private String email;
  
  @JsonFormat(pattern = "M/d/yyyy")
  private LocalDate transactionDate;
  private String workflowStage;
  private String lastWorkflowStage;
  
  private Boolean prescreen;
  private Boolean submission;
  private Boolean clientSubmission;
  private Boolean interview;
  private Boolean placement;
  
  List<AbstractWorkflowEvent> events;
  
  public JobOrderCandidateForm() {
    super();
  }
  
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Boolean getIsSelected() {
    return isSelected;
  }
  
  public void setIsSelected(Boolean isSelected) {
    this.isSelected = isSelected;
  }

  public Integer getJobOrderId() {
    return jobOrderId;
  }

  public void setJobOrderId(Integer jobOrderId) {
    this.jobOrderId = jobOrderId;
  }
  
  public String getJobOrderNumber() {
    return jobOrderNumber;
  }

  public void setJobOrderNumber(String jobOrderNumber) {
    this.jobOrderNumber = jobOrderNumber;
  }

  public String getJobOrderTitle() {
    return jobOrderTitle;
  }

  public void setJobOrderTitle(String jobOrderTitle) {
    this.jobOrderTitle = jobOrderTitle;
  }
  
  public Double getJoBillRateFrom() {
    return joBillRateFrom;
  }

  public void setJoBillRateFrom(Double joBillRateFrom) {
    this.joBillRateFrom = joBillRateFrom;
  }

  public Double getJoBillRateTo() {
    return joBillRateTo;
  }

  public void setJoBillRateTo(Double joBillRateTo) {
    this.joBillRateTo = joBillRateTo;
  }

  public String getJoBillPeriodRate() {
    return joBillPeriodRate;
  }

  public void setJoBillPeriodRate(String joBillPeriodRate) {
    this.joBillPeriodRate = joBillPeriodRate;
  }
  
  public String getJoBillRate() {
    return joBillRate;
  }

  public void setJoBillRate(String joBillRate) {
    this.joBillRate = joBillRate;
  }

  public Double getJoPayRateFrom() {
    return joPayRateFrom;
  }

  public void setJoPayRateFrom(Double joPayRateFrom) {
    this.joPayRateFrom = joPayRateFrom;
  }

  public Double getJoPayRateTo() {
    return joPayRateTo;
  }

  public void setJoPayRateTo(Double joPayRateTo) {
    this.joPayRateTo = joPayRateTo;
  }

  public String getJoPayPeriodRate() {
    return joPayPeriodRate;
  }

  public void setJoPayPeriodRate(String joPayPeriodRate) {
    this.joPayPeriodRate = joPayPeriodRate;
  }
  
  public String getJoPayRate() {
    return joPayRate;
  }

  public void setJoPayRate(String joPayRate) {
    this.joPayRate = joPayRate;
  }

  public String getJobOrderLocation() {
    return jobOrderLocation;
  }

  public void setJobOrderLocation(String jobOrderLocation) {
    this.jobOrderLocation = jobOrderLocation;
  }

  public String getEndClientName() {
    return endClientName;
  }

  public void setEndClientName(String endClientName) {
    this.endClientName = endClientName;
  }
  
  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }
  
  public String getOwnerName() {
    return ownerName;
  }

  public void setOwnerName(String ownerName) {
    this.ownerName = ownerName;
  }

  public Integer getCandidateRowId() {
    return candidateRowId;
  }

  public void setCandidateRowId(Integer candidateRowId) {
    this.candidateRowId = candidateRowId;
  }

  public String getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(String candidateId) {
    this.candidateId = candidateId;
  }
  
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
  

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDate getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(LocalDate transactionDate) {
    this.transactionDate = transactionDate;
  }
  
  public String getWorkflowStage() {
    return workflowStage;
  }

  public void setWorkflowStage(String workflowStage) {
    this.workflowStage = workflowStage;
  }

  public String getLastWorkflowStage() {
    return lastWorkflowStage;
  }

  public void setLastWorkflowStage(String lastWorkflowStage) {
    this.lastWorkflowStage = lastWorkflowStage;
  }

  public Boolean getPrescreen() {
    return prescreen;
  }

  public void setPrescreen(Boolean prescreen) {
    this.prescreen = prescreen;
  }

  public Boolean getSubmission() {
    return submission;
  }

  public void setSubmission(Boolean submission) {
    this.submission = submission;
  }

  public Boolean getClientSubmission() {
    return clientSubmission;
  }

  public void setClientSubmission(Boolean clientSubmission) {
    this.clientSubmission = clientSubmission;
  }

  public Boolean getInterview() {
    return interview;
  }

  public void setInterview(Boolean interview) {
    this.interview = interview;
  }

  public Boolean getPlacement() {
    return placement;
  }

  public void setPlacement(Boolean placement) {
    this.placement = placement;
  }

  public Double getPayRateFrom() {
    return payRateFrom;
  }

  public void setPayRateFrom(Double payRateFrom) {
    this.payRateFrom = payRateFrom;
  }

  public Double getPayRateTo() {
    return payRateTo;
  }

  public void setPayRateTo(Double payRateTo) {
    this.payRateTo = payRateTo;
  }

  public String getPayPeriodRate() {
    return payPeriodRate;
  }

  public void setPayPeriodRate(String payPeriodRate) {
    this.payPeriodRate = payPeriodRate;
  }

  public String getEventRemarks() {
    return eventRemarks;
  }

  public void setEventRemarks(String eventRemarks) {
    this.eventRemarks = eventRemarks;
  }

  public List<AbstractWorkflowEvent> getEvents() {
    return events;
  }

  public void setEvents(List<AbstractWorkflowEvent> events) {
    this.events = events;
  }
  
}
