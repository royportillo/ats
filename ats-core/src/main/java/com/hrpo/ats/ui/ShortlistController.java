package com.hrpo.ats.ui;

import java.security.Principal;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.hrpo.ats.DaxtraDatabase;
import com.hrpo.ats.SessionKey;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.exception.ShortlistException;
import com.hrpo.ats.service.CandidateService;
import com.hrpo.ats.service.JobOrderService;
import com.hrpo.ats.ui.transformer.JobOrderFormTransformer;
import com.hrpo.daxtra.dto.Candidate;

@Controller
@RequestMapping("shortlist")
public class ShortlistController {

  Logger log = LoggerFactory.getLogger(ShortlistController.class);

  @Autowired
  private CandidateService candidateService;
  
  @Autowired
  private JobOrderService jobOrderService;
  
  @Autowired
  private JobOrderFormTransformer jobOrderFormTransformer;

  @GetMapping("")
  public String show(Model model, @RequestParam("joid") String joId ) {
    
    JobOrder jobOrder = null;
    
    try {
      jobOrder = jobOrderService.getJobOrderById(Integer.valueOf(joId));
    } catch (ShortlistException se) {
      if (se.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(se.getMessage());
      }
    }
    
    model.addAttribute("joborder", jobOrderFormTransformer.transform(jobOrder));
    return "shortlist";
  }

  @RequestMapping(value = "/search", method = RequestMethod.GET)
  public String load(Model model, @RequestParam("searchText") String searchText, 
      @RequestParam("joid") String joId, Principal principal, HttpSession session) {
    
    log.info("Search text: " + searchText);
    
    JobOrder jobOrder = null;
    
    try {
      jobOrder = jobOrderService.getJobOrderById(Integer.valueOf(joId));
    } catch (ShortlistException se) {
      if (se.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(se.getMessage());
      }
    }
    
    // Looks like Daxtra returns candidates unfiltered when an empty string 
    // is passed to its API service. So we need to return null candidates
    // if user accidentally passed a blank search text.
    
    List<Candidate> candidates = null;
    if (!searchText.isEmpty()) {
      String database = ((DaxtraDatabase) session.getAttribute(
          SessionKey.SELECTED_DAXTRA_DB.getName())).getDatabase();
      candidates = 
          candidateService.getCandidateDetailsByShortlistName(searchText, principal.getName(), 
              database);
    }
    
    model.addAttribute("joborder", jobOrderFormTransformer.transform(jobOrder));
    model.addAttribute("candidates", candidates);
    
    return "shortlist";
  }
  
  @RequestMapping(value = "/import", method = RequestMethod.POST)
  public ModelAndView importCandidates(
      @RequestParam(value = "candidateid") List<Integer> candidateIdList, 
      @RequestParam(value = "id") String joId, ModelMap model, Principal principal) {
    
    log.info("Received candidate id list: " + candidateIdList.toString());
    
    jobOrderService.importCandidatesToJobOrder(
        Integer.valueOf(joId), candidateIdList, principal.getName());
    model.addAttribute("id", Integer.valueOf(joId));
    return new ModelAndView("redirect:/joborder/view", model);
  }
}
