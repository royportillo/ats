package com.hrpo.ats.ui.transformer;

import java.io.IOException;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.ui.form.JobOrderForm;

@FunctionalInterface
public interface JobOrderTransformer extends Transformer<JobOrderForm, JobOrder> {

  JobOrder transform(JobOrderForm jobOrderForm);
  
  default JobOrder transformToDto(JobOrderForm form) {
    JobOrder jobOrder = new JobOrder();
    jobOrder.setId(form.getId());
    jobOrder.setNumber(form.getNumber());
    jobOrder.setJoDate(form.getJoDate());
    jobOrder.setTitle(form.getTitle());
    jobOrder.setLocation(form.getLocation());
    jobOrder.setZipCode(form.getZipCode());
    
    jobOrder.setEndClient(form.getEndClient());
    jobOrder.setDivision(form.getDivision());
    
    jobOrder.setStartDate(form.getStartDate());
    jobOrder.setEndDate(form.getEndDate());
    
    jobOrder.setBillRateStart(form.getBillRateStart());
    jobOrder.setBillRateEnd(form.getBillRateEnd());
    jobOrder.setBillPeriodRate(form.getBillPeriodRate());
    
    jobOrder.setPayRateStart(form.getPayRateStart());
    jobOrder.setPayRateEnd(form.getPayRateEnd());
    jobOrder.setPayPeriodRate(form.getPayPeriodRate());
    
    jobOrder.setOpenings(form.getOpenings());
    jobOrder.setMaxSubs(form.getMaxSubs());
    jobOrder.setStatus(form.getStatus());
    
    try {
      jobOrder.setFile(form.getFile().getBytes());
      jobOrder.setMimeType(form.getFile().getContentType());
      jobOrder.setFileName(form.getFile().getOriginalFilename());
    } catch (IOException ioe) {
      //no-op
    }
    
    
    return jobOrder;
  }
}
