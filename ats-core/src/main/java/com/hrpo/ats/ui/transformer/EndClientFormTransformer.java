package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.ui.form.ClientForm;
import com.hrpo.ats.ui.form.EndClientForm;

public interface EndClientFormTransformer extends Transformer<EndClient, EndClientForm> {

  public EndClientForm transform(EndClient dto);
  
  default EndClientForm transformToForm(EndClient dto) {
    EndClientForm form = new EndClientForm();
    
    form.setId(dto.getId());
    form.setName(dto.getName());
    form.setDescription(dto.getDescription());
    
    Client client = dto.getClient();
    if (client != null) {
      ClientForm clientForm = new ClientForm();
      
      clientForm.setId(client.getClientId());
      clientForm.setName(client.getName());
      clientForm.setDescription(client.getDescription());
      
      form.setClient(clientForm);
    }
    
    return form;
  }
}
