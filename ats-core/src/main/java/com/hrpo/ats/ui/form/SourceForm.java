package com.hrpo.ats.ui.form;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;

public class SourceForm {

  private Integer id;
  
  @NotBlank(message = "Name cannot be blank.")
  private String name;
  @NotBlank
  private String category;
  private String description;
  
  @Digits(integer = 6, fraction = 0, message = "Please enter a valid number without decimals.")
  private Integer index;
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public Integer getIndex() {
    return index;
  }
  
  public void setIndex(Integer index) {
    this.index = index;
  }
}
