package com.hrpo.ats.ui.transformer;

import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.ui.form.ClientForm;

public interface ClientTransformer extends Transformer<ClientForm, Client> {
  
  Client transform(ClientForm type);
  
  default Client transformToDto(ClientForm form) {
    Client client = new Client();
    
    client.setClientId(form.getId());
    client.setName(form.getName());
    client.setDescription(form.getDescription());
    client.setIsDefault(form.getIsDefault() != null ? form.getIsDefault() : Boolean.FALSE);
    
    Team team = new Team();
    team.setId(form.getTeam().getId());
    team.setName(form.getTeam().getName());
    team.setDescription(form.getTeam().getDescription());
    client.setTeam(team);
    
    return client;
  }

}
