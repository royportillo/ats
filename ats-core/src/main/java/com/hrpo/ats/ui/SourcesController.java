package com.hrpo.ats.ui;

import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.hrpo.ats.exception.SourcesException;
import com.hrpo.ats.service.SourcesService;
import com.hrpo.ats.ui.form.SourceForm;
import com.hrpo.ats.ui.transformer.SourceFormTransformer;
import com.hrpo.ats.ui.transformer.SourceTransformer;

@Controller
@RequestMapping("sources")
public class SourcesController {
  
  @Autowired
  private SourcesService sourcesService;
  
  @Autowired
  private SourceFormTransformer sourceFormTransformer;
  
  @Autowired
  private SourceTransformer sourceTranformer;

  @GetMapping("new")
  public String show(Model model) {
    model.addAttribute("sourceForm", new SourceForm());
    
    return "sources-edit";
  }
  
  @GetMapping(value = "edit", params = { "id" })
  public String edit(@RequestParam("id") Integer id, Model model) {
    try {
      model.addAttribute("sourceForm", 
          sourceFormTransformer.transform(sourcesService.getSourceById(id)));
    } catch (SourcesException se) {
      if (se.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(se.getMessage());
      }
    }
    
    return "sources-edit";
  }
  
  @GetMapping(value = "view", params = { "id" })
  public String view(@RequestParam("id") Integer id, Model model) {
    try {
      model.addAttribute("sourceForm", 
          sourceFormTransformer.transform(sourcesService.getSourceById(id)));
    } catch (SourcesException se) {
      if (se.getCause() instanceof EmptyResultDataAccessException) {
        throw new ResourceNotFoundException(se.getMessage());
      }
    }
    
    return "sources-view";
  }
  
  @GetMapping("find")
  public String find(Model model) {
    var sourceForms = sourcesService.getAllSources().stream()
        .map(sourceFormTransformer::transform).collect(Collectors.toList());
    
    model.addAttribute("sourceForms", sourceForms);
    return "sources-find";
  }
  
  @PostMapping("save")
  public ModelAndView save(@Valid @ModelAttribute("sourceForm") SourceForm sourceForm,
      BindingResult bindingResult, ModelMap model) {
    
    if (bindingResult.hasErrors()) {
      return new ModelAndView("sources-edit");
    }
    
    Integer id = null; 
    
    try {
      id = sourcesService.save(sourceTranformer.transform(sourceForm));
    } catch (SourcesException se) {
      model.addAttribute("errorMessage", se.getMessage());
      return new ModelAndView("sources-edit");
    }
    
    model.addAttribute("id", id);
    return new ModelAndView("redirect:/sources/view", model);
  }
}
