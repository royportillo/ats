package com.hrpo.ats.dto;

import java.io.Serializable;

public class UserJobOrder implements Serializable {

  private static final long serialVersionUID = 8246384712316460095L;
  
  private Integer id;
  private JobOrder jobOrder;
  private User user;
  private Boolean isPrimary;
  
  public UserJobOrder() {
    super();
  }
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public JobOrder getJobOrder() {
    return jobOrder;
  }
  
  public void setJobOrder(JobOrder jobOrder) {
    this.jobOrder = jobOrder;
  }
  
  public User getUser() {
    return user;
  }
  
  public void setUser(User user) {
    this.user = user;
  }
  
  public Boolean getIsPrimary() {
    return isPrimary;
  }
  
  public void setIsPrimary(Boolean isPrimary) {
    this.isPrimary = isPrimary;
  }
  
  
}
