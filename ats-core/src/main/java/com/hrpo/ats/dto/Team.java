package com.hrpo.ats.dto;

import java.io.Serializable;
import java.util.List;

public class Team implements Serializable {

  private static final long serialVersionUID = -5093349763954053583L;
  
  private Integer id;
  private String name;
  private String email;
  private String description;
  private List<User> users;
  private List<Client> clients;
  
  public Team() {
    super();
  }
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public List<Client> getClients() {
    return clients;
  }

  public void setClients(List<Client> clients) {
    this.clients = clients;
  }
  
}
