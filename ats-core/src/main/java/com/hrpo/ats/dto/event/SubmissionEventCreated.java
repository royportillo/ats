package com.hrpo.ats.dto.event;

import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.service.PredecessorVisitor;

public class SubmissionEventCreated extends AbstractEventCreated {
  
  private static final long serialVersionUID = -6748935051539301725L;

  public SubmissionEventCreated(Object source, AbstractWorkflowEvent submissionEvent, 
      JobOrderCandidate jobOrderCandidate) {
    
    super(source, submissionEvent, jobOrderCandidate);
  }

  @Override
  public void accept(PredecessorVisitor visitor) {
    visitor.visit(this);
  }
}