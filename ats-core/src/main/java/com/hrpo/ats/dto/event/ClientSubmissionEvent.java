package com.hrpo.ats.dto.event;

import java.io.Serializable;

public class ClientSubmissionEvent extends AbstractWorkflowEvent implements Serializable {

  private static final long serialVersionUID = 2374357811878967625L;
}