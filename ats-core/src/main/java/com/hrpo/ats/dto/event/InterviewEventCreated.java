package com.hrpo.ats.dto.event;

import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.service.PredecessorVisitor;

public class InterviewEventCreated extends AbstractEventCreated {
  
  private static final long serialVersionUID = 1326635294125941957L;

  public InterviewEventCreated(Object source, AbstractWorkflowEvent submissionEvent, 
      JobOrderCandidate jobOrderCandidate) {
    
    super(source, submissionEvent, jobOrderCandidate);
  }
  
  @Override
  public void accept(PredecessorVisitor visitor) {
    visitor.visit(this);
  }
}