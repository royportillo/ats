package com.hrpo.ats.dto;

import java.time.LocalDate;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.daxtra.dto.Candidate;

public class JobOrderCandidate {

  private Integer id;
  private JobOrder jobOrder;
  private Candidate candidate;
  private WorkflowStage workflowStage;
  private WorkflowStage lastWorkflowStage;
  private WorkflowState workflowState;
  private LocalDate transactionDate;
  private User owner;
  private Double payRateFrom;
  private Double payRateTo;
  private String payPeriodRate;
  private String eventRemarks;
  
  public JobOrderCandidate() {
    super();
  }
  
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
  
  public JobOrder getJobOrder() {
    return jobOrder;
  }

  public void setJobOrder(JobOrder jobOrder) {
    this.jobOrder = jobOrder;
  }

  public Candidate getCandidate() {
    return candidate;
  }

  public void setCandidate(Candidate candidate) {
    this.candidate = candidate;
  }
  
  public WorkflowStage getWorkflowStage() {
    return workflowStage;
  }

  public void setWorkflowStage(WorkflowStage workflowStage) {
    this.workflowStage = workflowStage;
  }

  public WorkflowStage getLastWorkflowStage() {
    return lastWorkflowStage;
  }

  public void setLastWorkflowStage(WorkflowStage lastWorkflowStage) {
    this.lastWorkflowStage = lastWorkflowStage;
  }

  public WorkflowState getWorkflowState() {
    return workflowState;
  }

  public void setWorkflowState(WorkflowState workflowState) {
    this.workflowState = workflowState;
  }
  
  public LocalDate getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(LocalDate transactionDate) {
    this.transactionDate = transactionDate;
  }
  
  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }
  
  public Double getPayRateFrom() {
    return payRateFrom;
  }

  public void setPayRateFrom(Double payRateFrom) {
    this.payRateFrom = payRateFrom;
  }

  public Double getPayRateTo() {
    return payRateTo;
  }

  public void setPayRateTo(Double payRateTo) {
    this.payRateTo = payRateTo;
  }

  public String getPayPeriodRate() {
    return payPeriodRate;
  }

  public void setPayPeriodRate(String payPeriodRate) {
    this.payPeriodRate = payPeriodRate;
  }
  
  public String getEventRemarks() {
    return eventRemarks;
  }

  public void setEventRemarks(String eventRemarks) {
    this.eventRemarks = eventRemarks;
  }

  public class WorkflowState {
    
    private Boolean prescreenState;
    private Boolean submissionState;
    private Boolean clientSubmissionState;
    private Boolean interviewState;
    private Boolean placementState;
    
    public WorkflowState(Boolean prescreenState, Boolean submissionState,
        Boolean clientSubmissionState, Boolean interviewState, Boolean placementState) {
      super();
      this.prescreenState = prescreenState;
      this.submissionState = submissionState;
      this.clientSubmissionState = clientSubmissionState;
      this.interviewState = interviewState;
      this.placementState = placementState;
    }

    public Boolean getPrescreenState() {
      return prescreenState;
    }

    public Boolean getSubmissionState() {
      return submissionState;
    }

    public Boolean getClientSubmissionState() {
      return clientSubmissionState;
    }

    public Boolean getInterviewState() {
      return interviewState;
    }

    public Boolean getPlacementState() {
      return placementState;
    }
    
    public Candidate getDetails() {
      return JobOrderCandidate.this.getCandidate();
    }
  }
}
