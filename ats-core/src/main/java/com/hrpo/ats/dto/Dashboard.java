package com.hrpo.ats.dto;

import java.io.Serializable;

/**
 * This class will house all present and future dashboard data.
 */
public class Dashboard implements Serializable {
  
  private static final long serialVersionUID = 2648521837948835371L;
  
  /**
   * The workflow event count widget.
   */
  public class WorkflowEventCountPortlet implements Serializable  {
    
    private static final long serialVersionUID = 7867084109292718020L;
    
    private Integer prescreenForTheDayCount;
    private Integer prescreenForTheWeekCount;
    private Integer prescreenForTheMonthCount;
    private Integer prescreenLastMonthCount;
    private Integer prescreenLastLastMonthCount;
    
    private Integer submissionForTheDayCount;
    private Integer submissionForTheWeekCount;
    private Integer submissionForTheMonthCount;
    private Integer submissionLastMonthCount;
    private Integer submissionLastLastMonthCount;
    
    private Integer clientSubmissionForTheDayCount;
    private Integer clientSubmissionForTheWeekCount;
    private Integer clientSubmissionForTheMonthCount;
    private Integer clientSubmissionLastMonthCount;
    private Integer clientSubmissionLastLastMonthCount;
    
    private Integer interviewForTheDayCount;
    private Integer interviewForTheWeekCount;
    private Integer interviewForTheMonthCount;
    private Integer interviewLastMonthCount;
    private Integer interviewLastLastMonthCount;
    
    private Integer placementForTheDayCount;
    private Integer placementForTheWeekCount;
    private Integer placementForTheMonthCount;
    private Integer placementLastMonthCount;
    private Integer placementLastLastMonthCount;
    
    private Integer jobOrderForTheDayCount;
    private Integer jobOrderForTheWeekCount;
    private Integer jobOrderForTheMonthCount;
    private Integer jobOrderLastMonthCount;
    private Integer jobOrderLastLastMonthCount;
    
    
    public WorkflowEventCountPortlet(Integer prescreenForTheDayCount,
        Integer submissionForTheDayCount, Integer clientSubmissionForTheDayCount,
        Integer interviewForTheDayCount, Integer placementForTheDayCount) {
      super();
      this.prescreenForTheDayCount = prescreenForTheDayCount;
      this.submissionForTheDayCount = submissionForTheDayCount;
      this.clientSubmissionForTheDayCount = clientSubmissionForTheDayCount;
      this.interviewForTheDayCount = interviewForTheDayCount;
      this.placementForTheDayCount = placementForTheDayCount;
    }
    
    public WorkflowEventCountPortlet(Integer prescreenForTheDayCount,
        Integer prescreenForTheWeekCount, Integer prescreenForTheMonthCount,
        Integer prescreenLastMonthCount, Integer prescreenLastLastMonthCount,
        Integer submissionForTheDayCount, Integer submissionForTheWeekCount,
        Integer submissionForTheMonthCount, Integer submissionLastMonthCount,
        Integer submissionLastLastMonthCount, Integer clientSubmissionForTheDayCount,
        Integer clientSubmissionForTheWeekCount, Integer clientSubmissionForTheMonthCount,
        Integer clientSubmissionLastMonthCount, Integer clientSubmissionLastLastMonthCount,
        Integer interviewForTheDayCount, Integer interviewForTheWeekCount,
        Integer interviewForTheMonthCount, Integer interviewLastMonthCount,
        Integer interviewLastLastMonthCount, Integer placementForTheDayCount,
        Integer placementForTheWeekCount, Integer placementForTheMonthCount,
        Integer placementLastMonthCount, Integer placementLastLastMonthCount,
        Integer jobOrderForTheDayCount, Integer jobOrderForTheWeekCount,
        Integer jobOrderForTheMonthCount, Integer jobOrderLastMonthCount,
        Integer jobOrderLastLastMonthCount) {
      super();
      
      this.prescreenForTheDayCount = prescreenForTheDayCount;
      this.prescreenForTheWeekCount = prescreenForTheWeekCount;
      this.prescreenForTheMonthCount = prescreenForTheMonthCount;
      this.prescreenLastMonthCount = prescreenLastMonthCount;
      this.prescreenLastLastMonthCount = prescreenLastLastMonthCount;
      
      this.submissionForTheDayCount = submissionForTheDayCount;
      this.submissionForTheWeekCount = submissionForTheWeekCount;
      this.submissionForTheMonthCount = submissionForTheMonthCount;
      this.submissionLastMonthCount = submissionLastMonthCount;
      this.submissionLastLastMonthCount = submissionLastLastMonthCount;
      
      this.clientSubmissionForTheDayCount = clientSubmissionForTheDayCount;
      this.clientSubmissionForTheWeekCount = clientSubmissionForTheWeekCount;
      this.clientSubmissionForTheMonthCount = clientSubmissionForTheMonthCount;
      this.clientSubmissionLastMonthCount = clientSubmissionLastMonthCount;
      this.clientSubmissionLastLastMonthCount = clientSubmissionLastLastMonthCount;
      
      this.interviewForTheDayCount = interviewForTheDayCount;
      this.interviewForTheWeekCount = interviewForTheWeekCount;
      this.interviewForTheMonthCount = interviewForTheMonthCount;
      this.interviewLastMonthCount = interviewLastMonthCount;
      this.interviewLastLastMonthCount = interviewLastLastMonthCount;
      
      this.placementForTheDayCount = placementForTheDayCount;
      this.placementForTheWeekCount = placementForTheWeekCount;
      this.placementForTheMonthCount = placementForTheMonthCount;
      this.placementLastMonthCount = placementLastMonthCount;
      this.placementLastLastMonthCount = placementLastLastMonthCount;
      
      this.jobOrderForTheDayCount = jobOrderForTheDayCount;
      this.jobOrderForTheWeekCount = jobOrderForTheWeekCount;
      this.jobOrderForTheMonthCount = jobOrderForTheMonthCount;
      this.jobOrderLastMonthCount = jobOrderLastMonthCount;
      this.jobOrderLastLastMonthCount = jobOrderLastLastMonthCount;
    }

    public Integer getPrescreenForTheDayCount() {
      return prescreenForTheDayCount;
    }
    
    public Integer getPrescreenForTheWeekCount() {
      return prescreenForTheWeekCount;
    }

    public Integer getPrescreenForTheMonthCount() {
      return prescreenForTheMonthCount;
    }

    public Integer getPrescreenLastMonthCount() {
      return prescreenLastMonthCount;
    }

    public Integer getPrescreenLastLastMonthCount() {
      return prescreenLastLastMonthCount;
    }

    public Integer getSubmissionForTheDayCount() {
      return submissionForTheDayCount;
    }
    
    public Integer getSubmissionForTheWeekCount() {
      return submissionForTheWeekCount;
    }

    public Integer getSubmissionForTheMonthCount() {
      return submissionForTheMonthCount;
    }

    public Integer getSubmissionLastMonthCount() {
      return submissionLastMonthCount;
    }

    public Integer getSubmissionLastLastMonthCount() {
      return submissionLastLastMonthCount;
    }

    public Integer getClientSubmissionForTheDayCount() {
      return clientSubmissionForTheDayCount;
    }
    
    public Integer getClientSubmissionForTheWeekCount() {
      return clientSubmissionForTheWeekCount;
    }

    public Integer getClientSubmissionForTheMonthCount() {
      return clientSubmissionForTheMonthCount;
    }

    public Integer getClientSubmissionLastMonthCount() {
      return clientSubmissionLastMonthCount;
    }

    public Integer getClientSubmissionLastLastMonthCount() {
      return clientSubmissionLastLastMonthCount;
    }

    public Integer getInterviewForTheDayCount() {
      return interviewForTheDayCount;
    }
    
    public Integer getInterviewForTheWeekCount() {
      return interviewForTheWeekCount;
    }

    public Integer getInterviewForTheMonthCount() {
      return interviewForTheMonthCount;
    }

    public Integer getInterviewLastMonthCount() {
      return interviewLastMonthCount;
    }

    public Integer getInterviewLastLastMonthCount() {
      return interviewLastLastMonthCount;
    }

    public Integer getPlacementForTheDayCount() {
      return placementForTheDayCount;
    }

    public Integer getPlacementForTheWeekCount() {
      return placementForTheWeekCount;
    }

    public Integer getPlacementForTheMonthCount() {
      return placementForTheMonthCount;
    }

    public Integer getPlacementLastMonthCount() {
      return placementLastMonthCount;
    }

    public Integer getPlacementLastLastMonthCount() {
      return placementLastLastMonthCount;
    }

    public Integer getJobOrderForTheDayCount() {
      return jobOrderForTheDayCount;
    }

    public Integer getJobOrderForTheWeekCount() {
      return jobOrderForTheWeekCount;
    }

    public Integer getJobOrderForTheMonthCount() {
      return jobOrderForTheMonthCount;
    }

    public Integer getJobOrderLastMonthCount() {
      return jobOrderLastMonthCount;
    }

    public Integer getJobOrderLastLastMonthCount() {
      return jobOrderLastLastMonthCount;
    }
  }
}