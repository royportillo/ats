package com.hrpo.ats.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class EodReport implements Serializable {
  
  private static final long serialVersionUID = -4137003709151141846L;
  
  private Integer id;
  private User user;
  
  private LocalDate reportDate;
  private LocalDateTime createDate;
  private LocalDateTime updateDate;
  
  private String comment;
  
  private String reportHtml;
  
  private List<EodReportSource> eodReportSources;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public LocalDate getReportDate() {
    return reportDate;
  }

  public void setReportDate(LocalDate reportDate) {
    this.reportDate = reportDate;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  public LocalDateTime getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(LocalDateTime updateDate) {
    this.updateDate = updateDate;
  }
  

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getReportHtml() {
    return reportHtml;
  }

  public void setReportHtml(String reportHtml) {
    this.reportHtml = reportHtml;
  }

  public List<EodReportSource> getEodReportSources() {
    return eodReportSources;
  }

  public void setEodReportSources(List<EodReportSource> eodReportSources) {
    this.eodReportSources = eodReportSources;
  }
  
 
}
