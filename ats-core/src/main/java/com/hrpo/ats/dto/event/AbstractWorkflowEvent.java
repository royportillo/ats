package com.hrpo.ats.dto.event;

import java.io.Serializable;
import java.time.LocalDate;

public abstract class AbstractWorkflowEvent implements Serializable {
  
  private static final long serialVersionUID = 5517499308243361538L;
  
  private Integer id;
  private LocalDate transactionDate;
  private String remarks;

  public AbstractWorkflowEvent() {
    super();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public LocalDate getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(LocalDate transactionDate) {
    this.transactionDate = transactionDate;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

}