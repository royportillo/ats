package com.hrpo.ats.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class JobOrder implements Serializable {
  
  private static final long serialVersionUID = -3807630018717317150L;
  
  private Integer id;
  private List<JobOrderCandidate> jobOrderCandidates;
  
  private String number;
  private LocalDate dateCreated;
  private LocalDate joDate;
  private String title;
  private String location;
  private String zipCode;
  
  private EndClient endClient;
  private String division;
  
  private LocalDate startDate;
  private LocalDate endDate;
  
  private Double billRateStart;
  private Double billRateEnd;
  private String billPeriodRate;
  
  private Double payRateStart;
  private Double payRateEnd;
  private String payPeriodRate;
  
  private String openings;
  private String maxSubs;
  private String status;
  
  private byte[] file;
  private String mimeType;
  private String fileName;
  private Boolean hasFile;

  private UserJobOrder primaryOwner;
  
  public JobOrder() {
    super();
  }

  public Optional<Integer> getId() {
    return Optional.ofNullable(id);
  }

  public void setId(Integer id) {
    this.id = id;
  }
  
  public List<JobOrderCandidate> getJobOrderCandidates() {
    return jobOrderCandidates;
  }

  public void setJobOrderCandidates(List<JobOrderCandidate> jobOrderCandidates) {
    this.jobOrderCandidates = jobOrderCandidates;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String jobOrderNumber) {
    this.number = jobOrderNumber;
  }

  public LocalDate getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDate dateCreated) {
    this.dateCreated = dateCreated;
  }

  public LocalDate getJoDate() {
    return joDate;
  }

  public void setJoDate(LocalDate joDate) {
    this.joDate = joDate;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }
  
  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }
  
  public EndClient getEndClient() {
    return endClient;
  }

  public void setEndClient(EndClient endClient) {
    this.endClient = endClient;
  }

  public String getDivision() {
    return division;
  }

  public void setDivision(String division) {
    this.division = division;
  }
  
  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public Double getBillRateStart() {
    return billRateStart;
  }

  public void setBillRateStart(Double billRateStart) {
    this.billRateStart = billRateStart;
  }

  public Double getBillRateEnd() {
    return billRateEnd;
  }

  public void setBillRateEnd(Double billRateEnd) {
    this.billRateEnd = billRateEnd;
  }

  public String getBillPeriodRate() {
    return billPeriodRate;
  }

  public void setBillPeriodRate(String billPeriodRate) {
    this.billPeriodRate = billPeriodRate;
  }

  public Double getPayRateStart() {
    return payRateStart;
  }

  public void setPayRateStart(Double payRateStart) {
    this.payRateStart = payRateStart;
  }

  public Double getPayRateEnd() {
    return payRateEnd;
  }

  public void setPayRateEnd(Double payRateEnd) {
    this.payRateEnd = payRateEnd;
  }

  public String getPayPeriodRate() {
    return payPeriodRate;
  }

  public void setPayPeriodRate(String payPeriodRate) {
    this.payPeriodRate = payPeriodRate;
  }
  
  public String getOpenings() {
    return openings;
  }

  public void setOpenings(String openings) {
    this.openings = openings;
  }

  public String getMaxSubs() {
    return maxSubs;
  }

  public void setMaxSubs(String maxSubs) {
    this.maxSubs = maxSubs;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
  
  public byte[] getFile() {
    return file;
  }

  public void setFile(byte[] file) {
    this.file = file;
  }
  
  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }
  
  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Boolean getHasFile() {
    return hasFile;
  }

  public void setHasFile(Boolean hasFile) {
    this.hasFile = hasFile;
  }
  
  public UserJobOrder getPrimaryOwner() {
    return primaryOwner;
  }

  public void setPrimaryOwner(UserJobOrder primaryOwner) {
    this.primaryOwner = primaryOwner;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((number == null) ? 0 : number.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    JobOrder other = (JobOrder) obj;
    if (number == null) {
      if (other.number != null) {
        return false;
      }
    } else if (!number.equals(other.number)) {
      return false;
    }
    return true;
  }
  
}