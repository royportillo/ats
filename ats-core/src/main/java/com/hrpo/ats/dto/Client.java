package com.hrpo.ats.dto;

import java.io.Serializable;
import java.util.List;

public class Client implements Serializable {

  private static final long serialVersionUID = -742768625874099215L;

  private Integer clientId;
  private String name;
  private String description;
  private Team team;
  private Boolean isDefault;
  private List<EndClient> endClients;
  
  public Client() {
    super();
  }
  
  public Integer getClientId() {
    return clientId;
  }
  
  public void setClientId(Integer clientId) {
    this.clientId = clientId;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public List<EndClient> getEndClients() {
    return endClients;
  }

  public void setEndClients(List<EndClient> endClients) {
    this.endClients = endClients;
  }
}
