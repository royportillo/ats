package com.hrpo.ats.dto;

import java.io.Serializable;

public class LoggedUser implements Serializable {
  
  private static final long serialVersionUID = 4650452047503386606L;
  
  private String username;

  public LoggedUser(String username) {
    super();
    
    this.username = username;
  }

  protected String getUsername() {
    return username;
  }

  protected void setUsername(String username) {
    this.username = username;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((username == null) ? 0 : username.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    LoggedUser other = (LoggedUser) obj;
    if (username == null) {
      if (other.username != null) {
        return false;
      }
    } else if (!username.equals(other.username)) {
      return false;
    }
    return true;
  }

}