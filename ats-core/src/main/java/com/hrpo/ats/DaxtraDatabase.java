package com.hrpo.ats;

public enum DaxtraDatabase {
  OLD_DAXTRA("Daxtra", "ws_24hrpo"),
  NEW_DAXTRA("Daxtra (New)", "ws_24hrpo_dev");

  private String name;
  private String database;

  DaxtraDatabase(String name, String database) {
    this.name = name;
    this.database = database;
  }

  public String getName() {
    return name;
  }
  
  public String getDatabase() {
    return database;
  }
}
