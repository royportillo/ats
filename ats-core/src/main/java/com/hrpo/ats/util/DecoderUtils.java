package com.hrpo.ats.util;

import java.nio.charset.StandardCharsets;
import java.util.function.UnaryOperator;

/**
 * This class contains convenience methods for decoding data.
 * The caller is free to choose its own decoder implementation by passing a function.
 */
public final class DecoderUtils {
  
  /**
   * No reason to instantiate this class.
   */
  private DecoderUtils() {}

  /**
   * Decodes an encoded String and returns decoded byte[] based on the passed decoder function.
   * 
   * @param the encoded data in String format.
   * @param the function decoder implementation.
   * 
   * @return the decoded byte[].
   */
  public static byte[] decode(String encodedData, UnaryOperator<byte[]> function) {
    
    return function.apply(encodedData.getBytes(StandardCharsets.UTF_8));
  }
}
