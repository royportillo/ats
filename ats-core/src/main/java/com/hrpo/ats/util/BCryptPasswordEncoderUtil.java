package com.hrpo.ats.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder.BCryptVersion;

/**
 * Utility class for generating encoded password for base data.
 */
public final class BCryptPasswordEncoderUtil {
  
  private BCryptPasswordEncoderUtil() {}

  public static String encode(String raw) {

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(BCryptVersion.$2A, 10);
    
    //The {bcrypt} prefix is used by Spring Security for delegating password encoding
    return "{bcrypt}" + encoder.encode(raw);
  }
}
