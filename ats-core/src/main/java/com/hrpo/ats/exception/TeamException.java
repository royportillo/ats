package com.hrpo.ats.exception;

public class TeamException extends RuntimeException {
  
  private static final long serialVersionUID = -506660264264374644L;

  public TeamException(String message, Throwable cause) {
    super(message, cause);
  }
  
  public TeamException(String message) {
    super(message);
  }
}
