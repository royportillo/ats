package com.hrpo.ats.exception;

public class CandidateException extends RuntimeException {

  private static final long serialVersionUID = -669770033593541637L;

  public CandidateException(String message) {
    super(message);
  }
}
