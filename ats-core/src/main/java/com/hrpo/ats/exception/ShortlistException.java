package com.hrpo.ats.exception;

public class ShortlistException extends RuntimeException {

  private static final long serialVersionUID = 6521028375040055491L;

  public ShortlistException(String message) {
    super(message);
  }
}
