package com.hrpo.ats.exception;

public class EndClientException extends RuntimeException {

  private static final long serialVersionUID = -2830026263315572645L;

  public EndClientException(String message, Throwable cause) {
    super(message, cause);
  }
}
