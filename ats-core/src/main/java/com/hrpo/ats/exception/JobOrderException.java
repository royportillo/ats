package com.hrpo.ats.exception;

public class JobOrderException extends RuntimeException {

  private static final long serialVersionUID = 968922710762038977L;

  public JobOrderException(String message, Throwable cause) {
    super(message, cause);
  }
  
  public JobOrderException(String message) {
    super(message);
  }
}
