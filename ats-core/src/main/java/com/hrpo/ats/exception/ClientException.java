package com.hrpo.ats.exception;

public class ClientException extends RuntimeException {

  private static final long serialVersionUID = -6203755498190741725L;

  public ClientException(String message, Throwable cause) {
    super(message, cause);
  }
  
  public ClientException(String message) {
    super(message);
  }
}
