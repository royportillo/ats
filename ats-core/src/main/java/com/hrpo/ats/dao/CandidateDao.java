package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.daxtra.dto.Candidate;

public interface CandidateDao {
  
  /**
   * Returns {@code Candidate} by id.
   * 
   * @param candidateId the id of the candidate to be retrieved.
   * 
   * @return the {@code Candidate} of the id.
   */
  Candidate findByPrimaryKey(Integer candidateId);
  
  /**
   * Returns {@code Candidate} by DAXTRA candidate id.
   * 
   * @param daxtraCandidateId the DAXTRA id of the candidate to be retrieved.
   * @param hasHrXml set to true if returned {@code Candidate} includes HRXML.
   *        Set to false if returned {@code Candidate} does not include HRXML.
   * 
   * @return the {@code Candidate} of the DAXTRA id.
   */
  Candidate findByCandidateId(String daxtraCandidateId, Boolean hasHrXml);

  /**
   * Inserts candidate or updates, if candidate is already existing.
   * 
   * @param candidate the {@code Candidate}.
   * 
   * @return The row id.
   */
  Integer upsert(Candidate candidate);

  /**
   * Inserts shortlist and candidate many-to-many table.
   * 
   * @param shortlistId the shortlist row id.
   * @param candidateId the candidate row id.
   * 
   * @return The row id.
   */
  Integer insertShortlistCandidate(Integer shortlistId, Integer candidateId);

  /**
   * Update candidate's HRXML by candidate Id.
   * 
   * @param hrXml the Base64 encoded HRXML of the candidate.
   * @param candidateId the candidate row id.
   * 
   * @return the number of rows affected.
   */
  Integer updateHrXml(String hrXml, String candidateId);
  
  /**
   * Returns list of {@code Candidate} by shortlist name.
   * 
   * @param shortlistName the shortlist name of the candidates to be retrieved.
   * 
   * @return the list of {@code Candidate} of the shortlist.
   */
  List<Candidate> findByShortlistName(String shortlistName);
}
