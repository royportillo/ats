package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.Source;

public interface SourcesDao {

  /**
   * Inserts {@code Source}.
   * @param source The source to insert.
   * @return The row id of inserted source.
   */
  public Integer insert(Source source);
  
  /**
   * Updates {@code Source}.
   * @param source The source to update.
   * @return The row id of the updated source.
   */
  public Integer update(Source source);
  
  /**
   * Finds {@code Source} by its row ID.
   * @param id the row id of the source.
   * @return the source.
   */
  public Source findById(Integer id);
  
  /**
   * Finds all {@code Source}s.
   * @return the list of all {@code Source}s.
   */
  public List<Source> findAll();
}
