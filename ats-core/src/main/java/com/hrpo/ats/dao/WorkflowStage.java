package com.hrpo.ats.dao;

public enum WorkflowStage {
  
  IMPORT("import_event", "import"), //Special event for unprocessed jo-candidates.
  PRESCREEN("prescreen_event", "prescreen"),
  SUBMIT("submission_event", "submission"),
  PRESENTATION("client_submission_event", "presentation"),
  INTERVIEW("interview_event", "interview"),
  PLACEMENT("placement_event", "placement");
  
  private String workflowTableName;
  private String name;
  
  WorkflowStage(String workflowTableName, String name) {
    this.workflowTableName = workflowTableName;
    this.name = name;
  }
  
  public String getWorkflowTableName() {
    return workflowTableName;
  }
  
  public String getName() {
    return name;
  }
  
  public static WorkflowStage ofName(String name) {
    
    if (name == null) {
      return null;
    }
    
    for (WorkflowStage stage : WorkflowStage.values()) {
      if (stage.name.equalsIgnoreCase(name)) {
        return stage;
      }
    }
    throw new IllegalArgumentException("No constant with text \"" + name + "\" found");
  }
}
