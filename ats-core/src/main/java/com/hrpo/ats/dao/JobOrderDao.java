package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.List;
import com.hrpo.ats.dto.JobOrder;

public interface JobOrderDao {

  /**
   * Returns {@code JobOrder} by id.
   * 
   * @param jobOrderId the id of the job order to be retrieved.
   * @return the {@code JobOrder} of the id.
   */
  JobOrder findByPrimaryKey(Integer jobOrderId);
  
  /**
   * Returns {@code JobOrder} with attached file.
   * 
   * @param jobOrderId the id of the job order to be retrieved.
   * @return the {@code JobOrder} of the id with attached file.
   */
  JobOrder findJobOrderFileById(Integer jobOrderId);
  
  /**
   * Inserts job order.
   * 
   * @param jobOrder the {@code JobOrder}.
   * 
   * @return the row id.
   */
  Integer insert(final JobOrder jobOrder);
  
  /**
   * Updates job order.
   * 
   * @param jobOrder the {@code JobOrder}.
   * @return the row id.
   */
  Integer update(final JobOrder jobOrder);
  
  /**
   * Find all job orders owned by the user.
   * 
   * @param userId the user id of the job orders' owner. {@code null} will return all job orders.
   * @return the list of job orders.
   */
  List<JobOrder> findAllByUserId(Integer userId);

  /**
   * Find all job orders owned by the user and by client.
   * 
   * @param userId the user id of the job orders' owner
   * @return the list of job orders.
   */
  List<JobOrder> findAllByUserIdAndClientId(Integer userId, Integer clientId);
  
  /**
   * Find job order by job order number and/or title.
   * 
   * @param userId the user id of the job orders' owner
   * @param joNumber the job order number.
   * @param title the job order title.
   * 
   * @return the list of job orders. If no match found, then returns an empty list.
   */
  List<JobOrder> findByCriteria(Integer userId, 
      final String joNumber, final String title, final Integer clientId);
  
  /**
   * Returns count of job order instances 'owned' by user within a date range.
   * @param userId The user id.
   * @param teamId The team id.
   * @param dateFrom The start date.
   * @param dateTo The end date.
   * @return The count of job orders found.
   */
  public Integer findCountByUserAndDateRange(Integer userId, Integer teamId, LocalDate dateFrom,
      LocalDate dateTo);
  
  /**
   * Returns list of {@code JobOrder}s 'owned' by user within a date range.
   * @param userId The user id.
   * @param teamid The team id.
   * @param dateFrom The start date.
   * @param dateTo The end date.
   * @return The list of {@code JobOrder}s found.
   */
  public List<JobOrder> findByUserAndDateRange(Integer userId, Integer teamId, LocalDate dateFrom,
      LocalDate dateTo);
}
