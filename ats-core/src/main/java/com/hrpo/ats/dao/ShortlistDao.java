package com.hrpo.ats.dao;

import com.hrpo.ats.dto.Shortlist;

public interface ShortlistDao {

  /**
   * Returns {@code Shortlist} by id.
   * 
   * @param shortlistId the id of the shortlist to be retrieved.
   * @return the {@code Shortlist} of the id.
   */
  Shortlist findByPrimaryKey(Integer shortlistId);
  
  /**
   * Inserts shortlist and the associated logged user id.
   * 
   * @param shortlist the {@code Shortlist}.
   * @param loggedUserId the user id of the associated logged user.
   * @return the row id.
   */
  Integer insert(final Shortlist shortlist, final Integer loggedUserId);
}
