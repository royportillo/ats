package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.Client;

public interface ClientDao {
  
  /**
   * Inserts {@code Client}.
   * @param client The client to insert.
   * @return The row id of inserted client.
   */
  public Integer insert(Client client);
  
  /**
   * Updates {@code Client}.
   * @param client The client to update.
   * @return The row id of the updated client.
   */
  public Integer update(Client client);
  
  /**
   * Finds {@code Client} by its row ID.
   * @param id the row id of the client.
   * @return the client.
   */
  public Client findById(Integer id);
  
  /**
   * Finds all {@code Client}s.
   * @return the list of all {@code Client}s.
   */
  public List<Client> findAll();
  
  /**
   * Finds {@code Client}s by team. 
   * @param teamId The id of the team which the clients belong to.
   * @return a list of {@code Client}s.
   */
  public List<Client> findByTeam(Integer teamId);
}
