package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Source;

@Repository
public class SourcesJdbcDao extends AbstractJdbcDao implements SourcesDao {
  
  public SourcesJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public Integer insert(Source source) {
    final String sql = "INSERT INTO sources (name, category, description, index) "
        + "VALUES (:name, :category::source_category, :description, :index)";
    
    Map<String, Object> params = new HashMap<>();
    params.put("name", source.getName());
    params.put("category", source.getCategory());
    params.put("description", source.getDescription());
    params.put("index", source.getIndex());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    KeyHolder holder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, paramSource, holder, new String[] {"id"});
    
    return holder.getKey().intValue();
  }
  
  @Override
  public Integer update(Source source) {
    final String sql = "UPDATE sources SET name = :name, description = :description, "
        + "category = :category::source_category, index = :index WHERE id = :id";
    
    Map<String, Object> params = new HashMap<>();
    params.put("name", source.getName());
    params.put("category", source.getCategory());
    params.put("description", source.getDescription());
    params.put("index", source.getIndex());
    params.put("id", source.getId());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    jdbcTemplate.update(sql, paramSource);
    
    return source.getId();
  }

  @Override
  public Source findById(Integer id) {
    final String sql = "SELECT * FROM sources WHERE id = :id ORDER BY index ASC";
    
    Source result = jdbcTemplate.queryForObject(sql, 
        new MapSqlParameterSource("id", id), (res, rowNum) -> {
          Source source = new Source();
          
          source.setId(res.getInt("id"));
          source.setName(res.getString("name"));
          source.setCategory(res.getString("category"));
          source.setDescription(res.getString("description"));
          source.setIndex(res.getInt("index"));
          
          return source;
        });
    
    return result;
  }
  
  @Override
  public List<Source> findAll() {
    final String sql = "SELECT * FROM sources ORDER BY index ASC, name ASC";
    
    List<Source> result = jdbcTemplate.query(sql, (res, rowNum) -> {
      Source source = new Source();
      
      source.setId(res.getInt("id"));
      source.setName(res.getString("name"));
      source.setCategory(res.getString("category"));
      source.setDescription(res.getString("description"));
      source.setIndex(res.getInt("index"));
      
      return source;
    });
    
    return result;
  }
  
}
