package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.UserJobOrder;
import com.hrpo.daxtra.dto.Candidate;

@Repository
public class JobOrderCandidateJdbcDao extends AbstractJdbcDao implements JobOrderCandidateDao {

  Logger log = LoggerFactory.getLogger(JobOrderCandidateJdbcDao.class);
  
  private static final int MAX_DATABASE_LIMIT = 100;

  public JobOrderCandidateJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public JobOrderCandidate findByPrimaryKey(final Integer jobOrderCandidateId) {
    
    String sql = "SELECT joc.id AS joc_id, candidate_id, job_order_id, last_workflow, "
        + "joc.pay_rate_start, joc.pay_rate_end, joc.pay_period_rate, joc.event_remarks, "
        + "jo.jo_number, jo.jo_date, "
        + "ec.id AS ec_id, ec.name AS ec_name, "
        + "ur.id AS ur_id, ur.firstname AS ur_firstname, ur.lastname AS ur_lastname, ur.username "
        + "FROM job_order_candidate joc "
        + "INNER JOIN users ur ON joc.users_id = ur.id "
        + "INNER JOIN job_order jo ON joc.job_order_id = jo.id "
        + "INNER JOIN end_client ec on jo.end_client_id = ec.id "
        + "WHERE joc.id = :id";

    JobOrderCandidate candidate = (JobOrderCandidate) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("id", jobOrderCandidateId), (res, rowNum) -> {
          
          Candidate cd = new Candidate();
          cd.setId(res.getInt("candidate_id"));
          
          JobOrder jo = new JobOrder();
          jo.setId(res.getInt("job_order_id"));
          jo.setNumber(res.getString("jo_number"));
          jo.setJoDate(res.getDate("jo_date").toLocalDate());
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jo.setEndClient(ec);
          
          User user = new User();
          user.setId(res.getInt("ur_id"));
          user.setFirstName(res.getString("ur_firstname"));
          user.setLastName(res.getString("ur_lastname"));
          user.setUsername(res.getString("username"));
          
          JobOrderCandidate joc = new JobOrderCandidate();
          joc.setId(res.getInt("joc_id"));
          joc.setCandidate(cd);
          joc.setJobOrder(jo);
          joc.setOwner(user);
          joc.setPayRateFrom(res.getDouble("pay_rate_start"));
          joc.setPayRateTo(res.getDouble("pay_rate_end"));
          joc.setPayPeriodRate(res.getString("pay_period_rate"));
          joc.setLastWorkflowStage(WorkflowStage.ofName(res.getString("last_workflow")));
          joc.setEventRemarks(res.getString("event_remarks"));
          
          return joc;
        });

    return candidate;
  }

  @Override
  public Integer insert(final Integer joId, 
      final Integer usersId, final Integer candidateId, final JobOrderCandidate joCandidate) {
    
    final String insertSql = 
        "INSERT INTO job_order_candidate "
        + "(job_order_id, candidate_id, users_id, pay_rate_start, pay_rate_end, pay_period_rate) "
        + "VALUES (:job_order_id, :candidate_id, :users_id, "
        + ":pay_rate_start, :pay_rate_end, :pay_period_rate::rate) ";

    Map<String,Object> params = new HashMap<>();
    params.put("job_order_id", joId);
    params.put("candidate_id", candidateId);
    params.put("users_id", usersId);
    params.put("pay_rate_start", joCandidate.getPayRateFrom());
    params.put("pay_rate_end", joCandidate.getPayRateTo());
    params.put("pay_period_rate", joCandidate.getPayPeriodRate());
    
    SqlParameterSource namedParameters = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(insertSql, namedParameters, keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public void batchInsert(final Integer joId, 
      final Integer usersId, final List<Integer> candidateIds, JobOrderCandidate joCandidate) {
    
    final String insertSql = 
        "INSERT INTO job_order_candidate "
        + "(job_order_id, users_id, candidate_id, pay_rate_start, pay_rate_end, pay_period_rate) "
        + "VALUES (:job_order_id, :users_id, :candidate_id, "
        + ":pay_rate_start, :pay_rate_end, :pay_period_rate::rate)";
    
    SqlParameterSource[] namedParameters = new SqlParameterSource[candidateIds.size()];
    
    for (int i = 0; i < candidateIds.size(); i++) {
      Map<String, Object> params = new HashMap<>();
      params.put("job_order_id", joId);
      params.put("users_id", usersId);
      params.put("candidate_id", candidateIds.get(i));
      params.put("pay_rate_start", joCandidate.getPayRateFrom());
      params.put("pay_rate_end", joCandidate.getPayRateTo());
      params.put("pay_period_rate", joCandidate.getPayPeriodRate());
      
      namedParameters[i] = new MapSqlParameterSource(params);
    }
    
    jdbcTemplate.batchUpdate(insertSql, namedParameters);
  }

  @Override
  public List<JobOrderCandidate> getByJobOrderId(final Integer jobOrderId, Integer userId) {
    final String query = "SELECT cd.id AS cd_id, joc.id AS joc_id, job_order_id, "
        + "joc.pay_rate_start, joc.pay_rate_end, joc.pay_period_rate, joc.event_remarks, "
        + "cd.candidate_id AS cd_candidate_id, joc.job_order_id, source, "
        + "cd.firstname as cd_firstname, cd.lastname as cd_lastname, cd.fullname as cd_fullname, "
        + "contact_phone, contact_mobile, contact_email, "
        + "ur.id AS ur_id, ur.username, ur.firstname as ur_firstname, ur.lastname as ur_lastname "
        + "FROM candidate cd INNER JOIN job_order_candidate joc ON cd.id = joc.candidate_id "
        + "INNER JOIN users ur ON joc.users_id = ur.id "
        + "WHERE joc.job_order_id = :job_order_id "
        + "AND ur.id = :user_id ORDER BY joc.id ASC";
    
    Map<String, Object> params = new HashMap<>();
    params.put("job_order_id", jobOrderId);
    params.put("user_id", userId);
    
    List<JobOrderCandidate> results = jdbcTemplate.query(query, 
        new MapSqlParameterSource(params), (res, rowNum) -> {
          JobOrderCandidate jobOrderCandidate = new JobOrderCandidate();
          Candidate candidate = new Candidate();
          
          jobOrderCandidate.setId(res.getInt("joc_id"));
          
          candidate.setId(res.getInt("cd_id"));
          candidate.setCandidateId(res.getString("cd_candidate_id"));
          candidate.setSource(res.getString("source"));
          candidate.setFirstName(res.getString("cd_firstname"));
          candidate.setLastName(res.getString("cd_lastname"));
          candidate.setFullName(res.getString("cd_fullname"));
          candidate.setPhone(res.getString("contact_phone"));
          candidate.setMobile(res.getString("contact_mobile"));
          candidate.setEmail(res.getString("contact_email"));
          
          JobOrder jobOrder = new JobOrder();
          jobOrder.setId(res.getInt("job_order_id"));
          
          User user = new User();
          user.setId(res.getInt("ur_id"));
          user.setFirstName(res.getString("ur_firstname"));
          user.setLastName(res.getString("ur_lastname"));
          user.setUsername(res.getString("username"));
          
          jobOrderCandidate.setCandidate(candidate);
          jobOrderCandidate.setJobOrder(jobOrder);
          jobOrderCandidate.setOwner(user);
          jobOrderCandidate.setPayRateFrom(res.getDouble("pay_rate_start"));
          jobOrderCandidate.setPayRateTo(res.getDouble("pay_rate_end"));
          jobOrderCandidate.setPayPeriodRate(res.getString("pay_period_rate"));
          jobOrderCandidate.setEventRemarks(res.getString("event_remarks"));
          
          return jobOrderCandidate;
        });
    
    return results;
  }
  
  @Override
  public JobOrderCandidate.WorkflowState getWorkflowStatesByJobOrderCandidate(
      final JobOrderCandidate jobOrderCandidate) {
    final String query = "SELECT ps.id AS prescreen_id, sm.id AS submission_id, "
        + "cs.id AS client_submission_id, iv.id AS interview_id, pm.id AS placement_id "
        + "FROM job_order_candidate joc LEFT JOIN prescreen_event ps "
        + "ON joc.id = ps.job_order_candidate_id "
        + "LEFT JOIN submission_event sm ON joc.id = sm.job_order_candidate_id "
        + "LEFT JOIN client_submission_event cs ON joc.id = cs.job_order_candidate_id "
        + "LEFT JOIN interview_event iv ON joc.id = iv.job_order_candidate_id "
        + "LEFT JOIN placement_event pm ON joc.id = pm.job_order_candidate_id " 
        + "WHERE joc.id = :job_order_candidate_id";
    
    SqlParameterSource param = new MapSqlParameterSource(
        "job_order_candidate_id", jobOrderCandidate.getId());
    
    JobOrderCandidate.WorkflowState result = 
        (JobOrderCandidate.WorkflowState) jdbcTemplate.queryForObject(query, param, 
            (res, rowNum) -> {
              return jobOrderCandidate.new WorkflowState(
                  res.getObject("prescreen_id") != null ? true : false,
                  res.getObject("submission_id") != null ? true : false, 
                  res.getObject("client_submission_id") != null ? true : false,
                  res.getObject("interview_id") != null ? true : false,
                  res.getObject("placement_id") != null ? true : false);
            });
    
    return result;
  }
  
  @Override
  public List<JobOrderCandidate> findByEventWorkflow(Integer userId, 
      Integer teamId, WorkflowStage stage, LocalDate fromDate, LocalDate toDate, 
      Boolean isLastWorkflowOnly) {
    
    StringJoiner whereClause = new StringJoiner(" AND ", " WHERE ", " ORDER BY cd.id ASC");
    
    if (userId != null) {
      whereClause.add("ujoc.id = :user_id");
    }
    
    if (teamId != null) {
      whereClause.add("t.id = :team_id");
    }
    
    if (isLastWorkflowOnly) {
      whereClause.add("last_workflow = :last_workflow::workflow_stage");
    }
    
    if (fromDate != null && toDate != null) {
      whereClause.add("wf.txn_date >= :from_date AND wf.txn_date <= :to_date");
    }
    
    final String sql = "SELECT "
        + "cd.id, cd.candidate_id, cd.source, cd.firstname, cd.lastname, cd.fullname, "
        + "cd.contact_phone, cd.contact_mobile, cd.contact_email, "
        + "joc.id AS joc_id, joc.last_workflow, wf.txn_date, "
        + "joc.pay_rate_start AS joc_pay_rate_start, joc.pay_rate_end AS joc_pay_rate_end, "
        + "joc.pay_period_rate AS joc_pay_period_rate, "
        + "joc.event_remarks AS joc_event_remarks, "
        + "jo.id AS jo_id, jo.jo_number, jo.jo_date, jo.date_created, jo.title, "
        + "jo.location, jo.zip_code, "
        + "jo.division, jo.start_date, jo.end_date, jo.bill_rate_start, jo.bill_rate_end, "
        + "jo.bill_period_rate, jo.pay_rate_start, jo.pay_rate_end, jo.pay_period_rate, "
        + "jo.openings, jo.max_subs, jo.status, "
        + "ec.id AS ec_id, ec.name AS ec_name, "
        + "cl.id AS cl_id, cl.name AS cl_name, "
        + "uujo.id AS uujo_id, uujo.firstname AS uujo_firstname, uujo.lastname AS uujo_lastname, "
        + "uujo.username AS uujo_username, ujo.primary_user, "
        + "ujoc.id AS ujoc_id, ujoc.firstname AS ujoc_firstname, ujoc.lastname AS ujoc_lastname, "
        + "ujoc.username AS ujoc_username "
        + "FROM " + stage.getWorkflowTableName() + " wf "  
        + "INNER JOIN job_order_candidate joc on wf.job_order_candidate_id = joc.id "
        + "INNER JOIN users ujoc ON ujoc.id = joc.users_id "
        + "INNER JOIN candidate cd on joc.candidate_id = cd.id "
        + "INNER JOIN job_order jo on joc.job_order_id = jo.id "
        + "INNER JOIN end_client ec on jo.end_client_id = ec.id "
        + "INNER JOIN client cl on ec.client_id = cl.id "
        + "INNER JOIN team t on cl.team_id = t.id " 
        + "INNER JOIN users tu on ujoc.team_id = tu.id "
        + "INNER JOIN user_job_order ujo ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE "
        + "INNER JOIN users uujo ON uujo.id = ujo.users_id "
        + whereClause;
    
    Map<String, Object> params =
        new HashMap<>();
    params.put("user_id", userId);
    params.put("team_id", teamId);
    params.put("last_workflow", isLastWorkflowOnly ? stage.getName() : null);
    params.put("from_date", fromDate);
    params.put("to_date", toDate);
    
    List<JobOrderCandidate> results = jdbcTemplate.query(sql, 
        params, (res, rowNum) -> {
          Candidate candidate = new Candidate();
          candidate.setId(res.getInt("id"));
          candidate.setCandidateId(res.getString("candidate_id"));
          candidate.setSource(res.getString("source"));
          candidate.setFirstName(res.getString("firstname"));
          candidate.setLastName(res.getString("lastname"));
          candidate.setFullName(res.getString("fullname"));
          candidate.setPhone(res.getString("contact_phone"));
          candidate.setMobile(res.getString("contact_mobile"));
          candidate.setEmail(res.getString("contact_email"));
          
          JobOrder jobOrder = new JobOrder();
          jobOrder.setId(res.getInt("jo_id"));
          jobOrder.setNumber(res.getString("jo_number"));
          jobOrder.setJoDate(res.getDate("jo_date").toLocalDate());
          jobOrder.setDateCreated(res.getDate("date_created").toLocalDate());
          jobOrder.setTitle(res.getString("title"));
          jobOrder.setLocation(res.getString("location"));
          jobOrder.setZipCode(res.getString("zip_code"));
          jobOrder.setDivision(res.getString("division"));
          jobOrder.setStartDate(res.getDate("start_date").toLocalDate());
          jobOrder.setEndDate(res.getDate("end_date").toLocalDate());
          jobOrder.setBillRateStart(res.getDouble("bill_rate_start"));
          jobOrder.setBillRateEnd(res.getDouble("bill_rate_end"));
          jobOrder.setBillPeriodRate(res.getString("bill_period_rate"));
          jobOrder.setPayRateStart(res.getDouble("pay_rate_start"));
          jobOrder.setPayRateEnd(res.getDouble("pay_rate_end"));
          jobOrder.setPayPeriodRate(res.getString("pay_period_rate"));
          jobOrder.setOpenings(res.getString("openings"));
          jobOrder.setMaxSubs(res.getString("max_subs"));
          jobOrder.setStatus(res.getString("status"));
          
          User joOwner = new User();
          joOwner.setId(res.getInt("uujo_id"));
          joOwner.setFirstName(res.getString("uujo_firstname"));
          joOwner.setLastName(res.getString("uujo_lastname"));
          joOwner.setUsername(res.getString("uujo_username"));
          
          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setUser(joOwner);
          userJobOrder.setJobOrder(jobOrder);
          
          jobOrder.setPrimaryOwner(userJobOrder);
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jobOrder.setEndClient(ec);
          
          Client cl = new Client();
          cl.setClientId(res.getInt("cl_id"));
          cl.setName(res.getString("cl_name"));
          ec.setClient(cl);
          
          User jocOwner = new User();
          jocOwner.setId(res.getInt("ujoc_id"));
          jocOwner.setFirstName(res.getString("ujoc_firstname"));
          jocOwner.setLastName(res.getString("ujoc_lastname"));
          jocOwner.setUsername(res.getString("ujoc_username"));
          
          JobOrderCandidate jobOrderCandidate = new JobOrderCandidate();
          jobOrderCandidate.setId(res.getInt("joc_id"));
          jobOrderCandidate.setCandidate(candidate);
          jobOrderCandidate.setJobOrder(jobOrder);
          jobOrderCandidate.setWorkflowStage(stage);
          jobOrderCandidate.setLastWorkflowStage(
              WorkflowStage.ofName(res.getString("last_workflow")));
          jobOrderCandidate.setTransactionDate(res.getDate("txn_date").toLocalDate());
          jobOrderCandidate.setOwner(jocOwner);
          jobOrderCandidate.setPayRateFrom(res.getDouble("joc_pay_rate_start"));
          jobOrderCandidate.setPayRateTo(res.getDouble("joc_pay_rate_end"));
          jobOrderCandidate.setPayPeriodRate(res.getString("joc_pay_period_rate"));
          jobOrderCandidate.setEventRemarks(res.getString("joc_event_remarks"));
          
          return jobOrderCandidate;
        });
    
    return results;
  }
  
  
  
  @Override
  public List<JobOrderCandidate> findUnprocessed(Integer userId, LocalDate fromDate,
      LocalDate toDate) {
    
    String sql = "SELECT " 
        + "cd.id, cd.candidate_id, cd.source, cd.firstname, cd.lastname, cd.fullname," 
        + "cd.contact_phone, cd.contact_mobile, cd.contact_email," 
        + "joc.id AS joc_id, joc.last_workflow, joc.import_date, "  
        + "joc.pay_rate_start AS joc_pay_rate_start, joc.pay_rate_end AS joc_pay_rate_end," 
        + "joc.pay_period_rate AS joc_pay_period_rate, " 
        + "joc.event_remarks AS joc_event_remarks, " 
        + "jo.id AS jo_id, jo.jo_number, jo.jo_date, jo.date_created, jo.title, " 
        + "jo.location, jo.zip_code, " 
        + "jo.division, jo.start_date, jo.end_date, jo.bill_rate_start, jo.bill_rate_end, " 
        + "jo.bill_period_rate, jo.pay_rate_start, jo.pay_rate_end, jo.pay_period_rate, " 
        + "jo.openings, jo.max_subs, jo.status, " 
        + "ec.id AS ec_id, ec.name AS ec_name, " 
        + "cl.id AS cl_id, cl.name AS cl_name, " 
        + "uujo.id AS uujo_id, uujo.firstname AS uujo_firstname, uujo.lastname AS uujo_lastname, " 
        + "uujo.username AS uujo_username, ujo.primary_user, "  
        + "ujoc.id AS ujoc_id, ujoc.firstname AS ujoc_firstname, ujoc.lastname AS ujoc_lastname, " 
        + "ujoc.username AS ujoc_username " 
        + "FROM job_order_candidate joc "
        + "LEFT JOIN prescreen_event pre ON joc.id = pre.job_order_candidate_id " 
        + "INNER JOIN users ujoc ON ujoc.id = joc.users_id " 
        + "INNER JOIN candidate cd on joc.candidate_id = cd.id " 
        + "INNER JOIN job_order jo on joc.job_order_id = jo.id " 
        + "INNER JOIN end_client ec on jo.end_client_id = ec.id " 
        + "INNER JOIN client cl on ec.client_id = cl.id " 
        + "INNER JOIN team t on cl.team_id = t.id " 
        + "INNER JOIN users tu on ujoc.team_id = tu.id " 
        + "INNER JOIN user_job_order ujo ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE " 
        + "INNER JOIN users uujo ON uujo.id = ujo.users_id ";
    
    if (userId != null) {
      sql += "WHERE ujoc.id = :user_id ";
    }
    
    if (fromDate != null && toDate != null) {
      sql += (userId != null ? "AND " : "WHERE ")
          + "joc.import_date >= :from_date AND joc.import_date <= :to_date ";
    }
    
    sql += "ORDER BY cd.id ASC";
    
    Map<String, Object> params =
        new HashMap<>();
    
    if (userId != null) {
      params.put("user_id", userId);
    }
    
    if (fromDate != null && toDate != null) {
      params.put("from_date", fromDate);
      params.put("to_date", toDate);
    }

    List<JobOrderCandidate> results = jdbcTemplate.query(sql, 
        params, (res, rowNum) -> {
          Candidate candidate = new Candidate();
          candidate.setId(res.getInt("id"));
          candidate.setCandidateId(res.getString("candidate_id"));
          candidate.setSource(res.getString("source"));
          candidate.setFirstName(res.getString("firstname"));
          candidate.setLastName(res.getString("lastname"));
          candidate.setFullName(res.getString("fullname"));
          candidate.setPhone(res.getString("contact_phone"));
          candidate.setMobile(res.getString("contact_mobile"));
          candidate.setEmail(res.getString("contact_email"));
          
          JobOrder jobOrder = new JobOrder();
          jobOrder.setId(res.getInt("jo_id"));
          jobOrder.setNumber(res.getString("jo_number"));
          jobOrder.setJoDate(res.getDate("jo_date").toLocalDate());
          jobOrder.setDateCreated(res.getDate("date_created").toLocalDate());
          jobOrder.setTitle(res.getString("title"));
          jobOrder.setLocation(res.getString("location"));
          jobOrder.setZipCode(res.getString("zip_code"));
          jobOrder.setDivision(res.getString("division"));
          jobOrder.setStartDate(res.getDate("start_date").toLocalDate());
          jobOrder.setEndDate(res.getDate("end_date").toLocalDate());
          jobOrder.setBillRateStart(res.getDouble("bill_rate_start"));
          jobOrder.setBillRateEnd(res.getDouble("bill_rate_end"));
          jobOrder.setBillPeriodRate(res.getString("bill_period_rate"));
          jobOrder.setPayRateStart(res.getDouble("pay_rate_start"));
          jobOrder.setPayRateEnd(res.getDouble("pay_rate_end"));
          jobOrder.setPayPeriodRate(res.getString("pay_period_rate"));
          jobOrder.setOpenings(res.getString("openings"));
          jobOrder.setMaxSubs(res.getString("max_subs"));
          jobOrder.setStatus(res.getString("status"));
          
          User joOwner = new User();
          joOwner.setId(res.getInt("uujo_id"));
          joOwner.setFirstName(res.getString("uujo_firstname"));
          joOwner.setLastName(res.getString("uujo_lastname"));
          joOwner.setUsername(res.getString("uujo_username"));
          
          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setUser(joOwner);
          userJobOrder.setJobOrder(jobOrder);
          
          jobOrder.setPrimaryOwner(userJobOrder);
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jobOrder.setEndClient(ec);
          
          Client cl = new Client();
          cl.setClientId(res.getInt("cl_id"));
          cl.setName(res.getString("cl_name"));
          ec.setClient(cl);
          
          User jocOwner = new User();
          jocOwner.setId(res.getInt("ujoc_id"));
          jocOwner.setFirstName(res.getString("ujoc_firstname"));
          jocOwner.setLastName(res.getString("ujoc_lastname"));
          jocOwner.setUsername(res.getString("ujoc_username"));
          
          JobOrderCandidate jobOrderCandidate = new JobOrderCandidate();
          jobOrderCandidate.setId(res.getInt("joc_id"));
          jobOrderCandidate.setCandidate(candidate);
          jobOrderCandidate.setJobOrder(jobOrder);
          jobOrderCandidate.setWorkflowStage(WorkflowStage.IMPORT);
          jobOrderCandidate.setLastWorkflowStage(
              WorkflowStage.ofName(res.getString("last_workflow")));
          jobOrderCandidate.setTransactionDate(res.getDate("import_date").toLocalDate());
          jobOrderCandidate.setOwner(jocOwner);
          jobOrderCandidate.setPayRateFrom(res.getDouble("joc_pay_rate_start"));
          jobOrderCandidate.setPayRateTo(res.getDouble("joc_pay_rate_end"));
          jobOrderCandidate.setPayPeriodRate(res.getString("joc_pay_period_rate"));
          jobOrderCandidate.setEventRemarks(res.getString("joc_event_remarks"));
          
          return jobOrderCandidate;
        });
    
    return results;
  }

  @Override
  public void updateWorkflowStage(final JobOrderCandidate joCandidate, final WorkflowStage stage) {
    final String sql = "UPDATE job_order_candidate SET "
        + "last_workflow = :workflow_stage::workflow_stage, "
        + "pay_rate_start = COALESCE(:pay_rate_from, pay_rate_start), "
        + "pay_rate_end = COALESCE(:pay_rate_to, pay_rate_end), "
        + "pay_period_rate = COALESCE(:pay_period_rate::rate, pay_period_rate), "
        + "event_remarks = COALESCE(:event_remarks, event_remarks) "
        + "WHERE id = :joc_id";
    
    Map<String, Object> params = new HashMap<>();
    params.put("workflow_stage", stage == null ? null : stage.getName());
    params.put("joc_id", joCandidate.getId());
    params.put("pay_rate_from", joCandidate.getPayRateFrom());
    params.put("pay_rate_to", joCandidate.getPayRateTo());
    params.put("pay_period_rate", joCandidate.getPayPeriodRate());
    params.put("event_remarks", joCandidate.getEventRemarks());
    
    jdbcTemplate.update(sql, params);
  }
  
}