package com.hrpo.ats.dao;

import javax.sql.DataSource;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;

@Repository("clientSubmissionDao")
public class ClientSubmissionJdbcDao extends AbstractWorkflowJdbcDao<ClientSubmissionEvent> {

  public ClientSubmissionJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  String getWorkflowTableName() {
    return "client_submission_event";
  }

  @Override
  ClientSubmissionEvent getEvent() {
    return new ClientSubmissionEvent();
  }
}
