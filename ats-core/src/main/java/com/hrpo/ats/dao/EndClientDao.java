package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.EndClient;

public interface EndClientDao {
  
  /**
   * Inserts end client.
   * @param endClient The end client to insert.
   * @return The row id of the inserted end client.
   */
  public Integer insert(EndClient endClient);
  
  /**
   * Updates end client.
   * @param endClient The end client to update.
   * @return The row id of the updated end client.
   */
  public Integer update(EndClient endClient);
  
  /**
   * Finds the {@code EndClient} by its id.
   * 
   * @param id the id of the {@code EndClient} to find.
   * @return The end client.
   */
  public EndClient findById(Integer id);
  
  /**
   * Retrieves a list of {@code EndClient}s available to the user
   * 
   * @param userId the id of the user.
   * @return a list of {@code EndClient}s available to the user
   */
  public List<EndClient> findEndClientsByUserId(final Integer userId);
  
  /**
   * Retrieves a list of {@code EndClient}s by client.
   * @param clientId the row ID of the client.
   * @return a list of {@code EndClient}s belonging to the client.
   */
  public List<EndClient> findEndClientsByClient(final Integer clientId);
  
  /**
   * Retrieves a list of all {@code EndClient}s.
   * @return The list of all {@code EndClient}s.
   */
  public List<EndClient> findAll();
}
