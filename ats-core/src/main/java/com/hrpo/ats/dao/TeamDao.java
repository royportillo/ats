package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.Team;

public interface TeamDao {
  
  /**
   * Gets the list of all available teams.
   * @return the {@code List} of {@code Team}s.
   */
  List<Team> findAllTeams();
  
  /**
   * Inserts a {@code Team}.
   * @param team The {@code Team} to insert.
   * @return The row id of the inserted {@code Team}.
   */
  Integer insert(Team team);

  /**
   * Updates a {@code Team}.
   * @param team The {@code Team} to update.
   * @return The row id of the updated {@code Team}.
   */
  Integer update(Team team);
  
  /**
   * Finds {@code Team} by its id.
   * @param teamId the ID of the team.
   * @return The team.
   */
  Team findById(Integer teamId);
}
