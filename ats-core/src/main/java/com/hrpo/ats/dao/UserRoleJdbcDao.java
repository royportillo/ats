package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Role;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.UserRole;

@Repository
public class UserRoleJdbcDao extends AbstractJdbcDao implements UserRoleDao {

  public UserRoleJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }

  @Override
  public Integer insert(final UserRole userRole) {
    final String sql = "INSERT INTO users_role (users_id, role_id) VALUES (:users_id, :role_id)";
    
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("users_id", userRole.getUser().getId());
    params.put("role_id", userRole.getRole().getId());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, paramSource, keyHolder, new String[] {"id"});
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public UserRole findByPrimaryKey(Integer id) {

    final String sql = "SELECT ur.id, u.id AS users_id, u.firstname, u.lastname, u.username, "
        + "u.email, "
        + "r.id AS role_id, r.name "
        + "FROM users_role ur INNER JOIN users u ON ur.users_id = u.id "
        + "INNER JOIN role r ON ur.role_id = r.id "
        + "WHERE ur.id = :id";
    
    UserRole result = (UserRole) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("id", id), (resultSet, rowNum) -> {
          User user = new User();
          user.setId(resultSet.getInt("users_id"));
          user.setFirstName(resultSet.getString("firstname"));
          user.setLastName(resultSet.getString("lastname"));
          user.setEmail(resultSet.getString("email"));
          user.setUsername(resultSet.getString("username"));
          
          Role role = new Role();
          role.setId(resultSet.getInt("role_id"));
          role.setName(resultSet.getString("name"));
          
          UserRole userRole = new UserRole();
          userRole.setId(resultSet.getInt("id"));
          userRole.setUser(user);
          userRole.setRole(role);
          
          return userRole;
        });
    
    return result;
  }
  
  @Override
  public void batchInsert(final Integer userId, final List<UserRole> userRoles) {
    final String sql = "INSERT INTO users_role (users_id, role_id) VALUES (:users_id, :role_id)";
    
    SqlParameterSource[] namedParams = new SqlParameterSource[userRoles.size()];
    
    for (int i = 0; i < userRoles.size(); i++) {
      Map<String, Object> params = new HashMap<>();
      params.put("users_id", userId);
      params.put("role_id", userRoles.get(i).getRole().getId());
      
      namedParams[i] = new MapSqlParameterSource(params);
    }
    
    jdbcTemplate.batchUpdate(sql, namedParams);
  }
  
  @Override
  public List<UserRole> findByUserId(Integer userId) {
    final String sql = "SELECT ur.id, u.id AS users_id, u.firstname, u.lastname, u.username, "
        + "u.email, "
        + "r.id AS role_id, r.name "
        + "FROM users_role ur INNER JOIN users u ON ur.users_id = u.id "
        + "INNER JOIN role r ON ur.role_id = r.id "
        + "WHERE users_id = :users_id";
    
    
    List<UserRole> result = jdbcTemplate.query(
        sql, new MapSqlParameterSource("users_id", userId), (resultSet, rowNum) -> {
          User user = new User();
          user.setFirstName(resultSet.getString("firstname"));
          user.setLastName(resultSet.getString("lastname"));
          user.setEmail(resultSet.getString("email"));
          user.setId(resultSet.getInt("users_id"));
          user.setUsername(resultSet.getString("username"));
          
          Role role = new Role();
          role.setId(resultSet.getInt("role_id"));
          role.setName(resultSet.getString("name"));
          
          UserRole userRole = new UserRole();
          userRole.setId(resultSet.getInt("id"));
          userRole.setUser(user);
          userRole.setRole(role);
          
          return userRole;
        });
    
    return result;
  }
  
  @Override
  public void deleteByUserId(final Integer userId) {
    final String sql = "DELETE from users_role WHERE users_id = :users_id";
    
    jdbcTemplate.update(sql, new MapSqlParameterSource("users_id", userId));
  }
}
