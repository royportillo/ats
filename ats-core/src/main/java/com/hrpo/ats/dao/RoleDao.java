package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.Role;

public interface RoleDao {
  
  /**
   * Returns the list of all available roles.
   * @return the {@code List} of all roles.
   */
  public List<Role> findAllRoles();
}
