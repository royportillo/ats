package com.hrpo.ats.dao;

import javax.sql.DataSource;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.event.PlacementEvent;

@Repository("placementDao")
public class PlacementJdbcDao extends AbstractWorkflowJdbcDao<PlacementEvent> {

  public PlacementJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  String getWorkflowTableName() {
    return "placement_event";
  }

  @Override
  PlacementEvent getEvent() {
    return new PlacementEvent();
  }
}
