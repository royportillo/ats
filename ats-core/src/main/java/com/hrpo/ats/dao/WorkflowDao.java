package com.hrpo.ats.dao;

import java.time.LocalDate;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;

public interface WorkflowDao<T> {
  
  /**
   * Returns a subclass of {@code T} by id.
   * 
   * @param eventId the row id of the workflow event to be retrieved.
   * 
   * @return the {@code T} of the row id.
   */
  T findByPrimaryKey(Integer eventId);
  
  /**
   * Returns a subclass of {@code T} by job order candidate id.
   * 
   * @param jocId the row id of the job order candidate.
   * 
   * @return the {@code T} of the job order candidate.
   */
  T findByJobOrderCandidateId(Integer jocId);

  /**
   * Inserts a workflow event, or updates workflow event if an entry is existing for a 
   * job order candidate id.
   * 
   * @param workflowEvent the {@code AbstractWorkflowEvent} to be inserted.
   * @param jobOrderCandidateId the {@code JobOrderCandidate} row id to be inserted.
   * 
   * @return The row id.
   */
  Integer upsert(AbstractWorkflowEvent workflowEvent, Integer jobOrderCandidateId);

  /**
   * Counts event instances credited to the user within a date range.
   * 
   * @param userId the {@code User} id owning the events.
   * @param teamId the {@code User}'s current team's id.
   * @param dateFrom the start date.
   * @param dateTo the end date.
   * 
   * @return The count of the events found.
   */
  Integer findCountByUserAndDateRange(Integer userId, Integer teamId, LocalDate dateFrom, 
      LocalDate dateTo);

  /**
   * Check workflow state based on job order candidate id.
   * This is usually used in the UI to identify whether the workflow was already done.
   * 
   * @param jobOrderCandidateId the {@code JobOrderCandidate} row id.
   * 
   * @return The state of the job order candidate workflow event.
   */
  Boolean findStateByJobOrderCandidateId(Integer jobOrderCandidateId);
  
  /**
   * Deletes {@code JobOrderCandidate}'s workflow entry.
   * @param jobOrderCandidateId The id of the job order candidate.
   */
  void deleteByJobOrderCandidateId(Integer jobOrderCandidateId);
}
