package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Team;

@Repository
public class TeamJdbcDao extends AbstractJdbcDao implements TeamDao {
  
  public TeamJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public List<Team> findAllTeams() {
    final String sql = "SELECT * FROM team";
    
    List<Team> results = jdbcTemplate.query(sql, (resultSet, rownNum) -> {
      Team team = new Team();
      team.setId(resultSet.getInt("id"));
      team.setName(resultSet.getString("name"));
      team.setEmail(resultSet.getString("email"));
      team.setDescription(resultSet.getString("description"));
      return team;
    });
    
    return results;
  }
  
  @Override
  public Integer insert(Team team) {
    final String sql = "INSERT INTO team (name, description, email, users_group_id) "
        + "VALUES (:name, :description, :email, 1)"; //TODO change when users-group is implemented
    
    Map<String, Object> params = new HashMap<>();
    params.put("name", team.getName());
    params.put("description", team.getDescription());
    params.put("email", team.getEmail());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    KeyHolder holder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, paramSource, holder, new String[] { "id" });
    
    return holder.getKey().intValue();
  }
  
  @Override
  public Integer update(Team team) {
    final String sql = "UPDATE team SET name = :name, description = :description, email = :email "
        + "WHERE id = :id";

    Map<String, Object> params = new HashMap<>();
    params.put("name", team.getName());
    params.put("description", team.getDescription());
    params.put("email", team.getEmail());
    params.put("id", team.getId());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    jdbcTemplate.update(sql, paramSource);
    
    return team.getId();
  }
  
  @Override
  public Team findById(Integer teamId) {
    final String sql = "SELECT * FROM TEAM WHERE id = :teamId";
    
    Team result = (Team) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("teamId", teamId), (res, rowNum) -> {
          Team team = new Team();
          
          team.setId(res.getInt("id"));
          team.setName(res.getString("name"));
          team.setDescription(res.getString("description"));
          team.setEmail(res.getString("email"));
          
          return team;
        });
    
    return result;
  }
}
