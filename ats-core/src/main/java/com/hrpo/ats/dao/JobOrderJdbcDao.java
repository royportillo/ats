package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.UserJobOrder;

@Repository
public class JobOrderJdbcDao extends AbstractJdbcDao implements JobOrderDao {
  
  private static final int MAX_ROW_LIMIT = 100;

  Logger log = LoggerFactory.getLogger(JobOrderJdbcDao.class);

  public JobOrderJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public JobOrder findByPrimaryKey(Integer jobOrderId) {
    String sql = "SELECT jo.id AS jo_id, jo_number, jo_date, date_created, "
        + "title, location, zip_code, "
        + "division, start_date, end_date, bill_rate_start, bill_rate_end, bill_period_rate, "
        + "pay_rate_start, pay_rate_end, pay_period_rate, openings, max_subs, status, "
        + "ec.id AS ec_id, ec.name AS ec_name, "
        + "u.id AS u_id, u.firstname, u.lastname, u.username, ujo.primary_user, "
        + "OCTET_LENGTH(jo_file) AS file_length "
        + "FROM job_order jo INNER JOIN end_client ec ON jo.end_client_id = ec.id "
        + "INNER JOIN user_job_order ujo "
        + "ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE "
        + "INNER JOIN users u ON u.id = ujo.users_id "
        + "WHERE jo.id = :jo_id";

    JobOrder jobOrder = (JobOrder) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("jo_id", jobOrderId), (res, rowNum) -> {
          JobOrder jo = new JobOrder();
          jo.setId(res.getInt("jo_id"));
          jo.setNumber(res.getString("jo_number"));
          jo.setJoDate(res.getDate("jo_date").toLocalDate());
          jo.setDateCreated(res.getDate("date_created").toLocalDate());
          jo.setTitle(res.getString("title"));
          jo.setLocation(res.getString("location"));
          jo.setZipCode(res.getString("zip_code"));
          jo.setDivision(res.getString("division"));
          jo.setStartDate(res.getDate("start_date").toLocalDate());
          jo.setEndDate(res.getDate("end_date").toLocalDate());
          jo.setBillRateStart(res.getDouble("bill_rate_start"));
          jo.setBillRateEnd(res.getDouble("bill_rate_end"));
          jo.setBillPeriodRate(res.getString("bill_period_rate"));
          jo.setPayRateStart(res.getDouble("pay_rate_start"));
          jo.setPayRateEnd(res.getDouble("pay_rate_end"));
          jo.setPayPeriodRate(res.getString("pay_period_rate"));
          jo.setOpenings(res.getString("openings"));
          jo.setMaxSubs(res.getString("max_subs"));
          jo.setStatus(res.getString("status"));
          jo.setHasFile((res.getInt("file_length") > 0) ? true : false);
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jo.setEndClient(ec);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          user.setUsername(res.getString("username"));
          
          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setUser(user);
          userJobOrder.setIsPrimary(res.getBoolean("primary_user"));
          userJobOrder.setJobOrder(jo);
          
          jo.setPrimaryOwner(userJobOrder);
          
          return jo;
        });

    return jobOrder;
  }
  
  @Override
  public JobOrder findJobOrderFileById(Integer jobOrderId) {
    String sql = "SELECT id, jo_number, mime_type, file_name, jo_file "
        + "FROM job_order WHERE id = :id";
    
    JobOrder jobOrder = (JobOrder) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("id", jobOrderId), (res, rowNum) -> {
          JobOrder jo = new JobOrder();
          jo.setId(res.getInt("id"));
          jo.setNumber(res.getString("jo_number"));
          jo.setMimeType(res.getString("mime_type"));
          jo.setFileName(res.getString("file_name"));
          jo.setFile(res.getBytes("jo_file"));
          
          return jo;
        });

    return jobOrder;
  }

  @Override
  public Integer insert(final JobOrder jobOrder) {
    
    final String sql = "INSERT INTO job_order (jo_number, jo_date, date_created, title, location, "
        + "zip_code, division, start_date, end_date, bill_rate_start, bill_rate_end, "
        + "bill_period_rate, pay_rate_start, pay_rate_end, pay_period_rate, openings, "
        + "max_subs, status, end_client_id, mime_type, file_name, jo_file) "
        + "VALUES (:joborder_number, :jo_date, :date_created, :title, "
        + ":location, :zip_code, :division, :start_date, :end_date, "
        + ":bill_rate_start, :bill_rate_end, :bill_period_rate::rate, "
        + ":pay_rate_start, :pay_rate_end, :pay_period_rate::rate, :openings, :max_subs, :status, "
        + ":end_client_id, :mime_type, :file_name, :jo_file::bytea)";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("joborder_number", jobOrder.getNumber());
    params.put("jo_date", jobOrder.getJoDate());
    params.put("date_created", LocalDate.now());
    params.put("title", jobOrder.getTitle());
    params.put("location", jobOrder.getLocation());
    params.put("zip_code", jobOrder.getZipCode());
    params.put("division", jobOrder.getDivision());
    params.put("start_date", jobOrder.getStartDate());
    params.put("end_date", jobOrder.getEndDate());
    params.put("bill_rate_start", jobOrder.getBillRateStart());
    params.put("bill_rate_end", jobOrder.getBillRateEnd());
    params.put("bill_period_rate", jobOrder.getBillPeriodRate());
    params.put("pay_rate_start", jobOrder.getPayRateStart());
    params.put("pay_rate_end", jobOrder.getPayRateEnd());
    params.put("pay_period_rate", jobOrder.getPayPeriodRate());
    params.put("openings", jobOrder.getOpenings());
    params.put("max_subs", jobOrder.getMaxSubs());
    params.put("status", jobOrder.getStatus());
    params.put("end_client_id", jobOrder.getEndClient().getId());
    
    if (jobOrder.getFile() == null || jobOrder.getFile().length < 1) {
      params.put("mime_type", null);
      params.put("file_name", null);
      params.put("jo_file", null);
    } else {
      params.put("mime_type", jobOrder.getMimeType());
      params.put("file_name", jobOrder.getFileName());
      params.put("jo_file", jobOrder.getFile());
    }
    
    SqlParameterSource namedParameters = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, namedParameters, keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }

  @Override
  public Integer update(final JobOrder jobOrder) {
    
    String sql = "UPDATE job_order set jo_number = :joborder_number, jo_date = :jo_date, "
        + "title = :title, location = :location, zip_code = :zip_code, "
        + "division = :division, start_date = :start_date, end_date = :end_date, "
        + "bill_rate_start = :bill_rate_start, bill_rate_end = :bill_rate_end, "
        + "bill_period_rate = :bill_period_rate::rate, "
        + "pay_rate_start = :pay_rate_start, pay_rate_end = :pay_rate_end, "
        + "pay_period_rate = :pay_period_rate::rate, "
        + "openings = :openings, max_subs = :max_subs, status = :status ";
    
    if (jobOrder.getFile() != null && jobOrder.getFile().length > 0) {
      sql += ", mime_type = :mime_type, file_name = :file_name, jo_file = :jo_file::bytea ";
    }
    sql += "WHERE id = :id";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("id", jobOrder.getId().get());
    params.put("joborder_number", jobOrder.getNumber());
    params.put("jo_date", jobOrder.getJoDate());
    params.put("title", jobOrder.getTitle());
    params.put("location", jobOrder.getLocation());
    params.put("zip_code", jobOrder.getZipCode());
    params.put("division", jobOrder.getDivision());
    params.put("start_date", jobOrder.getStartDate());
    params.put("end_date", jobOrder.getEndDate());
    params.put("bill_rate_start", jobOrder.getBillRateStart());
    params.put("bill_rate_end", jobOrder.getBillRateEnd());
    params.put("bill_period_rate", jobOrder.getBillPeriodRate());
    params.put("pay_rate_start", jobOrder.getPayRateStart());
    params.put("pay_rate_end", jobOrder.getPayRateEnd());
    params.put("pay_period_rate", jobOrder.getPayPeriodRate());
    params.put("openings", jobOrder.getOpenings());
    params.put("max_subs", jobOrder.getMaxSubs());
    params.put("status", jobOrder.getStatus());
    
    if (jobOrder.getFile() != null && jobOrder.getFile().length > 0) {
      params.put("mime_type", jobOrder.getMimeType());
      params.put("file_name", jobOrder.getFileName());
      params.put("jo_file", jobOrder.getFile());
    }
    
    SqlParameterSource namedParameters = new MapSqlParameterSource(params);
    jdbcTemplate.update(sql, namedParameters);
    
    return jobOrder.getId().get();
  }

  @Override
  public List<JobOrder> findAllByUserId(Integer userId) {
    
    String sql = "SELECT jo.id AS jo_id, jo_number, "
        + "jo_date, date_created, title, location, zip_code, "
        + "division, start_date, end_date, bill_rate_start, bill_rate_end, bill_period_rate,"
        + "pay_rate_start, pay_rate_end, pay_period_rate, openings, max_subs, status,"
        + "ec.id AS ec_id, ec.name AS ec_name, "
        + "u.id AS u_id, u.firstname, u.lastname, u.username, ujo.primary_user, "
        + "OCTET_LENGTH(jo_file) AS file_length "
        + "FROM job_order jo INNER JOIN end_client ec ON jo.end_client_id = ec.id "
        + "INNER JOIN user_job_order ujo "
        + "ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE "
        + "INNER JOIN users u ON u.id = ujo.users_id ";
    
    if (userId != null) {
      sql += "WHERE ujo.users_id = :users_id ";
    }
    
    sql += "ORDER BY jo_number ASC LIMIT " + MAX_ROW_LIMIT;
    
    List<JobOrder> results = jdbcTemplate.query(sql, 
        new MapSqlParameterSource("users_id", userId), (res, rowNum) -> {
          JobOrder jobOrder = new JobOrder();
          jobOrder.setId(res.getInt("jo_id"));
          jobOrder.setNumber(res.getString("jo_number"));
          jobOrder.setJoDate(res.getDate("jo_date").toLocalDate());
          jobOrder.setDateCreated(res.getDate("date_created").toLocalDate());
          jobOrder.setTitle(res.getString("title"));
          jobOrder.setLocation(res.getString("location"));
          jobOrder.setZipCode(res.getString("zip_code"));
          jobOrder.setDivision(res.getString("division"));
          jobOrder.setStartDate(res.getDate("start_date").toLocalDate());
          jobOrder.setEndDate(res.getDate("end_date").toLocalDate());
          jobOrder.setBillRateStart(res.getDouble("bill_rate_start"));
          jobOrder.setBillRateEnd(res.getDouble("bill_rate_end"));
          jobOrder.setBillPeriodRate(res.getString("bill_period_rate"));
          jobOrder.setPayRateStart(res.getDouble("pay_rate_start"));
          jobOrder.setPayRateEnd(res.getDouble("pay_rate_end"));
          jobOrder.setPayPeriodRate(res.getString("pay_period_rate"));
          jobOrder.setOpenings(res.getString("openings"));
          jobOrder.setMaxSubs(res.getString("max_subs"));
          jobOrder.setStatus(res.getString("status"));
          jobOrder.setHasFile((res.getInt("file_length") > 0) ? true : false);
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jobOrder.setEndClient(ec);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          user.setUsername(res.getString("username"));

          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setUser(user);
          userJobOrder.setIsPrimary(res.getBoolean("primary_user"));
          userJobOrder.setJobOrder(jobOrder);

          jobOrder.setPrimaryOwner(userJobOrder);
          
          return jobOrder;
        });
        
    return results;
  }
  
  @Override
  public List<JobOrder> findAllByUserIdAndClientId(Integer userId, Integer clientId) {
    
    final String sql = "SELECT jo.id AS jo_id, jo_number, "
        + "jo_date, date_created, title, location, zip_code, "
        + "division, start_date, end_date, bill_rate_start, bill_rate_end, bill_period_rate,"
        + "pay_rate_start, pay_rate_end, pay_period_rate, openings, max_subs, status,"
        + "ec.id AS ec_id, ec.name AS ec_name, "
        + "u.id AS u_id, u.firstname, u.lastname, u.username, ujo.primary_user, "
        + "OCTET_LENGTH(jo_file) AS file_length "
        + "FROM job_order jo INNER JOIN end_client ec ON jo.end_client_id = ec.id "
        + "INNER JOIN user_job_order ujo "
        + "ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE "
        + "INNER JOIN users u ON u.id = ujo.users_id "
        + "INNER JOIN client cl ON cl.id = ec.client_id "
        + "WHERE ujo.users_id = :users_id AND cl.id = :cl_id "
        + "ORDER BY jo_number ASC LIMIT " + MAX_ROW_LIMIT;
    
    Map<String, Object> params = new HashMap<>();
    params.put("users_id", userId);
    params.put("cl_id", clientId);
    
    List<JobOrder> results = jdbcTemplate.query(sql, 
        new MapSqlParameterSource(params), (res, rowNum) -> {
          JobOrder jobOrder = new JobOrder();
          jobOrder.setId(res.getInt("jo_id"));
          jobOrder.setNumber(res.getString("jo_number"));
          jobOrder.setJoDate(res.getDate("jo_date").toLocalDate());
          jobOrder.setDateCreated(res.getDate("date_created").toLocalDate());
          jobOrder.setTitle(res.getString("title"));
          jobOrder.setLocation(res.getString("location"));
          jobOrder.setZipCode(res.getString("zip_code"));
          jobOrder.setDivision(res.getString("division"));
          jobOrder.setStartDate(res.getDate("start_date").toLocalDate());
          jobOrder.setEndDate(res.getDate("end_date").toLocalDate());
          jobOrder.setBillRateStart(res.getDouble("bill_rate_start"));
          jobOrder.setBillRateEnd(res.getDouble("bill_rate_end"));
          jobOrder.setBillPeriodRate(res.getString("bill_period_rate"));
          jobOrder.setPayRateStart(res.getDouble("pay_rate_start"));
          jobOrder.setPayRateEnd(res.getDouble("pay_rate_end"));
          jobOrder.setPayPeriodRate(res.getString("pay_period_rate"));
          jobOrder.setOpenings(res.getString("openings"));
          jobOrder.setMaxSubs(res.getString("max_subs"));
          jobOrder.setStatus(res.getString("status"));
          jobOrder.setHasFile((res.getInt("file_length") > 0) ? true : false);
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jobOrder.setEndClient(ec);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          user.setUsername(res.getString("username"));

          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setUser(user);
          userJobOrder.setIsPrimary(res.getBoolean("primary_user"));
          userJobOrder.setJobOrder(jobOrder);

          jobOrder.setPrimaryOwner(userJobOrder);
          
          return jobOrder;
        });
        
    return results;
  }
  
  @Override
  public List<JobOrder> findByCriteria(Integer userId, String joNumber, 
      String title, Integer clientId) {
    
    final String sqlString = 
        "SELECT jo.id AS jo_id, jo_number, jo_date, date_created, title, location, zip_code, "
        + "division, start_date, end_date, bill_rate_start, bill_rate_end, bill_period_rate, "
        + "pay_rate_start, pay_rate_end, pay_period_rate, openings, max_subs, status, "
        + "ec.id AS ec_id, ec.name AS ec_name, "
        + "u.id AS u_id, u.firstname, u.lastname, u.username, ujo.primary_user, "
        + "OCTET_LENGTH(jo_file) AS file_length "
        + "FROM job_order jo INNER JOIN end_client ec ON jo.end_client_id = ec.id "
        + "INNER JOIN user_job_order ujo "
        + "ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE "
        + "INNER JOIN users u ON u.id = ujo.users_id "
        + "INNER JOIN client cl ON cl.id = ec.client_id "
        + "WHERE ";

    final StringBuilder sql = new StringBuilder(sqlString);

    Map<String,Object> params = new HashMap<String,Object>();

    if (joNumber == null && title == null) {
      throw new IllegalStateException("JO number and title can't be both null");
    } else if (joNumber != null && title == null) {
      sql.append("jo_number = :jo_number");
      params.put("jo_number", joNumber);
    } else if (joNumber == null && title != null) {
      sql.append("LOWER(title) LIKE LOWER(:title) ");
      params.put("title", title.toLowerCase() + "%");
    } else if (joNumber != null && title != null) {
      sql.append("(jo_number = :jo_number OR ");
      sql.append("LOWER(title) LIKE LOWER(:title)) ");
      params.put("jo_number", joNumber);
      params.put("title", title.toLowerCase() + "%");
    }
    
    if (userId != null && clientId != null) {
      sql.append(" AND ujo.users_id = :users_id ");
      sql.append(" AND cl.id = :cl_id ");
      params.put("users_id", userId);
      params.put("cl_id", clientId);
    }
    
    List<JobOrder> jobOrders = jdbcTemplate.query(
        sql.toString(), params, (res, rowNum) -> {
          JobOrder jo = new JobOrder();
          jo.setId(res.getInt("jo_id"));
          jo.setNumber(res.getString("jo_number"));
          jo.setJoDate(res.getDate("jo_date").toLocalDate());
          jo.setDateCreated(res.getDate("date_created").toLocalDate());
          jo.setTitle(res.getString("title"));
          jo.setLocation(res.getString("location"));
          jo.setZipCode(res.getString("zip_code"));
          jo.setDivision(res.getString("division"));
          jo.setStartDate(res.getDate("start_date").toLocalDate());
          jo.setEndDate(res.getDate("end_date").toLocalDate());
          jo.setBillRateStart(res.getDouble("bill_rate_start"));
          jo.setBillRateEnd(res.getDouble("bill_rate_end"));
          jo.setBillPeriodRate(res.getString("bill_period_rate"));
          jo.setPayRateStart(res.getDouble("pay_rate_start"));
          jo.setPayRateEnd(res.getDouble("pay_rate_end"));
          jo.setPayPeriodRate(res.getString("pay_period_rate"));
          jo.setOpenings(res.getString("openings"));
          jo.setMaxSubs(res.getString("max_subs"));
          jo.setStatus(res.getString("status"));
          jo.setHasFile((res.getInt("file_length") > 0) ? true : false);
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jo.setEndClient(ec);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          user.setUsername(res.getString("username"));

          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setUser(user);
          userJobOrder.setIsPrimary(res.getBoolean("primary_user"));
          userJobOrder.setJobOrder(jo);

          jo.setPrimaryOwner(userJobOrder);
          
          return jo;
        });
    
    return jobOrders;
  }
  
  @Override
  public Integer findCountByUserAndDateRange(Integer userId, Integer teamId, LocalDate dateFrom,
      LocalDate dateTo) {
    final String sql = "SELECT COUNT(*) FROM job_order jo "
        + "INNER JOIN user_job_order ujo ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE "
        + "INNER JOIN end_client ec ON jo.end_client_id = ec.id "
        + "INNER JOIN client cl ON ec.client_id = cl.id "
        + "INNER JOIN team t ON cl.team_id = t.id "
        + "WHERE ujo.users_id = :userId "
        + (teamId != null ? "AND t.id = :team_id " : "")
        + "AND jo_date >= :dateFrom AND jo_date <= :dateTo ";
    
    Map<String, Object> params = new HashMap<>();
    params.put("userId", userId);
    params.put("dateFrom", dateFrom);
    params.put("dateTo", dateTo);
    if (teamId != null) {
      params.put("team_id", teamId);
    }
    
    return jdbcTemplate.queryForObject(sql, params, Integer.class);
  }
  
  @Override
  public List<JobOrder> findByUserAndDateRange(Integer userId, Integer teamId, LocalDate dateFrom,
      LocalDate dateTo) {
    
    final String sql = "SELECT jo.id AS jo_id, jo_number, "
        + "jo_date, date_created, title, location, zip_code, "
        + "division, start_date, end_date, bill_rate_start, bill_rate_end, bill_period_rate,"
        + "pay_rate_start, pay_rate_end, pay_period_rate, openings, max_subs, status,"
        + "ec.id AS ec_id, ec.name AS ec_name, "
        + "u.id AS u_id, u.firstname, u.lastname, u.username, ujo.primary_user, "
        + "OCTET_LENGTH(jo_file) AS file_length "
        + "FROM job_order jo INNER JOIN end_client ec ON jo.end_client_id = ec.id "
        + "INNER JOIN user_job_order ujo "
        + "ON jo.id = ujo.job_order_id AND ujo.primary_user = TRUE "
        + "INNER JOIN client cl ON ec.client_id = cl.id "
        + "INNER JOIN team t ON cl.team_id = t.id "
        + "INNER JOIN users u ON u.id = ujo.users_id "
        + "WHERE ujo.users_id = :userId "
        + (teamId != null ? "AND t.id = :team_id " : "")
        + "AND jo_date >= :dateFrom AND jo_date <= :dateTo "
        + "ORDER BY jo_number ASC LIMIT " + MAX_ROW_LIMIT;
    
    Map<String, Object> params = new HashMap<>();
    params.put("userId", userId);
    params.put("dateFrom", dateFrom);
    params.put("dateTo", dateTo);
    
    if (teamId != null) {
      params.put("team_id", teamId);
    }
    
    List<JobOrder> results = jdbcTemplate.query(sql, 
        params, (res, rowNum) -> {
          JobOrder jobOrder = new JobOrder();
          jobOrder.setId(res.getInt("jo_id"));
          jobOrder.setNumber(res.getString("jo_number"));
          jobOrder.setJoDate(res.getDate("jo_date").toLocalDate());
          jobOrder.setDateCreated(res.getDate("date_created").toLocalDate());
          jobOrder.setTitle(res.getString("title"));
          jobOrder.setLocation(res.getString("location"));
          jobOrder.setZipCode(res.getString("zip_code"));
          jobOrder.setDivision(res.getString("division"));
          jobOrder.setStartDate(res.getDate("start_date").toLocalDate());
          jobOrder.setEndDate(res.getDate("end_date").toLocalDate());
          jobOrder.setBillRateStart(res.getDouble("bill_rate_start"));
          jobOrder.setBillRateEnd(res.getDouble("bill_rate_end"));
          jobOrder.setBillPeriodRate(res.getString("bill_period_rate"));
          jobOrder.setPayRateStart(res.getDouble("pay_rate_start"));
          jobOrder.setPayRateEnd(res.getDouble("pay_rate_end"));
          jobOrder.setPayPeriodRate(res.getString("pay_period_rate"));
          jobOrder.setOpenings(res.getString("openings"));
          jobOrder.setMaxSubs(res.getString("max_subs"));
          jobOrder.setStatus(res.getString("status"));
          jobOrder.setHasFile((res.getInt("file_length") > 0) ? true : false);
          
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          jobOrder.setEndClient(ec);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          user.setUsername(res.getString("username"));

          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setUser(user);
          userJobOrder.setIsPrimary(res.getBoolean("primary_user"));
          userJobOrder.setJobOrder(jobOrder);

          jobOrder.setPrimaryOwner(userJobOrder);
          
          return jobOrder;
        });
        
    return results;
  }
}
