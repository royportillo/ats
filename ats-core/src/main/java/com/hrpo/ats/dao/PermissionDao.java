package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.Permission;

public interface PermissionDao {

  /**
   * Returns {@code Permission} of the username.
   * This API is used in relation to Spring framework security.
   * 
   * @param username the username.
   * 
   * @return {@code Permission} details of the username in relation to its roles.
   */
  List<Permission> findByUsername(String username);
}
