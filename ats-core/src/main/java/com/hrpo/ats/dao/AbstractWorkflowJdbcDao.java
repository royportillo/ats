package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;

public abstract class AbstractWorkflowJdbcDao<T extends AbstractWorkflowEvent> 
    extends AbstractJdbcDao implements WorkflowDao<T> {
  
  public AbstractWorkflowJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }

  @Override
  public T findByPrimaryKey(final Integer eventId) {

    String sql = "SELECT * FROM " + getWorkflowTableName() + " WHERE id = :id";

    @SuppressWarnings("unchecked")
    T event = (T) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("id", eventId), (res, rowNum) -> {
          
          AbstractWorkflowEvent e0 = getEvent();
          e0.setId(res.getInt("id"));
          e0.setTransactionDate(res.getObject("txn_date", LocalDate.class));
          e0.setRemarks(res.getString("remarks"));
          return e0;
        });

    return event;
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public T findByJobOrderCandidateId(Integer jocId) {
    String sql = "SELECT * FROM " + getWorkflowTableName() + " WHERE job_order_candidate_id = :id";

    T event;
    try {
      event = (T) jdbcTemplate.queryForObject(
          sql, new MapSqlParameterSource("id", jocId), (res, rowNum) -> {
            
            AbstractWorkflowEvent e0 = getEvent();
            e0.setId(res.getInt("id"));
            e0.setTransactionDate(res.getObject("txn_date", LocalDate.class));
            e0.setRemarks(res.getString("remarks"));
            return e0;
          });
    } catch (EmptyResultDataAccessException erdae) {
      return null;
    }

    return event;
  }
  
  @Override
  public Integer upsert(AbstractWorkflowEvent workflowEvent, 
      final Integer jobOrderCandidateId) {
    
    final String insertSql = "INSERT INTO " + getWorkflowTableName() 
        + "(txn_date, job_order_candidate_id, remarks) "
        + "VALUES (:txn_date, :job_order_candidate_id, :remarks) "
        + "ON CONFLICT (job_order_candidate_id) DO UPDATE SET txn_date = excluded.txn_date, "
        + "remarks = excluded.remarks";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("txn_date", workflowEvent.getTransactionDate());
    params.put("job_order_candidate_id", jobOrderCandidateId);
    params.put("remarks", workflowEvent.getRemarks());
    
    SqlParameterSource namedParameters = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(insertSql, namedParameters, keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public Integer findCountByUserAndDateRange(final Integer loggedUserId, final Integer teamId,
      final LocalDate dateFrom, final LocalDate dateTo) {

    final String sql = "SELECT count(*) FROM " + getWorkflowTableName() + " wf "  
        + "INNER JOIN job_order_candidate joc on wf.job_order_candidate_id = joc.id "
        + "INNER JOIN users u on u.id = joc.users_id "
        + "INNER JOIN job_order jo on joc.job_order_id = jo.id "
        + "INNER JOIN end_client ec on jo.end_client_id = ec.id "
        + "INNER JOIN client cl on ec.client_id = cl.id "
        + "INNER JOIN team t on cl.team_id = t.id "
        + "INNER JOIN team tu on u.team_id = tu.id "
        + "WHERE u.id = :users_id " + (teamId != null ? "AND t.id = :team_id " : "")
        + "AND txn_date >= :dateFrom AND txn_date <= :dateTo";

    Map<String,Object> params = new HashMap<String,Object>();
    params.put("users_id", loggedUserId);
    params.put("dateFrom", dateFrom);
    params.put("dateTo", dateTo);
    if (teamId != null) {
      params.put("team_id", teamId);
    }

    return jdbcTemplate.queryForObject(sql, params, Integer.class);
  }
  
  @Override
  public Boolean findStateByJobOrderCandidateId(Integer jobOrderCandidateId) {
    
    final String sql = "SELECT wf.id FROM " + getWorkflowTableName() + " wf "  
        + "WHERE wf.job_order_candidate_id = :jobOrderCandidateId";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("jobOrderCandidateId", jobOrderCandidateId);
    
    Integer eventId = null;
    try {
      eventId = jdbcTemplate.queryForObject(sql, params, Integer.class);
    } catch (EmptyResultDataAccessException ex) {
      // no-op;
    }
    
    return eventId != null ? true : false;
  }
  
  @Override
  public void deleteByJobOrderCandidateId(Integer jobOrderCandidateId) {
    final String sql = "DELETE from " + getWorkflowTableName() + " wf "
        + "WHERE wf.job_order_candidate_id = :jobOrderCandidateId";
    
    jdbcTemplate.update(sql, new MapSqlParameterSource("jobOrderCandidateId", jobOrderCandidateId));
  }
  
  /**
   * Gets the table name associated to the subclass of {@code AbstractWorkflowJdbcDao}.
   * 
   * @return the table name of the workflow event.
   */
  abstract String getWorkflowTableName();
  
  /**
   * Gets the workflow event associated to the subclass of {@code AbstractWorkflowJdbcDao}.
   * 
   * @return the event associated to this subclass.
   */
  abstract T getEvent();
}
