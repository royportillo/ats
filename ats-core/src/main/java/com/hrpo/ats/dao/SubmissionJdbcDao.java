package com.hrpo.ats.dao;

import javax.sql.DataSource;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.event.SubmissionEvent;

@Repository("submissionDao")
public class SubmissionJdbcDao extends AbstractWorkflowJdbcDao<SubmissionEvent> {

  public SubmissionJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  String getWorkflowTableName() {
    return "submission_event";
  }

  @Override
  SubmissionEvent getEvent() {
    return new SubmissionEvent();
  }
}
