package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.List;
import com.hrpo.ats.dto.EodReport;

public interface EodReportDao {

  /**
   * Inserts EOD report.
   * 
   * @param eodReport the {@code EodReport}.
   * @param userId The ID of the owning user.
   * @return The row id.
   */
  Integer insert(final EodReport eodReport, final Integer userId);
  
  /**
   * Updates EOD report.
   * @param eodReport the {@code EodReport} to update.
   * @param userId The ID of the owning user.
   * @return The row id of the updated {@code EodReport}.
   */
  Integer update(final EodReport eodReport, final Integer userId);
  
  /**
   * Finds {@code EodReport} by id and owner's user id.
   * 
   * @param id The id of the Eod Report.
   * @param userid The id of the report's owner.
   * @return the {@code EodReport}.
   */
  EodReport findByIdAndUserId(final Integer id, final Integer userId);
  
  /**
   * Finds {@code EodReport}s later than or equal to specified date range.
   * @param fromDate The EOD report date range lower bound
   * @param toDate The EOD report date range upper bound
   * @param userId The ID of the EOD report's owner.
   * @return The {@code EodReport}.
   */
  List<EodReport> findByReportDate(final LocalDate fromDate, final LocalDate toDate, 
      final Integer userId);
}
