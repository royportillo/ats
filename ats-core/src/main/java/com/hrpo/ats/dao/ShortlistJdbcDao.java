package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Shortlist;

@Repository
public class ShortlistJdbcDao extends AbstractJdbcDao implements ShortlistDao {

  Logger log = LoggerFactory.getLogger(ShortlistJdbcDao.class);

  public ShortlistJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public Shortlist findByPrimaryKey(final Integer shortlistId) {

    String sql = "SELECT * FROM shortlist WHERE id = :id";

    Shortlist shortlist = (Shortlist) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("id", shortlistId), (res, rowNum) -> {
          return new Shortlist(res.getString("name"));
        });

    return shortlist;
  }

  @Override
  public Integer insert(final Shortlist shortlistName, final Integer loggedUserId) {
    
    final String insertSql = "INSERT INTO shortlist (name, users_id) "
        + "VALUES (:shortlist_name, :users_id)";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("shortlist_name", shortlistName.getName());
    params.put("users_id", loggedUserId);
    
    SqlParameterSource namedParameters = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(insertSql, namedParameters, keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }
}
