package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;

@Repository
public class UserJdbcDao extends AbstractJdbcDao implements UserDao {

  Logger log = LoggerFactory.getLogger(UserJdbcDao.class);

  public UserJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public Integer insert(User user) {
    final String sql = "INSERT INTO USERS "
        + "(firstname, lastname, email, username, password, enabled, team_id) "
        + "VALUES (:firstname, :lastname, :email, :username, :password, :enabled, :team_id)";
    
    Map<String, Object> params = new HashMap<>();
    
    params.put("firstname", user.getFirstName());
    params.put("lastname", user.getLastName());
    params.put("email", user.getEmail());
    params.put("enabled", user.isEnabled());
    params.put("username", user.getUsername());
    params.put("password", user.getPassword());
    params.put("team_id", user.getTeam().getId());
    
    SqlParameterSource sqlParams = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, sqlParams, keyHolder, new String[] {"id"});
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public Integer update(User user) {
    String sql = "UPDATE USERS "
        + "SET firstname = :firstname, lastname = :lastname, "
        + "username = :username, enabled = :enabled, team_id = :team_id, email = :email ";
    if (user.getPassword() != null) {
      sql += ", password = :password ";
    }
    sql +=  "WHERE id = :id";
    
    Map<String, Object> params = new HashMap<>();
    
    params.put("id", user.getId());
    params.put("firstname", user.getFirstName());
    params.put("lastname", user.getLastName());
    params.put("email", user.getEmail());
    params.put("username", user.getUsername());
    params.put("enabled", user.isEnabled()); 
    params.put("team_id", user.getTeam().getId());
    if (user.getPassword() != null) {
      params.put("password", user.getPassword());
    }
    
    SqlParameterSource sqlParams = new MapSqlParameterSource(params);
    jdbcTemplate.update(sql, sqlParams);
    
    return user.getId();
  }

  @Override 
  public Integer findIdByUsername(String username) {
    
    String sql = "SELECT id FROM users WHERE username = :username";
    
    return this.jdbcTemplate.queryForObject(sql, 
        new MapSqlParameterSource("username", username), Integer.class);
  }
  
  @Override 
  public User findByUsername(String username) {
    
    String sql = "SELECT ur.id AS ur_id, firstname, lastname, ur.email AS ur_email, username, "
        + "password, enabled, "
        + "tm.id AS tm_id, tm.name AS tm_name, tm.email AS tm_email "
        + "FROM users ur INNER JOIN team tm ON ur.team_id = tm.id "
        + "WHERE username = :username";
    
    User user = this.jdbcTemplate.queryForObject(sql, 
        new MapSqlParameterSource("username", username), (res, rowNum) -> {
          
          User u0 = new User();
          u0.setId(res.getInt("ur_id"));
          u0.setFirstName(res.getString("firstname"));
          u0.setLastName(res.getString("lastname"));
          u0.setEmail(res.getString("ur_email"));
          u0.setUsername(res.getString("username"));
          u0.setPassword(res.getString("password"));
          u0.setEnabled(res.getBoolean("enabled"));
          
          Team tm = new Team();
          tm.setId(res.getInt("tm_id"));
          tm.setName(res.getString("tm_name"));
          tm.setEmail(res.getString("tm_email"));
          u0.setTeam(tm);
          
          return u0;
        });
    
    return user;
  }
  
  @Override
  public User findById(Integer id) {
    String sql = "SELECT ur.id AS ur_id, username, firstname, ur.email AS ur_email, lastname, "
        + "password, enabled, "
        + "tm.id AS tm_id, tm.name AS tm_name, tm.email AS tm_email "
        + "FROM users ur INNER JOIN team tm ON ur.team_id = tm.id "
        + "WHERE ur.id = :ur_id";
    
    User user = this.jdbcTemplate.queryForObject(sql, 
        new MapSqlParameterSource("ur_id", id), (res, rowNum) -> {
          
          User u0 = new User();
          u0.setId(res.getInt("ur_id"));
          u0.setFirstName(res.getString("firstname"));
          u0.setLastName(res.getString("lastname"));
          u0.setEmail(res.getString("ur_email"));
          u0.setUsername(res.getString("username"));
          u0.setPassword(res.getString("password"));
          u0.setEnabled(res.getBoolean("enabled"));
          
          Team tm = new Team();
          tm.setId(res.getInt("tm_id"));
          tm.setName(res.getString("tm_name"));
          tm.setEmail(res.getString("tm_email"));
          u0.setTeam(tm);
          
          return u0;
        });
    
    return user;
  }
  
  @Override 
  public List<User> findAllUsers(Boolean isEnabledOnly) {

    String sql = "SELECT ur.id AS ur_id, firstname, lastname, ur.email AS ur_email, username, "
        + "password, enabled, "
        + "tm.id AS tm_id, tm.name AS tm_name, tm.email AS tm_email "
        + "FROM users ur INNER JOIN team tm ON ur.team_id = tm.id ";
    if (isEnabledOnly) {
      sql += "WHERE enabled = true ";
    }
    sql += "ORDER BY username ASC ";

    List<User> results = jdbcTemplate.query(sql, 
        new HashMap<String,String>(), (res, rowNum) -> {
          User user = new User();
          user.setId(res.getInt("ur_id"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          user.setEmail(res.getString("ur_email"));
          user.setUsername(res.getString("username"));
          user.setEnabled(res.getBoolean("enabled"));
          
          Team tm = new Team();
          tm.setId(res.getInt("tm_id"));
          tm.setName(res.getString("tm_name"));
          tm.setEmail(res.getString("tm_email"));
          user.setTeam(tm);
          
          return user;
        });

    return results;
  }
  
  @Override
  public List<User> findAllUsersByTeam(Integer teamId, Boolean isEnabledOnly) {
    String sql = "SELECT ur.id AS ur_id, firstname, lastname, username, ur.email AS ur_email, "
        + "password, enabled, "
        + "tm.id AS tm_id, tm.name AS tm_name, tm.email AS tm_email "
        + "FROM users ur INNER JOIN team tm ON ur.team_id = tm.id "
        + "WHERE tm.id = :id ";
    
    if (isEnabledOnly) {
      sql += "AND enabled = true ";
    }
    
    sql += "ORDER BY username ASC ";

    List<User> results = jdbcTemplate.query(sql, 
        new MapSqlParameterSource("id", teamId), (res, rowNum) -> {
          User user = new User();
          user.setId(res.getInt("ur_id"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          user.setEmail(res.getString("ur_email"));
          user.setUsername(res.getString("username"));
          user.setEnabled(res.getBoolean("enabled"));
          
          Team tm = new Team();
          tm.setId(res.getInt("tm_id"));
          tm.setName(res.getString("tm_name"));
          tm.setEmail(res.getString("tm_email"));
          user.setTeam(tm);
          
          return user;
        });

    return results;
  }
}
