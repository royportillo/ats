package com.hrpo.ats.dao;

import java.util.List;
import com.hrpo.ats.dto.UserRole;

public interface UserRoleDao {

  /**
   * Inserts new {@code UserRole}.
   * @param userRole the {@code UserRole} to insert.
   * @return the primary key of the new {@code UserRole}.
   */
  Integer insert(UserRole userRole);
  
  /**
   * Finds {@code UserRole} by id.
   * @param id the ID of the {@code UserRole}
   * @return the {@code UserRole}.
   */
  UserRole findByPrimaryKey(Integer id);
  
  /**
   * Inserts {@code UserRole}s by batch.
   * @param userId The id of the user.
   * @param userRoles The list of {@code UserRole}s to insert.
   */
  void batchInsert(Integer userId, List<UserRole> userRoles);
  
  /**
   * Finds {@code UserRole}s by user id.
   * @param userId The id of the user.
   * @return The list of {@code UserRole}s.
   */
  List<UserRole> findByUserId(Integer userId);
  
  /**
   * Deletes {@code UserRole}s by user id.
   * @param userId The id of the user.
   */
  void deleteByUserId(Integer userId);
  
}
