package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.daxtra.dto.Candidate;

@Repository
public class CandidateJdbcDao extends AbstractJdbcDao implements CandidateDao {

  Logger log = LoggerFactory.getLogger(CandidateJdbcDao.class);
  
  @SuppressWarnings("unused")
  private static final int MAX_DATABASE_LIMIT = 100;

  public CandidateJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }

  @Override
  public Candidate findByPrimaryKey(Integer candidateId) {
    
    String sql = "SELECT * FROM candidate WHERE id = :id";

    Candidate candidate = (Candidate) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("id", candidateId), (res, rowNum) -> {
          
          Candidate cd = new Candidate();
          cd.setCandidateId(res.getString("candidate_id"));
          cd.setSource(res.getString("source"));
          cd.setFirstName(res.getString("firstname"));
          cd.setLastName(res.getString("lastname"));
          cd.setFullName(res.getString("fullname"));
          cd.setPhone(res.getString("contact_phone"));
          cd.setMobile(res.getString("contact_mobile"));
          cd.setEmail(res.getString("contact_email"));
          return cd;
        });

    return candidate;
  }
  
  @Override
  public Candidate findByCandidateId(String daxtraCandidateId, Boolean hasHrXml) {
    
    String sql = "SELECT * FROM candidate WHERE candidate_id = :candidate_id";

    Candidate candidate = (Candidate) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("candidate_id", daxtraCandidateId), (res, rowNum) -> {
          
          Candidate cd = new Candidate();
          cd.setId(res.getInt("id"));
          cd.setCandidateId(res.getString("candidate_id"));
          cd.setSource(res.getString("source"));
          cd.setFirstName(res.getString("firstname"));
          cd.setLastName(res.getString("lastname"));
          cd.setFullName(res.getString("fullname"));
          cd.setPhone(res.getString("contact_phone"));
          cd.setMobile(res.getString("contact_mobile"));
          cd.setEmail(res.getString("contact_email"));
          
          if (hasHrXml) {
            cd.setHrXml(res.getString("hrxml"));
          }
          
          return cd;
        });

    return candidate;
  }
  
  @Override
  public Integer upsert(final Candidate candidate) {
    
    final String insertSql = "INSERT INTO candidate "
        + "(candidate_id, source, firstname, lastname, fullname, contact_phone, contact_mobile, "
        + "contact_email) "
        + "VALUES (:candidate_id, :source, :firstname, :lastname, :fullname, :contact_phone, "
        + ":contact_mobile, :contact_email) "
        //PostgreSQL-specific clause for 'upsert' - update insert
        + "ON CONFLICT (candidate_id) DO UPDATE SET candidate_id = excluded.candidate_id, "
        + "source = excluded.source, firstname = excluded.firstname, lastname = excluded.lastname, "
        + "fullname = excluded.fullname, contact_phone = excluded.contact_phone, "
        + "contact_mobile = excluded.contact_mobile, "
        + "contact_email = excluded.contact_email";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("candidate_id", candidate.getCandidateId());
    params.put("source", candidate.getSource());
    params.put("firstname", candidate.getFirstName());
    params.put("lastname", candidate.getLastName());
    params.put("fullname", candidate.getFullName());
    params.put("contact_phone", candidate.getPhone());
    params.put("contact_mobile", candidate.getMobile());
    params.put("contact_email", candidate.getEmail());
    
    SqlParameterSource namedParameters = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(insertSql, namedParameters, keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public Integer insertShortlistCandidate(final Integer shortlistId, final Integer candidateId) {
    
    final String insertSql = "INSERT INTO shortlist_candidate "
        + "(shortlist_id, candidate_id) VALUES (:shortlistId, :candidateId)";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("shortlistId", shortlistId);
    params.put("candidateId", candidateId);
    
    SqlParameterSource namedParameters = new MapSqlParameterSource(params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(insertSql, namedParameters, keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }
  
  @Override
  public Integer updateHrXml(String hrXml, String candidateId) {
    
    final String updateSql = "UPDATE candidate SET hrxml = :hrxml "
        + "WHERE candidate_id = :candidate_id";
    
    Map<String,Object> params = new HashMap<String,Object>();
    params.put("hrxml", hrXml);
    params.put("candidate_id", candidateId);
    
    return jdbcTemplate.update(updateSql, params);
  }

  @Override
  public List<Candidate> findByShortlistName(String shortlistName) {
    
    String sql = "SELECT c.id, c.candidate_id, c.source, c.firstname, c.lastname, c.fullname, "
        + "c.contact_phone, c.contact_mobile, c.contact_email "
        + "FROM shortlist s INNER JOIN shortlist_candidate sc "
        + "ON s.id = sc.shortlist_id INNER JOIN candidate c ON c.id = sc.candidate_id "
        + "WHERE s.name = :shortlist_name";
    
    SqlParameterSource namedParameters = 
        new MapSqlParameterSource("shortlist_name", shortlistName);
    
    List<Candidate> results = jdbcTemplate.query(sql, 
        namedParameters, (res, rowNum) -> {
          Candidate candidate = new Candidate();
          candidate.setId(res.getInt("id"));
          candidate.setCandidateId(res.getString("candidate_id"));
          candidate.setSource(res.getString("source"));
          candidate.setFirstName(res.getString("firstname"));
          candidate.setLastName(res.getString("lastname"));
          candidate.setFullName(res.getString("fullname"));
          candidate.setMobile(res.getString("contact_mobile"));
          candidate.setPhone(res.getString("contact_phone"));
          candidate.setEmail(res.getString("contact_email"));
          
          return candidate;
        });
        
    return results;
  }
  
}