package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.UserJobOrder;

@Repository
public class UserJobOrderJdbcDao extends AbstractJdbcDao implements UserJobOrderDao {

  public UserJobOrderJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }
  
  @Override
  public Integer insert(Integer jobOrderId, Integer userId, Boolean isPrimaryOwner) {
    final String sql = "INSERT INTO user_job_order (job_order_id, users_id, primary_user) "
        + "VALUES (:job_order_id, :user_id, :is_primary)";
    
    Map<String,Object> params = new HashMap<>();
    params.put("job_order_id", jobOrderId);
    params.put("user_id", userId);
    params.put("is_primary", isPrimaryOwner);
    
    SqlParameterSource namedParamSource = new MapSqlParameterSource(params);
    KeyHolder holder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, namedParamSource, holder, new String[] { "id" });
    
    return holder.getKey().intValue();
  }

  @Override
  public List<UserJobOrder> findByJobOrderId(Integer jobOrderId) {
    String sql =
        "SELECT ujo.id AS ujo_id, jo.id AS jo_id, jo.jo_number, "
        + "u.id AS u_id, u.username, ujo.primary_user "
        + "FROM user_job_order ujo INNER JOIN job_order jo ON jo.id = ujo.job_order_id "
        + "INNER JOIN users u ON u.id = ujo.users_id WHERE jo.id = :job_order_id";
    
    List<UserJobOrder> userJobOrders = jdbcTemplate.query(
        sql.toString(), new MapSqlParameterSource("job_order_id", jobOrderId), (res, rowNum) -> {
          JobOrder jo = new JobOrder();
          jo.setId(res.getInt("jo_id"));
          jo.setNumber(res.getString("jo_number"));
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setUsername(res.getString("username"));

          UserJobOrder userJobOrder = new UserJobOrder();
          userJobOrder.setId(res.getInt("ujo_id"));
          userJobOrder.setUser(user);
          userJobOrder.setIsPrimary(res.getBoolean("primary_user"));
          userJobOrder.setJobOrder(jo);
          
          return userJobOrder;
        });
        
    return userJobOrders;
  }

}
