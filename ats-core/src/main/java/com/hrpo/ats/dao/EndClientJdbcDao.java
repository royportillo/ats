package com.hrpo.ats.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;

@Repository
public class EndClientJdbcDao extends AbstractJdbcDao implements EndClientDao {
  
  Logger log = LoggerFactory.getLogger(EndClientJdbcDao.class);
  
  public EndClientJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }

  @Override
  public Integer insert(EndClient endClient) {
    final String sql = "INSERT INTO end_client (name, description, client_id) "
        + "VALUES (:name, :description, :client_id)";
    
    Map<String, Object> params = new HashMap<>();
    params.put("name", endClient.getName());
    params.put("description", endClient.getDescription());
    params.put("client_id", endClient.getClient().getClientId());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    KeyHolder holder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, paramSource, holder, new String[] { "id" });
    
    return holder.getKey().intValue();
  }
  
  @Override
  public Integer update(EndClient endClient) {
    final String sql = "UPDATE end_client SET "
        + "name = :name, description = :description, client_id = :client_id "
        + "WHERE id = :id";
    
    Map<String, Object> params = new HashMap<>();
    params.put("id", endClient.getId());
    params.put("name", endClient.getName());
    params.put("description", endClient.getDescription());
    params.put("client_id", endClient.getClient().getClientId());
    
    SqlParameterSource paramSource = new MapSqlParameterSource(params);
    jdbcTemplate.update(sql, paramSource);
    
    return endClient.getId();
  }
  
  @Override
  public EndClient findById(Integer id) {
    final String sql = "SELECT ec.id AS ec_id, ec.name AS ec_name, "
        + "ec.description AS ec_description, cl.id AS cl_id, cl.name AS cl_name, "
        + "cl.description AS cl_description "
        + "FROM end_client ec "
        + "INNER JOIN client cl ON ec.client_id = cl.id "
        + "WHERE ec.id = :id";
    
    EndClient endClient = (EndClient) jdbcTemplate.queryForObject(
        sql, new MapSqlParameterSource("id", id), (res, rowNum) -> {
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          ec.setDescription(res.getString("ec_description"));
          
          Client cl = new Client();
          cl.setClientId(res.getInt("cl_id"));
          cl.setName(res.getString("cl_name"));
          cl.setDescription(res.getString("cl_description"));

          ec.setClient(cl);
          return ec;
        });
    
    return endClient;
  }
  
  @Deprecated
  @Override
  public List<EndClient> findEndClientsByUserId(Integer userId) {
    String sql = "SELECT ec.id, ec.name FROM end_client ec "
        + "INNER JOIN client ct ON ct.id = ec.client_id " 
        + "INNER JOIN team tm ON ct.team_id = tm.id " 
        + "INNER JOIN users ur ON ur.team_id = tm.id " 
        + "WHERE ur.id = :user_id ORDER BY ec.id ASC";
    
    List<EndClient> endClients = jdbcTemplate.query(
        sql, new MapSqlParameterSource("user_id", userId), (res, rowNum) -> {
          EndClient ec = new EndClient();
          ec.setId(res.getInt("id"));
          ec.setName(res.getString("name"));
          
          return ec;
        });
    
    return endClients;
  }
  
  @Override
  public List<EndClient> findEndClientsByClient(Integer clientId) {
    String sql = "SELECT * FROM end_client WHERE client_id = :client_id ORDER BY id ASC";
    
    List<EndClient> endClients = jdbcTemplate.query(
        sql, new MapSqlParameterSource("client_id", clientId), (res, rowNum) -> {
          EndClient ec = new EndClient();
          ec.setId(res.getInt("id"));
          ec.setName(res.getString("name"));
          ec.setDescription(res.getString("description"));
          
          return ec;
        });
    
    return endClients;
  }
  
  @Override
  public List<EndClient> findAll() {
    final String sql = "SELECT ec.id AS ec_id, ec.name AS ec_name, "
        + "ec.description AS ec_description, cl.id AS cl_id, cl.name AS cl_name, "
        + "cl.description AS cl_description "
        + "FROM end_client ec "
        + "INNER JOIN client cl ON ec.client_id = cl.id ";
    
    var endClients = jdbcTemplate.query(
        sql, (res, rowNum) -> {
          EndClient ec = new EndClient();
          ec.setId(res.getInt("ec_id"));
          ec.setName(res.getString("ec_name"));
          ec.setDescription(res.getString("ec_description"));
          
          Client cl = new Client();
          cl.setClientId(res.getInt("cl_id"));
          cl.setName(res.getString("cl_name"));
          cl.setDescription(res.getString("cl_description"));

          ec.setClient(cl);
          return ec;
        });
    
    return endClients;
  }
}
