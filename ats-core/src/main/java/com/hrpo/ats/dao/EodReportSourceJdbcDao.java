package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.Source;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;

@Repository
public class EodReportSourceJdbcDao extends AbstractJdbcDao implements EodReportSourceDao {
  
  public EodReportSourceJdbcDao(DataSource theDataSource) {
    super(theDataSource);
  }

  @Override
  public EodReportSource findById(Integer id) {
    final String sql = "SELECT es.id AS es_id, source_views, source_leads, leads, activity, "
        + "e.id AS e_id, report_date, comment, "
        + "s.id AS s_id, s.name, s.category, "
        + "u.id AS u_id, u.username, u.firstname, u.lastname "
        + "FROM eod_report_source es INNER JOIN eod_report e ON es.eod_report_id = e.id "
        + "INNER JOIN sources s ON es.sources_id = s.id "
        + "INNER JOIN users u ON e.users_id = u.id "
        + "WHERE es.id = :id";
    
    EodReportSource result = jdbcTemplate.queryForObject(sql, new MapSqlParameterSource("id", id),
        (res, rowNum) -> {
          EodReportSource es = new EodReportSource();
          
          es.setId(res.getInt("es_id"));
          es.setSourceViews(res.getInt("source_views"));
          es.setSourceLeads(res.getInt("source_leads"));
          es.setLeads(res.getInt("leads"));
          es.setActivity(res.getInt("activity"));
          
          EodReport er = new EodReport();
          er.setId(res.getInt("e_id"));
          er.setReportDate(res.getDate("report_date").toLocalDate());
          er.setComment(res.getString("comment"));
          es.setEodReport(er);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setUsername(res.getString("username"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          er.setUser(user);
          
          Source so = new Source();
          so.setId(res.getInt("s_id"));
          so.setName(res.getString("name"));
          so.setCategory(res.getString("category"));
          es.setSource(so);
          
          return es;
        });
    
    return result;
  }

  @Override
  public Integer insert(EodReportSource eodReportSource) {
    final String sql = "INSERT INTO eod_report_source "
        + "(eod_report_id, sources_id, "
        + "source_views, source_leads, leads, activity) VALUES "
        + "(:eod_report_id, :sources_id, "
        + ":source_views, :source_leads, :leads, :activity)";
    
    Map<String, Object> params = new HashMap<>();
    params.put("eod_report_id", eodReportSource.getEodReport().getId());
    params.put("sources_id", eodReportSource.getSource().getId());
    params.put("source_views", eodReportSource.getSourceViews());
    params.put("source_leads", eodReportSource.getSourceLeads());
    params.put("leads", eodReportSource.getLeads());
    params.put("activity", eodReportSource.getActivity());
    
    KeyHolder keyHolder = new GeneratedKeyHolder();
    jdbcTemplate.update(sql, new MapSqlParameterSource(params), keyHolder, new String[] { "id" });
    
    return keyHolder.getKey().intValue();
  }

  @Override
  public void batchInsert(Integer eodReportId, List<EodReportSource> eodReportSources) {
    final String sql = "INSERT INTO eod_report_source "
        + "(eod_report_id, sources_id, "
        + "source_views, source_leads, leads, activity) VALUES "
        + "(:eod_report_id, :sources_id, "
        + ":source_views, :source_leads, :leads, :activity)";
    
    SqlParameterSource[] namedParams = new SqlParameterSource[eodReportSources.size()];
    
    for (int i = 0; i < eodReportSources.size(); i++) {
      Map<String, Object> params = new HashMap<>();
      params.put("eod_report_id", eodReportId);
      params.put("sources_id", eodReportSources.get(i).getSource().getId());
      params.put("source_views", eodReportSources.get(i).getSourceViews());
      params.put("source_leads", eodReportSources.get(i).getSourceLeads());
      params.put("leads", eodReportSources.get(i).getLeads());
      params.put("activity", eodReportSources.get(i).getActivity());
      
      namedParams[i] = new MapSqlParameterSource(params);
    }
    
    jdbcTemplate.batchUpdate(sql, namedParams);
    
  }

  @Override
  public List<EodReportSource> findByEodReportId(Integer eodReportId) {
    final String sql = "SELECT es.id AS es_id, source_views, source_leads, leads, activity, "
        + "e.id AS e_id, report_date, comment, "
        + "s.id AS s_id, s.name, s.category, "
        + "u.id AS u_id, u.username, u.firstname, u.lastname "
        + "FROM eod_report_source es INNER JOIN eod_report e ON es.eod_report_id = e.id "
        + "INNER JOIN sources s ON es.sources_id = s.id "
        + "INNER JOIN users u ON e.users_id = u.id "
        + "WHERE e.id = :id ORDER BY s.index ASC, s.name ASC";
    
    List<EodReportSource> result = jdbcTemplate.query(sql, 
        new MapSqlParameterSource("id", eodReportId),
        (res, rowNum) -> {
          EodReportSource es = new EodReportSource();
          
          es.setId(res.getInt("es_id"));
          es.setSourceViews(res.getInt("source_views"));
          es.setSourceLeads(res.getInt("source_leads"));
          es.setLeads(res.getInt("leads"));
          es.setActivity(res.getInt("activity"));
          
          EodReport er = new EodReport();
          er.setId(res.getInt("e_id"));
          er.setReportDate(res.getDate("report_date").toLocalDate());
          er.setComment(res.getString("comment"));
          es.setEodReport(er);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setUsername(res.getString("username"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          er.setUser(user);
          
          Source so = new Source();
          so.setId(res.getInt("s_id"));
          so.setName(res.getString("name"));
          so.setCategory(res.getString("category"));
          es.setSource(so);
          
          return es;
        });
    
    return result;
  }
  
  @Override
  public List<EodReportSource> findByDate(LocalDate dateFrom, LocalDate dateTo) {
    final String sql = "SELECT es.id AS es_id, source_views, source_leads, leads, activity, "
        + "e.id AS e_id, report_date, comment, "
        + "s.id AS s_id, s.name, s.category, "
        + "u.id AS u_id, u.username, u.firstname, u.lastname, "
        + "t.id AS t_id, t.name AS t_name "
        + "FROM eod_report_source es INNER JOIN eod_report e ON es.eod_report_id = e.id "
        + "INNER JOIN sources s ON es.sources_id = s.id "
        + "INNER JOIN users u ON e.users_id = u.id "
        + "INNER JOIN team t ON t.id = u.team_id "
        + "WHERE report_date >= :date_from AND report_date <= :date_to "
        + "ORDER BY report_date ASC, u_id ASC, s.name ASC";
    
    Map<String, Object> params = new HashMap<>();
    
    params.put("date_from", dateFrom);
    params.put("date_to", dateTo);
    
    List<EodReportSource> result = jdbcTemplate.query(sql, 
        params, (res, rowNum) -> {
          EodReportSource es = new EodReportSource();
          
          es.setId(res.getInt("es_id"));
          es.setSourceViews(res.getInt("source_views"));
          es.setSourceLeads(res.getInt("source_leads"));
          es.setLeads(res.getInt("leads"));
          es.setActivity(res.getInt("activity"));
          
          EodReport er = new EodReport();
          er.setId(res.getInt("e_id"));
          er.setReportDate(res.getDate("report_date").toLocalDate());
          er.setComment(res.getString("comment"));
          es.setEodReport(er);
          
          User user = new User();
          user.setId(res.getInt("u_id"));
          user.setUsername(res.getString("username"));
          user.setFirstName(res.getString("firstname"));
          user.setLastName(res.getString("lastname"));
          er.setUser(user);
          
          Team team = new Team();
          team.setId(res.getInt("t_id"));
          team.setName(res.getString("t_name"));
          user.setTeam(team);
          
          Source so = new Source();
          so.setId(res.getInt("s_id"));
          so.setName(res.getString("name"));
          so.setCategory(res.getString("category"));
          es.setSource(so);
          
          return es;
        });
    
    return result;
  }
  
  @Override
  public void deleteByEodReportId(Integer eodReportId) {
    final String sql = "DELETE from eod_report_source WHERE eod_report_id = :id";
    
    jdbcTemplate.update(sql, new MapSqlParameterSource("id", eodReportId));
  }

}
