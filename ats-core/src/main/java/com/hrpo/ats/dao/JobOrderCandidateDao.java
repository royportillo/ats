package com.hrpo.ats.dao;

import java.time.LocalDate;
import java.util.List;
import com.hrpo.ats.dto.JobOrderCandidate;

public interface JobOrderCandidateDao {
  
  /**
   * Returns {@code JobOrderCandidate} by id.
   * 
   * @param jobOrderCandidateId the id of the job order candidate to be retrieved.
   * 
   * @return the {@code JobOrderCandidate} of the id.
   */
  JobOrderCandidate findByPrimaryKey(Integer jobOrderCandidateId);
  
  /**
   * Inserts job order candidate.
   * 
   * @param joId the id of {@code JobOrder} associated with the job order candidate.
   * @param usersId the user owner of the job order.
   * @param candidateId the ID of the candidate.
   * @param joCandidateInfo the {@code JobOrderCandidate} object which shall contain the initial
   *        job order candidate information.
   * 
   * @return The row id.
   */
  Integer insert(Integer joId, Integer usersId, Integer candidateId, 
      JobOrderCandidate joCandidateInfo);
  
  /**
   * Inserts job order candidate by batch.
   * 
   * @param joId the id of {@code JobOrder} associated with the job order candidates.
   * @param usersId the user owner of the job order.
   * @param candidateIds the list of candidate ID to batch insert.
   * @param joCandidateInfo the {@code JobOrderCandidate} object which shall contain the initial
   *        job order candidate information.
   */
  void batchInsert(Integer joId, Integer usersId, List<Integer> candidateIds, 
      JobOrderCandidate joCandidateInfo);
  
  /**
   * Gets {@code JobOrderCandidate}s by the job order id associated with it.
   * @param jobOrderId the job order id
   * @param userId the owner of the {@code JobOrderCandidate}s.
   * @return the list of {@code JobOrderCandidates} related to the job order.
   */
  List<JobOrderCandidate> getByJobOrderId(Integer jobOrderId, Integer userId);
  
  /**
   * Gets the workflow states of a {@code JobOrderCandidate}.
   * @param jobOrderCandidate the job order candidate.
   * @return the {@code WorkflowStage} and its corresponding states.
   */
  JobOrderCandidate.WorkflowState getWorkflowStatesByJobOrderCandidate(
      JobOrderCandidate jobOrderCandidate);
  
  /**
   * Returns list of {@code JobOrderCandidate} by workflow event and user ID.
   * 
   * @param userId the ID of user currently logged in.
   * @param teamId the ID of currently logged in user's team.
   * @param stage the workflow stage of the job order candidates to be retrieved.
   * @param fromDate the start date of inclusive date period.
   * @param toDate the end date of inclusive date period.
   * @param isTeamOnly set to true if querying for job order candidates
   *        of the user's current team only.
   * 
   * @return the list of {@code JobOrderCandidate} of the workflow event.
   */
  List<JobOrderCandidate> findByEventWorkflow(Integer userId, Integer teamId, WorkflowStage stage, 
      LocalDate fromDate, LocalDate toDate, Boolean isLastWorkflowOnly);
  
  /**
   * Returns list of {@code JobOrderCandidate} that are not yet processed in any of the workflows.
   * 
   * @param userId userId the ID of user currently logged in.
   * @param fromDate the start date of inclusive date period.
   * @param toDate the end date of inclusive date period.
   * @return the list of {@code JobOrderCandidate}s that are not yet processed.
   */
  List<JobOrderCandidate> findUnprocessed(Integer userId, LocalDate fromDate, LocalDate toDate);
  
  /**
   * Updates {@code JobOrderCandidate}'s workflow stage.
   * 
   * @param jobOrderCandidateId the row ID of job order candidate.
   * @param stage The workflow stage to apply to job order candidate.
   */
  void updateWorkflowStage(JobOrderCandidate jobOrderCandidate, WorkflowStage stage);
}
