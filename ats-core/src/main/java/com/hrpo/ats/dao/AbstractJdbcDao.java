package com.hrpo.ats.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractJdbcDao {

  @Autowired
  NamedParameterJdbcTemplate jdbcTemplate;

  public AbstractJdbcDao(DataSource theDataSource) {
    jdbcTemplate = new NamedParameterJdbcTemplate(theDataSource);
  }
}
