package com.hrpo.ats;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AtsLogoutSuccessHandler implements LogoutSuccessHandler {
    
  Logger log = LoggerFactory.getLogger(AtsLogoutSuccessHandler.class);

  @Override
  public void onLogoutSuccess(HttpServletRequest request, 
      HttpServletResponse response, Authentication authentication)
      throws IOException, ServletException {
    
    // invalidate session.
    request.getSession().invalidate();
    response.sendRedirect("/login");
  }

}
