package com.hrpo.ats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import com.hrpo.ats.service.UserService;

@Configuration
@EnableWebSecurity
public class AtsSecurityConfig extends WebSecurityConfigurerAdapter {
  
  @Autowired
  private UserService userService;
  
  @Autowired
  private AtsLoginSuccessHandler atsLoginHandler;
  
  @Autowired
  private AtsLogoutSuccessHandler atsLogoutHandler;
  
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
    .authorizeRequests()
    .antMatchers("/images/**", "/css/**", "/webjars/**").permitAll()
    .antMatchers("/dashboard").hasAuthority("app.dashboard.view")
    .antMatchers("/shortlist").hasAuthority("app.shortlist.find")
    .antMatchers("/joborder").hasAuthority("app.joborder.edit")
    .antMatchers("/workflow/**").hasAuthority("app.workflow.view")
    .antMatchers("/workflow/delete/**").hasAuthority("app.workflow.edit")
    .antMatchers("/users/**").hasAuthority("app.users.edit")
    .antMatchers("/teams/**").hasAuthority("app.teams.edit")
    .antMatchers("/clients/new", "/clients/edit", "/clients/view").hasAuthority("app.clients.edit")
    .antMatchers("/endclients/**").hasAuthority("app.endclients.edit")
    .antMatchers("/sources/**").hasAuthority("app.sources.edit")
    .antMatchers("/reports-generation/**").hasAuthority("app.reports-generation.view")
    .and()
    .authorizeRequests()
    .anyRequest().authenticated()
    .and()
    .formLogin()
    .loginPage("/login")
    .permitAll()
    .successHandler(atsLoginHandler)
    .and()
    .logout()
    .permitAll()
    .logoutSuccessHandler(atsLogoutHandler)
    .and()
    .cors()
    .and()
    .csrf()
      .disable();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder authBuilder) throws Exception {

    authBuilder.userDetailsService(userService);
  }
}
 