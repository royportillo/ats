package com.hrpo.ats.service;

import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.Team;

public interface EmailService {
  /**
   * Saves EOD report and sends email to a list of recipients.
   * 
   * @param username the user name of the currently logged-in user.
   * @param team The team of the currently logged-in user.
   * @param eodReport the EOD report details that is put in manually by the user.
   */
  public void saveAndSendEmail(String username, Team team, 
      EodReport eodReport);
}
