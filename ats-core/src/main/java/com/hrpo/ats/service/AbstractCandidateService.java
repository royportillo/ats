package com.hrpo.ats.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.hrpo.ats.dao.CandidateDao;
import com.hrpo.ats.dao.ShortlistDao;
import com.hrpo.ats.dao.UserDao;
import com.hrpo.ats.dao.WorkflowDao;
import com.hrpo.ats.dto.Shortlist;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.daxtra.dao.DaxtraCandidateDao;
import com.hrpo.daxtra.dto.Candidate;

abstract class AbstractCandidateService {

  @Autowired
  private DaxtraCandidateDao daxtraCandidateDao;
  
  @Autowired
  private CandidateDao jdbcCandidateDao;
  
  @Autowired
  private ShortlistDao jdbcShortlistDao;
  
  @Autowired
  private UserDao jdbcUserDao;
  
  @Autowired
  private WorkflowDao<PrescreenEvent> jdbcPrescreenDao;
  
  @Autowired
  private WorkflowDao<SubmissionEvent> jdbcSubmissionDao;
  
  @Autowired
  private WorkflowDao<ClientSubmissionEvent> jdbcClientSubmissionDao;
  
  @Autowired
  private WorkflowDao<InterviewEvent> jdbcInterviewDao;
  
  @Autowired
  private WorkflowDao<PlacementEvent> jdbcPlacementDao;

  public DaxtraCandidateDao getDaxtraCandidateDao() {
    return daxtraCandidateDao;
  }

  public CandidateDao getJdbcCandidateDao() {
    return jdbcCandidateDao;
  }

  public ShortlistDao getJdbcShortlistDao() {
    return jdbcShortlistDao;
  }

  public UserDao getJdbcUserDao() {
    return jdbcUserDao;
  }
  
  public WorkflowDao<PrescreenEvent> getJdbcPrescreenDao() {
    return jdbcPrescreenDao;
  }
  
  public WorkflowDao<SubmissionEvent> getJdbcSubmissionDao() {
    return jdbcSubmissionDao;
  }
  
  public WorkflowDao<ClientSubmissionEvent> getJdbcClientSubmissionDao() {
    return jdbcClientSubmissionDao;
  }
  
  public WorkflowDao<InterviewEvent> getJdbcInterviewDao() {
    return jdbcInterviewDao;
  }
  
  public WorkflowDao<PlacementEvent> getJdbcPlacementDao() {
    return jdbcPlacementDao;
  }

  /**
   * Retrieves list of {@code Candidate}s of the shortlist name.
   * Shortlist name text is converted to lower case for comparison to local database.
   * 
   * @param shortlistName the shortlist name.
   * 
   * @return list of {@code Candidate}s of the shortlist name.
   */
  protected List<Candidate> getCachedCandidatesByShortlist(String shortlistName) {
    List<Candidate> cachedCandidates = 
        getJdbcCandidateDao().findByShortlistName(shortlistName.toLowerCase());
    return cachedCandidates;
  }

  /**
   * Cached the shortlist and associated {@code Candidate}s in the local DB.
   * The saved shortlist name is converted to lower case.
   * 
   * @param shortlistName the shortlist name.
   * @param candidatesFromDaxtra list of {@code Candidate}s retrieved from DAXTRA.
   * @param username the logged user.
   * 
   * @return list of {@code Candidate}s of the shortlist name.
   */
  protected void cacheShortlistCandidates(String shortlistName, 
      List<Candidate> candidatesFromDaxtra, String username) {
    
    Integer userId = getJdbcUserDao().findIdByUsername(username);
    Integer shortlistId = 
        getJdbcShortlistDao().insert(new Shortlist(shortlistName.toLowerCase()), userId);
    
    for (Candidate c0 : candidatesFromDaxtra) {
      Integer candidateId = getJdbcCandidateDao().upsert(c0);
      getJdbcCandidateDao().insertShortlistCandidate(shortlistId, candidateId);
      c0.setId(candidateId);
    }
  }

  /**
   * Retrieves {@code Candidate} by candidate Id.
   * 
   * @param candidateId the DAXTRA candidate Id.
   * 
   * @return {@code Candidate} of the DAXTRA candidate Id.
   */
  protected Candidate getCachedCandidateByCandidateId(String candidateId) {
    
    return getJdbcCandidateDao().findByCandidateId(candidateId, true);
  }

  /**
   * Cache the shortlist and associated {@code Candidate}s in the local DB.
   * 
   * @param candidateId the DAXTRA candidate Id.
   * @param candidateHrXml the HR XML of the candidate in Base64 String format.
   * 
   * @return {@code Candidate} of the DAXTRA candidate Id.
   */
  protected void cacheCandidateHrXml(String candidateHrXml, String candidateId) {
    getJdbcCandidateDao().updateHrXml(candidateHrXml, candidateId);
  }
}
