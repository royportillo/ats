package com.hrpo.ats.service;

import java.util.List;
import com.hrpo.ats.dto.Role;

public interface RoleService {
  /**
   * Gets all available roles.
   * @return the {@code List} of all available roles.
   */
  List<Role> getAllRoles();
}
