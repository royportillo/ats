package com.hrpo.ats.service;

import java.time.LocalDate;
import java.util.List;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;

public interface EodReportService {
  
  /**
   * Saves or updates {@code EodReport}.
   * @param eodReport The {@code EodReport} to save.
   * @return The id of the {@code EodReport}.
   */
  Integer save(EodReport eodReport, String username);
  
  /**
   * Finds {@code EodReport} by its id.
   * @param id The id of the {@code EodReport}
   * @return the {@code EodReport}.
   */
  EodReport getByIdAndUsername(Integer id, String username);
  
  /**
   * Finds {@code EodReport}s by report date range.
   * @param dateFrom The report date lower bound.
   * @param dateTo The report date upper bound.
   * @return the list of {@code EodReport}. 
   */
  List<EodReport> getByReportDate(LocalDate dateFrom, LocalDate dateTo, String username);
  
  /**
   * Finds {@code EodReportSource}s by EOD report date range.
   * @param dateFrom The report date lower bound.
   * @param dateTo The report date upper bound.
   * @return the list of {@code EodReportSource}.
   */
  List<EodReportSource> getSourceByReportDate(LocalDate dateFrom, LocalDate dateTo);

}
