package com.hrpo.ats.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.exception.CandidateException;
import com.hrpo.ats.util.DecoderUtils;
import com.hrpo.daxtra.dto.Candidate;

/**
 * This service integrates with Daxtra module that in turn calls Daxtra server via REST.
 */
@Service
@Transactional
public class CandidateRestService extends AbstractCandidateService implements CandidateService {

  Logger log = LoggerFactory.getLogger(CandidateRestService.class);

  @Override
  public List<Candidate> getCandidateDetailsByShortlistName(String shortlistName, 
      String username, String database) {

    // Check first if candidates of shortlist is cached in the local DB.
    List<Candidate> cachedCandidates = getCachedCandidatesByShortlist(shortlistName);
    List<Candidate> candidatesFromDaxtra = null;

    // If cached candidates does not exist yet, retrieve from DAXTRA.
    if (cachedCandidates.isEmpty()) { 

      List<Candidate> shallowCandidates = 
          getDaxtraCandidateDao().findCandidateIdsByShortListName(shortlistName, database);
      candidatesFromDaxtra = new ArrayList<>();
      
      for (Candidate c0 : shallowCandidates) {
        Candidate c1 = 
            getDaxtraCandidateDao().findCandidateWithoutHrXmlByCandidateId(c0.getCandidateId(), 
                database);
        candidatesFromDaxtra.add(c1);
      }
      
      // If shortlist candidates was retrieved from DAXTRA then
      // cache shortlist and associated candidates to local DB.
      if (!candidatesFromDaxtra.isEmpty()) {
        cacheShortlistCandidates(shortlistName, candidatesFromDaxtra, username);
      }
      
    }

    return cachedCandidates.isEmpty() ? candidatesFromDaxtra : cachedCandidates;
  }

  @Override
  public JobOrderCandidate getJobOrderCandidateHrXmlByCandidateId(
      String candidateId, String username, String database) {
    
    // Check first if candidate is cached in the local DB.
    Candidate cachedCandidate = getCachedCandidateByCandidateId(candidateId);
    
    if (cachedCandidate == null) {
      throw new CandidateException(
          "Illegal state. Candidate should have been cached at this point.");
    }
    
    Candidate candidateFromDaxtra = null;
    
    // Check if candidate's HR XML is already cached in local DB.
    if (cachedCandidate != null && cachedCandidate.getHrXml() == null) {
      
      // load HR XML from DAXTRA
      candidateFromDaxtra = 
          getDaxtraCandidateDao().findCandidateWithHrXmlByCandidateId(candidateId, database);
      
      // If candidate HR XML was retrieved from DAXTRA then
      // cache candidate's HRXML to local DB.
      cacheCandidateHrXml(candidateFromDaxtra.getHrXml(), candidateId);
      
      // set candidate Id from cache to be propagated to UI.
      candidateFromDaxtra.setId(cachedCandidate.getId());
    }
    
    Candidate candidate = 
        cachedCandidate.getHrXml() == null ? candidateFromDaxtra : cachedCandidate;
    
    // Decode Base64 HR XML to String XML format.
    candidate.setHrXml(new String(DecoderUtils.decode(
            candidate.getHrXml(), Base64.getMimeDecoder()::decode)));
    
    JobOrderCandidate joCandidateDetails = new JobOrderCandidate();
    joCandidateDetails.setCandidate(candidate);
    
    return joCandidateDetails;
  }
  
  @Override
  public Candidate getCandidateRawCvByCandidateId(String candidateId, String database) {

    Candidate candidate = getDaxtraCandidateDao()
        .findCandidateWithCvByCandidateId(candidateId, database);
    
    return candidate;
  }
}
