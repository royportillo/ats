package com.hrpo.ats.service;

import java.io.StringWriter;
import java.util.Map;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.stereotype.Component;
import com.hrpo.ats.exception.EodReportException;

@Component
public class EodReportVelocityEmailTemplate implements EodReportEmailTemplate {
  
  private VelocityEngine engine;
  
  public EodReportVelocityEmailTemplate() {
    engine = new VelocityEngine();
    engine.setProperty("resource.loader", "class");
    engine.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getName());
    engine.init();
  }

  @Override
  public String render(Map<String, Object> context) {
    
    VelocityContext velocityContext = new VelocityContext(context);
    
    Template template = null;
    try {
      template = engine.getTemplate("templates/email/eodreporttemplate.vm");
    } catch (ResourceNotFoundException rnfe) {
      throw new EodReportException(rnfe.getMessage(), rnfe);
    }
    
    StringWriter writer =  new StringWriter();
    template.merge(velocityContext, writer);
    
    return writer.toString();
  }

}
