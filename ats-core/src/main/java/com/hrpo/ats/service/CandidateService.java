package com.hrpo.ats.service;

import java.util.List;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.daxtra.dto.Candidate;

public interface CandidateService {

  /**
   * Returns list of {@code Candidate} retrieved from Daxtra module.
   * 
   * @param shortListName the short list name that contains list of candidates.
   * @param username the logged user.
   * @param database the selected Daxtra database.
   * 
   * @return The list of {@code Candidate} with details.
   */
  public List<Candidate> getCandidateDetailsByShortlistName(String shortListName, String username,
      String database);
  
  /**
   * Returns {@code JobOrderCandidate}'s detailed information with Base64 encoded HRXML
   *     based on {@code candidateId}.
   * 
   * @param candidateId the Id of the {@code Candidate} with HRXML to be retrieved.
   * @param username the logged user.
   * @param database the selected Daxtra database.
   * 
   * @return The {@code Candidate} information that includes HRXML in Base64 encoded format.
   */
  public JobOrderCandidate getJobOrderCandidateHrXmlByCandidateId(
      String candidateId, String username, String database);
  
  /**
   * Returns the {@code Candidate}'s response information with raw CV in binary format.
   * 
   * @param candidateId the Daxtra ID of the {@code Candidate} to be retrieved.
   * @param database the selected Daxtra database.
   * 
   * @return The {@code Candidate} response information that includes the raw CV in binary format.
   */
  public Candidate getCandidateRawCvByCandidateId(String candidateId, String database);
}
