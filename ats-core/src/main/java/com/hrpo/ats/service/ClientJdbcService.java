package com.hrpo.ats.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.ClientDao;
import com.hrpo.ats.dao.EndClientDao;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.exception.ClientException;

@Service
@Transactional
public class ClientJdbcService implements ClientService {

  @Autowired
  private ClientDao clientDao;
  
  @Autowired
  private EndClientDao endClientDao;
  
  @Override
  public Integer save(Client client) {
    try {
      if (client.getClientId() == null) {
        return clientDao.insert(client);
      } else {
        return clientDao.update(client);
      }
    } catch (DuplicateKeyException dke) {
      throw new ClientException(
          "Cannot set client to default; default client already exists.", dke);
    }
  }
  
  @Override
  public Client getClientById(Integer id, Boolean isLoadEndClients) {
    try {
      Client client = clientDao.findById(id);
      
      if (isLoadEndClients) {
        client.setEndClients(endClientDao.findEndClientsByClient(client.getClientId()));
      }
      return client;
    } catch (EmptyResultDataAccessException erdae) {
      throw new ClientException("Client id no. " + id + " does not exist.", erdae);
    }
  }
  
  @Override
  public List<Client> getAllClients() {
    return clientDao.findAll();
  }

  @Override
  public List<Client> getClientsByTeam(final Team team) {
    return clientDao.findByTeam(team.getId());
  }
}
