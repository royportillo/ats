package com.hrpo.ats.service;

import java.util.List;
import com.hrpo.ats.dto.Source;

public interface SourcesService {

  /**
   * Saves a {@code Source}.
   * @param source The source to save.
   * @return The row id of the source.
   */
  public Integer save(Source source);
  
  /**
   * Gets {@code Source} by its id.
   * @param id the id of the end source to be retrieved.
   * @param isLoadEndSources Will load end sources if set to true.
   * @return the source.
   */
  public Source getSourceById(final Integer id);
  
  /**
   * Gets all {@code Source}s.
   * @return the list of all {@Source}s.
   */
  public List<Source> getAllSources();
}
