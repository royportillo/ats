package com.hrpo.ats.service;

import java.io.StringWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import org.springframework.stereotype.Component;

/**
 * A helper class to transform the Candidate's HRXML to HTML format through use of XSL.
 */
@Component
public final class XmlToHtmlTransformer {
  
  static final TransformerFactory tFactory = TransformerFactory.newInstance();

  public String transform(Source xslDoc, Source xmlDoc) throws TransformerException {
    
    Transformer transformer = tFactory.newTransformer(xslDoc);
    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    StringWriter sw = new StringWriter();
    
    transformer.transform(xmlDoc, new StreamResult(sw));
    
    return sw.toString();
  }
}
