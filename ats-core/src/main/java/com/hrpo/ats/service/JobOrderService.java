package com.hrpo.ats.service;

import java.time.Clock;
import java.time.LocalDate;
import java.util.List;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;

public interface JobOrderService {

  /**
   * Finds {@code JobOrder} by id.
   * 
   * @param id the job order id to fetch.
   * 
   * @return the {@code JobOrder}.
   */
  public JobOrder getJobOrderById(final Integer id);
  
  /**
   * Finds {@code JobOrder} with file by id.
   * 
   * @param id the id of the {@code JobOrder} with file to get.
   * @return the {@code JobOrder} with file.
   */
  public JobOrder getJobOrderFileById(final Integer id);
  
  /**
   * Finds {@code JobOrder} by ID plus the list of {@code JobOrderCandidate}s
   * associated with this job order.
   * @param id the ID of the job order to fetch.
   * @param username the user name of the currently logged user.
   * @return the {@code JobOrder} with list of {@code JobOrderCandidate}s.
   */
  public JobOrder getJobOrderWithCandidatesById(final Integer id, String username);
  
  /**
   * Saves or updates {@code JobOrder}.
   * 
   * @param jobOrder the job order to save.
   * @param userName the primary owner user name of the job order
   * 
   * @return the row id.
   */
  public Integer saveJobOrder(final JobOrder jobOrder, final String userName);
  
  /**
   * Search {@code JobOrder} by job order number and/or title. If both fields are entered
   * then this API will perform an OR operation. Title field is matched by starts with semantic. 
   * 
   * @param username The user name of the currently logged-in user.
   * @param joNumber the job order number to search.
   * @param title the job order title to search. It is matched using SQL starts with 
   *    {@code LIKE} semantic.
   * @param client The selected client for the session.
   * 
   * @return the list of job orders.
   */
  public List<JobOrder> searchJobOrderByCriteria(String username, 
      final String joNumber, final String title, final Client client);
  
  /**
   * Gets all {@code JobOrder} owned by user with row limit.
   * 
   * @param username The user name of the job orders' owner.
   * @param client The currently selected client.
   * 
   * @return the list of {@code JobOrder}s.
   */
  public List<JobOrder> getAllJobOrdersForUser(String username, Client client);
  
  /**
   * Imports {@code Candidate}s to @{JobOrder}.
   * 
   * @param joId the ID of job order to import candidates to
   * @param candidateIds the list of candidate ID's to import
   * @param username the "owner" of the candidates imported to job order.
   */
  public void importCandidatesToJobOrder(
      final Integer joId, final List<Integer> candidateIds, final String username);
  
  /**
   * Returns a {@code JobOrderCandidate} by ID.
   * @param jocId The ID of the {@code JobOrderCandidate} to retrieve.
   * @return the {@code JobOrderCandidate}.
   */
  public JobOrderCandidate getJobOrderCandidateById(final Integer jocId);
  
  /**
   * Returns list of {@code JobOrderCandidate} by latest workflow event and inclusive date range.
   * 
   * @param stage the workflow stage.
   * @param username the logged user.
   * @param dateFrom the inclusive start date.
   * @param dateTo the inclusive end date.
   * @return The list of {@code JobOrderCandidate}.
   */
  public List<JobOrderCandidate> getJobOrderCandidatesByLastWorkflowEvent(WorkflowStage stage, 
      String username, LocalDate dateFrom, LocalDate dateTo);
  
  /**
   * Returns list of {@code JobOrderCandidate} by workflow event and date period i.e.
   * FTD (For the day), FTW (For the week) etc.
   * @param stage the workflow stage.
   * @param username the logged user.
   * @param period the date period.
   * @param clock the system clock.
   * @param isTeamOnly get only candidates by user's current team
   * @return The list of {@code JobOrderCandidate}.
   */
  public List<JobOrderCandidate> getJobOrderCandidatesByWorkflowEvent(WorkflowStage stage, 
      String username, String period, Clock clock, Boolean isTeamOnly);
  
  /**
   * Returns list of {@code JobOrderCandidate} by latest workflow date range, sorted by
   * transaction date
   * @param dateFrom the inclusive start date.
   * @param dateTo the inclusive end date.
   * @return The list of {@code JobOrderCandidate}s. 
   */
  public List<JobOrderCandidate> getJobOrderCandidatesByWorkflowDate(LocalDate dateFrom, 
      LocalDate dateTo);
  
  /**
   * Returns list of {@code JobOrder}s by date period i.e.
   * FTD (For the day), FTW (For the week) etc.
   * @param username the logged user.
   * @param period the date period.
   * @param clock the system clock.
   * @param isTeamOnly true if get only the {@code JobOrder}s under the specified 
   *     user's current team.
   * @return The list of {@code JobOrder}s.
   */
  public List<JobOrder> getJobOrdersByUserAndPeriod(String username, 
      String period, Clock clock, Boolean isTeamOnly);
}
