package com.hrpo.ats.service;

import java.time.Clock;
import com.hrpo.ats.dto.Dashboard;

public interface DashboardService {

  public Dashboard.WorkflowEventCountPortlet getWorkflowEventCountPortletData(String username,
      Clock clock, Boolean isTeamOnly);
}
