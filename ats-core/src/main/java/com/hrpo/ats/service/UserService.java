package com.hrpo.ats.service;

import java.util.List;
import org.springframework.security.core.userdetails.UserDetailsService;
import com.hrpo.ats.dto.User;

public interface UserService extends UserDetailsService {

  /**
   * Finds all enabled {@code User}s.
   * @param isEnabledOnly true if find only enabled users, false if to include disabled users
   * @return The list of enabled {@code User}s.
   */
  public List<User> getAllUsers(Boolean isEnabledOnly);
  
  /**
   * Finds all enabled {@code User}s with {@code UserRole}s.
   * @param isEnabledOnly true if find only enabled users, false if to include disabled users
   * @return The list of {@code User}s with roles.
   */
  public List<User> getAllUsersWithRole(Boolean isEnabledOnly);
  
  /**
   * Saves or updates the user and its roles.
   * @param user The user to be saved.
   * @return the row id.
   */
  public Integer saveUser(final User user);
  
  /**
   * Find the user by id.
   * @param userId The id of the user.
   * @return The user.
   */
  public User getUserById(Integer userId);
  
  /**
   * Finds the {@code User} with {@code UserRole}s by id.
   * @param userId The id of the user.
   * @return The user with roles.
   */
  public User getUserWithRoleById(Integer userId);
}
