package com.hrpo.ats.service;

import java.util.List;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;

/**
 *  This class contains all workflow related events API. This class supports automatic saving
 *  of predecessor workflow events.
 */
public interface WorkflowEventService {

  /**
   * Saves @{code PrescreenEvent}.
   * 
   * @param event the prescreen event to be saved.
   * @param joCandidate the {@code JobOrderCandidate} that triggered the event.
   * @return row id of the event.
   */
  public Integer savePrescreenEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate);
  
  /**
   * Saves @{code SubmissionEvent}.
   * 
   * @param event the submission event to be saved.
   * @param joCandidate the {@code JobOrderCandidate} that triggered the event.
   * @return row id of the event.
   */
  public Integer saveSubmissionEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate);
  
  /**
   * Saves @{code ClientSubmissionEvent}.
   * 
   * @param event the client submission event to be saved.
   * @param joCandidate the {@code JobOrderCandidate} that triggered the event.
   * @return row id of the event.
   */
  public Integer saveClientSubmissionEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate);
  
  /**
   * Saves @{code InterviewEvent}.
   * 
   * @param event the interview event to be saved.
   * @param joCandidate the {@code JobOrderCandidate} that triggered the event.
   * @return row id of the event.
   */
  public Integer saveInterviewEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate);
  
  /**
   * Saves @{code PlacementEvent}.
   * 
   * @param event the placement event to be saved.
   * @param joCandidate the {@code JobOrderCandidate} that triggered the event.
   * @return row id of the event.
   */
  public Integer savePlacementEvent(AbstractWorkflowEvent event, 
      JobOrderCandidate joCandidate);

  /**
   * Deletes all of {@code JobOrderCandidate}'s workflow events.
   * @param joCandidateId The id of the job order candidate.
   */
  public void deleteWorkflowEventsByJoCandidateId(Integer joCandidateId);
  
  /**
   * Gets all of {@code JobOrderCandidate}'s workflow events.
   * @param joCandidateId The id of the job order candidate.
   * @return The list of {@code JobOrderCandidate}'s workflow events.
   */
  public List<AbstractWorkflowEvent> getWorkflowEventsByJoCandidateId(Integer joCandidateId);
}
