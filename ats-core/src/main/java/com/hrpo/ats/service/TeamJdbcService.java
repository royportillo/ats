package com.hrpo.ats.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.ClientDao;
import com.hrpo.ats.dao.TeamDao;
import com.hrpo.ats.dao.UserDao;
import com.hrpo.ats.dao.UserRoleDao;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.TeamException;

@Service
@Transactional
public class TeamJdbcService implements TeamService {
  
  @Autowired
  private TeamDao teamDao;
  
  @Autowired
  private UserDao userDao;
  
  @Autowired
  private ClientDao clientDao;
  
  @Autowired
  private UserRoleDao userRoleDao;
  
  @Override
  public List<Team> getAllTeams() {
    
    return teamDao.findAllTeams();
  }
  
  @Override
  public List<Team> getAllTeamsWithUsers() {
    
    return teamDao.findAllTeams().stream().map(team -> {
      team.setUsers(userDao.findAllUsersByTeam(team.getId(), true));
      return team;
    }).collect(Collectors.toList());
  }
  
  @Override
  public Integer saveTeam(Team team) {
    if (team.getId() == null) {
      return teamDao.insert(team);
    } else {
      return teamDao.update(team);
    }
  }
  
  @Override
  public Team getById(Integer teamId) {
    try {
      return teamDao.findById(teamId);
    } catch (EmptyResultDataAccessException erdae) {
      throw new TeamException("Team id " + teamId + " not found.", erdae);
    }
  }
  
  @Override
  public Team getByIdWithUserAndClients(Integer teamId, Boolean isLoadUserRoles) {
    try {
      Team team = teamDao.findById(teamId);
      List<User> users = new ArrayList<>();
      
      if (isLoadUserRoles == true) {
        users = userDao.findAllUsersByTeam(teamId, true).stream().map(user -> {
          user.setUserRoles(userRoleDao.findByUserId(user.getId()));
          
          return user;
        }).collect(Collectors.toList());
      } else {
        users = userDao.findAllUsersByTeam(teamId, true);
      }
      
      team.setUsers(users);
      team.setClients(clientDao.findByTeam(teamId));
      
      return team;
    } catch (EmptyResultDataAccessException erdae) {
      throw new TeamException("Team id " + teamId + " not found.", erdae);
    }
  }
}
