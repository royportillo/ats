package com.hrpo.ats.service;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.JobOrderCandidateDao;
import com.hrpo.ats.dao.JobOrderDao;
import com.hrpo.ats.dao.UserDao;
import com.hrpo.ats.dao.UserJobOrderDao;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.JobOrderException;

@Service
@Transactional
public class JobOrderJdbcService implements JobOrderService {

  Logger log = LoggerFactory.getLogger(JobOrderJdbcService.class);

  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private UserJobOrderDao userJobOrderDao;
  
  @Autowired
  private JobOrderCandidateDao jobOrderCandidateDao;
  
  @Autowired
  private UserDao userDao;
  
  @Override
  public JobOrder getJobOrderById(Integer id) {
    try {
      return jobOrderDao.findByPrimaryKey(id);
    } catch (EmptyResultDataAccessException erdao) {
      throw new JobOrderException("No such job order exists", erdao);
    }
  }
  
  @Override
  public JobOrder getJobOrderFileById(Integer id) {
    try {
      return jobOrderDao.findJobOrderFileById(id);
    } catch (EmptyResultDataAccessException erdae) {
      throw new JobOrderException("No such job order exists", erdae);
    }
  }
  
  @Override
  public JobOrder getJobOrderWithCandidatesById(Integer id, String username) {
    JobOrder jobOrder = jobOrderDao.findByPrimaryKey(id);
    Integer userId = username != null ? userDao.findIdByUsername(username) : 
        userDao.findIdByUsername(jobOrder.getPrimaryOwner().getUser().getUsername());
    
    List<JobOrderCandidate> jobOrderCandidates = jobOrderCandidateDao.getByJobOrderId(id, userId);
    for (JobOrderCandidate jobOrderCandidate : jobOrderCandidates) {
      jobOrderCandidate.setWorkflowState(
          computeJobOrderCandidateWorkflowState(jobOrderCandidate)
      );
    }
    
    jobOrder.setJobOrderCandidates(jobOrderCandidates);
    return jobOrder;
  }

  @Override
  public Integer saveJobOrder(JobOrder jobOrder, final String userName) {

    Integer rowId = null;

    try {
      if (jobOrder.getId().isPresent()) {
        log.debug("Update job order number: " + jobOrder.getNumber());
        rowId = jobOrderDao.update(jobOrder);
      } else {
        log.debug("Create job order number: " + jobOrder.getNumber());
        Integer primaryOwnerId = userDao.findIdByUsername(userName);
        rowId = jobOrderDao.insert(jobOrder);
        
        userJobOrderDao.insert(rowId, primaryOwnerId, true);
      }
    } catch (DuplicateKeyException ex) {
      throw new JobOrderException("Duplicate job order number.", ex);
    }

    return rowId;
  }

  @Override
  public List<JobOrder> getAllJobOrdersForUser(String username, Client client) {
    if (username != null) {
      Integer userId = userDao.findIdByUsername(username);
      return jobOrderDao.findAllByUserIdAndClientId(userId, client.getClientId());
    }
    return jobOrderDao.findAllByUserId(null);
  }

  @Override
  public List<JobOrder> searchJobOrderByCriteria(String username, String joNumber, 
      String title, Client client) {
    log.info("Searching by job order number '" + joNumber + "' or title '" + title + "'");
    
    if (username != null && client != null) {
      Integer userId = userDao.findIdByUsername(username);
      return jobOrderDao.findByCriteria(userId, joNumber, title, client.getClientId());
    }
    return jobOrderDao.findByCriteria(null, joNumber, title, null);
  }
  
  @Override
  public void importCandidatesToJobOrder(
      Integer joId, List<Integer> candidateIds, String username) {
    log.info("Importing candidates to job order '" + joId + ".' Candidate ID(s): " 
        + candidateIds.toString());
    
    final JobOrder jobOrder = jobOrderDao.findByPrimaryKey(joId);
    final Integer ownerId = userDao.findIdByUsername(username);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder.getPayPeriodRate());
    
    jobOrderCandidateDao.batchInsert(joId, ownerId, candidateIds, joCandidate);
  }
  
  /**
   * Compute the {@code JobOrderCandidate}'s workflow states by job order candidate
   * @param jobOrderCandidate The job order candidate object.
   * @return the workflow states of the supplied {@code JobOrderCandidate}.
   */
  private JobOrderCandidate.WorkflowState computeJobOrderCandidateWorkflowState(
      JobOrderCandidate jobOrderCandidate) {
    return jobOrderCandidateDao.getWorkflowStatesByJobOrderCandidate(jobOrderCandidate);
  }
  
  @Override
  public JobOrderCandidate getJobOrderCandidateById(Integer jocId) {
    return jobOrderCandidateDao.findByPrimaryKey(jocId);
  }
  
  @Override
  public List<JobOrderCandidate> getJobOrderCandidatesByLastWorkflowEvent(WorkflowStage stage,
      String username, LocalDate dateFrom, LocalDate dateTo) {
    
    Integer userId = userDao.findIdByUsername(username);
    return jobOrderCandidateDao.findByEventWorkflow(userId, null, stage, dateFrom, dateTo, true);
  }
  
  @Override
  public List<JobOrderCandidate> getJobOrderCandidatesByWorkflowDate(LocalDate dateFrom,
      LocalDate dateTo) {
    
    var imported = jobOrderCandidateDao.findUnprocessed(null, dateFrom, dateTo);
    var prescreened = jobOrderCandidateDao.findByEventWorkflow(null, null, WorkflowStage.PRESCREEN, 
        dateFrom, dateTo, false);
    var submitted = jobOrderCandidateDao.findByEventWorkflow(null, null, WorkflowStage.SUBMIT, 
        dateFrom, dateTo, false);
    var presented = jobOrderCandidateDao.findByEventWorkflow(null, null, 
        WorkflowStage.PRESENTATION, dateFrom, dateTo, false);
    var interviewed = jobOrderCandidateDao.findByEventWorkflow(null, null, WorkflowStage.INTERVIEW, 
        dateFrom, dateTo, false);
    var placed = jobOrderCandidateDao.findByEventWorkflow(null, null, WorkflowStage.PLACEMENT, 
        dateFrom, dateTo, false);
    
    var sorted = Stream.of(imported, prescreened, submitted, presented, interviewed, placed)
        .flatMap(Collection::stream)
        .sorted((joc1, joc2) -> {
          int compareVal = joc1.getTransactionDate().compareTo(joc2.getTransactionDate());
          
          if (compareVal == 0) {
            compareVal = joc1.getOwner().getFirstName().compareTo(joc2.getOwner().getFirstName());
          }
          
          if (compareVal == 0) {
            compareVal = joc1.getCandidate().getFirstName()
                .compareTo(joc2.getCandidate().getFirstName());
          }
          
          return compareVal;
        })
        .collect(Collectors.toList());
    
    return sorted;
  }
  
  @Override
  public List<JobOrderCandidate> getJobOrderCandidatesByWorkflowEvent(WorkflowStage stage,
      String username, String period, Clock clock, Boolean isTeamOnly) {
    
    LocalDate today = LocalDate.now(clock);
    LocalDate thisMonthFirstDay = today.withDayOfMonth(1);
    LocalDate thisWeekFirstDay = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)); 
    LocalDate lastMonth = today.minusMonths(1);
    LocalDate lastLastMonth = today.minusMonths(2);
    
    User user = userDao.findByUsername(username);
    Integer userId = user.getId();
    Integer teamId = isTeamOnly ? user.getTeam().getId() : null;
    switch (period) {
      case "ftd" :
        return jobOrderCandidateDao
            .findByEventWorkflow(userId, teamId, stage, today, today, false);
      case "ftw" :
        return jobOrderCandidateDao
            .findByEventWorkflow(userId, teamId, stage, thisWeekFirstDay, today, false);
      case "ftm" :
        return jobOrderCandidateDao
            .findByEventWorkflow(userId, teamId, stage, thisMonthFirstDay, today, false);
      case "lm" :
        return jobOrderCandidateDao.findByEventWorkflow(
            userId, teamId, stage, lastMonth.with(TemporalAdjusters.firstDayOfMonth()), 
            lastMonth.with(TemporalAdjusters.lastDayOfMonth()), false);
      case "llm" :
        return jobOrderCandidateDao.findByEventWorkflow(
            userId, teamId, stage, lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()), 
            lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()), false);
      default :
        throw new JobOrderException("Unrecognized date period", new IllegalArgumentException());
    }
  }
  
  @Override
  public List<JobOrder> getJobOrdersByUserAndPeriod(String username, String period, Clock clock,
      Boolean isTeamOnly) {
    
    LocalDate today = LocalDate.now(clock);
    LocalDate thisMonthFirstDay = today.withDayOfMonth(1);
    LocalDate thisWeekFirstDay = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)); 
    LocalDate lastMonth = today.minusMonths(1);
    LocalDate lastLastMonth = today.minusMonths(2);
    
    User user = userDao.findByUsername(username);
    Integer userId = user.getId();
    Integer teamId = isTeamOnly ? user.getTeam().getId() : null;
    switch (period) {
      case "ftd" :
        return jobOrderDao
            .findByUserAndDateRange(userId, teamId, today, today);
      case "ftw" :
        return jobOrderDao
            .findByUserAndDateRange(userId, teamId, thisWeekFirstDay, today);
      case "ftm" :
        return jobOrderDao
            .findByUserAndDateRange(userId, teamId, thisMonthFirstDay, today);
      case "lm" :
        return jobOrderDao.findByUserAndDateRange(
            userId, teamId, lastMonth.with(TemporalAdjusters.firstDayOfMonth()), 
            lastMonth.with(TemporalAdjusters.lastDayOfMonth()));
      case "llm" :
        return jobOrderDao.findByUserAndDateRange(
            userId, teamId, lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()), 
            lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()));
      default :
        throw new JobOrderException("Unrecognized date period", new IllegalArgumentException());
    }
  }
}
