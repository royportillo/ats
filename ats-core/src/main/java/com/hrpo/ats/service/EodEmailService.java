package com.hrpo.ats.service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.EodReportException;

@Component
public class EodEmailService implements EmailService {
  
  @Autowired
  private Environment env;
  
  @Autowired
  private EodReportEmailTemplate template;
  
  @Autowired
  private DashboardService dashboardService;
  
  @Autowired
  private JobOrderService jobOrderService;
  
  @Autowired
  private ClientService clientService;
  
  @Autowired
  private UserService userService;
  
  @Autowired
  private EodReportService eodReportService;

  @Override
  public void saveAndSendEmail(String username, Team team,
      EodReport eodReport) {
    final String mailUsername = "endofday.report.24hrpo@gmail.com";
    final String mailPassword = "eod.report";
    final User user = (User) userService.loadUserByUsername(username);
    String recipients = null;
    
    Properties properties = new Properties();
    properties.put("mail.smtp.host", "smtp.gmail.com");
    properties.put("mail.smtp.port", "587");
    properties.put("mail.smtp.auth", "true");
    properties.put("mail.smtp.starttls.enable", "true");
    
    Boolean isDevOrDefault = Arrays.stream(env.getActiveProfiles()).anyMatch(
        profile -> (profile.equalsIgnoreCase("dev") || profile.equalsIgnoreCase("default"))
      );
    
    Boolean isProd = Arrays.stream(env.getActiveProfiles()).anyMatch(
        profile -> (profile.equalsIgnoreCase("prod"))
      );
    
    if (isDevOrDefault) {
      recipients = "eod.testrecipient.24hrpo@gmail.com," //pw: test.recipient@24hrpo
          + team.getEmail(); 
    } else if (isProd) {
      recipients = team.getEmail();
    }
    
    Session session = Session.getInstance(properties, new Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(mailUsername, mailPassword);
      }
    });
    
    Integer totalViews = 0;
    Integer newLeads = 0;
    Integer otherLeads = 0;
    
    for (EodReportSource eodReportSource : eodReport.getEodReportSources()) {
      
      if (eodReportSource.getSource().getCategory().equalsIgnoreCase("source")) {
        totalViews += eodReportSource.getSourceViews();
        newLeads += eodReportSource.getSourceLeads();
      } else if (eodReportSource.getSource().getCategory().equalsIgnoreCase("leads")) {
        otherLeads += eodReportSource.getLeads();
      }
    }
    
    Integer totalLeads = otherLeads + newLeads;
    Map<String, Object> context = new HashMap<>();
    
    context.put("totalViews", totalViews.toString());
    context.put("newLeads", newLeads.toString());
    context.put("totalLeads", totalLeads.toString());
    
    var sourceFields = eodReport.getEodReportSources();
    var sources = sourceFields.stream()
        .filter(field -> field.getSource().getCategory().equals("source"))
        .collect(Collectors.toList());
    var activities = sourceFields.stream()
        .filter(field -> field.getSource().getCategory().equals("activity"))
        .collect(Collectors.toList());
    var leads = sourceFields.stream()
        .filter(field -> field.getSource().getCategory().equals("leads"))
        .collect(Collectors.toList());
    
    context.put("sources", sources);
    context.put("activities", activities);
    context.put("leads", leads);
    
    LocalDateTime createDate = LocalDateTime.now();
    Clock reportClock = Clock.systemDefaultZone();
    
    if (eodReport.getId() != null) { //i.e. is already persisted
      EodReport origReport = eodReportService.getByIdAndUsername(eodReport.getId(), username);
      
      createDate = origReport.getCreateDate();
      Instant instant = origReport.getReportDate()
          .atStartOfDay(ZoneId.systemDefault()).toInstant();
      reportClock = Clock.fixed(instant, ZoneId.systemDefault());
      
      var origFields = origReport.getEodReportSources();
      var origSources = origFields.stream()
          .filter(field -> field.getSource().getCategory().equals("source"))
          .collect(Collectors.toList());
      var origActivities = origFields.stream()
          .filter(field -> field.getSource().getCategory().equals("activity"))
          .collect(Collectors.toList());
      var origLeads = origFields.stream()
          .filter(field -> field.getSource().getCategory().equals("leads"))
          .collect(Collectors.toList());
      
      context.put("origSources", origSources);
      context.put("origActivities", origActivities);
      context.put("origLeads", origLeads);
      context.put("isUpdated", true);
    }
    
    context.put("createDate", createDate
        .format(DateTimeFormatter.ofPattern("MMMM d, yyyy hh:mm a")));
    
    context.put("decimalFormat", new DecimalFormat("$###,###.00"));
    context.put("name",  user.getFirstName() + " " + user.getLastName());
    context.put("email", user.getEmail());
    context.put("username", username);
    context.put("comment", eodReport.getComment());
    
    context.put("eventCount", 
        dashboardService.getWorkflowEventCountPortletData(username, reportClock, true));
    context.put("prescreened",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.PRESCREEN, username, "ftd", reportClock, true));
    context.put("submitted",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.SUBMIT, username, "ftd", reportClock, true));
    context.put("presented",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.PRESENTATION, username, "ftd", reportClock, true));
    context.put("interviewed",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.INTERVIEW, username, "ftd", reportClock, true));
    context.put("placed",
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(
            WorkflowStage.PLACEMENT, username, "ftd", reportClock, true));
    context.put("jobOrdersToday", 
        jobOrderService.getJobOrdersByUserAndPeriod(username, "ftd", reportClock, true));
    
    List<Client> teamClients = clientService.getClientsByTeam(team);
    Client defaultClient =
        teamClients.stream().filter(client -> client.getIsDefault() == true)
        .findAny().orElse(teamClients.isEmpty() ? null : teamClients.get(0));
    
    try {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(user.getEmail(), 
          user.getFirstName() + " " + user.getLastName()));
      message.setRecipients(RecipientType.TO, 
          InternetAddress.parse(recipients));
      message.setRecipients(RecipientType.BCC,
          InternetAddress.parse(user.getEmail()));
      message.setSubject(
          (eodReport.getId() != null ? "(re-sent)" : "") 
          + defaultClient.getName() 
          + " EOD Report for " + LocalDate.now(reportClock)
          .format(DateTimeFormatter.ofPattern("MM/dd/y")));
      message.setReplyTo(InternetAddress.parse(user.getEmail()));
      
      String reportHtml = template.render(context);
      
      message.setContent(reportHtml, 
          MediaType.TEXT_HTML_VALUE);
      
      byte[] encodedReportHtml = Base64.getMimeEncoder()
          .encode(reportHtml.getBytes(StandardCharsets.UTF_8));
      eodReport.setReportHtml(new String(encodedReportHtml));
      eodReportService.save(eodReport, username);
      
      Transport.send(message);
      
    } catch (MessagingException | UnsupportedEncodingException ex) {
      throw new EodReportException(ex.getMessage(), ex);
    }
  }

}
