package com.hrpo.ats.service;

import java.util.List;
import com.hrpo.ats.dto.Team;

public interface TeamService {

  /**
   * Gets all teams.
   * @return the {@code List} of {@code Team}s.
   */
  List<Team> getAllTeams();
  
  /**
   * Gets all teams and load team's users.
   * @return the {@code List} of {@code Teams}s, each with {@code List} of {@code User}s.
   */
  List<Team> getAllTeamsWithUsers();
  
  /**
   * Saves a {@code Team}.
   * @param team The team to save.
   * @return The id of the saved team.
   */
  Integer saveTeam(Team team);
  
  /**
   * Finds {@code Team} by id.
   * @param teamId The id of the team.
   * @return The team.
   */
  Team getById(Integer teamId);
  
  /**
   * Finds {@code Team} by Id with lists of {@code User}s and {@code Client}s
   * @param teamId The id of the team.
   * @param isLoadUserRoles Load user with roles if true, load users only if false.
   * @return The team, with users and clients.
   */
  Team getByIdWithUserAndClients(Integer teamId, Boolean isLoadUserRoles);
}
