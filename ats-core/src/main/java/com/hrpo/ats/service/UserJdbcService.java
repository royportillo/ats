package com.hrpo.ats.service;

import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.PermissionDao;
import com.hrpo.ats.dao.UserDao;
import com.hrpo.ats.dao.UserRoleDao;
import com.hrpo.ats.dto.Permission;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.exception.UserException;

@Service
@Transactional
public class UserJdbcService implements UserService {

  Logger log = LoggerFactory.getLogger(UserJdbcService.class);

  @Autowired
  private UserDao userDao;
  
  @Autowired
  private PermissionDao permissionDao;
  
  @Autowired
  private UserRoleDao userRoleDao;

  @Override
  public List<User> getAllUsers(Boolean isEnabledOnly) {
    return userDao.findAllUsers(isEnabledOnly);
  }
  
  @Override
  public List<User> getAllUsersWithRole(Boolean isEnabledOnly) {
    List<User> users = userDao.findAllUsers(isEnabledOnly).stream().map(user -> {
      user.setUserRoles(userRoleDao.findByUserId(user.getId()));
      return user;
    }).collect(Collectors.toList());
    return users;
  }
  
  @Override
  public Integer saveUser(User user) {
    Integer rowId = null;
    try {
      if (user.getId() == null) {
        rowId = userDao.insert(user);
      } else {
        rowId = userDao.update(user);
      }
      
      var userRoles = user.getUserRoles();
      if (userRoles != null) {
        userRoleDao.deleteByUserId(rowId);
        userRoleDao.batchInsert(rowId, userRoles);
      }
    } catch (DuplicateKeyException dke) {
      throw new UserException("Username already exists.", dke);
    }
    
    return rowId;
  }
  
  @Override
  public User getUserById(Integer userId) {
    try {
      return userDao.findById(userId);
    } catch (EmptyResultDataAccessException erdae) {
      throw new UserException("User id " + userId + "not found.", erdae);
    }
  }
  
  @Override
  public User getUserWithRoleById(Integer userId) {
    try {
      User user = userDao.findById(userId);
      user.setUserRoles(userRoleDao.findByUserId(userId));
      
      return user;
    } catch (EmptyResultDataAccessException erdae) {
      throw new UserException("User id " + userId + "not found.", erdae);
    }
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = null; 
    
    try {
      user = userDao.findByUsername(username);
    } catch (EmptyResultDataAccessException ex) {
      throw new UsernameNotFoundException("Username '" + username + "' not found.", ex);
    }
    
    List<Permission> permissions = permissionDao.findByUsername(username);
    
    user.setAuthorities(AuthorityUtils.createAuthorityList(
        permissions.stream().map((p0) -> p0.getName()).toArray(String[]::new)));
    
    return user;
  }
  
  
}
