package com.hrpo.ats.service;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dao.JobOrderDao;
import com.hrpo.ats.dao.UserDao;
import com.hrpo.ats.dao.WorkflowDao;
import com.hrpo.ats.dto.Dashboard;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;

@Service
@Transactional
public class DashboardJdbcService implements DashboardService {

  Logger log = LoggerFactory.getLogger(DashboardJdbcService.class);
  
  @Autowired
  private UserDao userDao;

  @Autowired
  @Qualifier("prescreenDao")
  private WorkflowDao<PrescreenEvent> prescreenDao;

  @Autowired
  @Qualifier("submissionDao")
  private WorkflowDao<SubmissionEvent> submissionDao;

  @Autowired
  @Qualifier("clientSubmissionDao")
  private WorkflowDao<ClientSubmissionEvent> clientSubmissionDao;

  @Autowired
  @Qualifier("interviewDao")
  private WorkflowDao<InterviewEvent> interviewDao;

  @Autowired
  @Qualifier("placementDao")
  private WorkflowDao<PlacementEvent> placementDao;
  
  @Autowired
  private JobOrderDao jobOrderDao;

  @Override
  public Dashboard.WorkflowEventCountPortlet getWorkflowEventCountPortletData(String username,
      Clock clock, Boolean isTeamOnly) {
    
    LocalDate today = LocalDate.now(clock);
    LocalDate thisMonthFirstDay = today.withDayOfMonth(1);
    LocalDate thisWeekFirstDay = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)); 
    LocalDate lastMonth = today.minusMonths(1);
    LocalDate lastLastMonth = today.minusMonths(2);
    
    User user = userDao.findByUsername(username);
    Integer userId = user.getId();
    Integer teamId = isTeamOnly ? user.getTeam().getId() : null;

    Dashboard dashboard = new Dashboard();

    Integer prescreenForTheDayCount = prescreenDao.findCountByUserAndDateRange(userId, teamId, 
        today, today);
    Integer prescreenForTheWeekCount = prescreenDao.findCountByUserAndDateRange(userId, teamId,
        thisWeekFirstDay, today);
    Integer prescreenForTheMonthCount = prescreenDao.findCountByUserAndDateRange(userId, teamId,
        thisMonthFirstDay, today);
    Integer prescreenLastMonthCount = prescreenDao.findCountByUserAndDateRange(userId, teamId,
        lastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    Integer prescreenLastLastMonthCount = prescreenDao.findCountByUserAndDateRange(userId, teamId,
        lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    
    Integer submissionForTheDayCount = submissionDao.findCountByUserAndDateRange(userId, teamId, 
        today, today);
    Integer submissionForTheWeekCount = submissionDao.findCountByUserAndDateRange(userId, teamId,
        thisWeekFirstDay, today);
    Integer submissionForTheMonthCount = submissionDao.findCountByUserAndDateRange(userId, teamId,
        thisMonthFirstDay, today);
    Integer submissionLastMonthCount = submissionDao.findCountByUserAndDateRange(userId, teamId, 
        lastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    Integer submissionLastLastMonthCount = submissionDao.findCountByUserAndDateRange(
        userId, teamId, 
        lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    
    Integer clientSubmissionForTheDayCount = clientSubmissionDao.findCountByUserAndDateRange(
        userId, teamId, today, today);
    Integer clientSubmissionForTheWeekCount = clientSubmissionDao.findCountByUserAndDateRange(
        userId, teamId, thisWeekFirstDay, today);
    Integer clientSubmissionForTheMonthCount = clientSubmissionDao.findCountByUserAndDateRange(
        userId, teamId, thisMonthFirstDay, today);
    Integer clientSubmissionLastMonthCount = clientSubmissionDao.findCountByUserAndDateRange(
        userId, teamId, lastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    Integer clientSubmissionLastLastMonthCount = clientSubmissionDao.findCountByUserAndDateRange(
        userId, teamId, lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    
    Integer interviewForTheDayCount = interviewDao.findCountByUserAndDateRange(userId, teamId, 
        today, today);
    Integer interviewForTheWeekCount = interviewDao.findCountByUserAndDateRange(userId, teamId,
        thisWeekFirstDay, today);
    Integer interviewForTheMonthCount = interviewDao.findCountByUserAndDateRange(userId, teamId,
        thisMonthFirstDay, today);
    Integer interviewLastMonthCount = interviewDao.findCountByUserAndDateRange(userId, teamId, 
        lastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    Integer interviewLastLastMonthCount = interviewDao.findCountByUserAndDateRange(userId, teamId, 
        lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    
    Integer placementForTheDayCount = placementDao.findCountByUserAndDateRange(userId, teamId, 
        today, today);
    Integer placementForTheWeekCount = placementDao.findCountByUserAndDateRange(userId, teamId,
        thisWeekFirstDay, today);
    Integer placementForTheMonthCount = placementDao.findCountByUserAndDateRange(userId, teamId,
        thisMonthFirstDay, today);
    Integer placementLastMonthCount = placementDao.findCountByUserAndDateRange(userId, teamId, 
        lastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    Integer placementLastLastMonthCount = placementDao.findCountByUserAndDateRange(userId, teamId, 
        lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    
    //TODO update me
    Integer jobOrderForTheDayCount = jobOrderDao.findCountByUserAndDateRange(userId, teamId, today, 
        today);
    Integer jobOrderForTheWeekCount = jobOrderDao.findCountByUserAndDateRange(userId, teamId,
        thisWeekFirstDay, today);
    Integer jobOrderForTheMonthCount = jobOrderDao.findCountByUserAndDateRange(userId, teamId,
        thisMonthFirstDay, today);
    Integer jobOrderLastMonthCount = jobOrderDao.findCountByUserAndDateRange(userId, teamId,
        lastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastMonth.with(TemporalAdjusters.lastDayOfMonth()));
    Integer jobOrderLastLastMonthCount = jobOrderDao.findCountByUserAndDateRange(userId, teamId,
        lastLastMonth.with(TemporalAdjusters.firstDayOfMonth()),
        lastLastMonth.with(TemporalAdjusters.lastDayOfMonth()));

    Dashboard.WorkflowEventCountPortlet eventCountPortlet = 
        dashboard.new WorkflowEventCountPortlet(
            prescreenForTheDayCount, prescreenForTheWeekCount, prescreenForTheMonthCount,
            prescreenLastMonthCount, prescreenLastLastMonthCount, submissionForTheDayCount,
            submissionForTheWeekCount, submissionForTheMonthCount, submissionLastMonthCount,
            submissionLastLastMonthCount, clientSubmissionForTheDayCount,
            clientSubmissionForTheWeekCount, clientSubmissionForTheMonthCount,
            clientSubmissionLastMonthCount, clientSubmissionLastLastMonthCount,
            interviewForTheDayCount, interviewForTheWeekCount, interviewForTheMonthCount,
            interviewLastMonthCount, interviewLastLastMonthCount, placementForTheDayCount,
            placementForTheWeekCount, placementForTheMonthCount, placementLastMonthCount,
            placementLastLastMonthCount, jobOrderForTheDayCount, jobOrderForTheWeekCount, 
            jobOrderForTheMonthCount, jobOrderLastMonthCount, jobOrderLastLastMonthCount);

    return eventCountPortlet;
  }
}
