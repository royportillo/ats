package com.hrpo.ats.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hrpo.ats.dao.RoleDao;
import com.hrpo.ats.dto.Role;

@Service
public class RoleJdbcService implements RoleService {
  
  @Autowired
  private RoleDao roleDao;
  
  @Override
  public List<Role> getAllRoles() {
    return roleDao.findAllRoles();
  }
  
}
