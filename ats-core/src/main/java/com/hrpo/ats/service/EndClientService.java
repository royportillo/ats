package com.hrpo.ats.service;

import java.util.List;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;

public interface EndClientService {
  
  /**
   * Saves {@code EndClient}.
   * @param endClient the end client to save.
   * @return The row id of the saved end client.
   */
  public Integer save(EndClient endClient);
  
  /**
   * Gets {@code EndClient} by its id.
   * @param id the id of the end client to be retrieved.
   * @return the end client.
   */
  public EndClient getEndClientById(final Integer id);
  
  /**
   * Gets a list of {@code EndClient}s available to the specified user.
   * @param username the user name of the user
   * @return The {@code List} of {@code EndClient}s available to the user.
   */
  public List<EndClient> getEndClientsByUser(final String username);
  
  /**
   * Gets a list of {@code EndClient}s of the specified {@code Client}.
   * @param client the client.
   * @return The {@code List} of {@code EndClient}s of the {@code Client}.
   */
  public List<EndClient> getEndClientsByClient(final Client client);
  
  /**
   * Gets the list of all {@code EndClient}s.
   * @return The list of all {@code EndClient}s.
   */
  public List<EndClient> getAllEndClients();
}
