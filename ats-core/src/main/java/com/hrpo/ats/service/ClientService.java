package com.hrpo.ats.service;

import java.util.List;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.Team;

public interface ClientService {
  
  /**
   * Saves a {@code Client}.
   * @param client The client to save.
   * @return The row id of the client.
   */
  public Integer save(Client client);
  
  /**
   * Gets {@code Client} by its id.
   * @param id the id of the end client to be retrieved.
   * @param isLoadEndClients Will load end clients if set to true.
   * @return the client.
   */
  public Client getClientById(final Integer id, Boolean isLoadEndClients);
  
  /**
   * Gets all {@code Client}s.
   * @return the list of all {@Client}s.
   */
  public List<Client> getAllClients();
  
  /**
   * Gets a list of {@code EndClient}s available to the specified user.
   * @param username the user name of the user
   * @return The {@code List} of {@code EndClient}s available to the user.
   */
  public List<Client> getClientsByTeam(final Team team);
}
