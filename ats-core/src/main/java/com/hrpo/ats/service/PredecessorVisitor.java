package com.hrpo.ats.service;

import com.hrpo.ats.dto.event.ClientSubmissionEventCreated;
import com.hrpo.ats.dto.event.InterviewEventCreated;
import com.hrpo.ats.dto.event.PlacementEventCreated;
import com.hrpo.ats.dto.event.SubmissionEventCreated;

public interface PredecessorVisitor {

  void visit(SubmissionEventCreated event);
  
  void visit(ClientSubmissionEventCreated event);
  
  void visit(InterviewEventCreated event);
  
  void visit(PlacementEventCreated event);
}
