package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Team;

@SpringBootTest
@Transactional
public class TeamDaoTest {
  
  @Autowired
  private TeamDao teamDao;

  @Test
  void testGetAllTeams() {
    List<Team> actualTeams = teamDao.findAllTeams();
    
    assertNotNull(actualTeams);
    assertEquals(2, actualTeams.size());
    assertEquals("Team A", actualTeams.get(0).getName());
    assertEquals("Team A Description", actualTeams.get(0).getDescription());
    assertEquals("team_a@24hrpo.com", actualTeams.get(0).getEmail());
    assertEquals("Team B", actualTeams.get(1).getName());
    assertEquals("Team B Description", actualTeams.get(1).getDescription());
    assertEquals("team_b@24hrpo.com", actualTeams.get(1).getEmail());
  }
  
  @Test
  void testInsert() {
    Team team = new Team();
    team.setName("Test Team");
    team.setDescription("Test Team Desc");
    team.setEmail("test@email.com");
    
    Integer teamId = teamDao.insert(team);
    
    Team actualTeam = teamDao.findById(teamId);
    
    assertNotNull(actualTeam);
    assertEquals("Test Team", actualTeam.getName());
    assertEquals("Test Team Desc", actualTeam.getDescription());
    assertEquals("test@email.com", actualTeam.getEmail());
  }
  
  @Test
  void testUpdate() {
    Team team = new Team();
    team.setName("Test Team");
    team.setDescription("Test Team Desc");
    team.setEmail("test@email.com");
    
    Integer teamId = teamDao.insert(team);
    
    Team updateTeam = teamDao.findById(teamId);
    updateTeam.setName("Update Team");
    updateTeam.setDescription("Update Desc");
    updateTeam.setEmail("update@email.com");
    
    teamDao.update(updateTeam);
    
    Team actualTeam = teamDao.findById(teamId);
    
    assertNotNull(actualTeam);
    assertEquals("Update Team", actualTeam.getName());
    assertEquals("Update Desc", actualTeam.getDescription());
    assertEquals("update@email.com", actualTeam.getEmail());
  }
}
