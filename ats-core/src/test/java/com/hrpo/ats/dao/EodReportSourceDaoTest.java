package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.time.LocalDate;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.Source;

@SpringBootTest
@Transactional
public class EodReportSourceDaoTest {
  
  @Autowired
  private EodReportDao eodReportDao;
  
  @Autowired
  private EodReportSourceDao eodReportSourceDao;
  
  @Test
  public void testInsert() {
    EodReport report = new EodReport();
    report.setComment("Comment");
    
    Integer reportId = eodReportDao.insert(report, 1);
    report.setId(reportId);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    eodReportSource1.setEodReport(report);
    
    Integer id = eodReportSourceDao.insert(eodReportSource1);
    EodReportSource actual = eodReportSourceDao.findById(id);
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.getEodReport().getReportDate());
    
    assertEquals("admin", actual.getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.getEodReport().getUser().getLastName());
    assertEquals("Comment", actual.getEodReport().getComment());
    
    assertEquals("Daxtra - CB", actual.getSource().getName());
    assertEquals("source", actual.getSource().getCategory());
    assertEquals(5, actual.getSourceViews());
    assertEquals(5, actual.getSourceLeads());
  }
  
  @Test
  public void testBatchInsert() {
    EodReport report = new EodReport();
    report.setComment("Comment");
    
    Integer reportId = eodReportDao.insert(report, 1);
    report.setId(reportId);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    eodReportSource1.setEodReport(report);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    eodReportSource2.setEodReport(report);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    eodReportSource3.setEodReport(report);
    
    eodReportSourceDao.batchInsert(
        reportId, Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    var actual = eodReportSourceDao.findByEodReportId(reportId);
    
    assertNotNull(actual);
    assertEquals(3, actual.size());
    
    assertEquals(LocalDate.now(), actual.get(0).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(0).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getEodReport().getUser().getLastName());
    assertEquals("Comment", actual.get(0).getEodReport().getComment());
    
    assertEquals("Daxtra - CB", actual.get(0).getSource().getName());
    assertEquals("source", actual.get(0).getSource().getCategory());
    assertEquals(5, actual.get(0).getSourceViews());
    assertEquals(5, actual.get(0).getSourceLeads());
    
    assertEquals(LocalDate.now(), actual.get(1).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(1).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(1).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(1).getEodReport().getUser().getLastName());
    
    assertEquals("Pipeline Leads", actual.get(1).getSource().getName());
    assertEquals("leads", actual.get(1).getSource().getCategory());
    assertEquals(10, actual.get(1).getLeads());
    
    assertEquals(LocalDate.now(), actual.get(2).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(2).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(2).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(2).getEodReport().getUser().getLastName());
    
    assertEquals("Dials", actual.get(2).getSource().getName());
    assertEquals("activity", actual.get(2).getSource().getCategory());
    assertEquals(15, actual.get(2).getActivity());
  }
  
  @Test
  public void testFindByDate() {
    EodReport report = new EodReport();
    report.setComment("Comment");
    
    Integer reportId = eodReportDao.insert(report, 1);
    report.setId(reportId);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    eodReportSource1.setEodReport(report);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    eodReportSource2.setEodReport(report);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    eodReportSource3.setEodReport(report);
    
    eodReportSourceDao.batchInsert(
        reportId, Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    var actual = eodReportSourceDao.findByDate(LocalDate.now(), LocalDate.now());
    
    assertNotNull(actual);
    assertEquals(3, actual.size());
    
    assertEquals(LocalDate.now(), actual.get(0).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(0).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getEodReport().getUser().getLastName());
    assertEquals("Team A", actual.get(0).getEodReport().getUser().getTeam().getName());
    assertEquals("Comment", actual.get(0).getEodReport().getComment());
    
    assertEquals("Daxtra - CB", actual.get(0).getSource().getName());
    assertEquals("source", actual.get(0).getSource().getCategory());
    assertEquals(5, actual.get(0).getSourceViews());
    assertEquals(5, actual.get(0).getSourceLeads());
    
    assertEquals(LocalDate.now(), actual.get(1).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(1).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(1).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(1).getEodReport().getUser().getLastName());
    assertEquals("Team A", actual.get(1).getEodReport().getUser().getTeam().getName());
    
    assertEquals("Dials", actual.get(1).getSource().getName());
    assertEquals("activity", actual.get(1).getSource().getCategory());
    assertEquals(15, actual.get(1).getActivity());
    
    assertEquals(LocalDate.now(), actual.get(2).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(2).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(2).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(2).getEodReport().getUser().getLastName());
    assertEquals("Team A", actual.get(2).getEodReport().getUser().getTeam().getName());
    
    assertEquals("Pipeline Leads", actual.get(2).getSource().getName());
    assertEquals("leads", actual.get(2).getSource().getCategory());
    assertEquals(10, actual.get(2).getLeads());
  }
  
  @Test
  public void testEodReportValuesInCorrectFields() {
    EodReport report = new EodReport();
    report.setComment("Comment");
    
    Integer reportId = eodReportDao.insert(report, 1);
    report.setId(reportId);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    eodReportSource1.setEodReport(report);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setSourceLeads(10);
    eodReportSource2.setSourceViews(10);
    
    Source source2 = new Source();
    source2.setId(2);
    eodReportSource2.setSource(source2);
    eodReportSource2.setEodReport(report);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setSourceLeads(15);
    eodReportSource3.setSourceViews(15);
    
    Source source3 = new Source();
    source3.setId(3);
    eodReportSource3.setSource(source3);
    eodReportSource3.setEodReport(report);
    
    eodReportSourceDao.batchInsert(
        reportId, Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    var actual = eodReportSourceDao.findByEodReportId(reportId);
    
    assertNotNull(actual);
    assertEquals(3, actual.size());
    
    assertEquals(LocalDate.now(), actual.get(0).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(0).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getEodReport().getUser().getLastName());
    assertEquals("Comment", actual.get(0).getEodReport().getComment());
    
    assertEquals("Daxtra - CB", actual.get(0).getSource().getName());
    assertEquals("source", actual.get(0).getSource().getCategory());
    assertEquals(5, actual.get(0).getSourceViews());
    assertEquals(5, actual.get(0).getSourceLeads());
    
    assertEquals(LocalDate.now(), actual.get(1).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(1).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(1).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(1).getEodReport().getUser().getLastName());
    
    assertEquals("Daxtra - M", actual.get(1).getSource().getName());
    assertEquals("source", actual.get(1).getSource().getCategory());
    assertEquals(10, actual.get(1).getSourceViews());
    assertEquals(10, actual.get(1).getSourceLeads());
    
    assertEquals(LocalDate.now(), actual.get(2).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(2).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(2).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(2).getEodReport().getUser().getLastName());
    
    assertEquals("Daxtra - TP", actual.get(2).getSource().getName());
    assertEquals("source", actual.get(2).getSource().getCategory());
    assertEquals(15, actual.get(2).getSourceViews());
    assertEquals(15, actual.get(2).getSourceLeads());
  }
  
  @Test
  public void testDeleteByEodReportId() {
    EodReport report = new EodReport();
    
    Integer reportId = eodReportDao.insert(report, 1);
    report.setId(reportId);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    eodReportSource1.setEodReport(report);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    eodReportSource2.setEodReport(report);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    eodReportSource3.setEodReport(report);
    
    eodReportSourceDao.batchInsert(
        reportId, Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    
    eodReportSourceDao.deleteByEodReportId(reportId);
    var actual = eodReportSourceDao.findByEodReportId(reportId);
    
    assertNotNull(actual);
    assertEquals(0, actual.size());
  }
}
