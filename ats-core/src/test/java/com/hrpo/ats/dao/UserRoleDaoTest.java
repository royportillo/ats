package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Role;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.UserRole;

@ActiveProfiles("testdata")
@SpringBootTest
@Transactional
public class UserRoleDaoTest {
  
  @Autowired
  private UserRoleDao userRoleDao;
  
  @Autowired
  private UserDao userDao;
  
  @Test
  void testInsertUserRole() {
    
    Team testTeam = new Team();
    testTeam.setId(1);
    
    User testUser = new User();
    testUser.setFirstName("Test FN");
    testUser.setLastName("Test LN");
    testUser.setEmail("test@email.com");
    testUser.setUsername("testuser");
    testUser.setPassword("testpassword");
    testUser.setEnabled(true);
    testUser.setTeam(testTeam);
    Integer userId = userDao.insert(testUser);
    testUser.setId(userId);
    
    Role testRole = new Role();
    testRole.setId(1);
    
    UserRole userRole = new UserRole();
    userRole.setUser(testUser);
    userRole.setRole(testRole);
    Integer userRoleId = userRoleDao.insert(userRole);
    
    UserRole actualUserRole = userRoleDao.findByPrimaryKey(userRoleId);
    
    assertNotNull(actualUserRole);
    assertEquals(userRoleId, actualUserRole.getId());
    
    assertEquals(userId, actualUserRole.getUser().getId());
    assertEquals("Test FN", actualUserRole.getUser().getFirstName());
    assertEquals("Test LN", actualUserRole.getUser().getLastName());
    assertEquals("test@email.com", actualUserRole.getUser().getEmail());
    assertEquals("testuser", actualUserRole.getUser().getUsername());
    
    assertEquals(1, actualUserRole.getRole().getId());
    assertEquals("ADMIN", actualUserRole.getRole().getName());
  }
  
  @Test
  void testBatchInsertUserRole() {
    Team testTeam = new Team();
    testTeam.setId(1);
    
    User testUser = new User();
    testUser.setFirstName("Test FN");
    testUser.setLastName("Test LN");
    testUser.setEmail("test@email.com");
    testUser.setUsername("testuser");
    testUser.setPassword("testpassword");
    testUser.setEnabled(true);
    testUser.setTeam(testTeam);
    Integer userId = userDao.insert(testUser);
    
    Role testRoleAdmin = new Role();
    testRoleAdmin.setId(1);
    
    Role testRoleUser = new Role();
    testRoleUser.setId(2);
    
    UserRole userRole1 = new UserRole();
    UserRole userRole2 = new UserRole();
    
    userRole1.setRole(testRoleAdmin);
    userRole2.setRole(testRoleUser);
    
    List<UserRole> userRoles = Arrays.asList(userRole1, userRole2);
    
    userRoleDao.batchInsert(userId, userRoles);
    List<UserRole> actualUserRoles = userRoleDao.findByUserId(userId);
    
    assertNotNull(actualUserRoles);
    assertEquals(2, actualUserRoles.size());
    
    UserRole actualUserRole1 = actualUserRoles.get(0);
    
    assertEquals(userId, actualUserRole1.getUser().getId());
    assertEquals("Test FN", actualUserRole1.getUser().getFirstName());
    assertEquals("Test LN", actualUserRole1.getUser().getLastName());
    assertEquals("test@email.com", actualUserRole1.getUser().getEmail());
    assertEquals("testuser", actualUserRole1.getUser().getUsername());
    assertEquals(1, actualUserRole1.getRole().getId());
    assertEquals("ADMIN", actualUserRole1.getRole().getName());
    
    UserRole actualUserRole2 = actualUserRoles.get(1);
    
    assertEquals(userId, actualUserRole2.getUser().getId());
    assertEquals("Test FN", actualUserRole2.getUser().getFirstName());
    assertEquals("Test LN", actualUserRole2.getUser().getLastName());
    assertEquals("test@email.com", actualUserRole2.getUser().getEmail());
    assertEquals("testuser", actualUserRole2.getUser().getUsername());
    assertEquals(2, actualUserRole2.getRole().getId());
    
    assertEquals("USER", actualUserRole2.getRole().getName());
  }
  
  @Test
  void testDeleteByUserId() {
    Team testTeam = new Team();
    testTeam.setId(1);
    
    User testUser = new User();
    testUser.setFirstName("Test FN");
    testUser.setLastName("Test LN");
    testUser.setEmail("test@email.com");
    testUser.setUsername("testuser");
    testUser.setPassword("testpassword");
    testUser.setEnabled(true);
    testUser.setTeam(testTeam);
    Integer userId = userDao.insert(testUser);
    
    Role testRoleAdmin = new Role();
    testRoleAdmin.setId(1);
    
    UserRole userRole1 = new UserRole();
    userRole1.setRole(testRoleAdmin);
    
    userRoleDao.batchInsert(userId, Arrays.asList(userRole1));
    userRoleDao.deleteByUserId(userId);
    
    List<UserRole> actualUserRoles = userRoleDao.findByUserId(userId);
    assertNotNull(actualUserRoles);
    assertEquals(0, actualUserRoles.size());
  }

}
