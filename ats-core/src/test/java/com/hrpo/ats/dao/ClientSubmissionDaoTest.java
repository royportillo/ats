package com.hrpo.ats.dao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;

@SpringBootTest
@Transactional
public class ClientSubmissionDaoTest extends AbstractWorkflowEventDaoTest {

  @Override
  AbstractWorkflowEvent getWorkflowEvent() {
    return new ClientSubmissionEvent();
  }

  @Override
  WorkflowDao<? extends AbstractWorkflowEvent> getWorkflowDao() {
    return getClientSubmissionDao();
  }
  
  @Test
  void testInsertClientSubmissionEvent() {
    
    testUpsertWorkflowEvent_New();
  }
  
  @Test
  void testDeleteClientSubmissionEvent() {
    
    testDeleteByJobOrderCandidateId();
  }
  
  @Test
  void testCountClientSubmissionEventsByUserDateRange_TodayOnly() {
    
    testCountWorkflowEventsByUserDateRange_TodayOnly();
  }
  
  @Test
  void testCountClientSubmissionEventsByUserDateRange_OtherUser() {
    
    testCountWorkflowEventsByUserDateRange_OtherUser();
  }
  
  @Test
  void testCountClientSubmissionEventsByUserDateRange_ThreeDays() {
    
    testCountWorkflowEventsByUserDateRange_ThreeDays();
  }
  
}
