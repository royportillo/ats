package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Shortlist;
import com.hrpo.daxtra.dto.Candidate;

@SpringBootTest
@Transactional
public class CandidateDaoTest extends AbstractCandidateDaoTest {
  
  @Test
  void testInsertCandidate() {
    
    Candidate candidate = getCandidateTestData().getCandidate1();
    
    Shortlist shortlist = new Shortlist("dummy shortlist name");
    Integer shortlistId = getShortlistDao().insert(shortlist, 1);
    Integer candidateId = getCandidateDao().upsert(candidate);
    Integer shortlistCandidateId = 
        getCandidateDao().insertShortlistCandidate(shortlistId, candidateId);
    
    Candidate actualCandidate = getCandidateDao().findByPrimaryKey(candidateId);
    
    assertNotNull(shortlistCandidateId);
    assertNotNull(actualCandidate);
    assertEquals("X50", actualCandidate.getCandidateId());
    assertEquals("Dummy Source 1", actualCandidate.getSource());
    assertEquals("C1 First", actualCandidate.getFirstName());
    assertEquals("C1 Last", actualCandidate.getLastName());
    assertEquals("C1 Fullname", actualCandidate.getFullName());
    assertEquals("+1358132101", actualCandidate.getMobile());
    assertEquals("+1234567890", actualCandidate.getPhone());
    assertEquals("c1@email.com", actualCandidate.getEmail());
  }
  
  @Test
  void testInsertCandidate_DuplicateCandidateId() {
    
    Candidate candidate = getCandidateTestData().getCandidate1();
    Candidate dupCandidate = new Candidate();
    dupCandidate.setCandidateId("X50");
    dupCandidate.setSource("Updated Dummy Source 1");
    dupCandidate.setFirstName("C1 First Update");
    dupCandidate.setLastName("C1 Last Update");
    dupCandidate.setFullName("C1 Fullname Update");
    dupCandidate.setPhone("+1234567891");
    dupCandidate.setMobile("+1358132101");
    dupCandidate.setEmail("c1_updated@email.com");
    
    Shortlist shortlist = new Shortlist("dummy shortlist name");
    Integer shortlistId = getShortlistDao().insert(shortlist, 1);
    Integer candidateId = getCandidateDao().upsert(candidate);
    
    Integer dupCandidateId = getCandidateDao().upsert(dupCandidate);
    
    Integer shortlistCandidateId = 
        getCandidateDao().insertShortlistCandidate(shortlistId, dupCandidateId);
    
    Candidate actualCandidate = getCandidateDao().findByPrimaryKey(candidateId);
    
    assertNotNull(shortlistCandidateId);
    assertNotNull(actualCandidate);
    assertEquals("X50", actualCandidate.getCandidateId());
    assertEquals("Updated Dummy Source 1", actualCandidate.getSource());
    assertEquals("C1 First Update", actualCandidate.getFirstName());
    assertEquals("C1 Last Update", actualCandidate.getLastName());
    assertEquals("C1 Fullname Update", actualCandidate.getFullName());
    assertEquals("+1358132101", actualCandidate.getMobile());
    assertEquals("+1234567891", actualCandidate.getPhone());
    assertEquals("c1_updated@email.com", actualCandidate.getEmail());
  }
  
  @Test
  void testFindByCandidateId() {
    
    Candidate candidate = getCandidateTestData().getCandidate1();
    Integer rowId = getCandidateDao().upsert(candidate);
    
    Candidate actualCandidate = getCandidateDao().findByCandidateId("X50", false);
    assertNotNull(actualCandidate);
    assertEquals(rowId, actualCandidate.getId());
    assertEquals("X50", actualCandidate.getCandidateId());
    assertEquals("Dummy Source 1", actualCandidate.getSource());
    assertEquals("C1 First", actualCandidate.getFirstName());
    assertEquals("C1 Last", actualCandidate.getLastName());
    assertEquals("C1 Fullname", actualCandidate.getFullName());
    assertEquals("+1358132101", actualCandidate.getMobile());
    assertEquals("+1234567890", actualCandidate.getPhone());
    assertEquals("c1@email.com", actualCandidate.getEmail());
  }
  
  @Test
  void testUpdateCandidateHrXml() {
    
    Candidate candidate = getCandidateTestData().getCandidate1();
    
    Shortlist shortlist = new Shortlist("dummy shortlist name");
    Integer shortlistId = getShortlistDao().insert(shortlist, 1);
    Integer candidateId = getCandidateDao().upsert(candidate);
    getCandidateDao().insertShortlistCandidate(shortlistId, candidateId);
    
    byte[] encodedHrXml = Base64.getMimeEncoder().encode(
        "<SomeXml><Value>Test</Value></SomeXml>".getBytes(StandardCharsets.UTF_8));
    
    Integer rows = getCandidateDao().updateHrXml(
        new String(encodedHrXml), candidate.getCandidateId());
    
    assertEquals(1, rows.intValue());
    
    Candidate actualCandidate = getCandidateDao()
        .findByCandidateId(candidate.getCandidateId(), true);
    assertNotNull(actualCandidate);
    
    byte[] decodedHrXml = Base64.getMimeDecoder().decode(
        actualCandidate.getHrXml().getBytes(StandardCharsets.UTF_8));
    
    assertEquals("<SomeXml><Value>Test</Value></SomeXml>", new String(decodedHrXml));
  }
  
  @Test
  void testFindByShortlistName() {
    
    Candidate candidate1 = getCandidateTestData().getCandidate1();
    Candidate candidate2 = getCandidateTestData().getCandidate2();
    
    Shortlist shortlist = new Shortlist("dummy shortlist name");
    Integer shortlistId = getShortlistDao().insert(shortlist, 1);
    Integer candidateId1 = getCandidateDao().upsert(candidate1);
    Integer candidateId2 = getCandidateDao().upsert(candidate2);
    getCandidateDao().insertShortlistCandidate(shortlistId, candidateId1);
    getCandidateDao().insertShortlistCandidate(shortlistId, candidateId2);
    
    List<Candidate> actualCandidates = 
        getCandidateDao().findByShortlistName("dummy shortlist name");
    
    assertNotNull(actualCandidates);
    assertEquals(2, actualCandidates.size());
    
    assertAll("Candidate1", 
        () -> assertEquals("X50", actualCandidates.get(0).getCandidateId()),
        () -> assertEquals("Dummy Source 1", actualCandidates.get(0).getSource()),
        () -> assertEquals("C1 First", actualCandidates.get(0).getFirstName()),
        () -> assertEquals("C1 Last", actualCandidates.get(0).getLastName()),
        () -> assertEquals("C1 Fullname", actualCandidates.get(0).getFullName()),
        () -> assertEquals("+1358132101", actualCandidates.get(0).getMobile()),
        () -> assertEquals("+1234567890", actualCandidates.get(0).getPhone()),
        () -> assertEquals("c1@email.com", actualCandidates.get(0).getEmail()));
    
    assertAll("Candidate2", 
        () -> assertEquals("X51", actualCandidates.get(1).getCandidateId()),
        () -> assertEquals("Dummy Source 2", actualCandidates.get(1).getSource()),
        () -> assertEquals("C2 First", actualCandidates.get(1).getFirstName()),
        () -> assertEquals("C2 Last", actualCandidates.get(1).getLastName()),
        () -> assertEquals("C2 Fullname", actualCandidates.get(1).getFullName()),
        () -> assertEquals("+1358132102", actualCandidates.get(1).getMobile()),
        () -> assertEquals("+1234567892", actualCandidates.get(1).getPhone()),
        () -> assertEquals("c2@email.com", actualCandidates.get(1).getEmail()));
  }
}
