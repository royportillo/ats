package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.JobOrderTestData;
import com.hrpo.ats.dto.UserJobOrder;

@ActiveProfiles("testdata")
@SpringBootTest
@Transactional
public class UserJobOrderDaoTest {
  @Autowired
  private UserJobOrderDao userJobOrderDao;
  
  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private JobOrderTestData jobOrderTestData;
  
  @Test
  public void testInsertUserJobOrder() {
    Integer jobOrderId = jobOrderDao.insert(jobOrderTestData.getJobOrder1());
    
    Integer userJobOrderId = userJobOrderDao.insert(jobOrderId, 2, true);
    
    List<UserJobOrder> actualUserJobOrders = userJobOrderDao.findByJobOrderId(jobOrderId);
    
    assertNotNull(actualUserJobOrders);
    assertEquals(1, actualUserJobOrders.size());
    assertEquals(userJobOrderId, actualUserJobOrders.get(0).getId());
    assertEquals("jo-number-101", actualUserJobOrders.get(0).getJobOrder().getNumber());
    assertEquals("dhilario", actualUserJobOrders.get(0).getUser().getUsername());
    assertTrue(actualUserJobOrders.get(0).getIsPrimary());
  }
}
