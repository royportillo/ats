package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.EodReport;

@SpringBootTest
@Transactional
public class EodReportDaoTest {
  
  Logger log = LoggerFactory.getLogger(EodReportDaoTest.class);
  
  @Autowired
  private EodReportDao eodReportDao;
  
  @Test
  void testInsert() {
    
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    Integer id = eodReportDao.insert(report, 1);
    EodReport actual = eodReportDao.findByIdAndUserId(id, 1);
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.getReportDate());
    assertEquals("<p>Test</p>", new String(actualHtml));
    
    assertEquals("admin", actual.getUser().getUsername());
    assertEquals("Recsys", actual.getUser().getFirstName());
    assertEquals("Admin", actual.getUser().getLastName());
    assertEquals("Comment", actual.getComment());
  }
  
  @Test
  void testUpdate() {
    
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    Integer id = eodReportDao.insert(report, 1);
    
    EodReport update = eodReportDao.findByIdAndUserId(id, 1);
    byte[] encodedUpdateHtml = Base64.getMimeEncoder().encode(
        "<p>Update</p>".getBytes(StandardCharsets.UTF_8));
    update.setReportHtml(new String(encodedUpdateHtml));
    update.setComment("Updated Comment");
    
    eodReportDao.update(update, 1);
    
    EodReport actual = eodReportDao.findByIdAndUserId(id, 1);
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.getReportDate());
    assertEquals("<p>Update</p>", new String(actualHtml));
    
    assertEquals("admin", actual.getUser().getUsername());
    assertEquals("Recsys", actual.getUser().getFirstName());
    assertEquals("Admin", actual.getUser().getLastName());
    assertEquals("Updated Comment", actual.getComment());
  }
  
  @Test
  void testFindByReportDate() {
    
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    eodReportDao.insert(report, 1);
    List<EodReport> actual = eodReportDao.findByReportDate(LocalDate.now(), LocalDate.now(), 1);
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.get(0).getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.get(0).getReportDate());
    assertEquals("<p>Test</p>", new String(actualHtml));
    
    assertEquals("admin", actual.get(0).getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getUser().getLastName());
    assertEquals("Comment", actual.get(0).getComment());
  }
  
  @Test
  void testFindByReportDate_NullUserId() {
    
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    eodReportDao.insert(report, 1);
    List<EodReport> actual = eodReportDao.findByReportDate(LocalDate.now(), LocalDate.now(), null);
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.get(0).getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.get(0).getReportDate());
    assertEquals("<p>Test</p>", new String(actualHtml));
    
    assertEquals("admin", actual.get(0).getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getUser().getLastName());
    assertEquals("Comment", actual.get(0).getComment());
  }

}
