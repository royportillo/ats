package com.hrpo.ats.dao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;

@SpringBootTest
@Transactional
public class SubmissionDaoTest extends AbstractWorkflowEventDaoTest {

  @Override
  AbstractWorkflowEvent getWorkflowEvent() {
    return new SubmissionEvent();
  }

  @Override
  WorkflowDao<? extends AbstractWorkflowEvent> getWorkflowDao() {
    return getSubmissionDao();
  }
  
  @Test
  void testInsertSubmissionEvent() {
    
    testUpsertWorkflowEvent_New();
  }
  
  @Test
  void testDeleteSubmissionEvent() {
    
    testDeleteByJobOrderCandidateId();
  }
  
  @Test
  void testCountSubmissionEventsByUserDateRange_TodayOnly() {
    
    testCountWorkflowEventsByUserDateRange_TodayOnly();
  }
  
  @Test
  void testCountSubmissionEventsByUserDateRange_OtherUser() {
    
    testCountWorkflowEventsByUserDateRange_OtherUser();
  }
  
  @Test
  void testCountSubmissionEventsByUserDateRange_ThreeDays() {
    
    testCountWorkflowEventsByUserDateRange_ThreeDays();
  }
}
