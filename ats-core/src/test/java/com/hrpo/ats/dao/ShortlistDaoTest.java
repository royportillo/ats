package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Shortlist;

@SpringBootTest
@Transactional
public class ShortlistDaoTest {

  @Autowired
  private ShortlistDao shortlistDao;

  @Test
  void testInsertShortlist() {

    Shortlist shortlist = new Shortlist("dummy shortlist name");
    Integer shortlistId = shortlistDao.insert(shortlist, 1);

    Shortlist actualShortlist = shortlistDao.findByPrimaryKey(shortlistId);
    
    assertNotNull(actualShortlist);
    assertEquals("dummy shortlist name", actualShortlist.getName());
  }
}
