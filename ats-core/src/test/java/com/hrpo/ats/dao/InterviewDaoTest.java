package com.hrpo.ats.dao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.InterviewEvent;

@SpringBootTest
@Transactional
public class InterviewDaoTest extends AbstractWorkflowEventDaoTest {

  @Override
  AbstractWorkflowEvent getWorkflowEvent() {
    return new InterviewEvent();
  }

  @Override
  WorkflowDao<? extends AbstractWorkflowEvent> getWorkflowDao() {
    return getInterviewDao();
  }
  
  @Test
  void testInsertInterviewEvent() {
    
    testUpsertWorkflowEvent_New();
  }
  
  @Test
  void testDeleteInterviewEvent() {
    
    testDeleteByJobOrderCandidateId();
  }
  
  @Test
  void testCountInterviewEventsByUserDateRange_TodayOnly() {
    
    testCountWorkflowEventsByUserDateRange_TodayOnly();
  }
  
  @Test
  void testCountInterviewEventsByUserDateRange_OtherUser() {
    
    testCountWorkflowEventsByUserDateRange_OtherUser();
  }
  
  @Test
  void testCountInterviewEventsByUserDateRange_ThreeDays() {
    
    testCountWorkflowEventsByUserDateRange_ThreeDays();
  }
}
