package com.hrpo.ats.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import com.hrpo.ats.CandidateTestData;
import com.hrpo.ats.dto.event.PrescreenEvent;

@ActiveProfiles("testdata")
abstract class AbstractCandidateDaoTest {

  @Autowired
  private ShortlistDao shortlistDao;
  
  @Autowired
  private CandidateDao candidateDao;
  
  @Autowired
  @Qualifier("prescreenDao")
  private WorkflowDao<PrescreenEvent> prescreenDao;
  
  @Autowired
  private CandidateTestData candidateTestData;
  
  public AbstractCandidateDaoTest() {
    super();
  }
  
  protected ShortlistDao getShortlistDao() {
    return shortlistDao;
  }
  
  protected CandidateDao getCandidateDao() {
    return candidateDao;
  }
  
  protected WorkflowDao<PrescreenEvent> getPrescreenDao() {
    return prescreenDao;
  }

  protected CandidateTestData getCandidateTestData() {
    return candidateTestData;
  }
}
