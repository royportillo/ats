package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Role;

@SpringBootTest
@Transactional
public class RoleDaoTest {

  @Autowired
  private RoleDao roleDao;
  
  @Test
  public void testFindAllRoles() {
    List<Role> actualRoles = roleDao.findAllRoles();
    
    assertNotNull(actualRoles);
    assertEquals(2, actualRoles.size());
    
    assertEquals("ADMIN", actualRoles.get(0).getName());
    assertEquals("Admin Role", actualRoles.get(0).getDescription());
    assertEquals("USER", actualRoles.get(1).getName());
    assertEquals("User Role", actualRoles.get(1).getDescription());
  }
}
