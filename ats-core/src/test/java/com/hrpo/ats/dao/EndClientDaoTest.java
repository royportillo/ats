package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;

@SpringBootTest
@Transactional
public class EndClientDaoTest {
  
  @Autowired
  private EndClientDao endClientDao;
  
  @Autowired
  private UserDao userDao;
  
  @Test
  public void testInsert() {
    EndClient testEndClient = new EndClient();
    
    testEndClient.setName("Test End Client");
    testEndClient.setDescription("Test Description");
    
    Client client = new Client();
    client.setClientId(1);
    testEndClient.setClient(client);
    
    Integer endClientId = endClientDao.insert(testEndClient);
    
    EndClient actual = endClientDao.findById(endClientId);
    
    assertNotNull(actual);
    assertEquals("Test End Client", actual.getName());
    assertEquals("Client A", actual.getClient().getName());
    assertEquals("Test Description", actual.getDescription());
  }
  
  @Test
  public void testUpdate() {
    EndClient testEndClient = new EndClient();
    
    testEndClient.setName("Test End Client");
    testEndClient.setDescription("Test Description");
    
    Client client = new Client();
    client.setClientId(1);
    testEndClient.setClient(client);
    
    Integer endClientId = endClientDao.insert(testEndClient);
    
    EndClient updateEndClient = endClientDao.findById(endClientId);
    
    updateEndClient.setName("Update End Client");
    updateEndClient.setDescription("Update Description");
    Client testClient = new Client();
    testClient.setClientId(2);
    updateEndClient.setClient(testClient);
    
    endClientDao.update(updateEndClient);
    
    EndClient actual = endClientDao.findById(endClientId);
    
    assertNotNull(actual);
    assertEquals("Update End Client", actual.getName());
    assertEquals("Client B", actual.getClient().getName());
    assertEquals("Update Description", actual.getDescription());
  }
  
  @Test
  public void testFindById() {
    EndClient endClient = endClientDao.findById(1);
    assertNotNull(endClient);
    assertEquals("End Client A", endClient.getName());
    assertEquals("Client A", endClient.getClient().getName());
    assertEquals("End Client A Description", endClient.getDescription());
  }
  
  @Test
  public void testFindEndClientsByUserId() {
    Integer userId = userDao.findIdByUsername("dhilario");
    
    List<EndClient> endClients = endClientDao.findEndClientsByUserId(userId);
    
    assertNotNull(endClients);
    assertEquals(6, endClients.size());
    assertEquals("End Client A", endClients.get(0).getName());
    assertEquals("End Client B", endClients.get(1).getName());
  }
  
  @Test
  public void testFindEndClientsByClient() {
    List<EndClient> endClients = endClientDao.findEndClientsByClient(1);
    
    assertNotNull(endClients);
    assertEquals(3, endClients.size());
    assertEquals("End Client A", endClients.get(0).getName());
    assertEquals("End Client A Description", endClients.get(0).getDescription());
    assertEquals("End Client B", endClients.get(1).getName());
    assertEquals("End Client B Description", endClients.get(1).getDescription());
    assertEquals("End Client C", endClients.get(2).getName());
    assertEquals("End Client C Description", endClients.get(2).getDescription());
  }
  
  @Test
  public void testFindAll() {
    List<EndClient> endClients = endClientDao.findAll();
    
    assertNotNull(endClients);
    assertEquals("End Client A", endClients.get(0).getName());
    assertEquals("End Client A Description", endClients.get(0).getDescription());
    assertEquals("Client A", endClients.get(0).getClient().getName());
    
    assertEquals("End Client B", endClients.get(1).getName());
    assertEquals("End Client B Description", endClients.get(1).getDescription());
    assertEquals("Client A", endClients.get(1).getClient().getName());
    
    assertEquals("End Client C", endClients.get(2).getName());
    assertEquals("End Client C Description", endClients.get(2).getDescription());
    assertEquals("Client A", endClients.get(2).getClient().getName());
  }
}
