package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import com.hrpo.ats.CandidateTestData;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.daxtra.dto.Candidate;

@ActiveProfiles("testdata")
abstract class AbstractWorkflowEventDaoTest {

  @Autowired
  @Qualifier("prescreenDao")
  private WorkflowDao<PrescreenEvent> prescreenDao;
  
  @Autowired
  @Qualifier("submissionDao")
  private WorkflowDao<SubmissionEvent> submissionDao;
  
  @Autowired
  @Qualifier("clientSubmissionDao")
  private WorkflowDao<ClientSubmissionEvent> clientSubmissionDao;
  
  @Autowired
  @Qualifier("interviewDao")
  private WorkflowDao<InterviewEvent> interviewDao;
  
  @Autowired
  @Qualifier("placementDao")
  private WorkflowDao<PlacementEvent> placementDao;
  
  @Autowired
  private CandidateDao candidateDao;
  
  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private JobOrderCandidateDao jobOrderCandidateDao;
  
  @Autowired
  private CandidateTestData candidateTestData;
  
  private final JobOrder jobOrder1 = new JobOrder();
  private final JobOrder jobOrder2 = new JobOrder();
  
  public AbstractWorkflowEventDaoTest() {
    super();
  }
  
  @BeforeEach
  void initJobOrders(TestInfo testInfo) {
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(LocalDate.now());
    jobOrder1.setDateCreated(LocalDate.now());
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(10D);
    jobOrder1.setPayRateEnd(10D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    
    jobOrder2.setNumber("jo-number-103");
    jobOrder2.setJoDate(LocalDate.now());
    jobOrder2.setDateCreated(LocalDate.now());
    jobOrder2.setTitle("test jo title3");
    jobOrder2.setLocation("test jo location3");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    
    
    EndClient endClient = new EndClient();
    EndClient endClient2 = new EndClient();
    endClient.setId(1);
    endClient2.setId(7);
    
    jobOrder1.setEndClient(endClient);
    jobOrder2.setEndClient(endClient2);
  }
  
  protected WorkflowDao<PrescreenEvent> getPrescreenDao() {
    return prescreenDao;
  }
  
  protected WorkflowDao<SubmissionEvent> getSubmissionDao() {
    return submissionDao;
  }
  
  protected WorkflowDao<ClientSubmissionEvent> getClientSubmissionDao() {
    return clientSubmissionDao;
  }
  
  protected WorkflowDao<InterviewEvent> getInterviewDao() {
    return interviewDao;
  }
  
  protected WorkflowDao<PlacementEvent> getPlacementDao() {
    return placementDao;
  }
  
  protected CandidateDao getCandidateDao() {
    return candidateDao;
  }
  
  protected JobOrderDao getJobOrderDao() {
    return jobOrderDao;
  }
  
  protected JobOrderCandidateDao getJobOrderCandidateDao() {
    return jobOrderCandidateDao;
  }
  
  protected JobOrder getJobOrder1TestData() {
    return jobOrder1;
  }
  
  protected JobOrder getJobOrder2TestData() {
    return jobOrder2;
  }

  protected CandidateTestData getCandidateTestData() {
    return candidateTestData;
  }
  
  protected void testUpsertWorkflowEvent_New() {
    Integer candidateId = getCandidateDao().upsert(getCandidateTestData().getCandidate1());
    Integer jobOrderId = getJobOrderDao().insert(getJobOrder1TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    Integer jobOrderCandidateId = getJobOrderCandidateDao()
        .insert(jobOrderId, 2, candidateId, joCandidate);
    
    AbstractWorkflowEvent event = getWorkflowEvent();
    event.setTransactionDate(LocalDate.now());
    event.setRemarks("test remarks");
    Integer eventId = getWorkflowDao().upsert(event, jobOrderCandidateId);

    AbstractWorkflowEvent actualEvent = getWorkflowDao().findByPrimaryKey(eventId);
    
    assertNotNull(actualEvent);
    assertEquals("test remarks", actualEvent.getRemarks());
    assertEquals(LocalDate.now(), actualEvent.getTransactionDate());
  }
  
  protected void testUpsertWorkflowEvent_Existing() {
    Integer candidateId = getCandidateDao().upsert(getCandidateTestData().getCandidate1());
    Integer jobOrderId = getJobOrderDao().insert(getJobOrder1TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    Integer jobOrderCandidateId = getJobOrderCandidateDao()
        .insert(jobOrderId, 2, candidateId, joCandidate);
    
    AbstractWorkflowEvent event = getWorkflowEvent();
    event.setTransactionDate(LocalDate.now());
    event.setRemarks("test remarks");
    Integer eventId = getWorkflowDao().upsert(event, jobOrderCandidateId);
    
    AbstractWorkflowEvent updatedEvent = getWorkflowDao().findByPrimaryKey(eventId);
    updatedEvent.setRemarks("updated remarks");
    updatedEvent.setTransactionDate(LocalDate.now().plusDays(1));

    AbstractWorkflowEvent actualEvent = getWorkflowDao().findByPrimaryKey(eventId);
    
    assertNotNull(actualEvent);
    assertEquals("updated remarks", actualEvent.getRemarks());
    assertEquals(LocalDate.now().plusDays(1), actualEvent.getTransactionDate());
  }
  
  protected void testFindByJobOrderCandidateId() {
    Integer candidateId = getCandidateDao().upsert(getCandidateTestData().getCandidate1());
    Integer jobOrderId = getJobOrderDao().insert(getJobOrder1TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    Integer jobOrderCandidateId = getJobOrderCandidateDao()
        .insert(jobOrderId, 2, candidateId, joCandidate);
    
    AbstractWorkflowEvent event = getWorkflowEvent();
    event.setRemarks("test remarks");
    event.setTransactionDate(LocalDate.now());
    getWorkflowDao().upsert(event, jobOrderCandidateId);

    AbstractWorkflowEvent actualEvent = getWorkflowDao()
        .findByJobOrderCandidateId(jobOrderCandidateId);
    
    assertNotNull(actualEvent);
    assertEquals("test remarks", actualEvent.getRemarks());
    assertEquals(LocalDate.now(), actualEvent.getTransactionDate());
  }
  
  protected void testDeleteByJobOrderCandidateId() {
    Integer candidateId = getCandidateDao().upsert(getCandidateTestData().getCandidate1());
    Integer jobOrderId = getJobOrderDao().insert(getJobOrder1TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    Integer jobOrderCandidateId = getJobOrderCandidateDao()
        .insert(jobOrderId, 2, candidateId, joCandidate);
    
    AbstractWorkflowEvent event = getWorkflowEvent();
    event.setTransactionDate(LocalDate.now());
    Integer eventId = getWorkflowDao().upsert(event, jobOrderCandidateId);

    getWorkflowDao().deleteByJobOrderCandidateId(jobOrderCandidateId);
    
    assertThrows(EmptyResultDataAccessException.class, 
        () -> getWorkflowDao().findByPrimaryKey(eventId));  
  }
  
  protected void testCountWorkflowEventsByUserDateRange_TodayOnly() {
    List<Candidate> candidates = getCandidateTestData().getFiveMockCandidates();
    Integer jobOrderId = getJobOrderDao().insert(getJobOrder1TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    candidates.forEach(candidate -> {
      Integer candidateId = getCandidateDao().upsert(candidate);
      AbstractWorkflowEvent event = getWorkflowEvent();
      event.setTransactionDate(LocalDate.now());
      Integer jobOrderCandidateId = getJobOrderCandidateDao()
          .insert(jobOrderId, 2, candidateId, joCandidate);
      getWorkflowDao().upsert(event, jobOrderCandidateId);
    });
    
    Integer count = getWorkflowDao().findCountByUserAndDateRange(2, null, 
        LocalDate.now(), LocalDate.now());
    
    assertEquals(5, count.intValue());
  }
  
  protected void testCountWorkflowEventsByUserDateRange_OtherUser() {
    List<Candidate> candidates = getCandidateTestData().getFiveMockCandidates();
    Integer jobOrderId = getJobOrderDao().insert(getJobOrder1TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    candidates.forEach(candidate -> {
      Integer candidateId = getCandidateDao().upsert(candidate);
      AbstractWorkflowEvent event = getWorkflowEvent();
      event.setTransactionDate(LocalDate.now());
      Integer jobOrderCandidateId = getJobOrderCandidateDao()
          .insert(jobOrderId, 2, candidateId, joCandidate);
      getWorkflowDao().upsert(event, jobOrderCandidateId);
    });
    
    Integer count = getWorkflowDao().findCountByUserAndDateRange(1, null,
        LocalDate.now(), LocalDate.now());
    
    assertEquals(0, count.intValue());
  }
  
  void testCountWorkflowEventsByUserDateRange_ThreeDays() {
    
    List<Integer> candidateIds = new ArrayList<>();
    
    Candidate candidate1 = getCandidateTestData().getCandidate1();
    Integer candidateId1 = getCandidateDao().upsert(candidate1);
    candidateIds.add(candidateId1);
    
    Candidate candidate2 = getCandidateTestData().getCandidate2();
    Integer candidateId2 = getCandidateDao().upsert(candidate2);
    candidateIds.add(candidateId2);
    
    Candidate candidate3 = getCandidateTestData().getCandidate3();
    Integer candidateId3 = getCandidateDao().upsert(candidate3);
    candidateIds.add(candidateId3);
    
    Integer jobOrderId = getJobOrderDao().insert(getJobOrder1TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    Integer numberOfDays = 3;
    
    for (int i = 0; i < numberOfDays; i++) {
      AbstractWorkflowEvent event1 = getWorkflowEvent();
      event1.setTransactionDate(LocalDate.now().minusDays(i));
      Integer jobOrderCandidateId = 
          getJobOrderCandidateDao().insert(jobOrderId, 1, candidateIds.get(i), joCandidate);
      getWorkflowDao().upsert(event1, jobOrderCandidateId);
    }

    Integer count = getWorkflowDao().findCountByUserAndDateRange(1, null,
        LocalDate.now().minusDays(numberOfDays), LocalDate.now());
    
    assertEquals(3, count.intValue());
  }
  
  protected void testCountWorkflowEventsByUserDateRange_TeamOnly() {
    List<Candidate> candidates1 = new ArrayList<>();
    candidates1.add(getCandidateTestData().getCandidate1());
    candidates1.add(getCandidateTestData().getCandidate2());
    candidates1.add(getCandidateTestData().getCandidate3());
    
    List<Candidate> candidates2 = new ArrayList<>();
    candidates2.add(getCandidateTestData().getCandidate4());
    candidates2.add(getCandidateTestData().getCandidate5());
    
    Integer jobOrderId1 = getJobOrderDao().insert(getJobOrder1TestData());
    Integer jobOrderId2 = getJobOrderDao().insert(getJobOrder2TestData());
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(getJobOrder1TestData().getPayRateStart());
    joCandidate.setPayRateTo(getJobOrder1TestData().getPayRateEnd());
    joCandidate.setPayPeriodRate(getJobOrder1TestData().getBillPeriodRate());
    
    candidates1.forEach(candidate -> {
      Integer candidateId = getCandidateDao().upsert(candidate);
      AbstractWorkflowEvent event = getWorkflowEvent();
      event.setTransactionDate(LocalDate.now());
      Integer jobOrderCandidateId = getJobOrderCandidateDao()
          .insert(jobOrderId1, 2, candidateId, joCandidate);
      getWorkflowDao().upsert(event, jobOrderCandidateId);
    });
    
    candidates2.forEach(candidate -> {
      Integer candidateId = getCandidateDao().upsert(candidate);
      AbstractWorkflowEvent event = getWorkflowEvent();
      event.setTransactionDate(LocalDate.now());
      Integer jobOrderCandidateId = getJobOrderCandidateDao()
          .insert(jobOrderId2, 2, candidateId, joCandidate);
      getWorkflowDao().upsert(event, jobOrderCandidateId);
    });
    
    Integer count = getWorkflowDao().findCountByUserAndDateRange(2, 1, 
        LocalDate.now(), LocalDate.now());
    
    assertEquals(3, count.intValue());
  }
  
  /**
   * The workflow event of the subclass.
   * 
   * @return the workflow event of the subclass.
   */
  abstract AbstractWorkflowEvent getWorkflowEvent();
  
  /**
   * The workflow DAO of the subclass.
   * 
   * @return the workflow DAO of the subclass.
   */
  abstract WorkflowDao<? extends AbstractWorkflowEvent> getWorkflowDao();
}
