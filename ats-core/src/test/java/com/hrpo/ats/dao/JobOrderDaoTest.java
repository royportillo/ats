package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;

@SpringBootTest
@Transactional
public class JobOrderDaoTest extends AbstractJobOrderDaoTest {
  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private UserJobOrderDao userJobOrderDao;

  @Test
  void testInsertJobOrder() {
    JobOrder jobOrder = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId = jobOrderDao.insert(jobOrder);
    userJobOrderDao.insert(jobOrderId, 2, true);

    JobOrder actualJobOrder = jobOrderDao.findByPrimaryKey(jobOrderId);

    assertNotNull(actualJobOrder);
    assertEquals("jo-number-101", actualJobOrder.getNumber());
    assertEquals(LocalDate.now(), actualJobOrder.getJoDate());
    assertEquals(LocalDate.now(), actualJobOrder.getDateCreated());
    assertEquals("test jo title", actualJobOrder.getTitle());
    assertEquals("test jo location", actualJobOrder.getLocation());
    assertEquals("12345", actualJobOrder.getZipCode());
    assertEquals("TEST", actualJobOrder.getDivision());
    assertEquals(LocalDate.now(), actualJobOrder.getStartDate());
    assertEquals(LocalDate.now(), actualJobOrder.getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateEnd());
    assertEquals("perhour", actualJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateEnd());
    assertEquals("perhour", actualJobOrder.getPayPeriodRate());
    assertEquals("3", actualJobOrder.getOpenings());
    assertEquals("3", actualJobOrder.getMaxSubs());
    assertEquals("New", actualJobOrder.getStatus());
    assertEquals("End Client A", actualJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(1), actualJobOrder.getEndClient().getId());
    assertEquals("dhilario", actualJobOrder.getPrimaryOwner().getUser().getUsername());
    
    assertFalse(actualJobOrder.getHasFile());
  }
  
  @Test
  void testFindJobOrderFileById() {
    JobOrder jobOrder = getJobOrderTestData().getJobOrderWithFile();
    
    Integer jobOrderId = jobOrderDao.insert(jobOrder);
    
    JobOrder actualJobOrder = jobOrderDao.findJobOrderFileById(jobOrderId);
    
    assertNotNull(actualJobOrder);
    assertEquals("jo-number-101", actualJobOrder.getNumber());
    assertEquals("dummy/test-data", actualJobOrder.getMimeType());
    assertEquals("dummy.test", actualJobOrder.getFileName());
    assertEquals("TEST", new String(actualJobOrder.getFile(), StandardCharsets.UTF_8));
  }

  @Test
  void testUpdateJobOrder() {

    JobOrder jobOrder = new JobOrder();
    jobOrder.setNumber("jo-number-101");
    jobOrder.setJoDate(LocalDate.now());
    jobOrder.setDateCreated(LocalDate.now());
    jobOrder.setTitle("test jo title");
    jobOrder.setLocation("test jo location");
    jobOrder.setZipCode("12345");
    jobOrder.setDivision("TEST");
    jobOrder.setStartDate(LocalDate.now());
    jobOrder.setEndDate(LocalDate.now());
    jobOrder.setBillRateStart(10D);
    jobOrder.setBillRateEnd(10D);
    jobOrder.setBillPeriodRate("perhour");
    jobOrder.setPayRateStart(9D);
    jobOrder.setPayRateEnd(9D);
    jobOrder.setPayPeriodRate("perhour");
    jobOrder.setOpenings("3");
    jobOrder.setMaxSubs("3");
    jobOrder.setStatus("New");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    jobOrder.setEndClient(endClient);

    Integer jobOrderId = jobOrderDao.insert(jobOrder);
    userJobOrderDao.insert(jobOrderId, 2, true);

    JobOrder jobOrder1 = jobOrderDao.findByPrimaryKey(jobOrderId);
    jobOrder1.setNumber("updated-jo-number");
    jobOrder1.setTitle("updated jo title");
    jobOrder1.setLocation("updated location");
    jobOrder1.setZipCode("23456");
    jobOrder1.setDivision("TEST2");
    jobOrder1.setStartDate(LocalDate.now().plusDays(10));
    jobOrder1.setEndDate(LocalDate.now().plusDays(10));
    jobOrder1.setBillRateStart(11D);
    jobOrder1.setBillRateEnd(11D);
    jobOrder1.setBillPeriodRate("perannum");
    jobOrder1.setPayRateStart(10D);
    jobOrder1.setPayRateEnd(10D);
    jobOrder1.setPayPeriodRate("perannum");
    jobOrder1.setOpenings("4");
    jobOrder1.setMaxSubs("4");
    jobOrder1.setStatus("Updated");

    jobOrderId = jobOrderDao.update(jobOrder1);

    JobOrder updateJobOrder = jobOrderDao.findByPrimaryKey(jobOrderId);

    assertNotNull(updateJobOrder);
    assertEquals("updated-jo-number", updateJobOrder.getNumber());
    assertEquals(LocalDate.now(), updateJobOrder.getJoDate());
    assertEquals(LocalDate.now(), updateJobOrder.getDateCreated());
    assertEquals("updated jo title", updateJobOrder.getTitle());
    assertEquals("updated location", updateJobOrder.getLocation());
    assertEquals("23456", updateJobOrder.getZipCode());
    assertEquals("TEST2", updateJobOrder.getDivision());
    assertEquals(LocalDate.now().plusDays(10), updateJobOrder.getStartDate());
    assertEquals(LocalDate.now().plusDays(10), updateJobOrder.getEndDate());
    assertEquals(Double.valueOf(11D), updateJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(11D), updateJobOrder.getBillRateEnd());
    assertEquals("perannum", updateJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(10D), updateJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(10D), updateJobOrder.getPayRateEnd());
    assertEquals("perannum", updateJobOrder.getPayPeriodRate());
    assertEquals("4", updateJobOrder.getOpenings());
    assertEquals("4", updateJobOrder.getMaxSubs());
    assertEquals("Updated", updateJobOrder.getStatus());
    assertEquals("End Client A", updateJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(1), updateJobOrder.getEndClient().getId());
    assertEquals("dhilario", updateJobOrder.getPrimaryOwner().getUser().getUsername());
    assertFalse(updateJobOrder.getHasFile());
  }

  @Test
  void testFindAll() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();

    Integer jobOrderId2 = jobOrderDao.insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);

    List<JobOrder> jobOrders = jobOrderDao.findAllByUserId(2);
    assertEquals(2, jobOrders.size());
    assertEquals("jo-number-101", jobOrders.get(0).getNumber());
    assertEquals("jo-number-102", jobOrders.get(1).getNumber());
  }
  
  @Test
  void testFindAll_OtherUser() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();

    Integer jobOrderId2 = jobOrderDao.insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);

    List<JobOrder> jobOrders = jobOrderDao.findAllByUserId(1);
    assertEquals(0, jobOrders.size());
  }
  
  @Test
  void testFindAll_NullUserId() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 1, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();

    Integer jobOrderId2 = jobOrderDao.insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);

    List<JobOrder> jobOrders = jobOrderDao.findAllByUserId(null);
    assertEquals(2, jobOrders.size());
    assertEquals("jo-number-101", jobOrders.get(0).getNumber());
    assertEquals("jo-number-102", jobOrders.get(1).getNumber());
  }
  
  @Test
  void testFindAllByUserIdAndClientId() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();

    Integer jobOrderId2 = jobOrderDao.insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);

    List<JobOrder> jobOrders = jobOrderDao.findAllByUserIdAndClientId(2, 1);
    assertEquals(2, jobOrders.size());
    assertEquals("jo-number-101", jobOrders.get(0).getNumber());
    assertEquals("jo-number-102", jobOrders.get(1).getNumber());
  }
  
  @Test
  void testFindAllByUserIdAndClientId_OtherClient() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();

    Integer jobOrderId2 = jobOrderDao.insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);

    List<JobOrder> jobOrders = jobOrderDao.findAllByUserIdAndClientId(2, 2);
    assertEquals(0, jobOrders.size());
  }
  
  @Test
  void testFindAllByUserIdAndClientId_OtherUser() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();

    Integer jobOrderId2 = jobOrderDao.insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);

    List<JobOrder> jobOrders = jobOrderDao.findAllByUserIdAndClientId(1, 1);
    assertEquals(0, jobOrders.size());
  }
  
  @Test
  void testFindByCriteria_IllegalState() {

    assertThrows(IllegalStateException.class, () -> {
      getJobOrderDao().findByCriteria(2, null, null, 1);
    });
  }
  
  @Test
  void testFindByCriteria_JoNumberOnly() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao().findByCriteria(2, "jo-number-101", null, 1);
    
    assertEquals(1, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(0).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(0).getHasFile());
  }
  
  @Test
  void testFindByCriteria_OtherUser() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao().findByCriteria(
        1, "jo-number-101", "test jo", 1);
    
    assertEquals(0, jobOrdersMatched.size());
  }
  
  @Test
  void testFindByCriteria_NullUserId() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao().findByCriteria(
        1, "jo-number-101", "test jo", null);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(0).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(0).getHasFile());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(1).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(1).getHasFile());
  }
  
  @Test
  void testFindByCriteria_TitleOnly() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao().findByCriteria(2, null, "test jo", 1);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(0).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(0).getHasFile());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(1).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(1).getHasFile());
  }
  
  @Test
  void testFindByCriteria_TitleOnly_IgnoreCase() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao().findByCriteria(2, null, "tEsT jO", 1);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(0).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(0).getHasFile());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(1).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(1).getHasFile());
  }
  
  @Test
  void testFindByCriteria_JoNumber_Or_Title_MatchJoNumberOnly() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao()
        .findByCriteria(2, "jo-number-101", "no match title", 1);
    
    assertEquals(1, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(0).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(0).getHasFile());
  }
  
  @Test
  void testFindByCriteria_JoNumber_Or_Title_MatchTitleOnly() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);
    
    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao()
        .findByCriteria(2, "no-jonumber-match", "test jo ti", 1);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(0).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(0).getHasFile());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(1).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(1).getHasFile());
  }
  
  @Test
  void testFindByCriteria_JoNumber_Or_Title_MatchTitleOnly_IgnoreCase() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);
    
    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao()
        .findByCriteria(2, "no-jonumber-match", "tEsT JO Ti", 1);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(0).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(0).getHasFile());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(1), jobOrdersMatched.get(1).getEndClient().getId());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
    assertFalse(jobOrdersMatched.get(1).getHasFile());
  }
  
  @Test
  void testFindByCriteria_NoMatch() {

    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> jobOrdersMatched = getJobOrderDao()
        .findByCriteria(2, "no-jonumber-match", "no match title", 1);
    assertTrue(jobOrdersMatched.isEmpty());
  }
  
  @Test
  void testCountWorkflowEventsByUserDateRange_TeamOnly() {
    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder3();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    Integer count = getJobOrderDao().findCountByUserAndDateRange(2, 1, 
        LocalDate.now(), LocalDate.now());
    
    assertEquals(1, count.intValue());
  }
  
  @Test
  void testCountWorkflowEventsByUserDateRange_TodayOnly() {
    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    Integer count = getJobOrderDao().findCountByUserAndDateRange(2, null, 
        LocalDate.now(), LocalDate.now());
    
    assertEquals(2, count.intValue());
  }
  
  @Test
  void testCountWorkflowEventsByUserDateRange_ThreeDays() {
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    JobOrder jobOrder1 = new JobOrder();
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(LocalDate.now().minusDays(2));
    jobOrder1.setDateCreated(LocalDate.now());
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    jobOrder1.setEndClient(endClient);
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = new JobOrder();
    jobOrder2.setNumber("jo-number-102");
    jobOrder2.setJoDate(LocalDate.now());
    jobOrder2.setDateCreated(LocalDate.now());
    jobOrder2.setTitle("test jo title2");
    jobOrder2.setLocation("test jo location2");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    jobOrder2.setEndClient(endClient);
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    Integer count = getJobOrderDao().findCountByUserAndDateRange(2, null,
        LocalDate.now().minusDays(3), LocalDate.now());
    
    assertEquals(2, count.intValue());
  }
  
  @Test
  void testFindByUserAndDateRange_TodayOnly() {
    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder2();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> actualJobOrders = getJobOrderDao().findByUserAndDateRange(2, null, 
        LocalDate.now(), LocalDate.now());
    
    assertEquals(2, actualJobOrders.size());
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), actualJobOrders.get(0).getEndClient().getId());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(actualJobOrders.get(0).getHasFile());
    
    assertEquals("jo-number-102", actualJobOrders.get(1).getNumber());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getDateCreated());
    assertEquals("test jo title2", actualJobOrders.get(1).getTitle());
    assertEquals("test jo location2", actualJobOrders.get(1).getLocation());
    assertEquals("23456", actualJobOrders.get(1).getZipCode());
    assertEquals("TEST", actualJobOrders.get(1).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(1).getOpenings());
    assertEquals("3", actualJobOrders.get(1).getMaxSubs());
    assertEquals("New", actualJobOrders.get(1).getStatus());
    assertEquals("End Client A", actualJobOrders.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(1), actualJobOrders.get(1).getEndClient().getId());
    assertEquals("dhilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getUsername());
    assertFalse(actualJobOrders.get(1).getHasFile());
  }
  
  @Test
  void testFindByUserAndDateRange_ThreeDays() {
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    JobOrder jobOrder1 = new JobOrder();
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(LocalDate.now().minusDays(2));
    jobOrder1.setDateCreated(LocalDate.now());
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    jobOrder1.setEndClient(endClient);
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = new JobOrder();
    jobOrder2.setNumber("jo-number-102");
    jobOrder2.setJoDate(LocalDate.now());
    jobOrder2.setDateCreated(LocalDate.now());
    jobOrder2.setTitle("test jo title2");
    jobOrder2.setLocation("test jo location2");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    jobOrder2.setEndClient(endClient);
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> actualJobOrders = getJobOrderDao().findByUserAndDateRange(2, null, 
        LocalDate.now().minusDays(3), LocalDate.now());
    
    assertEquals(2, actualJobOrders.size());
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(LocalDate.now().minusDays(2), actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), actualJobOrders.get(0).getEndClient().getId());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(actualJobOrders.get(0).getHasFile());
    
    assertEquals("jo-number-102", actualJobOrders.get(1).getNumber());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getDateCreated());
    assertEquals("test jo title2", actualJobOrders.get(1).getTitle());
    assertEquals("test jo location2", actualJobOrders.get(1).getLocation());
    assertEquals("23456", actualJobOrders.get(1).getZipCode());
    assertEquals("TEST", actualJobOrders.get(1).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(1).getOpenings());
    assertEquals("3", actualJobOrders.get(1).getMaxSubs());
    assertEquals("New", actualJobOrders.get(1).getStatus());
    assertEquals("End Client A", actualJobOrders.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(1), actualJobOrders.get(1).getEndClient().getId());
    assertEquals("dhilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getUsername());
    assertFalse(actualJobOrders.get(1).getHasFile());
  }
  
  @Test
  void testFindByUserAndDateRange_TeamOnly() {
    JobOrder jobOrder1 = getJobOrderTestData().getJobOrder1();
    Integer jobOrderId1 = getJobOrderDao().insert(jobOrder1);
    userJobOrderDao.insert(jobOrderId1, 2, true);

    JobOrder jobOrder2 = getJobOrderTestData().getJobOrder3();
    Integer jobOrderId2 = getJobOrderDao().insert(jobOrder2);
    userJobOrderDao.insert(jobOrderId2, 2, true);
    
    List<JobOrder> actualJobOrders = getJobOrderDao().findByUserAndDateRange(2, 1, 
        LocalDate.now(), LocalDate.now());
    
    assertEquals(1, actualJobOrders.size());
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(1), actualJobOrders.get(0).getEndClient().getId());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    assertFalse(actualJobOrders.get(0).getHasFile());
  }
}
