package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.Team;

@SpringBootTest
@Transactional
public class ClientDaoTest {
  
  Logger log = LoggerFactory.getLogger(ClientDaoTest.class);
  
  @Autowired
  private ClientDao clientDao;
  
  @Test
  public void testInsert() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.FALSE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    Integer clientId = clientDao.insert(client);
    
    Client actualClient = clientDao.findById(clientId);
    
    assertNotNull(actualClient);
    assertEquals("Test Name", actualClient.getName());
    assertEquals("Test Desc", actualClient.getDescription());
    assertEquals(Boolean.FALSE, actualClient.getIsDefault());
    assertEquals("Team A", actualClient.getTeam().getName());
  }
  
  @Test
  public void testInsert_Existing_Default() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.TRUE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    assertThrows(DuplicateKeyException.class, () -> {
      clientDao.insert(client);
    });
  }
  
  @Test
  public void testUpdate() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.FALSE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    Integer clientId = clientDao.insert(client);
    
    Client updateClient = clientDao.findById(clientId);
    
    updateClient.setName("Update Name");
    updateClient.setDescription("Update Description");
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    
    updateClient.setTeam(updateTeam);
    clientDao.update(updateClient);
    
    Client actualClient = clientDao.findById(clientId);
    
    assertNotNull(actualClient);
    assertEquals("Update Name", actualClient.getName());
    assertEquals("Update Description", actualClient.getDescription());
    assertEquals(Boolean.FALSE, actualClient.getIsDefault());
    assertEquals("Team B", actualClient.getTeam().getName());
  }
  
  @Test
  public void testUpdate_Existing_ToDefault() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.FALSE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    Integer clientId = clientDao.insert(client);
    
    Client updateClient = clientDao.findById(clientId);
    
    updateClient.setName("Update Name");
    updateClient.setDescription("Update Description");
    updateClient.setIsDefault(Boolean.TRUE);
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    
    updateClient.setTeam(updateTeam);

    assertThrows(DuplicateKeyException.class, () -> {
      clientDao.update(updateClient);
    });
  }
  
  @Test
  public void testUpdate_Existing_AlreadyDefault() {
    Client updateClient = clientDao.findById(2);
    
    updateClient.setName("Update Name");
    updateClient.setDescription("Update Description");
    updateClient.setIsDefault(Boolean.TRUE);
    clientDao.update(updateClient);
    
    Client actualClient = clientDao.findById(2);
    
    assertNotNull(actualClient);
    assertEquals("Update Name", actualClient.getName());
    assertEquals("Update Description", actualClient.getDescription());
    assertEquals(Boolean.TRUE, actualClient.getIsDefault());
    assertEquals("Team A", actualClient.getTeam().getName());
  }
  
  @Test
  public void testFindById() {
    Client client = clientDao.findById(1);
    
    assertNotNull(client);
    assertEquals("Client A", client.getName());
    assertEquals("Client A Description", client.getDescription());
    assertEquals("Team A", client.getTeam().getName());
  }
  
  @Test
  public void testFindAll() {
    var clients = clientDao.findAll();
    
    assertNotNull(clients);
    assertEquals("Client A", clients.get(0).getName());
    assertEquals("Client A Description", clients.get(0).getDescription());
    assertEquals("Team A", clients.get(0).getTeam().getName());
    
    assertEquals("Client B", clients.get(1).getName());
    assertEquals("Client B Description", clients.get(1).getDescription());
    assertEquals("Team A", clients.get(1).getTeam().getName());
    
    assertEquals("Client C", clients.get(2).getName());
    assertEquals("Client C Description", clients.get(2).getDescription());
    assertEquals("Team B", clients.get(2).getTeam().getName());
  }
  
  @Test
  public void testFindByTeam() {
    
    List<Client> clients = clientDao.findByTeam(1);
    
    assertNotNull(clients);
    assertEquals(2, clients.size());
    assertEquals("Client A", clients.get(0).getName());
    assertEquals("Client A Description", clients.get(0).getDescription());
    assertEquals("Team A", clients.get(0).getTeam().getName());
    assertEquals("Client B", clients.get(1).getName());
    assertEquals("Client B Description", clients.get(1).getDescription());
    assertEquals("Team A", clients.get(0).getTeam().getName());
  }
  
}
