package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;

@SpringBootTest
@Transactional
public class UserDaoTest {

  @Autowired
  private UserDao userDao;
  
  @Test
  void testInsert() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testuser");
    user.setPassword("testpassword");
    user.setEnabled(true);
   
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    userDao.insert(user);
    
    User actualUser = userDao.findByUsername("testuser");
    assertNotNull(actualUser);
    assertEquals("Test FN", actualUser.getFirstName());
    assertEquals("Test LN", actualUser.getLastName());
    assertEquals("test@email.com", actualUser.getEmail());
    assertEquals("testuser", actualUser.getUsername());
    assertEquals("testpassword", actualUser.getPassword());
    assertEquals(1, actualUser.getTeam().getId());
    assertTrue(actualUser.isEnabled());
  }
  
  @Test
  void testUpdate() {
    User origUser = new User();
    origUser.setFirstName("Test FN");
    origUser.setLastName("Test LN");
    origUser.setEmail("test@email.com");
    origUser.setUsername("testuser");
    origUser.setPassword("testpassword");
    origUser.setEnabled(true);
   
    Team origTeam = new Team();
    origTeam.setId(1);
    origUser.setTeam(origTeam);
    
    Integer userId = userDao.insert(origUser);
    
    User updateUser = userDao.findById(userId);
    updateUser.setFirstName("Update FN");
    updateUser.setLastName("Update LN");
    updateUser.setEmail("update@email.com");
    updateUser.setUsername("updateuser");
    updateUser.setPassword("updatepassword");
    updateUser.setEnabled(false);
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    updateUser.setTeam(updateTeam);
    
    userDao.update(updateUser);
    
    User actualUser = userDao.findByUsername("updateuser");
    assertNotNull(actualUser);
    assertEquals("Update FN", actualUser.getFirstName());
    assertEquals("Update LN", actualUser.getLastName());
    assertEquals("update@email.com", actualUser.getEmail());
    assertEquals("updateuser", actualUser.getUsername());
    assertEquals("updatepassword", actualUser.getPassword());
    assertEquals(2, actualUser.getTeam().getId());
    assertFalse(actualUser.isEnabled());
  }
  
  @Test
  void testFindIdByUsername() {

    Integer userId = userDao.findIdByUsername("dhilario");

    assertNotNull(userId);
    assertEquals(2, userId.intValue());
  }
  
  @Test
  void testFindByUsername() {

    User user = userDao.findByUsername("dhilario");

    assertNotNull(user);
    assertEquals(2, user.getId().intValue());
    assertEquals("Dann", user.getFirstName());
    assertEquals("Hilario", user.getLastName());
    assertEquals("dhilario", user.getUsername());
    assertEquals("{bcrypt}$2a$10$ANpH5jaJE8Bs5EVuuM6Km.ZxN7p89eodAUquU/FjfJDYzsjhtU/MK", user.getPassword());
    assertEquals(Integer.valueOf(1), user.getTeam().getId());
    assertEquals("Team A", user.getTeam().getName());
    assertTrue(user.isEnabled());
  }
  
  @Test
  void testFindById() {
    User user = userDao.findById(2);

    assertNotNull(user);
    assertEquals(2, user.getId().intValue());
    assertEquals("Dann", user.getFirstName());
    assertEquals("Hilario", user.getLastName());
    assertEquals("dhilario@24hrpo.com", user.getEmail());
    assertEquals("dhilario", user.getUsername());
    assertEquals("{bcrypt}$2a$10$ANpH5jaJE8Bs5EVuuM6Km.ZxN7p89eodAUquU/FjfJDYzsjhtU/MK", user.getPassword());
    assertEquals(Integer.valueOf(1), user.getTeam().getId());
    assertEquals("Team A", user.getTeam().getName());
    assertTrue(user.isEnabled());
  }

  @Test
  void testFindAllUser_EnabledOnly() {

    List<User> users = userDao.findAllUsers(true);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertTrue(users.get(2).isEnabled());
  }
  
  @Test
  void testFindAllUser() {

    List<User> users = userDao.findAllUsers(false);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest");
    assertEquals(users.get(3).getUsername(), "guest0");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertTrue(users.get(2).isEnabled());
    assertFalse(users.get(3).isEnabled());
  }
  
  @Test
  void testFindAllUserByTeam_EnabledOnly() {

    List<User> users = userDao.findAllUsersByTeam(1, true);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest1");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertTrue(users.get(2).isEnabled());
  }
  
  @Test
  void testFindAllEnabledUserByTeam() {

    List<User> users = userDao.findAllUsersByTeam(1, false);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest0");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertFalse(users.get(2).isEnabled());
  }
}
