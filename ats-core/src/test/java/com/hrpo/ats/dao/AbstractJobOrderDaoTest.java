package com.hrpo.ats.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import com.hrpo.ats.JobOrderTestData;

@ActiveProfiles("testdata")
abstract class AbstractJobOrderDaoTest {

  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private JobOrderTestData jobOrderTestData;
  
  public AbstractJobOrderDaoTest() {
    super();
  }
  
  protected JobOrderDao getJobOrderDao() {
    return jobOrderDao;
  }

  protected JobOrderTestData getJobOrderTestData() {
    return jobOrderTestData;
  }
}
