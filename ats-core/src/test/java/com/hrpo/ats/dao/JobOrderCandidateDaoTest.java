package com.hrpo.ats.dao;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.CandidateTestData;
import com.hrpo.ats.JobOrderTestData;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.daxtra.dto.Candidate;

@ActiveProfiles("testdata")
@SpringBootTest
@Transactional
public class JobOrderCandidateDaoTest {

  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private UserJobOrderDao userJobOrderDao;
  
  @Autowired
  private CandidateDao candidateDao;
  
  @Autowired
  private UserDao userDao;
  
  @Autowired
  private JobOrderCandidateDao jobOrderCandidateDao;
  
  @Autowired
  private WorkflowDao<PrescreenEvent> prescreenDao;
  @Autowired
  private WorkflowDao<SubmissionEvent> submissionDao;
  @Autowired
  private WorkflowDao<ClientSubmissionEvent> clientSubmissionDao;
  @Autowired
  private WorkflowDao<InterviewEvent> interviewDao;
  @Autowired
  private WorkflowDao<PlacementEvent> placementDao;
  
  @Autowired
  private CandidateTestData candidateTestData;
  
  @Autowired
  private JobOrderTestData jobOrderTestData;

  @Test
  void testInsertJobOrderCandidate() {
    
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId = jobOrderDao.insert(jobOrder1);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId = candidateDao.upsert(candidate1);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());

    Integer jobOrderCandidateId = jobOrderCandidateDao.insert(joId, 2, cId, joCandidate);

    JobOrderCandidate actualJobOrderCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);

    assertNotNull(actualJobOrderCandidate);
    assertEquals(jobOrderCandidateId, actualJobOrderCandidate.getId());
    assertEquals(joId, actualJobOrderCandidate.getJobOrder().getId().get());
    assertEquals("jo-number-101", actualJobOrderCandidate.getJobOrder().getNumber());
    assertEquals(LocalDate.now(), actualJobOrderCandidate.getJobOrder().getJoDate());
    assertEquals(cId, actualJobOrderCandidate.getCandidate().getId());
    assertEquals("Dann", actualJobOrderCandidate.getOwner().getFirstName());
    assertEquals("Hilario", actualJobOrderCandidate.getOwner().getLastName());
    assertEquals("dhilario", actualJobOrderCandidate.getOwner().getUsername());
    assertEquals(Double.valueOf(9D), actualJobOrderCandidate.getPayRateFrom());
    assertEquals(Double.valueOf(9D), actualJobOrderCandidate.getPayRateTo());
    assertEquals("perhour",  actualJobOrderCandidate.getPayPeriodRate());
  }
  
  @Test
  void testBatchInsertJobOrderCandidate() {
    
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Candidate candidate2 = candidateTestData.getCandidate2();
    
    Integer cId1 = candidateDao.upsert(candidate1);
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIds = List.of(cId1, cId2);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());

    jobOrderCandidateDao.batchInsert(joId, 2, candidateIds, joCandidate);

    List<JobOrderCandidate> actualJobOrderCandidateList = 
        jobOrderCandidateDao.getByJobOrderId(joId, 2);

    assertNotNull(actualJobOrderCandidateList);
    assertEquals(2, actualJobOrderCandidateList.size());
    
    JobOrderCandidate actualJobOrderCandidate0 = actualJobOrderCandidateList.get(0);
    
    assertEquals(joId, actualJobOrderCandidate0.getJobOrder().getId().get());
    assertEquals(cId1, actualJobOrderCandidate0.getCandidate().getId());
    assertEquals("X50", actualJobOrderCandidate0.getCandidate().getCandidateId());
    assertEquals("Dummy Source 1", actualJobOrderCandidate0.getCandidate().getSource());
    assertEquals("C1 First", actualJobOrderCandidate0.getCandidate().getFirstName());
    assertEquals("C1 Last", actualJobOrderCandidate0.getCandidate().getLastName());
    assertEquals("C1 Fullname", actualJobOrderCandidate0.getCandidate().getFullName());
    assertEquals("+1234567890", actualJobOrderCandidate0.getCandidate().getPhone());
    assertEquals("+1358132101", actualJobOrderCandidate0.getCandidate().getMobile());
    assertEquals("c1@email.com", actualJobOrderCandidate0.getCandidate().getEmail());
    assertEquals("Dann", actualJobOrderCandidate0.getOwner().getFirstName());
    assertEquals("Hilario", actualJobOrderCandidate0.getOwner().getLastName());
    assertEquals("dhilario", actualJobOrderCandidate0.getOwner().getUsername());
    assertEquals(Double.valueOf(9D), actualJobOrderCandidate0.getPayRateFrom());
    assertEquals(Double.valueOf(9D), actualJobOrderCandidate0.getPayRateTo());
    assertEquals("perhour",  actualJobOrderCandidate0.getPayPeriodRate());
    
    JobOrderCandidate actualJobOrderCandidate1 = actualJobOrderCandidateList.get(1);
    assertEquals(joId, actualJobOrderCandidate1.getJobOrder().getId().get());
    assertEquals(cId2, actualJobOrderCandidate1.getCandidate().getId());
    assertEquals("X51", actualJobOrderCandidate1.getCandidate().getCandidateId());
    assertEquals("Dummy Source 2", actualJobOrderCandidate1.getCandidate().getSource());
    assertEquals("C2 First", actualJobOrderCandidate1.getCandidate().getFirstName());
    assertEquals("C2 Last", actualJobOrderCandidate1.getCandidate().getLastName());
    assertEquals("C2 Fullname", actualJobOrderCandidate1.getCandidate().getFullName());
    assertEquals("+1234567892", actualJobOrderCandidate1.getCandidate().getPhone());
    assertEquals("+1358132102", actualJobOrderCandidate1.getCandidate().getMobile());
    assertEquals("c2@email.com", actualJobOrderCandidate1.getCandidate().getEmail());
    assertEquals("Dann", actualJobOrderCandidate1.getOwner().getFirstName());
    assertEquals("Hilario", actualJobOrderCandidate1.getOwner().getLastName());
    assertEquals("dhilario", actualJobOrderCandidate1.getOwner().getUsername());
    assertEquals(Double.valueOf(9D), actualJobOrderCandidate1.getPayRateFrom());
    assertEquals(Double.valueOf(9D), actualJobOrderCandidate1.getPayRateTo());
    assertEquals("perhour",  actualJobOrderCandidate1.getPayPeriodRate());
  }
  
  @Test
  void testBatchInsertJobOrderCandidate_OtherUser() {
    
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Candidate candidate2 = candidateTestData.getCandidate2();
    
    Integer cId1 = candidateDao.upsert(candidate1);
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIds = List.of(cId1, cId2);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());

    jobOrderCandidateDao.batchInsert(joId, 2, candidateIds, joCandidate);

    List<JobOrderCandidate> actualJobOrderCandidateList = 
        jobOrderCandidateDao.getByJobOrderId(joId, 1);

    assertNotNull(actualJobOrderCandidateList);
    assertEquals(0, actualJobOrderCandidateList.size());
  }
  
  @Test
  void testGetWorkflowStatesByJobOrderCandidate_AllStatesTrue() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId = candidateDao.upsert(candidate1);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId = jobOrderCandidateDao.insert(joId, 2, cId, joCandidate);
    
    JobOrderCandidate actualJobOrderCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    var prescreenEvent = new PrescreenEvent();
    prescreenEvent.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(prescreenEvent, jobOrderCandidateId);
    
    var submissionEvent = new SubmissionEvent();
    submissionEvent.setTransactionDate(LocalDate.now());
    submissionDao.upsert(submissionEvent, jobOrderCandidateId);
    
    var clientSubmissionEvent = new ClientSubmissionEvent();
    clientSubmissionEvent.setTransactionDate(LocalDate.now());
    clientSubmissionDao.upsert(clientSubmissionEvent, jobOrderCandidateId);
    
    var interviewEvent = new InterviewEvent();
    interviewEvent.setTransactionDate(LocalDate.now());
    interviewDao.upsert(interviewEvent, jobOrderCandidateId);
    
    var placementEvent = new PlacementEvent();
    placementEvent.setTransactionDate(LocalDate.now());
    placementDao.upsert(placementEvent, jobOrderCandidateId);
    
    JobOrderCandidate.WorkflowState actualWorkflowState = 
        jobOrderCandidateDao.getWorkflowStatesByJobOrderCandidate(actualJobOrderCandidate);
    
    assertNotNull(actualWorkflowState);
    assertTrue(actualWorkflowState.getPrescreenState());
    assertTrue(actualWorkflowState.getSubmissionState());
    assertTrue(actualWorkflowState.getClientSubmissionState());
    assertTrue(actualWorkflowState.getInterviewState());
    assertTrue(actualWorkflowState.getPlacementState());
  }
  
  @Test
  void testGetWorkflowStatesByJobOrderCandidate_AllStatesFalse() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId = candidateDao.upsert(candidate1);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId = jobOrderCandidateDao.insert(joId, 2, cId, joCandidate);
    
    JobOrderCandidate actualJobOrderCandidate = 
        jobOrderCandidateDao.findByPrimaryKey(jobOrderCandidateId);
    
    JobOrderCandidate.WorkflowState actualWorkflowState = 
        jobOrderCandidateDao.getWorkflowStatesByJobOrderCandidate(actualJobOrderCandidate);
    
    assertNotNull(actualWorkflowState);
    assertFalse(actualWorkflowState.getPrescreenState());
    assertFalse(actualWorkflowState.getSubmissionState());
    assertFalse(actualWorkflowState.getClientSubmissionState());
    assertFalse(actualWorkflowState.getInterviewState());
    assertFalse(actualWorkflowState.getPlacementState());
  }
  
  @Test
  void testFindByEventWorkflow_LatestWorkflowOnly() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId1);
    userJobOrderDao.insert(joId1, 2, true);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    var joCandidate1 = new JobOrderCandidate();
    joCandidate1.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate1.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate1.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId1 = jobOrderCandidateDao.insert(joId1, 2, cId1, joCandidate1);
    joCandidate1.setId(jobOrderCandidateId1);
    
    JobOrder jobOrder2 = jobOrderTestData.getJobOrder2();
    Integer joId2 = jobOrderDao.insert(jobOrder2);
    jobOrder2.setId(joId2);
    userJobOrderDao.insert(joId2, 2, true);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    var joCandidate2 = new JobOrderCandidate();
    joCandidate2.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate2.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate2.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId2 = jobOrderCandidateDao.insert(joId2, 2, cId2, joCandidate2);
    joCandidate2.setId(jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate1, WorkflowStage.PRESCREEN);
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.PRESCREEN);
    
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId1);
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderCandidateDao.findByEventWorkflow(2, null, WorkflowStage.PRESCREEN, LocalDate.now(),
            LocalDate.now(), true);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderCandidateDao.findByEventWorkflow(2, null, WorkflowStage.SUBMIT, LocalDate.now(),
            LocalDate.now(), true);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(0).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-102", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title2", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location2", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("23456", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(1).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testFindByEventWorkflow() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId1);
    userJobOrderDao.insert(joId1, 2, true);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    var joCandidate1 = new JobOrderCandidate();
    joCandidate1.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate1.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate1.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId1 = jobOrderCandidateDao.insert(joId1, 2, cId1, joCandidate1);
    joCandidate1.setId(jobOrderCandidateId1);
    
    JobOrder jobOrder2 = jobOrderTestData.getJobOrder2();
    Integer joId2 = jobOrderDao.insert(jobOrder2);
    jobOrder2.setId(joId2);
    userJobOrderDao.insert(joId2, 2, true);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    var joCandidate2 = new JobOrderCandidate();
    joCandidate2.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate2.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate2.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId2 = jobOrderCandidateDao.insert(joId2, 2, cId2, joCandidate2);
    joCandidate2.setId(jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate1, WorkflowStage.PRESCREEN);
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.PRESCREEN);
    
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId1);
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.SUBMIT);
    
    AbstractWorkflowEvent eventSubmit = new SubmissionEvent();
    eventSubmit.setTransactionDate(LocalDate.now());
    submissionDao.upsert(eventSubmit, jobOrderCandidateId2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderCandidateDao.findByEventWorkflow(2, null, WorkflowStage.PRESCREEN, LocalDate.now(),
            LocalDate.now(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderCandidateDao.findByEventWorkflow(2, null, WorkflowStage.PRESENTATION, LocalDate.now(),
            LocalDate.now(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(0).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-102", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title2", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location2", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("23456", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.SUBMIT,
            actualJobOrderCandidates.get(1).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testFindByEventWorkflow_NullUserId() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId1);
    userJobOrderDao.insert(joId1, 2, true);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    var joCandidate1 = new JobOrderCandidate();
    joCandidate1.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate1.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate1.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId1 = jobOrderCandidateDao.insert(joId1, 1, cId1, joCandidate1);
    joCandidate1.setId(jobOrderCandidateId1);
    
    JobOrder jobOrder2 = jobOrderTestData.getJobOrder2();
    Integer joId2 = jobOrderDao.insert(jobOrder2);
    jobOrder2.setId(joId2);
    userJobOrderDao.insert(joId2, 2, true);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    var joCandidate2 = new JobOrderCandidate();
    joCandidate2.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate2.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate2.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId2 = jobOrderCandidateDao.insert(joId2, 2, cId2, joCandidate2);
    joCandidate2.setId(jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate1, WorkflowStage.PRESCREEN);
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.PRESCREEN);
    
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId1);
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.SUBMIT);
    
    AbstractWorkflowEvent eventSubmit = new SubmissionEvent();
    eventSubmit.setTransactionDate(LocalDate.now());
    submissionDao.upsert(eventSubmit, jobOrderCandidateId2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderCandidateDao.findByEventWorkflow(null, null, WorkflowStage.PRESCREEN, 
            LocalDate.now(), LocalDate.now(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderCandidateDao.findByEventWorkflow(null, null, WorkflowStage.PRESENTATION, 
            LocalDate.now(), LocalDate.now(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(0).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Recsys", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Admin", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("admin", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-102", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title2", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location2", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("23456", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.SUBMIT,
            actualJobOrderCandidates.get(1).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testFindByEventWorkflow_TeamOnly() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId1);
    userJobOrderDao.insert(joId1, 2, true);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    var joCandidate1 = new JobOrderCandidate();
    joCandidate1.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate1.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate1.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId1 = jobOrderCandidateDao.insert(joId1, 2, cId1, joCandidate1);
    joCandidate1.setId(jobOrderCandidateId1);
    
    JobOrder jobOrder3 = jobOrderTestData.getJobOrder3();
    Integer joId3 = jobOrderDao.insert(jobOrder3);
    jobOrder3.setId(joId3);
    userJobOrderDao.insert(joId3, 2, true);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    var joCandidate2 = new JobOrderCandidate();
    joCandidate2.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate2.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate2.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId2 = jobOrderCandidateDao.insert(joId3, 2, cId2, joCandidate2);
    joCandidate2.setId(jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate1, WorkflowStage.PRESCREEN);
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.PRESCREEN);
    
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId1);
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderCandidateDao.findByEventWorkflow(2, 1, WorkflowStage.PRESCREEN, 
            LocalDate.now(), LocalDate.now(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderCandidateDao.findByEventWorkflow(2, 2, WorkflowStage.PRESENTATION, 
            LocalDate.now(), LocalDate.now(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(1, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(0).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
  }
  
  @Test
  void testFindByEventWorkflow_OtherUser() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId1);
    userJobOrderDao.insert(joId1, 2, true);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    var joCandidate1 = new JobOrderCandidate();
    joCandidate1.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate1.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate1.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId1 = jobOrderCandidateDao.insert(joId1, 1, cId1, joCandidate1);
    joCandidate1.setId(jobOrderCandidateId1);
    
    JobOrder jobOrder2 = jobOrderTestData.getJobOrder2();
    Integer joId2 = jobOrderDao.insert(jobOrder2);
    jobOrder2.setId(joId2);
    userJobOrderDao.insert(joId2, 2, true);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    var joCandidate2 = new JobOrderCandidate();
    joCandidate2.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate2.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate2.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId2 = jobOrderCandidateDao.insert(joId2, 1, cId2, joCandidate2);
    joCandidate2.setId(jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate1, WorkflowStage.PRESCREEN);
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.PRESCREEN);
    
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId1);
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.SUBMIT);
    
    AbstractWorkflowEvent eventSubmit = new SubmissionEvent();
    eventSubmit.setTransactionDate(LocalDate.now());
    submissionDao.upsert(eventSubmit, jobOrderCandidateId2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderCandidateDao.findByEventWorkflow(2, null, WorkflowStage.PRESCREEN, LocalDate.now(),
            LocalDate.now(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(0, actualJobOrderCandidates.size());
  }
  
  @Test
  void testFindUnprocessed() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(joId1);
    userJobOrderDao.insert(joId1, 2, true);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    var joCandidate1 = new JobOrderCandidate();
    joCandidate1.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate1.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate1.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId1 = jobOrderCandidateDao.insert(joId1, 1, cId1, joCandidate1);
    joCandidate1.setId(jobOrderCandidateId1);
    
    JobOrder jobOrder2 = jobOrderTestData.getJobOrder2();
    Integer joId2 = jobOrderDao.insert(jobOrder2);
    jobOrder2.setId(joId2);
    userJobOrderDao.insert(joId2, 2, true);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    var joCandidate2 = new JobOrderCandidate();
    joCandidate2.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate2.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate2.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jobOrderCandidateId2 = jobOrderCandidateDao.insert(joId2, 2, cId2, joCandidate2);
    joCandidate2.setId(jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate1, WorkflowStage.PRESCREEN);
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.PRESCREEN);
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId1);
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId2);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.SUBMIT);
    AbstractWorkflowEvent eventSubmit = new SubmissionEvent();
    eventSubmit.setTransactionDate(LocalDate.now());
    submissionDao.upsert(eventSubmit, jobOrderCandidateId2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderCandidateDao.findUnprocessed(null, LocalDate.now(),
            LocalDate.now());
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.IMPORT,
            actualJobOrderCandidates.get(0).getWorkflowStage()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(0).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Recsys", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Admin", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("admin", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-102", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title2", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location2", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("23456", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getId()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.IMPORT,
            actualJobOrderCandidates.get(1).getWorkflowStage()),
        () -> assertEquals(WorkflowStage.SUBMIT,
            actualJobOrderCandidates.get(1).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testUpdateWorkflowStage() {
    JobOrder jobOrder1 = jobOrderTestData.getJobOrder1();
    Integer joId1 = jobOrderDao.insert(jobOrder1);
    userJobOrderDao.insert(joId1, 2, true);
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    
    Integer jobOrderCandidateId1 = jobOrderCandidateDao.insert(joId1, 2, cId1, joCandidate);
    joCandidate.setId(jobOrderCandidateId1);
    
    joCandidate.setEventRemarks("prescreen remarks");
    jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
    
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId1);
    
    joCandidate.setEventRemarks("placement remarks");
    jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PLACEMENT);
    
    AbstractWorkflowEvent eventPlacement = new PlacementEvent();
    eventPlacement.setTransactionDate(LocalDate.now());
    placementDao.upsert(eventPlacement, jobOrderCandidateId1);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderCandidateDao.findByEventWorkflow(2, null, WorkflowStage.PLACEMENT, LocalDate.now(),
            LocalDate.now(), true);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderCandidateDao.findByEventWorkflow(2, null, WorkflowStage.PRESCREEN, LocalDate.now(),
            LocalDate.now(), true);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(1, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(1), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getId()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(WorkflowStage.PLACEMENT,
            actualJobOrderCandidates.get(0).getLastWorkflowStage()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()),
        () -> assertEquals("placement remarks", 
            actualJobOrderCandidates.get(0).getEventRemarks()));
  }
}
