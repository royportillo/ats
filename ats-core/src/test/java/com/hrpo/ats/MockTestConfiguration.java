package com.hrpo.ats;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import com.hrpo.ats.dao.CandidateDao;
import com.hrpo.daxtra.dao.DaxtraCandidateDao;

@Profile("mocktest")
@Configuration
public class MockTestConfiguration {
  @Bean
  public DaxtraCandidateDao daxtraCandidateDao() {
      return Mockito.mock(DaxtraCandidateDao.class);
  }
  
  @Bean
  public CandidateDao jdbcCandidateDao() {
      return Mockito.mock(CandidateDao.class);
  }
}
