package com.hrpo.ats;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("testdata")
@Configuration
public class TestDataConfiguration {

  @Bean
  public CandidateTestData candidateTestData() {
      return new CandidateTestData();
  }
  
  @Bean
  public JobOrderTestData jobOrderTestData() {
      return new JobOrderTestData();
  }
}
