package com.hrpo.ats;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;

public class JobOrderTestData {

  private final JobOrder jobOrder1 = new JobOrder();
  private final JobOrder jobOrder2 = new JobOrder();
  private final JobOrder jobOrder3 = new JobOrder();
  private final JobOrder jobOrderWithFile = new JobOrder();
  
  public JobOrderTestData() {
    super();
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    EndClient endClient2 = new EndClient();
    endClient2.setId(7);
    
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(LocalDate.now());
    jobOrder1.setDateCreated(LocalDate.now());
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    jobOrder1.setEndClient(endClient);
    
    jobOrder2.setNumber("jo-number-102");
    jobOrder2.setJoDate(LocalDate.now());
    jobOrder2.setDateCreated(LocalDate.now());
    jobOrder2.setTitle("test jo title2");
    jobOrder2.setLocation("test jo location2");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    jobOrder2.setEndClient(endClient);
    
    jobOrder3.setNumber("jo-number-103");
    jobOrder3.setJoDate(LocalDate.now());
    jobOrder3.setDateCreated(LocalDate.now());
    jobOrder3.setTitle("test jo title3");
    jobOrder3.setLocation("test jo location3");
    jobOrder3.setZipCode("23456");
    jobOrder3.setDivision("TEST");
    jobOrder3.setStartDate(LocalDate.now());
    jobOrder3.setEndDate(LocalDate.now());
    jobOrder3.setBillRateStart(10D);
    jobOrder3.setBillRateEnd(10D);
    jobOrder3.setBillPeriodRate("perhour");
    jobOrder3.setPayRateStart(9D);
    jobOrder3.setPayRateEnd(9D);
    jobOrder3.setPayPeriodRate("perhour");
    jobOrder3.setOpenings("3");
    jobOrder3.setMaxSubs("3");
    jobOrder3.setStatus("New");
    jobOrder3.setEndClient(endClient2);
    
    jobOrderWithFile.setNumber("jo-number-101");
    jobOrderWithFile.setJoDate(LocalDate.now());
    jobOrderWithFile.setDateCreated(LocalDate.now());
    jobOrderWithFile.setTitle("test jo title");
    jobOrderWithFile.setLocation("test jo location");
    jobOrderWithFile.setZipCode("12345");
    jobOrderWithFile.setDivision("TEST");
    jobOrderWithFile.setStartDate(LocalDate.now());
    jobOrderWithFile.setEndDate(LocalDate.now());
    jobOrderWithFile.setBillRateStart(10D);
    jobOrderWithFile.setBillRateEnd(10D);
    jobOrderWithFile.setBillPeriodRate("perhour");
    jobOrderWithFile.setPayRateStart(9D);
    jobOrderWithFile.setPayRateEnd(9D);
    jobOrderWithFile.setPayPeriodRate("perhour");
    jobOrderWithFile.setOpenings("3");
    jobOrderWithFile.setMaxSubs("3");
    jobOrderWithFile.setStatus("New");
    jobOrderWithFile.setEndClient(endClient);
    jobOrderWithFile.setFileName("dummy.test");
    jobOrderWithFile.setMimeType("dummy/test-data");
    jobOrderWithFile.setFile("TEST".getBytes(StandardCharsets.UTF_8));
  }

  public JobOrder getJobOrder1() {
    return jobOrder1;
  }
  
  public JobOrder getJobOrder2() {
    return jobOrder2;
  }
  
  public JobOrder getJobOrder3() {
    return jobOrder3;
  }
  public JobOrder getJobOrderWithFile() {
    return jobOrderWithFile;
  }
}
