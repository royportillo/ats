package com.hrpo.ats;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;

/**
 * This class contains convenience methods for generating mock clock to assist JUnit tests.
 */
public final class MockClockUtils {
  
  /**
   * No reason to instantiate this class.
   */
  private MockClockUtils() {}

  /**
   * Returns a mock {@code Clock} of next or same Friday of this week.
   * 
   * @return the {@code Clock} of next or same Friday of this week.
   */
  public static Clock mockDateToNextOrSameFriday() {
    Instant fridayInstant = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY))
        .atStartOfDay(ZoneId.systemDefault()).toInstant();
    Clock testFridayClock = Clock.fixed(fridayInstant, ZoneId.systemDefault());
    return testFridayClock;
  }
  
  /**
   * Returns a mock {@code Clock} of last Friday of the month.
   * 
   * @return the {@code Clock} of last Friday of the month.
   */
  public static Clock mockDateToLastFridayOfMonth() {
    Instant lastFridayOfMonthInstant = LocalDate.now().with(
        TemporalAdjusters.lastInMonth(DayOfWeek.FRIDAY))
        .atStartOfDay(ZoneId.systemDefault()).toInstant();
    Clock testLastFridayClock = Clock.fixed(lastFridayOfMonthInstant, ZoneId.systemDefault());
    return testLastFridayClock;
  }
}
