package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.exception.CandidateException;
import com.hrpo.daxtra.dto.Candidate;

@SpringBootTest
@Transactional
class CandidateServiceTest extends AbstractCandidateServiceTest {

  @Test
  @DisplayName("Test get candidate ID by shortlist name (Cache does not exist)")
  void testCandidateIdsByShortListName_NoCache() {
    
    Candidate c1 = new Candidate();
    c1.setCandidateId("X50");
    c1.setScore(95.0);
    
    Candidate c2 = new Candidate();
    c2.setCandidateId("X51");
    c2.setScore(75.0);
    
    List<Candidate> cList = new ArrayList<>();
    cList.add(c1);
    cList.add(c2);
        
    Mockito.when(getDaxtraCandidateDao().findCandidateIdsByShortListName("Test MRE", "ws_24hrpo"))
      .thenReturn(cList);
    
    Mockito.when(getDaxtraCandidateDao().findCandidateWithoutHrXmlByCandidateId("X50", "ws_24hrpo"))
      .thenReturn(getCandidateTestData().getCandidate1());
    
    Mockito.when(getDaxtraCandidateDao().findCandidateWithoutHrXmlByCandidateId("X51","ws_24hrpo"))
      .thenReturn(getCandidateTestData().getCandidate2());

    List<Candidate> candidates = 
        getCandidateService().getCandidateDetailsByShortlistName("Test MRE", "dhilario", 
            "ws_24hrpo");

    assertNotNull(candidates);
    assertEquals(2, candidates.size());
    
    assertAll("Candidate1", 
        () -> assertEquals("X50", candidates.get(0).getCandidateId()),
        () -> assertEquals("Dummy Source 1", candidates.get(0).getSource()),
        () -> assertEquals("C1 First", candidates.get(0).getFirstName()),
        () -> assertEquals("C1 Last", candidates.get(0).getLastName()),
        () -> assertEquals("C1 Fullname", candidates.get(0).getFullName()),
        () -> assertEquals("+1234567890", candidates.get(0).getPhone()),
        () -> assertEquals("+1358132101", candidates.get(0).getMobile()),
        () -> assertEquals("c1@email.com", candidates.get(0).getEmail()));
    
    assertAll("Candidate2", 
        () -> assertEquals("X51", candidates.get(1).getCandidateId()),
        () -> assertEquals("Dummy Source 2", candidates.get(1).getSource()),
        () -> assertEquals("C2 First", candidates.get(1).getFirstName()),
        () -> assertEquals("C2 Last", candidates.get(1).getLastName()),
        () -> assertEquals("C2 Fullname", candidates.get(1).getFullName()),
        () -> assertEquals("+1234567892", candidates.get(1).getPhone()),
        () -> assertEquals("+1358132102", candidates.get(1).getMobile()),
        () -> assertEquals("c2@email.com", candidates.get(1).getEmail()));
  }
  
  @Test
  @DisplayName("Test get candidate ID by shortlist name (Cache exist)")
  void testCandidateIdsByShortListName_WithCache() {
    
    List<Candidate> cList = new ArrayList<>();
    cList.add(getCandidateTestData().getCandidate1());
    cList.add(getCandidateTestData().getCandidate2());
        
    // At this point, the shortlist name has been saved in cache as lower case.
    Mockito.when(getJdbcCandidateDao().findByShortlistName("test mre")).thenReturn(cList);
    
    List<Candidate> candidates = 
        getCandidateService().getCandidateDetailsByShortlistName("Test MRE", "dhilario", 
            "ws_24hrpo");

    assertNotNull(candidates);
    assertEquals(2, candidates.size());
    
    assertAll("Candidate1", 
        () -> assertEquals("X50", candidates.get(0).getCandidateId()),
        () -> assertEquals("Dummy Source 1", candidates.get(0).getSource()),
        () -> assertEquals("C1 First", candidates.get(0).getFirstName()),
        () -> assertEquals("C1 Last", candidates.get(0).getLastName()),
        () -> assertEquals("C1 Fullname", candidates.get(0).getFullName()),
        () -> assertEquals("+1234567890", candidates.get(0).getPhone()),
        () -> assertEquals("+1358132101", candidates.get(0).getMobile()),
        () -> assertEquals("c1@email.com", candidates.get(0).getEmail()));
    
    assertAll("Candidate2", 
        () -> assertEquals("X51", candidates.get(1).getCandidateId()),
        () -> assertEquals("Dummy Source 2", candidates.get(1).getSource()),
        () -> assertEquals("C2 First", candidates.get(1).getFirstName()),
        () -> assertEquals("C2 Last", candidates.get(1).getLastName()),
        () -> assertEquals("C2 Fullname", candidates.get(1).getFullName()),
        () -> assertEquals("+1234567892", candidates.get(1).getPhone()),
        () -> assertEquals("+1358132102", candidates.get(1).getMobile()),
        () -> assertEquals("c2@email.com", candidates.get(1).getEmail()));
  }
  
  @Test
  @DisplayName("Test get candidate HRXML by candidate ID "
      + "(Candidate cache does not exist which is an illegal state)")
  void testGetCandidateHrXmlByCandidateId_CandidateCacheDoesNotExist_IllegalState() {
    
    assertThrows(CandidateException.class, () -> {
      getCandidateService().getJobOrderCandidateHrXmlByCandidateId("X50", "dhilario", "ws_24hrpo");
    });
  }
  
  @Test
  @DisplayName("Test get candidate HRXML by candidate ID (HR XML cache does not exist)")
  void testGetCandidateHrXmlByCandidateId_NoCache() {
    
    // Uncached HR XML
    Candidate mockJdbcCandidate = getCandidateTestData().getCandidate1();
    mockJdbcCandidate.setHrXml(null);
    
    // DAXTRA returned HR XML
    Candidate mockDaxtraCandidate = new Candidate();
    mockDaxtraCandidate.setCandidateId("X50");
    mockDaxtraCandidate.setSource("Dummy Source 1");
    mockDaxtraCandidate.setFirstName("C1 First");
    mockDaxtraCandidate.setLastName("C1 Last");
    mockDaxtraCandidate.setFullName("C1 Fullname");
    mockDaxtraCandidate.setPhone("+1234567890");
    mockDaxtraCandidate.setMobile("+1358132101");
    
    mockDaxtraCandidate.setEmail("c1@email.com");
    mockDaxtraCandidate.setHrXml("RHVtbXkgUmVzdW1lIFRlc3Q=");
    
    Mockito.when(getJdbcCandidateDao().findByCandidateId("X50", true))
      .thenReturn(mockJdbcCandidate);
    
    Mockito.when(getDaxtraCandidateDao().findCandidateWithHrXmlByCandidateId("X50", "ws_24hrpo"))
      .thenReturn(mockDaxtraCandidate);
    
    JobOrderCandidate actualJobOrderCandidate = 
        getCandidateService().getJobOrderCandidateHrXmlByCandidateId("X50", "dhilario", 
            "ws_24hrpo");
    
    assertNotNull(actualJobOrderCandidate);
    // decoded Base64 value of "RHVtbXkgUmVzdW1lIFRlc3Q=" is "Dummy Resume Test"
    assertEquals("Dummy Resume Test", actualJobOrderCandidate.getCandidate().getHrXml());
    assertAll("Candidate1", 
        () -> assertEquals("X50", actualJobOrderCandidate.getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", actualJobOrderCandidate.getCandidate().getSource()),
        () -> assertEquals("C1 First", actualJobOrderCandidate.getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", actualJobOrderCandidate.getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", actualJobOrderCandidate.getCandidate().getFullName()),
        () -> assertEquals("+1234567890", actualJobOrderCandidate.getCandidate().getPhone()),
        () -> assertEquals("+1358132101", actualJobOrderCandidate.getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", actualJobOrderCandidate.getCandidate().getEmail()));
  }
  
  @Test
  @DisplayName("Test get candidate HRXML by candidate ID (HR XML cache exist)")
  void testGetCandidateHrXmlByCandidateId_WithHrXmlCache() {
    
    // Cached HR XML
    Candidate mockJdbcCandidate = getCandidateTestData().getCandidate1();
    mockJdbcCandidate.setHrXml("RHVtbXkgUmVzdW1lIFRlc3Q=");
    
    Mockito.when(getJdbcCandidateDao().findByCandidateId("X50", true))
      .thenReturn(mockJdbcCandidate);
    JobOrderCandidate actualJobOrderCandidate = 
        getCandidateService().getJobOrderCandidateHrXmlByCandidateId("X50", "dhilario", 
            "ws_24hrpo");
    
    assertNotNull(actualJobOrderCandidate);
    // decoded Base64 value of "RHVtbXkgUmVzdW1lIFRlc3Q=" is "Dummy Resume Test"
    assertEquals("Dummy Resume Test", actualJobOrderCandidate.getCandidate().getHrXml());
    assertAll("Candidate1", 
        () -> assertEquals("X50", actualJobOrderCandidate.getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", actualJobOrderCandidate.getCandidate().getSource()),
        () -> assertEquals("C1 First", actualJobOrderCandidate.getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", actualJobOrderCandidate.getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", actualJobOrderCandidate.getCandidate().getFullName()),
        () -> assertEquals("+1234567890", actualJobOrderCandidate.getCandidate().getPhone()),
        () -> assertEquals("+1358132101", actualJobOrderCandidate.getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", actualJobOrderCandidate.getCandidate().getEmail()));
  }
  
  @Test
  @DisplayName("Test get candidate raw CV by candidate ID (HR XML cache exist)")
  void testGetCandidateRawCv() {
    
    Candidate mockJdbcCandidate = getCandidateTestData().getCandidate1();
    mockJdbcCandidate.setRawCv("RHVtbXkgUmVzdW1lIFRlc3Q=");
    mockJdbcCandidate.setRawCvFileType("dum;dummy/test-data");
    
    Mockito.when(getDaxtraCandidateDao().findCandidateWithCvByCandidateId("X50", "ws_24hrpo"))
      .thenReturn(mockJdbcCandidate);
    
    Candidate actualCandidate = 
        getCandidateService().getCandidateRawCvByCandidateId("X50", "ws_24hrpo");
    
    assertNotNull(actualCandidate);
    assertEquals("RHVtbXkgUmVzdW1lIFRlc3Q=", actualCandidate.getRawCv());
    assertEquals("dum;dummy/test-data", actualCandidate.getRawCvFileType());
    
    assertAll("Candidate1", 
        () -> assertEquals("X50", actualCandidate.getCandidateId()),
        () -> assertEquals("Dummy Source 1", actualCandidate.getSource()),
        () -> assertEquals("C1 First", actualCandidate.getFirstName()),
        () -> assertEquals("C1 Last", actualCandidate.getLastName()),
        () -> assertEquals("C1 Fullname", actualCandidate.getFullName()),
        () -> assertEquals("+1234567890", actualCandidate.getPhone()),
        () -> assertEquals("+1358132101", actualCandidate.getMobile()),
        () -> assertEquals("c1@email.com", actualCandidate.getEmail()));
  }
}
