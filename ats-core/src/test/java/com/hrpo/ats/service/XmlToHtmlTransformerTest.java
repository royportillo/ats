package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.StringReader;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class XmlToHtmlTransformerTest {

  @Autowired
  private XmlToHtmlTransformer transformer;

  private final String xml = 
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
          "<Resume>" + 
          "    <StructuredXMLResume>This is a test value!</StructuredXMLResume>" +
          "</Resume>";

  private final String xsl = 
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + 
          "<xsl:stylesheet" + 
          "    xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">" + 
          "    <xsl:template match=\"/\">" + 
          "        <html>" + 
          "            <body>" + 
          "                <h1>Test Case Results</h1>" + 
          "                <div>" + 
          "                    <xsl:value-of select=\"/Resume/StructuredXMLResume\" />" + 
          "                </div>" + 
          "            </body>" + 
          "        </html>" + 
          "    </xsl:template>" + 
          "</xsl:stylesheet>";

  @Test
  @DisplayName("Test XML To HTML transformer")
  void testXmlToHtmlTransformer() throws TransformerException {

    Source xmlSource = new StreamSource(new StringReader(xml));
    Source xslSource = new StreamSource(new StringReader(xsl));

    String html = transformer.transform(xslSource, xmlSource);

    assertNotNull(html);
    assertTrue(html.contains("This is a test value!"));
  }
}
