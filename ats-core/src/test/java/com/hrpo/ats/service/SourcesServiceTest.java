package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Source;

@SpringBootTest
@Transactional
public class SourcesServiceTest {
  
  @Autowired
  private SourcesService sourcesService;

  private final Source source1 = new Source();
  private final Source source2 = new Source();
  private final Source source3 = new Source();
  
  @BeforeEach
  void init(TestInfo testInfo) {
    source1.setName("Test Source 1");
    source1.setCategory("source");
    source1.setDescription("Test Description 1");
    source1.setIndex(1);
    
    source2.setName("Test Source 2");
    source2.setCategory("source");
    source2.setDescription("Test Description 2");
    source2.setIndex(2);
    
    source3.setName("Test Source 3");
    source3.setCategory("activity");
    source3.setDescription("Test Description 3");
    source3.setIndex(3);
  }
  
  @Test
  void testSave_New() {
    Integer id = sourcesService.save(source1);
    
    Source actual = sourcesService.getSourceById(id);
    assertNotNull(actual);
    assertEquals("Test Source 1", actual.getName());
    assertEquals("source", actual.getCategory());
    assertEquals("Test Description 1", actual.getDescription());
    assertEquals(1, actual.getIndex());
  }
  
  @Test
  void testSave_Existing() {
    Integer id = sourcesService.save(source1);
    
    Source updated = sourcesService.getSourceById(id);
    updated.setName("Updated Source");
    updated.setCategory("activity");
    updated.setDescription("Updated Description");
    updated.setIndex(4);
    
    sourcesService.save(updated);
    
    Source actual = sourcesService.getSourceById(id);
    assertNotNull(actual);
    assertEquals("Updated Source", actual.getName());
    assertEquals("activity", actual.getCategory());
    assertEquals("Updated Description", actual.getDescription());
    assertEquals(4, actual.getIndex());
  }
  
  @Test
  void testFindAll() {
    var sources = sourcesService.getAllSources();
    
    assertNotNull(sources);
    assertEquals(17, sources.size());
    
    assertEquals("Daxtra - CB", sources.get(0).getName());
    assertEquals("source", sources.get(0).getCategory());
    assertEquals("Daxtra - Career Builder", sources.get(0).getDescription());
    assertEquals(0, sources.get(0).getIndex());
    
    assertEquals("Daxtra - M", sources.get(1).getName());
    assertEquals("source", sources.get(1).getCategory());
    assertEquals("Daxtra - Monster", sources.get(1).getDescription());
    assertEquals(1, sources.get(1).getIndex());
    
    assertEquals("Daxtra - TP", sources.get(2).getName());
    assertEquals("source", sources.get(2).getCategory());
    assertEquals("Daxtra - Talent Pool", sources.get(2).getDescription());
    assertEquals(2, sources.get(2).getIndex());
  }
}
