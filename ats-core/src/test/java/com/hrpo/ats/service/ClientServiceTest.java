package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.exception.ClientException;

@SpringBootTest
@Transactional
public class ClientServiceTest {
  
  @Autowired
  private ClientService clientService;
  
  @Test
  public void testSave_New() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.FALSE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    Integer clientId = clientService.save(client);
    
    Client actualClient = clientService.getClientById(clientId, false);
    
    assertNotNull(actualClient);
    assertEquals("Test Name", actualClient.getName());
    assertEquals("Test Desc", actualClient.getDescription());
    assertEquals(Boolean.FALSE, actualClient.getIsDefault());
    assertEquals("Team A", actualClient.getTeam().getName());
  }
  
  @Test
  public void testSave_Existing() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.FALSE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    Integer clientId = clientService.save(client);
    
    Client updateClient = clientService.getClientById(clientId, false);
    
    updateClient.setName("Update Name");
    updateClient.setDescription("Update Description");
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    
    updateClient.setTeam(updateTeam);
    clientService.save(updateClient);
    
    Client actualClient = clientService.getClientById(clientId, false);
    
    assertNotNull(actualClient);
    assertEquals("Update Name", actualClient.getName());
    assertEquals("Update Description", actualClient.getDescription());
    assertEquals(Boolean.FALSE, actualClient.getIsDefault());
    assertEquals("Team B", actualClient.getTeam().getName());
  }
  
  @Test
  public void testSave_New_ExistingDefault() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.TRUE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    assertThrows(ClientException.class, () -> {
      clientService.save(client);
    });
  }
  
  @Test
  public void testSave_Existing_ExistingDefault() {
    Client client = new Client();
    
    client.setName("Test Name");
    client.setDescription("Test Desc");
    client.setIsDefault(Boolean.FALSE);

    Team team = new Team();
    team.setId(1);
    
    client.setTeam(team);
    
    Integer clientId = clientService.save(client);
    
    Client updateClient = clientService.getClientById(clientId, false);
    
    updateClient.setName("Update Name");
    updateClient.setDescription("Update Description");
    updateClient.setIsDefault(Boolean.TRUE);
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    
    updateClient.setTeam(updateTeam);
    
    assertThrows(ClientException.class, () -> {
      clientService.save(updateClient);
    });
  }
  
  @Test
  public void testGetClientById() {
    Client client = clientService.getClientById(1, false);
    
    assertNotNull(client);
    assertEquals("Client A", client.getName());
  }
  
  @Test
  public void testGetClientById_LoadEndClients() {
    Client client = clientService.getClientById(1, true);
    
    assertNotNull(client);
    assertEquals("Client A", client.getName());
    assertEquals("End Client A", client.getEndClients().get(0).getName());
    assertEquals("End Client B", client.getEndClients().get(1).getName());
    assertEquals("End Client C", client.getEndClients().get(2).getName());
  }
  
  @Test
  public void testGetClientsByTeam() {
    Team team = new Team();
    team.setId(1);
    
    List<Client> actualClients = clientService.getClientsByTeam(team);
    
    assertNotNull(actualClients);
    assertEquals("Client A", actualClients.get(0).getName());
    assertEquals("Client A Description", actualClients.get(0).getDescription());
    assertFalse(actualClients.get(0).getIsDefault());
    assertEquals("Client B", actualClients.get(1).getName());
    assertEquals("Client B Description", actualClients.get(1).getDescription());
    assertTrue(actualClients.get(1).getIsDefault());
  }
  
}
