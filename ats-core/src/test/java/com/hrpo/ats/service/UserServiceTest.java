package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.Collection;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Role;
import com.hrpo.ats.dto.Team;
import com.hrpo.ats.dto.User;
import com.hrpo.ats.dto.UserRole;

@SpringBootTest
@Transactional
class UserServiceTest {

  @Autowired
  private UserService userService;
  
  @Autowired
  private UserDetailsService userServiceForTestingPermissions;
  
  @Test
  void testGetAllUser() {

    var users = userService.getAllUsers(false);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest");
    assertEquals(users.get(3).getUsername(), "guest0");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertTrue(users.get(2).isEnabled());
    assertFalse(users.get(3).isEnabled());
  }
  
  @Test
  void testGetAllUser_EnabledOnly() {

    var users = userService.getAllUsers(true);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertTrue(users.get(2).isEnabled());
  }
  
  @Test
  void testGetAllUserWithRole() {

    var users = userService.getAllUsersWithRole(false);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest");
    assertEquals(users.get(3).getUsername(), "guest0");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertTrue(users.get(2).isEnabled());
    assertFalse(users.get(3).isEnabled());
    
    var user1Roles = users.get(0).getUserRoles();
    assertEquals("ADMIN", user1Roles.get(0).getRole().getName());
    
    var user2Roles = users.get(1).getUserRoles();
    assertEquals("ADMIN", user2Roles.get(0).getRole().getName());
    assertEquals("USER", user2Roles.get(1).getRole().getName());
    
    var user3Roles = users.get(2).getUserRoles();
    assertEquals("USER", user3Roles.get(0).getRole().getName());
    
    var user4Roles = users.get(3).getUserRoles();
    assertEquals("USER", user4Roles.get(0).getRole().getName());
  }
  
  @Test
  void testGetAllUserWithRole_EnabledOnly() {

    var users = userService.getAllUsersWithRole(true);

    assertNotNull(users);
    assertEquals(users.get(0).getUsername(), "admin");
    assertEquals(users.get(1).getUsername(), "dhilario");
    assertEquals(users.get(2).getUsername(), "guest");
    
    assertTrue(users.get(0).isEnabled());
    assertTrue(users.get(1).isEnabled());
    assertTrue(users.get(2).isEnabled());
    
    var user1Roles = users.get(0).getUserRoles();
    assertEquals("ADMIN", user1Roles.get(0).getRole().getName());
    
    var user2Roles = users.get(1).getUserRoles();
    assertEquals("ADMIN", user2Roles.get(0).getRole().getName());
    assertEquals("USER", user2Roles.get(1).getRole().getName());
    
    var user3Roles = users.get(2).getUserRoles();
    assertEquals("USER", user3Roles.get(0).getRole().getName());
  }
  
  @Test
  void testSaveUser() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testusername");
    user.setPassword("testpassword");
    user.setEnabled(true);
    
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    Integer userId = userService.saveUser(user);
    
    User actualUser = userService.getUserById(userId);
    assertNotNull(actualUser);
    assertEquals("Test FN", actualUser.getFirstName());
    assertEquals("Test LN", actualUser.getLastName());
    assertEquals("test@email.com", actualUser.getEmail());
    assertEquals("testusername", actualUser.getUsername());
    assertEquals("testpassword", actualUser.getPassword());
    assertEquals("Team A", actualUser.getTeam().getName());
  }
  
  @Test
  void testSaveUser_WithRole() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testusername");
    user.setPassword("testpassword");
    user.setEnabled(true);
    
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    UserRole userRole = new UserRole();
    Role role = new Role();
    role.setId(2);
    userRole.setRole(role);
    user.setUserRoles(Arrays.asList(userRole));
    
    Integer userId = userService.saveUser(user);
    
    User actualUser = userService.getUserWithRoleById(userId);
    assertNotNull(actualUser);
    assertEquals("Test FN", actualUser.getFirstName());
    assertEquals("Test LN", actualUser.getLastName());
    assertEquals("test@email.com", actualUser.getEmail());
    assertEquals("testusername", actualUser.getUsername());
    assertEquals("testpassword", actualUser.getPassword());
    assertEquals("Team A", actualUser.getTeam().getName());
    assertEquals("USER", actualUser.getUserRoles().get(0).getRole().getName());
  }
  
  @Test
  void testSaveUser_ExistingUser() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testusername");
    user.setPassword("testpassword");
    user.setEnabled(true);
    
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    UserRole userRole = new UserRole();
    Role role = new Role();
    role.setId(2);
    userRole.setRole(role);
    user.setUserRoles(Arrays.asList(userRole));
    
    Integer userId = userService.saveUser(user);
    
    User updatedUser = userService.getUserWithRoleById(userId);
    updatedUser.setFirstName("Update FN");
    updatedUser.setLastName("Update LN");
    updatedUser.setEmail("update@email.com");
    updatedUser.setUsername("updateusername");
    updatedUser.setPassword("updatepassword");
    updatedUser.setEnabled(false);
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    updatedUser.setTeam(updateTeam);
    
    UserRole updateUserRole = new UserRole();
    Role updateRole = new Role();
    updateRole.setId(1);
    updateUserRole.setRole(updateRole);
    updatedUser.setUserRoles(Arrays.asList(updateUserRole));
    
    userService.saveUser(updatedUser);
    
    User actualUser = userService.getUserWithRoleById(userId);
    assertNotNull(actualUser);
    assertEquals("Update FN", actualUser.getFirstName());
    assertEquals("Update LN", actualUser.getLastName());
    assertEquals("update@email.com", actualUser.getEmail());
    assertEquals("updateusername", actualUser.getUsername());
    assertEquals("updatepassword", actualUser.getPassword());
    assertEquals("Team B", actualUser.getTeam().getName());
    assertEquals("ADMIN", actualUser.getUserRoles().get(0).getRole().getName());
    assertFalse(actualUser.isEnabled());
  }
  
  @Test
  void testSaveUser_ExistingUser_NoPassword() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testusername");
    user.setPassword("testpassword");
    user.setEnabled(true);
    
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    UserRole userRole = new UserRole();
    Role role = new Role();
    role.setId(2);
    userRole.setRole(role);
    user.setUserRoles(Arrays.asList(userRole));
    
    Integer userId = userService.saveUser(user);
    
    User updatedUser = userService.getUserById(userId);
    updatedUser.setUsername("updateusername");
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    updatedUser.setFirstName("Update FN");
    updatedUser.setLastName("Update LN");
    updatedUser.setEmail("update@email.com");
    updatedUser.setTeam(updateTeam);
    updatedUser.setEnabled(false);
    
    UserRole updateUserRole = new UserRole();
    Role updateRole = new Role();
    updateRole.setId(1);
    updateUserRole.setRole(updateRole);
    updatedUser.setUserRoles(Arrays.asList(updateUserRole));
    
    userService.saveUser(updatedUser);
    
    User actualUser = userService.getUserWithRoleById(userId);
    assertNotNull(actualUser);
    assertEquals("Update FN", actualUser.getFirstName());
    assertEquals("Update LN", actualUser.getLastName());
    assertEquals("update@email.com", actualUser.getEmail());
    assertEquals("updateusername", actualUser.getUsername());
    assertEquals("testpassword", actualUser.getPassword());
    assertEquals("Team B", actualUser.getTeam().getName());
    assertEquals("ADMIN", actualUser.getUserRoles().get(0).getRole().getName());
    assertFalse(actualUser.isEnabled());
  }
  
  @Test
  void testSaveUser_UpdatedRoles() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testusername");
    user.setPassword("testpassword");
    user.setEnabled(true);
    
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    UserRole adminUserRole = new UserRole();
    Role adminRole = new Role();
    adminRole.setId(1);
    adminUserRole.setRole(adminRole);
    
    user.setUserRoles(Arrays.asList(adminUserRole));
    
    Integer userId = userService.saveUser(user);
    
    User updatedUser = userService.getUserById(userId);
    updatedUser.setFirstName("Update FN");
    updatedUser.setLastName("Update LN");
    updatedUser.setEmail("update@email.com");
    updatedUser.setUsername("updateusername");
    updatedUser.setPassword("updatepassword");
    updatedUser.setEnabled(false);
    
    Team updateTeam = new Team();
    updateTeam.setId(2);
    updatedUser.setTeam(updateTeam);
    
    UserRole updateUserRole = new UserRole();
    Role updateRole = new Role();
    updateRole.setId(2);
    updateUserRole.setRole(updateRole);
    
    updatedUser.setUserRoles(Arrays.asList(updateUserRole));
    
    userService.saveUser(updatedUser);
    
    User actualUser = userService.getUserWithRoleById(userId);
    assertNotNull(actualUser);
    assertEquals("Update FN", actualUser.getFirstName());
    assertEquals("Update LN", actualUser.getLastName());
    assertEquals("update@email.com", actualUser.getEmail());
    assertEquals("updateusername", actualUser.getUsername());
    assertEquals("updatepassword", actualUser.getPassword());
    assertEquals("Team B", actualUser.getTeam().getName());
    assertEquals("USER", actualUser.getUserRoles().get(0).getRole().getName());
    assertFalse(actualUser.isEnabled());
  }
  
  @Test
  void testGetById() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testusername");
    user.setPassword("testpassword");
    user.setEnabled(true);
    
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    Integer userId = userService.saveUser(user);
    
    User actualUser = userService.getUserById(userId);
    assertNotNull(actualUser);
    assertEquals("Test FN", actualUser.getFirstName());
    assertEquals("Test LN", actualUser.getLastName());
    assertEquals("test@email.com", actualUser.getEmail());
    assertEquals("testusername", actualUser.getUsername());
    assertEquals("testpassword", actualUser.getPassword());
    assertEquals("Team A", actualUser.getTeam().getName());
  }
  
  @Test
  void testGetByIdWithRoles() {
    User user = new User();
    user.setFirstName("Test FN");
    user.setLastName("Test LN");
    user.setEmail("test@email.com");
    user.setUsername("testusername");
    user.setPassword("testpassword");
    user.setEnabled(true);
    
    Team team = new Team();
    team.setId(1);
    user.setTeam(team);
    
    UserRole adminUserRole = new UserRole();
    Role adminRole = new Role();
    adminRole.setId(1);
    adminUserRole.setRole(adminRole);
    
    user.setUserRoles(Arrays.asList(adminUserRole));
    
    Integer userId = userService.saveUser(user);
    
    User actualUser = userService.getUserWithRoleById(userId);
    assertNotNull(actualUser);
    assertEquals("Test FN", actualUser.getFirstName());
    assertEquals("Test LN", actualUser.getLastName());
    assertEquals("test@email.com", actualUser.getEmail());
    assertEquals("testusername", actualUser.getUsername());
    assertEquals("testpassword", actualUser.getPassword());
    assertEquals("Team A", actualUser.getTeam().getName());
    assertEquals("ADMIN", actualUser.getUserRoles().get(0).getRole().getName());
  }
  
  @Test
  void testLoadUserWithPermissions() {

    var user = userServiceForTestingPermissions.loadUserByUsername("dhilario");
    Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
    
    assertTrue(authorities.containsAll(Arrays.asList(
        new SimpleGrantedAuthority("menu.dashboard"), 
        new SimpleGrantedAuthority("menu.prescreen"),
        new SimpleGrantedAuthority("menu.submission"),
        new SimpleGrantedAuthority("menu.presentation"),
        new SimpleGrantedAuthority("menu.interview"),
        new SimpleGrantedAuthority("menu.placement"))));
  }
  
  @Test
  void testLoadUserWithPermissions_UsernameNotFoundException() {

    assertThrows(UsernameNotFoundException.class, () -> {
      userServiceForTestingPermissions.loadUserByUsername("notexistingusername");
    });
  }
}
