package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.EodReport;
import com.hrpo.ats.dto.EodReportSource;
import com.hrpo.ats.dto.Source;
import com.hrpo.ats.dto.User;

@SpringBootTest
@Transactional
public class EodReportServiceTest {
  
  @Autowired
  private EodReportService eodReportService;
  
  @Test
  public void testSave_New() {
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    User user = new User();
    user.setId(1);
    report.setUser(user);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    
    report.setEodReportSources(Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    
    Integer id = eodReportService.save(report, "admin");
    EodReport actual = eodReportService.getByIdAndUsername(id, "admin");
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.getReportDate());
    assertEquals("<p>Test</p>", new String(actualHtml));
    
    assertEquals("admin", actual.getUser().getUsername());
    assertEquals("Recsys", actual.getUser().getFirstName());
    assertEquals("Admin", actual.getUser().getLastName());
    assertEquals("Comment", actual.getComment());
    
    var actualEodReportSources = actual.getEodReportSources();
    assertEquals("Daxtra - CB", actualEodReportSources.get(0).getSource().getName());
    assertEquals("source", actualEodReportSources.get(0).getSource().getCategory());
    assertEquals(5, actualEodReportSources.get(0).getSourceViews());
    assertEquals(5, actualEodReportSources.get(0).getSourceLeads());
    
    assertEquals("Pipeline Leads", actualEodReportSources.get(1).getSource().getName());
    assertEquals("leads", actualEodReportSources.get(1).getSource().getCategory());
    assertEquals(10, actualEodReportSources.get(1).getLeads());
    
    assertEquals("Dials", actualEodReportSources.get(2).getSource().getName());
    assertEquals("activity", actualEodReportSources.get(2).getSource().getCategory());
    assertEquals(15, actualEodReportSources.get(2).getActivity());
  }
  
  @Test
  public void testSave_Existing() {
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    User user = new User();
    user.setId(1);
    report.setUser(user);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    
    report.setEodReportSources(Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    
    Integer id = eodReportService.save(report, "admin");
    EodReport update = eodReportService.getByIdAndUsername(id, "admin");
    
    byte[] encodedUpdateHtml = Base64.getMimeEncoder().encode(
        "<p>Update</p>".getBytes(StandardCharsets.UTF_8));
    update.setReportHtml(new String(encodedUpdateHtml));
    
    eodReportSource1.setSourceLeads(10);
    eodReportSource1.setSourceViews(10);
    
    source1.setId(1);
    eodReportSource1.setSource(source1);
    
    eodReportSource2.setLeads(20);
    
    source2.setId(13);
    eodReportSource2.setSource(source2);
    
    eodReportSource3.setActivity(30);
    
    source3.setId(14);
    eodReportSource3.setSource(source3);
    
    update.setEodReportSources(Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    
    eodReportService.save(update, "admin");
    EodReport actual = eodReportService.getByIdAndUsername(id, "admin");
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.getReportDate());
    assertEquals("<p>Update</p>", new String(actualHtml));
    
    assertEquals("admin", actual.getUser().getUsername());
    assertEquals("Recsys", actual.getUser().getFirstName());
    assertEquals("Admin", actual.getUser().getLastName());
    assertEquals("Comment", actual.getComment());
    
    var actualEodReportSources = actual.getEodReportSources();
    assertEquals("Daxtra - CB", actualEodReportSources.get(0).getSource().getName());
    assertEquals("source", actualEodReportSources.get(0).getSource().getCategory());
    assertEquals(10, actualEodReportSources.get(0).getSourceViews());
    assertEquals(10, actualEodReportSources.get(0).getSourceLeads());
    
    assertEquals("Pipeline Leads", actualEodReportSources.get(1).getSource().getName());
    assertEquals("leads", actualEodReportSources.get(1).getSource().getCategory());
    assertEquals(20, actualEodReportSources.get(1).getLeads());
    
    assertEquals("Dials", actualEodReportSources.get(2).getSource().getName());
    assertEquals("activity", actualEodReportSources.get(2).getSource().getCategory());
    assertEquals(30, actualEodReportSources.get(2).getActivity());
  }
  
  @Test
  public void testFindByReportDate() {
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    User user = new User();
    user.setId(1);
    report.setUser(user);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    
    report.setEodReportSources(Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    
    eodReportService.save(report, "admin");
    List<EodReport> actual = eodReportService.getByReportDate(LocalDate.now(), LocalDate.now(), 
        "admin");
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.get(0).getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.get(0).getReportDate());
    assertEquals("<p>Test</p>", new String(actualHtml));
    
    assertEquals("admin", actual.get(0).getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getUser().getLastName());
    assertEquals("Comment", actual.get(0).getComment());
    
    var actualEodReportSources = actual.get(0).getEodReportSources();
    assertEquals("Daxtra - CB", actualEodReportSources.get(0).getSource().getName());
    assertEquals("source", actualEodReportSources.get(0).getSource().getCategory());
    assertEquals(5, actualEodReportSources.get(0).getSourceViews());
    assertEquals(5, actualEodReportSources.get(0).getSourceLeads());
    
    assertEquals("Pipeline Leads", actualEodReportSources.get(1).getSource().getName());
    assertEquals("leads", actualEodReportSources.get(1).getSource().getCategory());
    assertEquals(10, actualEodReportSources.get(1).getLeads());
    
    assertEquals("Dials", actualEodReportSources.get(2).getSource().getName());
    assertEquals("activity", actualEodReportSources.get(2).getSource().getCategory());
    assertEquals(15, actualEodReportSources.get(2).getActivity());
  }
  
  @Test
  public void testFindByReportDate_NullUsername() {
    EodReport report = new EodReport();
    byte[] encodedReportHtml = Base64.getMimeEncoder().encode(
        "<p>Test</p>".getBytes(StandardCharsets.UTF_8));
    
    report.setReportHtml(new String(encodedReportHtml));
    report.setComment("Comment");
    
    User user = new User();
    user.setId(1);
    report.setUser(user);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    
    report.setEodReportSources(Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    
    eodReportService.save(report, "admin");
    List<EodReport> actual = eodReportService.getByReportDate(LocalDate.now(), LocalDate.now(), 
        null);
    
    byte[] actualHtml = Base64.getMimeDecoder()
        .decode(actual.get(0).getReportHtml().getBytes(StandardCharsets.UTF_8));
    
    assertNotNull(actual);
    assertEquals(LocalDate.now(), actual.get(0).getReportDate());
    assertEquals("<p>Test</p>", new String(actualHtml));
    
    assertEquals("admin", actual.get(0).getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getUser().getLastName());
    assertEquals("Comment", actual.get(0).getComment());
    
    var actualEodReportSources = actual.get(0).getEodReportSources();
    assertEquals("Daxtra - CB", actualEodReportSources.get(0).getSource().getName());
    assertEquals("source", actualEodReportSources.get(0).getSource().getCategory());
    assertEquals(5, actualEodReportSources.get(0).getSourceViews());
    assertEquals(5, actualEodReportSources.get(0).getSourceLeads());
    
    assertEquals("Pipeline Leads", actualEodReportSources.get(1).getSource().getName());
    assertEquals("leads", actualEodReportSources.get(1).getSource().getCategory());
    assertEquals(10, actualEodReportSources.get(1).getLeads());
    
    assertEquals("Dials", actualEodReportSources.get(2).getSource().getName());
    assertEquals("activity", actualEodReportSources.get(2).getSource().getCategory());
    assertEquals(15, actualEodReportSources.get(2).getActivity());
  }
  
  @Test
  public void testGetSourceByReportDate() {
    EodReport report = new EodReport();
    report.setComment("Comment");
   
    User user = new User();
    user.setId(1);
    report.setUser(user);
    
    EodReportSource eodReportSource1 = new EodReportSource();
    eodReportSource1.setSourceLeads(5);
    eodReportSource1.setSourceViews(5);
    
    Source source1 = new Source();
    source1.setId(1);
    eodReportSource1.setSource(source1);
    
    EodReportSource eodReportSource2 = new EodReportSource();
    eodReportSource2.setLeads(10);
    
    Source source2 = new Source();
    source2.setId(13);
    eodReportSource2.setSource(source2);
    
    EodReportSource eodReportSource3 = new EodReportSource();
    eodReportSource3.setActivity(15);
    
    Source source3 = new Source();
    source3.setId(14);
    eodReportSource3.setSource(source3);
    
    report.setEodReportSources(Arrays.asList(eodReportSource1, eodReportSource2, eodReportSource3));
    
    eodReportService.save(report, "admin");
    var actual = eodReportService.getSourceByReportDate(LocalDate.now(), LocalDate.now());
    
    assertNotNull(actual);
    assertEquals(3, actual.size());
    
    assertEquals(LocalDate.now(), actual.get(0).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(0).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(0).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(0).getEodReport().getUser().getLastName());
    assertEquals("Team A", actual.get(0).getEodReport().getUser().getTeam().getName());
    assertEquals("Comment", actual.get(0).getEodReport().getComment());
    
    assertEquals("Daxtra - CB", actual.get(0).getSource().getName());
    assertEquals("source", actual.get(0).getSource().getCategory());
    assertEquals(5, actual.get(0).getSourceViews());
    assertEquals(5, actual.get(0).getSourceLeads());
    
    assertEquals(LocalDate.now(), actual.get(1).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(1).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(1).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(1).getEodReport().getUser().getLastName());
    assertEquals("Team A", actual.get(1).getEodReport().getUser().getTeam().getName());
    
    assertEquals("Dials", actual.get(1).getSource().getName());
    assertEquals("activity", actual.get(1).getSource().getCategory());
    assertEquals(15, actual.get(1).getActivity());
    
    assertEquals(LocalDate.now(), actual.get(2).getEodReport().getReportDate());
    
    assertEquals("admin", actual.get(2).getEodReport().getUser().getUsername());
    assertEquals("Recsys", actual.get(2).getEodReport().getUser().getFirstName());
    assertEquals("Admin", actual.get(2).getEodReport().getUser().getLastName());
    assertEquals("Team A", actual.get(2).getEodReport().getUser().getTeam().getName());
    
    assertEquals("Pipeline Leads", actual.get(2).getSource().getName());
    assertEquals("leads", actual.get(2).getSource().getCategory());
    assertEquals(10, actual.get(2).getLeads());
  }
  
}
