package com.hrpo.ats.service;

import static com.hrpo.ats.MockClockUtils.mockDateToLastFridayOfMonth;
import static com.hrpo.ats.MockClockUtils.mockDateToNextOrSameFriday;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.CandidateTestData;
import com.hrpo.ats.dao.CandidateDao;
import com.hrpo.ats.dao.JobOrderCandidateDao;
import com.hrpo.ats.dao.JobOrderDao;
import com.hrpo.ats.dao.UserJobOrderDao;
import com.hrpo.ats.dao.WorkflowDao;
import com.hrpo.ats.dto.Dashboard;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.ClientSubmissionEvent;
import com.hrpo.ats.dto.event.InterviewEvent;
import com.hrpo.ats.dto.event.PlacementEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.daxtra.dto.Candidate;

@ActiveProfiles("testdata")
@SpringBootTest
@Transactional
class DashboardServiceTest {

  @Autowired
  private DashboardService dashboardService;

  @Autowired
  private CandidateDao candidateDao;
  
  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private JobOrderCandidateDao jobOrderCandidateDao;
  
  @Autowired
  private UserJobOrderDao userJobOrderDao;
  
  @Autowired
  @Qualifier("prescreenDao")
  private WorkflowDao<PrescreenEvent> prescreenDao;

  @Autowired
  @Qualifier("submissionDao")
  private WorkflowDao<SubmissionEvent> submissionDao;

  @Autowired
  @Qualifier("clientSubmissionDao")
  private WorkflowDao<ClientSubmissionEvent> clientSubmissionDao;

  @Autowired
  @Qualifier("interviewDao")
  private WorkflowDao<InterviewEvent> interviewDao;

  @Autowired
  @Qualifier("placementDao")
  private WorkflowDao<PlacementEvent> placementDao;
  
  @Autowired
  private CandidateTestData candidateTestData;
  
  private final JobOrder jobOrder1 = new JobOrder();
  private final JobOrder jobOrder3 = new JobOrder();
  
  @BeforeEach
  void initJobOrders(TestInfo testInfo) {
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(LocalDate.now());
    jobOrder1.setDateCreated(LocalDate.now());
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    
    jobOrder3.setNumber("jo-number-102");
    jobOrder3.setJoDate(LocalDate.now());
    jobOrder3.setDateCreated(LocalDate.now());
    jobOrder3.setTitle("test jo title2");
    jobOrder3.setLocation("test jo location2");
    jobOrder3.setZipCode("23456");
    jobOrder3.setDivision("TEST");
    jobOrder3.setStartDate(LocalDate.now());
    jobOrder3.setEndDate(LocalDate.now());
    jobOrder3.setBillRateStart(10D);
    jobOrder3.setBillRateEnd(10D);
    jobOrder3.setBillPeriodRate("perhour");
    jobOrder3.setPayRateStart(9D);
    jobOrder3.setPayRateEnd(9D);
    jobOrder3.setPayPeriodRate("perhour");
    jobOrder3.setOpenings("3");
    jobOrder3.setMaxSubs("3");
    jobOrder3.setStatus("New");
    
    EndClient endClient = new EndClient();
    EndClient endClient2 = new EndClient();
    endClient.setId(1);
    endClient2.setId(7);
    
    jobOrder1.setEndClient(endClient);
    jobOrder3.setEndClient(endClient2);
  }

  @Test
  void testGetWorkflowEventCount_TodayOnly() {

    List<Candidate> candidates = candidateTestData.getFiveMockCandidates();

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    Integer jobOrderId2 = jobOrderDao.insert(jobOrder3);
    jobOrder1.setId(jobOrderId1);
    jobOrder3.setId(jobOrderId2);

    userJobOrderDao.insert(jobOrderId1, 1, true);
    userJobOrderDao.insert(jobOrderId2, 1, true);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder3.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder3.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder3.getBillPeriodRate());
    
    candidates.forEach(candidate -> {
      Integer candidateId = candidateDao.upsert(candidate);
      Integer jobOrderCandidateId = 
          jobOrderCandidateDao.insert(jobOrderId2, 1, candidateId, joCandidate);

      LocalDate transactionDate = LocalDate.now();
      
      populateAllWorkflowEventTables(jobOrderCandidateId, transactionDate);

    });

    Dashboard.WorkflowEventCountPortlet actualData = 
        dashboardService.getWorkflowEventCountPortletData(
            "admin", Clock.systemDefaultZone(), false);

    assertEquals(5, actualData.getPrescreenForTheDayCount().intValue());
    assertEquals(5, actualData.getSubmissionForTheDayCount().intValue());
    assertEquals(5, actualData.getClientSubmissionForTheDayCount().intValue());
    assertEquals(5, actualData.getInterviewForTheDayCount().intValue());
    assertEquals(5, actualData.getPlacementForTheDayCount().intValue());
    assertEquals(2, actualData.getJobOrderForTheDayCount().intValue());
  }

  @Test
  void testGetWorkflowEventCount_ForTheWeek() {
    
    List<Candidate> candidates = candidateTestData.getFiveMockCandidates();
    
    LocalDate transactionDate = LocalDate.now(mockDateToNextOrSameFriday()).with(
        TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    
    jobOrder1.setJoDate(transactionDate);
    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(jobOrderId1);
    
    jobOrder3.setJoDate(transactionDate.plusDays(2));
    Integer jobOrderId2 = jobOrderDao.insert(jobOrder3);
    jobOrder3.setId(jobOrderId2);

    userJobOrderDao.insert(jobOrderId1, 1, true);
    userJobOrderDao.insert(jobOrderId2, 1, true);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    for (Candidate candidate : candidates) {
      Integer candidateId = candidateDao.upsert(candidate);
      Integer jobOrderCandidateId = 
          jobOrderCandidateDao.insert(jobOrderId1, 1, candidateId, joCandidate);
      
      populateAllWorkflowEventTables(jobOrderCandidateId, transactionDate);
      
      transactionDate = transactionDate.plusDays(1);
    };
    
    Dashboard.WorkflowEventCountPortlet actualData = 
        dashboardService.getWorkflowEventCountPortletData(
            "admin", mockDateToNextOrSameFriday(), false);
    
    assertEquals(5, actualData.getPrescreenForTheWeekCount().intValue());
    assertEquals(5, actualData.getSubmissionForTheWeekCount().intValue());
    assertEquals(5, actualData.getClientSubmissionForTheWeekCount().intValue());
    assertEquals(5, actualData.getInterviewForTheWeekCount().intValue());
    assertEquals(5, actualData.getPlacementForTheWeekCount().intValue());
    assertEquals(2, actualData.getJobOrderForTheWeekCount().intValue());
  }

  @Test
  void testGetWorkFlowEventCount_ForTheMonth() {
    
    List<Candidate> candidates = candidateTestData.getFiveMockCandidates();
    
    LocalDate transactionDate = LocalDate.now()
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    jobOrder1.setJoDate(transactionDate);
    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(jobOrderId1);
    
    jobOrder3.setJoDate(transactionDate.plusDays(2));
    Integer jobOrderId2 = jobOrderDao.insert(jobOrder3);
    jobOrder3.setId(jobOrderId2);

    userJobOrderDao.insert(jobOrderId1, 1, true);
    userJobOrderDao.insert(jobOrderId2, 1, true);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    for (Candidate candidate : candidates) {
      Integer candidateId = candidateDao.upsert(candidate);
      Integer jobOrderCandidateId = 
          jobOrderCandidateDao.insert(jobOrderId1, 1, candidateId, joCandidate);
      
      populateAllWorkflowEventTables(jobOrderCandidateId, transactionDate);
      
      transactionDate = transactionDate.plusDays(1);
    }
    
    Dashboard.WorkflowEventCountPortlet actualData = 
        dashboardService.getWorkflowEventCountPortletData(
            "admin", mockDateToLastFridayOfMonth(), false);
    
    assertEquals(5, actualData.getPrescreenForTheMonthCount().intValue());
    assertEquals(5, actualData.getSubmissionForTheMonthCount().intValue());
    assertEquals(5, actualData.getClientSubmissionForTheMonthCount().intValue());
    assertEquals(5, actualData.getInterviewForTheMonthCount().intValue());
    assertEquals(5, actualData.getPlacementForTheMonthCount().intValue());
    assertEquals(2, actualData.getJobOrderForTheMonthCount().intValue());
  }

  @Test
  void testGetWorkFlowEventCount_LastMonth() {
    
    List<Candidate> candidates = candidateTestData.getFiveMockCandidates();
    
    LocalDate lastMonth = LocalDate.now().minusMonths(1);
    LocalDate transactionDate = lastMonth.with(
        TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    jobOrder1.setJoDate(transactionDate);
    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(jobOrderId1);
    
    jobOrder3.setJoDate(transactionDate.plusDays(2));
    Integer jobOrderId2 = jobOrderDao.insert(jobOrder3);
    jobOrder3.setId(jobOrderId2);

    userJobOrderDao.insert(jobOrderId1, 1, true);
    userJobOrderDao.insert(jobOrderId2, 1, true);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    for (Candidate candidate : candidates) {
      Integer candidateId = candidateDao.upsert(candidate);
      Integer jobOrderCandidateId = 
          jobOrderCandidateDao.insert(jobOrderId1, 1, candidateId, joCandidate);
      
      populateAllWorkflowEventTables(jobOrderCandidateId, transactionDate);
      
      transactionDate = transactionDate.plusDays(1);
    }
    
    Dashboard.WorkflowEventCountPortlet actualData = 
        dashboardService.getWorkflowEventCountPortletData(
            "admin", Clock.systemDefaultZone(), false);
    
    assertEquals(5, actualData.getPrescreenLastMonthCount().intValue());
    assertEquals(5, actualData.getSubmissionLastMonthCount().intValue());
    assertEquals(5, actualData.getClientSubmissionLastMonthCount().intValue());
    assertEquals(5, actualData.getInterviewLastMonthCount().intValue());
    assertEquals(5, actualData.getPlacementLastMonthCount().intValue());
    assertEquals(2, actualData.getJobOrderLastMonthCount().intValue());
  }
  
  @Test
  void testGetWorkFlowEventCount_LastLastMonth() {
    
    List<Candidate> candidates = candidateTestData.getFiveMockCandidates();
    
    LocalDate lastMonth = LocalDate.now().minusMonths(2);
    LocalDate transactionDate = lastMonth.with(
        TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    jobOrder1.setJoDate(transactionDate);
    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    jobOrder1.setId(jobOrderId1);
    
    jobOrder3.setJoDate(transactionDate.plusDays(2));
    Integer jobOrderId2 = jobOrderDao.insert(jobOrder3);
    jobOrder3.setId(jobOrderId2);
    
    userJobOrderDao.insert(jobOrderId1, 1, true);
    userJobOrderDao.insert(jobOrderId2, 1, true);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    for (Candidate candidate : candidates) {
      Integer candidateId = candidateDao.upsert(candidate);
      Integer jobOrderCandidateId = 
          jobOrderCandidateDao.insert(jobOrderId1, 1, candidateId, joCandidate);
      
      populateAllWorkflowEventTables(jobOrderCandidateId, transactionDate);
      
      transactionDate = transactionDate.plusDays(1);
    }
    
    Dashboard.WorkflowEventCountPortlet actualData = 
        dashboardService.getWorkflowEventCountPortletData(
            "admin", Clock.systemDefaultZone(), false);
    
    assertEquals(5, actualData.getPrescreenLastLastMonthCount().intValue());
    assertEquals(5, actualData.getSubmissionLastLastMonthCount().intValue());
    assertEquals(5, actualData.getClientSubmissionLastLastMonthCount().intValue());
    assertEquals(5, actualData.getInterviewLastLastMonthCount().intValue());
    assertEquals(5, actualData.getPlacementLastLastMonthCount().intValue());
    assertEquals(2, actualData.getJobOrderLastLastMonthCount().intValue());
  }
  
  @Test
  void testGetWorkflowEventCount_TeamOnly() {

    List<Candidate> candidates1 = Arrays.asList(candidateTestData.getCandidate1(),
        candidateTestData.getCandidate2(), candidateTestData.getCandidate3());
    
    List<Candidate> candidates2 = Arrays.asList(candidateTestData.getCandidate4(),
        candidateTestData.getCandidate5());

    Integer jobOrderId1 = jobOrderDao.insert(jobOrder1);
    Integer jobOrderId3 = jobOrderDao.insert(jobOrder3);
    jobOrder1.setId(jobOrderId1);
    jobOrder3.setId(jobOrderId3);

    userJobOrderDao.insert(jobOrderId1, 2, true);
    userJobOrderDao.insert(jobOrderId3, 2, true);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder3.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder3.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder3.getBillPeriodRate());
    
    candidates1.forEach(candidate -> {
      Integer candidateId = candidateDao.upsert(candidate);
      Integer jobOrderCandidateId = 
          jobOrderCandidateDao.insert(jobOrderId1, 2, candidateId, joCandidate);

      LocalDate transactionDate = LocalDate.now();
      
      populateAllWorkflowEventTables(jobOrderCandidateId, transactionDate);
    });
    
    candidates2.forEach(candidate -> {
      Integer candidateId = candidateDao.upsert(candidate);
      Integer jobOrderCandidateId = 
          jobOrderCandidateDao.insert(jobOrderId3, 2, candidateId, joCandidate);

      LocalDate transactionDate = LocalDate.now();
      
      populateAllWorkflowEventTables(jobOrderCandidateId, transactionDate);
    });
    
    Dashboard.WorkflowEventCountPortlet actualData = 
        dashboardService.getWorkflowEventCountPortletData(
            "dhilario", Clock.systemDefaultZone(), true);
    
    assertEquals(3, actualData.getPrescreenForTheDayCount().intValue());
    assertEquals(3, actualData.getSubmissionForTheDayCount().intValue());
    assertEquals(3, actualData.getClientSubmissionForTheDayCount().intValue());
    assertEquals(3, actualData.getInterviewForTheDayCount().intValue());
    assertEquals(3, actualData.getPlacementForTheDayCount().intValue());
    assertEquals(1, actualData.getJobOrderForTheDayCount().intValue());
  }
  
  private void populateAllWorkflowEventTables(Integer jobOrderCandidateId, LocalDate transactionDate) {
    AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
    eventPrescreen.setTransactionDate(transactionDate);
    prescreenDao.upsert(eventPrescreen, jobOrderCandidateId);
    
    AbstractWorkflowEvent eventSubmission = new SubmissionEvent();
    eventSubmission.setTransactionDate(transactionDate);
    submissionDao.upsert(eventSubmission, jobOrderCandidateId);
    
    AbstractWorkflowEvent eventClientSubmission = new ClientSubmissionEvent();
    eventClientSubmission.setTransactionDate(transactionDate);
    clientSubmissionDao.upsert(eventClientSubmission, jobOrderCandidateId);
    
    AbstractWorkflowEvent eventInterview = new InterviewEvent();
    eventInterview.setTransactionDate(transactionDate);
    interviewDao.upsert(eventInterview, jobOrderCandidateId);
    
    AbstractWorkflowEvent eventPlacement = new PlacementEvent();
    eventPlacement.setTransactionDate(transactionDate);
    placementDao.upsert(eventPlacement, jobOrderCandidateId);
  }
}
