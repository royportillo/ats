package com.hrpo.ats.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.dto.Team;

@SpringBootTest
@Transactional
public class TeamServiceTest {
  
  @Autowired
  private TeamService teamService;

  @Test
  void testGetAllTeams() {
    
    List<Team> actualTeams = teamService.getAllTeams();
    
    assertEquals(2, actualTeams.size());
    assertEquals("Team A", actualTeams.get(0).getName());
    assertEquals("Team A Description", actualTeams.get(0).getDescription());
    assertEquals("team_a@24hrpo.com", actualTeams.get(0).getEmail());
    assertEquals("Team B", actualTeams.get(1).getName());
    assertEquals("Team B Description", actualTeams.get(1).getDescription());
    assertEquals("team_b@24hrpo.com", actualTeams.get(1).getEmail());
  }
  
  @Test
  void testGetAllTeamsWithUsers() {
    
    List<Team> actualTeams = teamService.getAllTeamsWithUsers();
    
    assertEquals(2, actualTeams.size());
    assertEquals("Team A", actualTeams.get(0).getName());
    assertEquals("Team A Description", actualTeams.get(0).getDescription());
    assertEquals("team_a@24hrpo.com", actualTeams.get(0).getEmail());
    
    var actualTeam1Users = actualTeams.get(0).getUsers();
    assertEquals("admin", actualTeam1Users.get(0).getUsername());
    assertEquals("dhilario", actualTeam1Users.get(1).getUsername());
    assertEquals("guest1", actualTeam1Users.get(2).getUsername());
    
    assertEquals("Team B", actualTeams.get(1).getName());
    assertEquals("Team B Description", actualTeams.get(1).getDescription());
    assertEquals("team_b@24hrpo.com", actualTeams.get(1).getEmail());
    
    var actualTeam2Users = actualTeams.get(1).getUsers();
    assertEquals("guest", actualTeam2Users.get(0).getUsername());
  }
  
  @Test
  void testSaveTeam_New() {
    Team team = new Team();
    team.setName("Test Team");
    team.setDescription("Test Team Desc");
    team.setEmail("test@email.com");
    
    Integer teamId = teamService.saveTeam(team);
    
    Team actualTeam = teamService.getById(teamId);
    
    assertNotNull(actualTeam);
    assertEquals("Test Team", actualTeam.getName());
    assertEquals("Test Team Desc", actualTeam.getDescription());
    assertEquals("test@email.com", actualTeam.getEmail());
  }
  
  @Test
  void testSaveTeam_Existing() {
    Team team = new Team();
    team.setName("Test Team");
    team.setDescription("Test Team Desc");
    team.setEmail("test@email.com");
    
    Integer teamId = teamService.saveTeam(team);
    
    Team updateTeam = teamService.getById(teamId);
    updateTeam.setName("Update Team");
    updateTeam.setDescription("Update Desc");
    updateTeam.setEmail("update@email.com");
    
    teamService.saveTeam(updateTeam);
    
    Team actualTeam = teamService.getById(teamId);
    
    assertNotNull(actualTeam);
    assertEquals("Update Team", actualTeam.getName());
    assertEquals("Update Desc", actualTeam.getDescription());
    assertEquals("update@email.com", actualTeam.getEmail());
  }
  
  @Test
  void testGetByIdWithUserAndClients() {
    Team actualTeam = teamService.getByIdWithUserAndClients(1, false);
    
    assertNotNull(actualTeam);
    assertEquals("Team A", actualTeam.getName());
    assertEquals("Team A Description", actualTeam.getDescription());
    assertEquals("team_a@24hrpo.com", actualTeam.getEmail());
    
    var actualUsers = actualTeam.getUsers();
    assertNotNull(actualUsers);
    assertEquals("admin", actualUsers.get(0).getUsername());
    assertEquals("dhilario", actualUsers.get(1).getUsername());
    assertEquals("guest1", actualUsers.get(2).getUsername());
    
    var actualClients = actualTeam.getClients();
    assertNotNull(actualClients);
    assertEquals("Client A", actualClients.get(0).getName());
    assertEquals("Client B", actualClients.get(1).getName());
  }
  
  @Test
  void testGetByIdWithUserAndClients_LoadUserRole() {
    Team actualTeam = teamService.getByIdWithUserAndClients(1, true);
    
    assertNotNull(actualTeam);
    assertEquals("Team A", actualTeam.getName());
    assertEquals("Team A Description", actualTeam.getDescription());
    assertEquals("team_a@24hrpo.com", actualTeam.getEmail());
    
    var actualUsers = actualTeam.getUsers();
    assertNotNull(actualUsers);
    
    assertEquals("admin", actualUsers.get(0).getUsername());
    assertEquals("ADMIN", actualUsers.get(0).getUserRoles().get(0).getRole().getName());
    
    assertEquals("dhilario", actualUsers.get(1).getUsername());
    assertEquals("ADMIN", actualUsers.get(1).getUserRoles().get(0).getRole().getName());
    assertEquals("USER", actualUsers.get(1).getUserRoles().get(1).getRole().getName());
    
    assertEquals("guest1", actualUsers.get(2).getUsername());
    assertEquals("USER", actualUsers.get(2).getUserRoles().get(0).getRole().getName());
    
    var actualClients = actualTeam.getClients();
    assertNotNull(actualClients);
    assertEquals("Client A", actualClients.get(0).getName());
    assertEquals("Client B", actualClients.get(1).getName());
  }
}
