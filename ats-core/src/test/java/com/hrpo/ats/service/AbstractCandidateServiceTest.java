package com.hrpo.ats.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import com.hrpo.ats.CandidateTestData;
import com.hrpo.ats.dao.CandidateDao;
import com.hrpo.daxtra.dao.DaxtraCandidateDao;

@ActiveProfiles( {"mocktest", "testdata"} )
abstract class AbstractCandidateServiceTest {

  @Autowired
  private CandidateService candidateService;
  
  @Autowired
  private DaxtraCandidateDao daxtraCandidateDao;
  
  @Autowired
  private CandidateDao jdbcCandidateDao;
  
  @Autowired
  private CandidateTestData candidateTestData;

  public AbstractCandidateServiceTest() {
    super();
  }

  protected CandidateService getCandidateService() {
    return candidateService;
  }

  protected DaxtraCandidateDao getDaxtraCandidateDao() {
    return daxtraCandidateDao;
  }
  
  protected CandidateDao getJdbcCandidateDao() {
    return jdbcCandidateDao;
  }

  protected CandidateTestData getCandidateTestData() {
    return candidateTestData;
  }
}
