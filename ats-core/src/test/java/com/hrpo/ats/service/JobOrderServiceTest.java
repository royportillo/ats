package com.hrpo.ats.service;

import static com.hrpo.ats.MockClockUtils.mockDateToLastFridayOfMonth;
import static com.hrpo.ats.MockClockUtils.mockDateToNextOrSameFriday;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import com.hrpo.ats.CandidateTestData;
import com.hrpo.ats.JobOrderTestData;
import com.hrpo.ats.dao.CandidateDao;
import com.hrpo.ats.dao.JobOrderCandidateDao;
import com.hrpo.ats.dao.JobOrderDao;
import com.hrpo.ats.dao.WorkflowDao;
import com.hrpo.ats.dao.WorkflowStage;
import com.hrpo.ats.dto.Client;
import com.hrpo.ats.dto.EndClient;
import com.hrpo.ats.dto.JobOrder;
import com.hrpo.ats.dto.JobOrderCandidate;
import com.hrpo.ats.dto.event.AbstractWorkflowEvent;
import com.hrpo.ats.dto.event.PrescreenEvent;
import com.hrpo.ats.dto.event.SubmissionEvent;
import com.hrpo.daxtra.dto.Candidate;

@ActiveProfiles("testdata")
@SpringBootTest
@Transactional
class JobOrderServiceTest {

  @Autowired
  private JobOrderService jobOrderService;
  
  @Autowired
  private JobOrderDao jobOrderDao;
  
  @Autowired
  private CandidateTestData candidateTestData;
  
  @Autowired
  private CandidateDao candidateDao;
  
  @Autowired
  private JobOrderCandidateDao jobOrderCandidateDao;
  
  @Autowired
  private WorkflowDao<PrescreenEvent> prescreenDao;
  
  @Autowired
  private WorkflowDao<SubmissionEvent> submissionDao;
  
  private JobOrder jobOrder1;
  private JobOrder jobOrder2;
  private JobOrder jobOrder3;
  
  @BeforeEach
  void initialize() {
    JobOrderTestData jobOrderTestData = new JobOrderTestData();
    jobOrder1 = jobOrderTestData.getJobOrder1();
    jobOrder2 = jobOrderTestData.getJobOrder2();
    jobOrder3 = jobOrderTestData.getJobOrder3();
  }
  
  @Test
  void testFindById() {
    
    Integer jobOrderId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderById(jobOrderId);
    
    assertNotNull(actualJobOrder);
    assertNotNull(actualJobOrder.getId().get().intValue());
    assertEquals("jo-number-101", actualJobOrder.getNumber());
    assertEquals(LocalDate.now(), actualJobOrder.getJoDate());
    assertEquals(LocalDate.now(), actualJobOrder.getDateCreated());
    assertEquals("test jo title", actualJobOrder.getTitle());
    assertEquals("test jo location", actualJobOrder.getLocation());
    assertEquals("12345", actualJobOrder.getZipCode());
    assertEquals("TEST", actualJobOrder.getDivision());
    assertEquals(LocalDate.now(), actualJobOrder.getStartDate());
    assertEquals(LocalDate.now(), actualJobOrder.getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateEnd());
    assertEquals("perhour", actualJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateEnd());
    assertEquals("perhour", actualJobOrder.getPayPeriodRate());
    assertEquals("3", actualJobOrder.getOpenings());
    assertEquals("3", actualJobOrder.getMaxSubs());
    assertEquals("New", actualJobOrder.getStatus());
    assertEquals("End Client A", actualJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrder.getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrder.getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrder.getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrder.getPrimaryOwner().getUser().getUsername());
    assertFalse(actualJobOrder.getHasFile());
  }
  
  @Test
  void testGetJobOrderFileById() {
    JobOrderTestData testData = new JobOrderTestData();
    JobOrder jobOrder = testData.getJobOrderWithFile();
    
    Integer jobOrderId = jobOrderService.saveJobOrder(jobOrder, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderFileById(jobOrderId);
    
    assertNotNull(actualJobOrder);
    assertEquals("jo-number-101", actualJobOrder.getNumber());
    assertEquals("dummy.test", actualJobOrder.getFileName());
    assertEquals("dummy/test-data", actualJobOrder.getMimeType());
    assertEquals("TEST", new String(actualJobOrder.getFile(), StandardCharsets.UTF_8));
  }
  
  @Test
  void testImportCandidatesToJobOrder_GetJobOrderWithCandidatesById() {
    
    Integer jobOrderId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Candidate candidate2 = candidateTestData.getCandidate2();
    
    Integer cId1 = candidateDao.upsert(candidate1);
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIds = new ArrayList<>();
    candidateIds.add(cId1);
    candidateIds.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(jobOrderId, candidateIds, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService
        .getJobOrderWithCandidatesById(jobOrderId, "dhilario");
    
    assertNotNull(actualJobOrder);
    assertNotNull(actualJobOrder.getId().get().intValue());
    assertEquals("jo-number-101", actualJobOrder.getNumber());
    assertEquals(LocalDate.now(), actualJobOrder.getJoDate());
    assertEquals(LocalDate.now(), actualJobOrder.getDateCreated());
    assertEquals("test jo title", actualJobOrder.getTitle());
    assertEquals("test jo location", actualJobOrder.getLocation());
    assertEquals("12345", actualJobOrder.getZipCode());
    assertEquals("TEST", actualJobOrder.getDivision());
    assertEquals(LocalDate.now(), actualJobOrder.getStartDate());
    assertEquals(LocalDate.now(), actualJobOrder.getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateEnd());
    assertEquals("perhour", actualJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateEnd());
    assertEquals("perhour", actualJobOrder.getPayPeriodRate());
    assertEquals("3", actualJobOrder.getOpenings());
    assertEquals("3", actualJobOrder.getMaxSubs());
    assertEquals("New", actualJobOrder.getStatus());
    assertEquals("End Client A", actualJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrder.getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrder.getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrder.getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrder.getPrimaryOwner().getUser().getUsername());
    
    var jobOrderCandidates = actualJobOrder.getJobOrderCandidates();
    assertEquals(2, jobOrderCandidates.size());
    
    assertEquals("Dann", jobOrderCandidates.get(0).getOwner().getFirstName());
    assertEquals("Hilario", jobOrderCandidates.get(0).getOwner().getLastName());
    assertEquals("dhilario", jobOrderCandidates.get(0).getOwner().getUsername());
    Candidate actualCandidate0 = jobOrderCandidates.get(0).getCandidate();
    assertAll("Candidate1", 
        () -> assertEquals(cId1, actualCandidate0.getId()),
        () -> assertEquals("X50", actualCandidate0.getCandidateId()),
        () -> assertEquals("Dummy Source 1", actualCandidate0.getSource()),
        () -> assertEquals("C1 First",  actualCandidate0.getFirstName()),
        () -> assertEquals("C1 Last", actualCandidate0.getLastName()),
        () -> assertEquals("C1 Fullname", actualCandidate0.getFullName()),
        () -> assertEquals("+1234567890", actualCandidate0.getPhone()),
        () -> assertEquals("+1358132101", actualCandidate0.getMobile()),
        () -> assertEquals("c1@email.com", actualCandidate0.getEmail()));
    
    assertEquals("Dann", jobOrderCandidates.get(1).getOwner().getFirstName());
    assertEquals("Hilario", jobOrderCandidates.get(1).getOwner().getLastName());
    assertEquals("dhilario", jobOrderCandidates.get(1).getOwner().getUsername());
    Candidate actualCandidate1 = jobOrderCandidates.get(1).getCandidate();
    assertAll("Candidate2", 
        () -> assertEquals(cId2, actualCandidate1.getId()),
        () -> assertEquals("X51", actualCandidate1.getCandidateId()),
        () -> assertEquals("Dummy Source 2", actualCandidate1.getSource()),
        () -> assertEquals("C2 First", actualCandidate1.getFirstName()),
        () -> assertEquals("C2 Last", actualCandidate1.getLastName()),
        () -> assertEquals("C2 Fullname", actualCandidate1.getFullName()),
        () -> assertEquals("+1234567892", actualCandidate1.getPhone()),
        () -> assertEquals("+1358132102", actualCandidate1.getMobile()),
        () -> assertEquals("c2@email.com", actualCandidate1.getEmail()));
  }
  
  @Test
  void testSaveJobOrder_Create() {
    
    Integer jobOrderId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    JobOrder actualJobOrder = jobOrderDao.findByPrimaryKey(jobOrderId);
    
    assertNotNull(actualJobOrder);
    assertNotNull(actualJobOrder.getId().get().intValue());
    assertEquals("jo-number-101", actualJobOrder.getNumber());
    assertEquals(LocalDate.now(), actualJobOrder.getJoDate());
    assertEquals(LocalDate.now(), actualJobOrder.getDateCreated());
    assertEquals("test jo title", actualJobOrder.getTitle());
    assertEquals("test jo location", actualJobOrder.getLocation());
    assertEquals("12345", actualJobOrder.getZipCode());
    assertEquals("TEST", actualJobOrder.getDivision());
    assertEquals(LocalDate.now(), actualJobOrder.getStartDate());
    assertEquals(LocalDate.now(), actualJobOrder.getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateEnd());
    assertEquals("perhour", actualJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateEnd());
    assertEquals("perhour", actualJobOrder.getPayPeriodRate());
    assertEquals("3", actualJobOrder.getOpenings());
    assertEquals("3", actualJobOrder.getMaxSubs());
    assertEquals("New", actualJobOrder.getStatus());
    assertEquals("End Client A", actualJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrder.getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrder.getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrder.getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrder.getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSaveJobOrder_Create_DuplicateKey() {
    
    Integer jobOrderId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    JobOrder dupJobOrder = new JobOrder();
    dupJobOrder.setNumber("jo-number-101");
    dupJobOrder.setJoDate(LocalDate.now());
    dupJobOrder.setDateCreated(LocalDate.now());
    dupJobOrder.setTitle("duplicate test jo title");
    dupJobOrder.setLocation("duplicate test jo location");
    dupJobOrder.setZipCode("duplicate 12345");
    dupJobOrder.setDivision("duplicate TEST");
    dupJobOrder.setStartDate(LocalDate.now());
    dupJobOrder.setEndDate(LocalDate.now());
    dupJobOrder.setBillRateStart(10D);
    dupJobOrder.setBillRateEnd(10D);
    dupJobOrder.setBillPeriodRate("perhour");
    dupJobOrder.setPayRateStart(9D);
    dupJobOrder.setPayRateEnd(9D);
    dupJobOrder.setPayPeriodRate("perhour");
    dupJobOrder.setOpenings("3");
    dupJobOrder.setMaxSubs("3");
    dupJobOrder.setStatus("New");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    dupJobOrder.setEndClient(endClient);
    
    Integer dupJobOrderId = jobOrderService.saveJobOrder(dupJobOrder, "dhilario");
    
    JobOrder actualJobOrder = jobOrderDao.findByPrimaryKey(jobOrderId);
    JobOrder dupActualJobOrder = jobOrderDao.findByPrimaryKey(dupJobOrderId);
    
    assertNotNull(actualJobOrder);
    assertNotNull(actualJobOrder.getId().get().intValue());
    assertEquals("jo-number-101", actualJobOrder.getNumber());
    assertEquals(LocalDate.now(), actualJobOrder.getJoDate());
    assertEquals(LocalDate.now(), actualJobOrder.getDateCreated());
    assertEquals("test jo title", actualJobOrder.getTitle());
    assertEquals("test jo location", actualJobOrder.getLocation());
    assertEquals("12345", actualJobOrder.getZipCode());
    assertEquals("TEST", actualJobOrder.getDivision());
    assertEquals(LocalDate.now(), actualJobOrder.getStartDate());
    assertEquals(LocalDate.now(), actualJobOrder.getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrder.getBillRateEnd());
    assertEquals("perhour", actualJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrder.getPayRateEnd());
    assertEquals("perhour", actualJobOrder.getPayPeriodRate());
    assertEquals("3", actualJobOrder.getOpenings());
    assertEquals("3", actualJobOrder.getMaxSubs());
    assertEquals("New", actualJobOrder.getStatus());
    assertEquals("End Client A", actualJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrder.getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrder.getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrder.getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrder.getPrimaryOwner().getUser().getUsername());
    
    assertNotNull(dupActualJobOrder);
    assertNotNull(dupActualJobOrder.getId().get().intValue());
    assertEquals("jo-number-101", dupActualJobOrder.getNumber());
    assertEquals(LocalDate.now(), dupActualJobOrder.getJoDate());
    assertEquals(LocalDate.now(), dupActualJobOrder.getDateCreated());
    assertEquals("duplicate test jo title", dupActualJobOrder.getTitle());
    assertEquals("duplicate test jo location", dupActualJobOrder.getLocation());
    assertEquals("duplicate 12345", dupActualJobOrder.getZipCode());
    assertEquals("duplicate TEST", dupActualJobOrder.getDivision());
    assertEquals(LocalDate.now(), dupActualJobOrder.getStartDate());
    assertEquals(LocalDate.now(), dupActualJobOrder.getEndDate());
    assertEquals(Double.valueOf(10D), dupActualJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(10D), dupActualJobOrder.getBillRateEnd());
    assertEquals("perhour", dupActualJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(9D), dupActualJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(9D), dupActualJobOrder.getPayRateEnd());
    assertEquals("perhour", dupActualJobOrder.getPayPeriodRate());
    assertEquals("3", dupActualJobOrder.getOpenings());
    assertEquals("3", dupActualJobOrder.getMaxSubs());
    assertEquals("New", dupActualJobOrder.getStatus());
    assertEquals("End Client A", dupActualJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(2), dupActualJobOrder.getPrimaryOwner().getUser().getId());
    assertEquals("Dann", dupActualJobOrder.getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", dupActualJobOrder.getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", dupActualJobOrder.getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSaveJobOrder_Update() {
    
    // save operation
    Integer jobOrderId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    JobOrder jobOrder1 = jobOrderDao.findByPrimaryKey(jobOrderId);
    
    jobOrder1.setNumber("updated-jo-number");
    jobOrder1.setTitle("updated jo title");
    jobOrder1.setLocation("updated location");
    jobOrder1.setZipCode("23456");
    jobOrder1.setDivision("TEST2");
    jobOrder1.setStartDate(LocalDate.now().plusDays(10));
    jobOrder1.setEndDate(LocalDate.now().plusDays(10));
    jobOrder1.setBillRateStart(11D);
    jobOrder1.setBillRateEnd(11D);
    jobOrder1.setBillPeriodRate("perannum");
    jobOrder1.setPayRateStart(10D);
    jobOrder1.setPayRateEnd(10D);
    jobOrder1.setPayPeriodRate("perannum");
    jobOrder1.setOpenings("4");
    jobOrder1.setMaxSubs("4");
    jobOrder1.setStatus("Updated");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    jobOrder1.setEndClient(endClient);
    
    // update operation (with id)
    jobOrderId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    JobOrder updateJobOrder = jobOrderDao.findByPrimaryKey(jobOrderId);
    
    assertNotNull(updateJobOrder);
    assertEquals("updated-jo-number", updateJobOrder.getNumber());
    assertEquals(LocalDate.now(), updateJobOrder.getJoDate());
    assertEquals(LocalDate.now(), updateJobOrder.getDateCreated());
    assertEquals("updated jo title", updateJobOrder.getTitle());
    assertEquals("updated location", updateJobOrder.getLocation());
    assertEquals("23456", updateJobOrder.getZipCode());
    assertEquals("TEST2", updateJobOrder.getDivision());
    assertEquals(LocalDate.now().plusDays(10), updateJobOrder.getStartDate());
    assertEquals(LocalDate.now().plusDays(10), updateJobOrder.getEndDate());
    assertEquals(Double.valueOf(11D), updateJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(11D), updateJobOrder.getBillRateEnd());
    assertEquals("perannum", updateJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(10D), updateJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(10D), updateJobOrder.getPayRateEnd());
    assertEquals("perannum", updateJobOrder.getPayPeriodRate());
    assertEquals("4", updateJobOrder.getOpenings());
    assertEquals("4", updateJobOrder.getMaxSubs());
    assertEquals("Updated", updateJobOrder.getStatus());
    assertEquals("End Client A", updateJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(2), updateJobOrder.getPrimaryOwner().getUser().getId());
    assertEquals("Dann", updateJobOrder.getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", updateJobOrder.getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", updateJobOrder.getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSaveJobOrder_Update_DuplicateKey() {
    
    // save operation jo1
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    // save operation jo2
    Integer jobOrderId = jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    JobOrder jobOrderToDuplicate = jobOrderDao.findByPrimaryKey(jobOrderId);
    
    jobOrderToDuplicate.setNumber("jo-number-101"); // duplicate JO number
    jobOrderToDuplicate.setTitle("updated jo title");
    jobOrderToDuplicate.setLocation("updated location");
    jobOrderToDuplicate.setZipCode("23456");
    jobOrderToDuplicate.setDivision("TEST2");
    jobOrderToDuplicate.setStartDate(LocalDate.now().plusDays(10));
    jobOrderToDuplicate.setEndDate(LocalDate.now().plusDays(10));
    jobOrderToDuplicate.setBillRateStart(11D);
    jobOrderToDuplicate.setBillRateEnd(11D);
    jobOrderToDuplicate.setBillPeriodRate("perannum");
    jobOrderToDuplicate.setPayRateStart(10D);
    jobOrderToDuplicate.setPayRateEnd(10D);
    jobOrderToDuplicate.setPayPeriodRate("perannum");
    jobOrderToDuplicate.setOpenings("4");
    jobOrderToDuplicate.setMaxSubs("4");
    jobOrderToDuplicate.setStatus("Updated");
    
    // update operation (with id)
    jobOrderId = jobOrderService.saveJobOrder(jobOrderToDuplicate, "dhilario");
    JobOrder updateJobOrder = jobOrderDao.findByPrimaryKey(jobOrderId);
    
    assertNotNull(updateJobOrder);
    assertEquals("jo-number-101", updateJobOrder.getNumber());
    assertEquals(LocalDate.now(), updateJobOrder.getJoDate());
    assertEquals(LocalDate.now(), updateJobOrder.getDateCreated());
    assertEquals("updated jo title", updateJobOrder.getTitle());
    assertEquals("updated location", updateJobOrder.getLocation());
    assertEquals("23456", updateJobOrder.getZipCode());
    assertEquals("TEST2", updateJobOrder.getDivision());
    assertEquals(LocalDate.now().plusDays(10), updateJobOrder.getStartDate());
    assertEquals(LocalDate.now().plusDays(10), updateJobOrder.getEndDate());
    assertEquals(Double.valueOf(11D), updateJobOrder.getBillRateStart());
    assertEquals(Double.valueOf(11D), updateJobOrder.getBillRateEnd());
    assertEquals("perannum", updateJobOrder.getBillPeriodRate());
    assertEquals(Double.valueOf(10D), updateJobOrder.getPayRateStart());
    assertEquals(Double.valueOf(10D), updateJobOrder.getPayRateEnd());
    assertEquals("perannum", updateJobOrder.getPayPeriodRate());
    assertEquals("4", updateJobOrder.getOpenings());
    assertEquals("4", updateJobOrder.getMaxSubs());
    assertEquals("Updated", updateJobOrder.getStatus());
    assertEquals("End Client A", updateJobOrder.getEndClient().getName());
    assertEquals(Integer.valueOf(2), updateJobOrder.getPrimaryOwner().getUser().getId());
    assertEquals("Dann", updateJobOrder.getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", updateJobOrder.getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", updateJobOrder.getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testGetAllJobOrders() {
    
    // save operation
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    // save operation
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrders = jobOrderService.getAllJobOrdersForUser("dhilario", client);
    assertEquals(2, jobOrders.size());
    assertEquals("jo-number-101", jobOrders.get(0).getNumber());
    assertEquals("jo-number-102", jobOrders.get(1).getNumber());
  }
  
  @Test
  void testGetAllJobOrders_OtherUser() {
    
    // save operation
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    // save operation
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrders = jobOrderService.getAllJobOrdersForUser("guest", client);
    assertEquals(0, jobOrders.size());
  }
  
  @Test
  void testGetAllJobOrders_NullUser() {
    
    // save operation
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    // save operation
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrders = jobOrderService.getAllJobOrdersForUser(null, client);
    assertEquals(2, jobOrders.size());
    assertEquals("jo-number-101", jobOrders.get(0).getNumber());
    assertEquals("jo-number-102", jobOrders.get(1).getNumber());
  }
  
  @Test
  void testSearchByCriteria_IllegalState() {

    assertThrows(IllegalStateException.class, () -> {
      jobOrderDao.findByCriteria(2, null, null, 1);
    });
  }
  
  @Test
  void testSearchByCriteria_IllegalState_NullUser() {

    assertThrows(IllegalStateException.class, () -> {
      jobOrderDao.findByCriteria(null, null, null, null);
    });
  }
  
  @Test
  void testSearchByCriteria_JoNumberOnly() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = jobOrderService
        .searchJobOrderByCriteria("dhilario", "jo-number-101", null, client);
    
    assertEquals(1, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_TitleOnly() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", null, "test jo", client);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_TitleOnly_IgnoreCase() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", null, "TeSt Jo", client);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_OtherUser() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("guest", "jo-number-101", "test jo", client);
    
    assertEquals(0, jobOrdersMatched.size());
  }
  
  @Test
  void testSearchByCriteria_NoUser() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "guest");
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria(null, "jo-number-101", "test jo", null);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(3), jobOrdersMatched.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Guest", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("User 0", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("guest", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_JoNumber_Or_Title_MatchJoNumberOnly() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", "jo-number-101", "no match title", client);
    
    assertEquals(1, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_JoNumber_Or_Title_MatchTitleOnly() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", 
            "no-jonumber-match", "test jo ti", client);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_JoNumber_Or_Title_MatchTitleOnly_IgnoreCase() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", "no-jonumber-match", 
            "tEsT jo tI", client);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_JoNumber_Or_Title_MatchBothNumberAndTitle() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", "jo-number-101", 
            "test jo ti", client);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_JoNumber_Or_Title_MatchBothNumberAndTitle_IgnoreCase() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", "jo-number-101", 
            "tESt jO tI", client);
    
    assertEquals(2, jobOrdersMatched.size());
    assertEquals("jo-number-101", jobOrdersMatched.get(0).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getDateCreated());
    assertEquals("test jo title", jobOrdersMatched.get(0).getTitle());
    assertEquals("test jo location", jobOrdersMatched.get(0).getLocation());
    assertEquals("12345", jobOrdersMatched.get(0).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(0).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(0).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(0).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(0).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(0).getOpenings());
    assertEquals("3", jobOrdersMatched.get(0).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(0).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", jobOrdersMatched.get(1).getNumber());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getJoDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getDateCreated());
    assertEquals("test jo title2", jobOrdersMatched.get(1).getTitle());
    assertEquals("test jo location2", jobOrdersMatched.get(1).getLocation());
    assertEquals("23456", jobOrdersMatched.get(1).getZipCode());
    assertEquals("TEST", jobOrdersMatched.get(1).getDivision());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getStartDate());
    assertEquals(LocalDate.now(), jobOrdersMatched.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), jobOrdersMatched.get(1).getBillRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), jobOrdersMatched.get(1).getPayRateEnd());
    assertEquals("perhour", jobOrdersMatched.get(1).getPayPeriodRate());
    assertEquals("3", jobOrdersMatched.get(1).getOpenings());
    assertEquals("3", jobOrdersMatched.get(1).getMaxSubs());
    assertEquals("New", jobOrdersMatched.get(1).getStatus());
    assertEquals("End Client A", jobOrdersMatched.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), jobOrdersMatched.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", jobOrdersMatched.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testSearchByCriteria_NoMatch() {

    jobOrderService.saveJobOrder(jobOrder1, "dhilario");

    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    Client client = new Client();
    client.setClientId(1);
    
    List<JobOrder> jobOrdersMatched = 
        jobOrderService.searchJobOrderByCriteria("dhilario", "no-jonumber-match", 
            "no match title", client);
    
    assertTrue(jobOrdersMatched.isEmpty());
  }
  
  @Test
  void testGetJobOrderCandidateById() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId = candidateDao.upsert(candidate1);
    
    var joCandidate = new JobOrderCandidate();
    joCandidate.setPayRateFrom(jobOrder1.getPayRateStart());
    joCandidate.setPayRateTo(jobOrder1.getPayRateEnd());
    joCandidate.setPayPeriodRate(jobOrder1.getBillPeriodRate());
    
    Integer jocId = jobOrderCandidateDao.insert(joId, 2, cId, joCandidate);
    
    JobOrderCandidate actualJobOrderCandidate = jobOrderService.getJobOrderCandidateById(jocId);
    
    assertNotNull(actualJobOrderCandidate);
    assertEquals(jocId, actualJobOrderCandidate.getId());
    assertEquals(joId, actualJobOrderCandidate.getJobOrder().getId().get());
    assertEquals("jo-number-101", actualJobOrderCandidate.getJobOrder().getNumber());
    assertEquals(LocalDate.now(), actualJobOrderCandidate.getJobOrder().getJoDate());
    assertEquals(cId, actualJobOrderCandidate.getCandidate().getId());
    assertEquals("Dann", actualJobOrderCandidate.getOwner().getFirstName());
    assertEquals("Hilario", actualJobOrderCandidate.getOwner().getLastName());
    assertEquals("dhilario", actualJobOrderCandidate.getOwner().getUsername());
  }
  
  @Test
  void testGetJobOrderCandidatesByLastWorkflowEvent() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIdList = new ArrayList<>();
    
    candidateIdList.add(cId1);
    candidateIdList.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(joId, candidateIdList, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderWithCandidatesById(joId, "dhilario");
    var jobOrderCandidateList = actualJobOrder.getJobOrderCandidates();
    
    for (JobOrderCandidate joCandidate : jobOrderCandidateList) {
      jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
      
      AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
      eventPrescreen.setTransactionDate(LocalDate.now());
      prescreenDao.upsert(eventPrescreen, joCandidate.getId());
    }
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderService.getJobOrderCandidatesByLastWorkflowEvent(WorkflowStage.PRESCREEN, 
            "dhilario", LocalDate.now(), LocalDate.now());
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderService.getJobOrderCandidatesByLastWorkflowEvent(WorkflowStage.PLACEMENT, 
            "dhilario", LocalDate.now(), LocalDate.now());
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testGetJobOrderCandidatesByWorkflowDate() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIdList = new ArrayList<>();
    
    candidateIdList.add(cId1);
    candidateIdList.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(joId, candidateIdList, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderWithCandidatesById(joId, "dhilario");
    var jobOrderCandidateList = actualJobOrder.getJobOrderCandidates();
    
    var joCandidate1 = jobOrderCandidateList.get(0);
    var joCandidate2 = jobOrderCandidateList.get(1);
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate1, WorkflowStage.PRESCREEN);
    AbstractWorkflowEvent eventPrescreenJoc1 = new PrescreenEvent();
    eventPrescreenJoc1.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreenJoc1, joCandidate1.getId());
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.PRESCREEN);
    AbstractWorkflowEvent eventPrescreenJoc2 = new PrescreenEvent();
    eventPrescreenJoc2.setTransactionDate(LocalDate.now());
    prescreenDao.upsert(eventPrescreenJoc2, joCandidate2.getId());
    
    jobOrderCandidateDao.updateWorkflowStage(joCandidate2, WorkflowStage.SUBMIT);
    AbstractWorkflowEvent eventSubmitJoc2 = new SubmissionEvent();
    eventSubmitJoc2.setTransactionDate(LocalDate.now());
    submissionDao.upsert(eventSubmitJoc2, joCandidate2.getId());
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderService.getJobOrderCandidatesByWorkflowDate(LocalDate.now(), LocalDate.now());
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(5, actualJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(WorkflowStage.IMPORT,
            actualJobOrderCandidates.get(0).getWorkflowStage()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(1).getWorkflowStage()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(2).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(2).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(2).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(2).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(2).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(2).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(2).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(2).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(2).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(2).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(2).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(2).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(2).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(2).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(2).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(2).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(2).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(2).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(2).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(2).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(2)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(2)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(2)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(2).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(2).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(2).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(2).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(2).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(2).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(2).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(2).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(2).getTransactionDate()),
        () -> assertEquals(WorkflowStage.IMPORT,
            actualJobOrderCandidates.get(2).getWorkflowStage()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(2).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(2).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(2).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(2).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(2).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(2).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(3).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(3).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(3).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(3).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(3).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(3).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(3).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(3).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(3).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(3).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(3).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(3).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(3).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(3).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(3).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(3).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(3).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(3).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(3).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(3).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(3)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(3)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(3)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(3).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(3).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(3).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(3).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(3).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(3).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(3).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(3).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(3).getTransactionDate()),
        () -> assertEquals(WorkflowStage.PRESCREEN,
            actualJobOrderCandidates.get(3).getWorkflowStage()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(3).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(3).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(3).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(3).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(3).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(3).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(4).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(4).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(4).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(4).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(4).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(4).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(4).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(4).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(4).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(4).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(4).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(4).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(4).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(4).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(4).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(4).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(4).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(4).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(4).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(4).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(4)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(4)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(4)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(4).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(4).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(4).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(4).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(4).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(4).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(4).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(4).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(4).getTransactionDate()),
        () -> assertEquals(WorkflowStage.SUBMIT,
            actualJobOrderCandidates.get(4).getWorkflowStage()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(4).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(4).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(4).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(4).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(4).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(4).getOwner().getUsername()));
  }
  
  @Test
  void testGetJobOrderCandidatesByWorkflowEvent_ForTheDay() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIdList = new ArrayList<>();
    
    candidateIdList.add(cId1);
    candidateIdList.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(joId, candidateIdList, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderWithCandidatesById(joId, "dhilario");
    var jobOrderCandidateList = actualJobOrder.getJobOrderCandidates();
    
    for (JobOrderCandidate joCandidate : jobOrderCandidateList) {
      jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
      
      AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
      eventPrescreen.setTransactionDate(LocalDate.now());
      prescreenDao.upsert(eventPrescreen, joCandidate.getId());
    }
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PRESCREEN, 
            "dhilario", "ftd", Clock.systemDefaultZone(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PLACEMENT, 
            "dhilario", "ftd", Clock.systemDefaultZone(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(LocalDate.now(),
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testGetJobOrderCandidatesByWorkflowEvent_ForTheWeek() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIdList = new ArrayList<>();
    
    candidateIdList.add(cId1);
    candidateIdList.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(joId, candidateIdList, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderWithCandidatesById(joId, "dhilario");
    var jobOrderCandidateList = actualJobOrder.getJobOrderCandidates();
    
    LocalDate transactionDate = LocalDate.now(mockDateToNextOrSameFriday()).with(
        TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    
    LocalDate expectedTransactionDate1 = transactionDate;
    
    for (JobOrderCandidate joCandidate : jobOrderCandidateList) {
      jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
      
      AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
      eventPrescreen.setTransactionDate(transactionDate);
      prescreenDao.upsert(eventPrescreen, joCandidate.getId());
      
      transactionDate = transactionDate.plusDays(2);
    }
    
    LocalDate expectedTransactionDate2 = expectedTransactionDate1.plusDays(2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PRESCREEN, 
            "dhilario", "ftw", mockDateToNextOrSameFriday(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PLACEMENT, 
            "dhilario", "ftw", mockDateToNextOrSameFriday(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate1,
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate2,
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testGetJobOrderCandidatesByWorkflowEvent_ForTheMonth() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIdList = new ArrayList<>();
    
    candidateIdList.add(cId1);
    candidateIdList.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(joId, candidateIdList, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderWithCandidatesById(joId, "dhilario");
    var jobOrderCandidateList = actualJobOrder.getJobOrderCandidates();
    
    LocalDate transactionDate = LocalDate.now()
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    LocalDate expectedTransactionDate1 = transactionDate;
    
    for (JobOrderCandidate joCandidate : jobOrderCandidateList) {
      jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
      
      AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
      eventPrescreen.setTransactionDate(transactionDate);
      prescreenDao.upsert(eventPrescreen, joCandidate.getId());
      
      transactionDate = transactionDate.plusDays(2);
    }
    
    LocalDate expectedTransactionDate2 = expectedTransactionDate1.plusDays(2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PRESCREEN, 
            "dhilario", "ftm", mockDateToLastFridayOfMonth(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PLACEMENT, 
            "dhilario", "ftm", mockDateToLastFridayOfMonth(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate1,
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate2,
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testGetJobOrderCandidatesByWorkflowEvent_LastMonth() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIdList = new ArrayList<>();
    
    candidateIdList.add(cId1);
    candidateIdList.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(joId, candidateIdList, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderWithCandidatesById(joId, "dhilario");
    var jobOrderCandidateList = actualJobOrder.getJobOrderCandidates();
    
    LocalDate lastMonth = LocalDate.now().minusMonths(1);
    LocalDate transactionDate = lastMonth.with(
        TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    LocalDate expectedTransactionDate1 = transactionDate;
    
    for (JobOrderCandidate joCandidate : jobOrderCandidateList) {
      jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
      
      AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
      eventPrescreen.setTransactionDate(transactionDate);
      prescreenDao.upsert(eventPrescreen, joCandidate.getId());
      
      transactionDate = transactionDate.plusDays(2);
    }
    
    LocalDate expectedTransactionDate2 = expectedTransactionDate1.plusDays(2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PRESCREEN, 
            "dhilario", "lm", Clock.systemDefaultZone(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PLACEMENT, 
            "dhilario", "lm", Clock.systemDefaultZone(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate1,
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate2,
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testGetJobOrderCandidatesByWorkflowEvent_LastLastMonth() {
    Integer joId = jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    
    Candidate candidate1 = candidateTestData.getCandidate1();
    Integer cId1 = candidateDao.upsert(candidate1);
    
    Candidate candidate2 = candidateTestData.getCandidate2();
    Integer cId2 = candidateDao.upsert(candidate2);
    
    List<Integer> candidateIdList = new ArrayList<>();
    
    candidateIdList.add(cId1);
    candidateIdList.add(cId2);
    
    jobOrderService.importCandidatesToJobOrder(joId, candidateIdList, "dhilario");
    
    JobOrder actualJobOrder = jobOrderService.getJobOrderWithCandidatesById(joId, "dhilario");
    var jobOrderCandidateList = actualJobOrder.getJobOrderCandidates();
    
    LocalDate lastMonth = LocalDate.now().minusMonths(2);
    LocalDate transactionDate = lastMonth.with(
        TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    LocalDate expectedTransactionDate1 = transactionDate;
    
    for (JobOrderCandidate joCandidate : jobOrderCandidateList) {
      jobOrderCandidateDao.updateWorkflowStage(joCandidate, WorkflowStage.PRESCREEN);
      
      AbstractWorkflowEvent eventPrescreen = new PrescreenEvent();
      eventPrescreen.setTransactionDate(transactionDate);
      prescreenDao.upsert(eventPrescreen, joCandidate.getId());
      
      transactionDate = transactionDate.plusDays(2);
    }
    
    LocalDate expectedTransactionDate2 = expectedTransactionDate1.plusDays(2);
    
    List<JobOrderCandidate> actualJobOrderCandidates = 
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PRESCREEN, 
            "dhilario", "llm", Clock.systemDefaultZone(), false);
    List<JobOrderCandidate> emptyJobOrderCandidates =
        jobOrderService.getJobOrderCandidatesByWorkflowEvent(WorkflowStage.PLACEMENT, 
            "dhilario", "llm", Clock.systemDefaultZone(), false);
    
    assertNotNull(actualJobOrderCandidates);
    assertEquals(2, actualJobOrderCandidates.size());
    assertEquals(0, emptyJobOrderCandidates.size());
    
    assertAll("JobOrderCandidate1",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(0).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(0).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(0).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(0).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(0).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(0).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(0).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(0).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(0).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(0).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(0).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X50", 
            actualJobOrderCandidates.get(0).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 1", 
            actualJobOrderCandidates.get(0).getCandidate().getSource()),
        () -> assertEquals("C1 First", 
            actualJobOrderCandidates.get(0).getCandidate().getFirstName()),
        () -> assertEquals("C1 Last", 
            actualJobOrderCandidates.get(0).getCandidate().getLastName()),
        () -> assertEquals("C1 Fullname", 
            actualJobOrderCandidates.get(0).getCandidate().getFullName()),
        () -> assertEquals("+1234567890", 
            actualJobOrderCandidates.get(0).getCandidate().getPhone()),
        () -> assertEquals("+1358132101", 
            actualJobOrderCandidates.get(0).getCandidate().getMobile()),
        () -> assertEquals("c1@email.com", 
            actualJobOrderCandidates.get(0).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate1,
            actualJobOrderCandidates.get(0).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(0).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(0).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(0).getOwner().getUsername()));
    
    assertAll("JobOrderCandidate2",
        () -> assertEquals("jo-number-101", 
            actualJobOrderCandidates.get(1).getJobOrder().getNumber()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getJoDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getDateCreated()),
        () -> assertEquals("test jo title", 
            actualJobOrderCandidates.get(1).getJobOrder().getTitle()),
        () -> assertEquals("test jo location", 
            actualJobOrderCandidates.get(1).getJobOrder().getLocation()),
        () -> assertEquals("12345", 
            actualJobOrderCandidates.get(1).getJobOrder().getZipCode()),
        () -> assertEquals("TEST", 
            actualJobOrderCandidates.get(1).getJobOrder().getDivision()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getStartDate()),
        () -> assertEquals(LocalDate.now(), 
            actualJobOrderCandidates.get(1).getJobOrder().getEndDate()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateStart()),
        () -> assertEquals(Double.valueOf(10D), 
            actualJobOrderCandidates.get(1).getJobOrder().getBillRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getBillPeriodRate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateStart()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getJobOrder().getPayRateEnd()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getJobOrder().getPayPeriodRate()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getOpenings()),
        () -> assertEquals("3", 
            actualJobOrderCandidates.get(1).getJobOrder().getMaxSubs()),
        () -> assertEquals("New", 
            actualJobOrderCandidates.get(1).getJobOrder().getStatus()),
        () -> assertEquals("End Client A", 
            actualJobOrderCandidates.get(1).getJobOrder().getEndClient().getName()),
        () -> assertEquals(Integer.valueOf(2), 
            actualJobOrderCandidates.get(1).getJobOrder().getPrimaryOwner().getUser().getId()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(0)
            .getJobOrder().getPrimaryOwner().getUser().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1)
            .getJobOrder().getPrimaryOwner().getUser().getUsername()),
        () -> assertEquals("X51", 
            actualJobOrderCandidates.get(1).getCandidate().getCandidateId()),
        () -> assertEquals("Dummy Source 2", 
            actualJobOrderCandidates.get(1).getCandidate().getSource()),
        () -> assertEquals("C2 First", 
            actualJobOrderCandidates.get(1).getCandidate().getFirstName()),
        () -> assertEquals("C2 Last", 
            actualJobOrderCandidates.get(1).getCandidate().getLastName()),
        () -> assertEquals("C2 Fullname", 
            actualJobOrderCandidates.get(1).getCandidate().getFullName()),
        () -> assertEquals("+1234567892", 
            actualJobOrderCandidates.get(1).getCandidate().getPhone()),
        () -> assertEquals("+1358132102", 
            actualJobOrderCandidates.get(1).getCandidate().getMobile()),
        () -> assertEquals("c2@email.com", 
            actualJobOrderCandidates.get(1).getCandidate().getEmail()),
        () -> assertEquals(expectedTransactionDate2,
            actualJobOrderCandidates.get(1).getTransactionDate()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateFrom()),
        () -> assertEquals(Double.valueOf(9D), 
            actualJobOrderCandidates.get(1).getPayRateTo()),
        () -> assertEquals("perhour", 
            actualJobOrderCandidates.get(1).getPayPeriodRate()),
        () -> assertEquals("Dann", 
            actualJobOrderCandidates.get(1).getOwner().getFirstName()),
        () -> assertEquals("Hilario", 
            actualJobOrderCandidates.get(1).getOwner().getLastName()),
        () -> assertEquals("dhilario", 
            actualJobOrderCandidates.get(1).getOwner().getUsername()));
  }
  
  @Test
  void testGetJobOrdersByUserAndPeriod_ForTheDay() {
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    List<JobOrder> actualJobOrders = jobOrderService
        .getJobOrdersByUserAndPeriod("dhilario", "ftd", Clock.systemDefaultZone(), false);
    
    assertNotNull(actualJobOrders);
    assertEquals(2, actualJobOrders.size());
    
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", actualJobOrders.get(1).getNumber());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getDateCreated());
    assertEquals("test jo title2", actualJobOrders.get(1).getTitle());
    assertEquals("test jo location2", actualJobOrders.get(1).getLocation());
    assertEquals("23456", actualJobOrders.get(1).getZipCode());
    assertEquals("TEST", actualJobOrders.get(1).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(1).getOpenings());
    assertEquals("3", actualJobOrders.get(1).getMaxSubs());
    assertEquals("New", actualJobOrders.get(1).getStatus());
    assertEquals("End Client A", actualJobOrders.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testGetJobOrdersByUserAndPeriod_ForTheWeek() {
    
    LocalDate transactionDate = LocalDate.now(mockDateToNextOrSameFriday())
        .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    
    JobOrder jobOrder1 = new JobOrder();
    JobOrder jobOrder2 = new JobOrder();
    
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(transactionDate);
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    
    jobOrder2.setNumber("jo-number-102");
    jobOrder2.setJoDate(transactionDate.plusDays(3));
    jobOrder2.setTitle("test jo title2");
    jobOrder2.setLocation("test jo location2");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    jobOrder1.setEndClient(endClient);
    jobOrder2.setEndClient(endClient);
    
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    List<JobOrder> actualJobOrders = jobOrderService
        .getJobOrdersByUserAndPeriod("dhilario", "ftw", mockDateToNextOrSameFriday(), false);
    
    LocalDate actualTransactionDate = LocalDate.now(mockDateToNextOrSameFriday())
        .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    
    assertNotNull(actualJobOrders);
    assertEquals(2, actualJobOrders.size());
    
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(actualTransactionDate, actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", actualJobOrders.get(1).getNumber());
    assertEquals(actualTransactionDate.plusDays(3), actualJobOrders.get(1).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getDateCreated());
    assertEquals("test jo title2", actualJobOrders.get(1).getTitle());
    assertEquals("test jo location2", actualJobOrders.get(1).getLocation());
    assertEquals("23456", actualJobOrders.get(1).getZipCode());
    assertEquals("TEST", actualJobOrders.get(1).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(1).getOpenings());
    assertEquals("3", actualJobOrders.get(1).getMaxSubs());
    assertEquals("New", actualJobOrders.get(1).getStatus());
    assertEquals("End Client A", actualJobOrders.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testGetJobOrdersByUserAndPeriod_ForTheMonth() {
    
    LocalDate transactionDate = LocalDate.now()
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    JobOrder jobOrder1 = new JobOrder();
    JobOrder jobOrder2 = new JobOrder();
    
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(transactionDate);
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    
    jobOrder2.setNumber("jo-number-102");
    jobOrder2.setJoDate(transactionDate.plusDays(3));
    jobOrder2.setTitle("test jo title2");
    jobOrder2.setLocation("test jo location2");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    jobOrder1.setEndClient(endClient);
    jobOrder2.setEndClient(endClient);
    
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    List<JobOrder> actualJobOrders = jobOrderService
        .getJobOrdersByUserAndPeriod("dhilario", "ftm", mockDateToLastFridayOfMonth(), false);
    
    LocalDate actualTransactionDate = LocalDate.now()
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    assertNotNull(actualJobOrders);
    assertEquals(2, actualJobOrders.size());
    
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(actualTransactionDate, actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", actualJobOrders.get(1).getNumber());
    assertEquals(actualTransactionDate.plusDays(3), actualJobOrders.get(1).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getDateCreated());
    assertEquals("test jo title2", actualJobOrders.get(1).getTitle());
    assertEquals("test jo location2", actualJobOrders.get(1).getLocation());
    assertEquals("23456", actualJobOrders.get(1).getZipCode());
    assertEquals("TEST", actualJobOrders.get(1).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(1).getOpenings());
    assertEquals("3", actualJobOrders.get(1).getMaxSubs());
    assertEquals("New", actualJobOrders.get(1).getStatus());
    assertEquals("End Client A", actualJobOrders.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testGetJobOrdersByUserAndPeriod_LastMonth() {
    
    LocalDate lastMonth = LocalDate.now().minusMonths(1);
    LocalDate transactionDate = lastMonth
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    JobOrder jobOrder1 = new JobOrder();
    JobOrder jobOrder2 = new JobOrder();
    
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(transactionDate);
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    
    jobOrder2.setNumber("jo-number-102");
    jobOrder2.setJoDate(transactionDate.plusDays(3));
    jobOrder2.setTitle("test jo title2");
    jobOrder2.setLocation("test jo location2");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    jobOrder1.setEndClient(endClient);
    jobOrder2.setEndClient(endClient);
    
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    List<JobOrder> actualJobOrders = jobOrderService
        .getJobOrdersByUserAndPeriod("dhilario", "lm", Clock.systemDefaultZone(), false);
    
    LocalDate actualTransactionDate = lastMonth
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    assertNotNull(actualJobOrders);
    assertEquals(2, actualJobOrders.size());
    
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(actualTransactionDate, actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", actualJobOrders.get(1).getNumber());
    assertEquals(actualTransactionDate.plusDays(3), actualJobOrders.get(1).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getDateCreated());
    assertEquals("test jo title2", actualJobOrders.get(1).getTitle());
    assertEquals("test jo location2", actualJobOrders.get(1).getLocation());
    assertEquals("23456", actualJobOrders.get(1).getZipCode());
    assertEquals("TEST", actualJobOrders.get(1).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(1).getOpenings());
    assertEquals("3", actualJobOrders.get(1).getMaxSubs());
    assertEquals("New", actualJobOrders.get(1).getStatus());
    assertEquals("End Client A", actualJobOrders.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testGetJobOrdersByUserAndPeriod_LastLastMonth() {
    
    LocalDate lastLastMonth = LocalDate.now().minusMonths(2);
    LocalDate transactionDate = lastLastMonth
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    JobOrder jobOrder1 = new JobOrder();
    JobOrder jobOrder2 = new JobOrder();
    
    jobOrder1.setNumber("jo-number-101");
    jobOrder1.setJoDate(transactionDate);
    jobOrder1.setTitle("test jo title");
    jobOrder1.setLocation("test jo location");
    jobOrder1.setZipCode("12345");
    jobOrder1.setDivision("TEST");
    jobOrder1.setStartDate(LocalDate.now());
    jobOrder1.setEndDate(LocalDate.now());
    jobOrder1.setBillRateStart(10D);
    jobOrder1.setBillRateEnd(10D);
    jobOrder1.setBillPeriodRate("perhour");
    jobOrder1.setPayRateStart(9D);
    jobOrder1.setPayRateEnd(9D);
    jobOrder1.setPayPeriodRate("perhour");
    jobOrder1.setOpenings("3");
    jobOrder1.setMaxSubs("3");
    jobOrder1.setStatus("New");
    
    jobOrder2.setNumber("jo-number-102");
    jobOrder2.setJoDate(transactionDate.plusDays(3));
    jobOrder2.setTitle("test jo title2");
    jobOrder2.setLocation("test jo location2");
    jobOrder2.setZipCode("23456");
    jobOrder2.setDivision("TEST");
    jobOrder2.setStartDate(LocalDate.now());
    jobOrder2.setEndDate(LocalDate.now());
    jobOrder2.setBillRateStart(10D);
    jobOrder2.setBillRateEnd(10D);
    jobOrder2.setBillPeriodRate("perhour");
    jobOrder2.setPayRateStart(9D);
    jobOrder2.setPayRateEnd(9D);
    jobOrder2.setPayPeriodRate("perhour");
    jobOrder2.setOpenings("3");
    jobOrder2.setMaxSubs("3");
    jobOrder2.setStatus("New");
    
    EndClient endClient = new EndClient();
    endClient.setId(1);
    
    jobOrder1.setEndClient(endClient);
    jobOrder2.setEndClient(endClient);
    
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    jobOrderService.saveJobOrder(jobOrder2, "dhilario");
    
    List<JobOrder> actualJobOrders = jobOrderService
        .getJobOrdersByUserAndPeriod("dhilario", "llm", Clock.systemDefaultZone(), false);
    
    LocalDate actualTransactionDate = lastLastMonth
        .with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    
    assertNotNull(actualJobOrders);
    assertEquals(2, actualJobOrders.size());
    
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(actualTransactionDate, actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
    
    assertEquals("jo-number-102", actualJobOrders.get(1).getNumber());
    assertEquals(actualTransactionDate.plusDays(3), actualJobOrders.get(1).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getDateCreated());
    assertEquals("test jo title2", actualJobOrders.get(1).getTitle());
    assertEquals("test jo location2", actualJobOrders.get(1).getLocation());
    assertEquals("23456", actualJobOrders.get(1).getZipCode());
    assertEquals("TEST", actualJobOrders.get(1).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(1).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(1).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(1).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(1).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(1).getOpenings());
    assertEquals("3", actualJobOrders.get(1).getMaxSubs());
    assertEquals("New", actualJobOrders.get(1).getStatus());
    assertEquals("End Client A", actualJobOrders.get(1).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(1).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(1).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(1).getPrimaryOwner().getUser().getUsername());
  }
  
  @Test
  void testGetJobOrdersByUserAndPeriod_TeamOnly() {
    jobOrderService.saveJobOrder(jobOrder1, "dhilario");
    jobOrderService.saveJobOrder(jobOrder3, "dhilario");
    
    List<JobOrder> actualJobOrders = jobOrderService
        .getJobOrdersByUserAndPeriod("dhilario", "ftd", Clock.systemDefaultZone(), true);
    
    assertNotNull(actualJobOrders);
    assertEquals(1, actualJobOrders.size());
    
    assertEquals("jo-number-101", actualJobOrders.get(0).getNumber());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getJoDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getDateCreated());
    assertEquals("test jo title", actualJobOrders.get(0).getTitle());
    assertEquals("test jo location", actualJobOrders.get(0).getLocation());
    assertEquals("12345", actualJobOrders.get(0).getZipCode());
    assertEquals("TEST", actualJobOrders.get(0).getDivision());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getStartDate());
    assertEquals(LocalDate.now(), actualJobOrders.get(0).getEndDate());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateStart());
    assertEquals(Double.valueOf(10D), actualJobOrders.get(0).getBillRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getBillPeriodRate());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateStart());
    assertEquals(Double.valueOf(9D), actualJobOrders.get(0).getPayRateEnd());
    assertEquals("perhour", actualJobOrders.get(0).getPayPeriodRate());
    assertEquals("3", actualJobOrders.get(0).getOpenings());
    assertEquals("3", actualJobOrders.get(0).getMaxSubs());
    assertEquals("New", actualJobOrders.get(0).getStatus());
    assertEquals("End Client A", actualJobOrders.get(0).getEndClient().getName());
    assertEquals(Integer.valueOf(2), actualJobOrders.get(0).getPrimaryOwner().getUser().getId());
    assertEquals("Dann", actualJobOrders.get(0).getPrimaryOwner().getUser().getFirstName());
    assertEquals("Hilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getLastName());
    assertEquals("dhilario", actualJobOrders.get(0).getPrimaryOwner().getUser().getUsername());
  }
}
