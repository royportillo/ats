package com.hrpo.ats;

import java.util.Arrays;
import java.util.List;
import com.hrpo.daxtra.dto.Candidate;

public class CandidateTestData {

  private final Candidate candidate1 = new Candidate();
  private final Candidate candidate2 = new Candidate();
  private final Candidate candidate3 = new Candidate();
  private final Candidate candidate4 = new Candidate();
  private final Candidate candidate5 = new Candidate();
  
  public CandidateTestData() {
    super();
    
    candidate1.setCandidateId("X50");
    candidate1.setSource("Dummy Source 1");
    candidate1.setFirstName("C1 First");
    candidate1.setLastName("C1 Last");
    candidate1.setFullName("C1 Fullname");
    candidate1.setMobile("+1358132101");
    candidate1.setPhone("+1234567890");
    candidate1.setEmail("c1@email.com");
    
    candidate2.setCandidateId("X51");
    candidate2.setSource("Dummy Source 2");
    candidate2.setFirstName("C2 First");
    candidate2.setLastName("C2 Last");
    candidate2.setFullName("C2 Fullname");
    candidate2.setMobile("+1358132102");
    candidate2.setPhone("+1234567892");
    candidate2.setEmail("c2@email.com");
    
    candidate3.setCandidateId("X52");
    candidate3.setSource("Dummy Source 3");
    candidate3.setFirstName("C3 First");
    candidate3.setLastName("C3 Last");
    candidate3.setFullName("C3 Fullname");
    candidate3.setMobile("+1358132103");
    candidate3.setPhone("+1234567893");
    candidate3.setEmail("c3@email.com");
    
    candidate4.setCandidateId("X53");
    candidate4.setSource("Dummy Source 4");
    candidate4.setFirstName("C4 First");
    candidate4.setLastName("C4 Last");
    candidate4.setFullName("C4 Fullname");
    candidate4.setMobile("+1358132104");
    candidate4.setPhone("+1234567894");
    candidate4.setEmail("c4@email.com");
    
    candidate5.setCandidateId("X54");
    candidate5.setSource("Dummy Source 5");
    candidate5.setFirstName("C5 First");
    candidate5.setLastName("C5 Last");
    candidate5.setFullName("C5 Fullname");
    candidate5.setMobile("+1358132105");
    candidate5.setPhone("+1234567895");
    candidate5.setEmail("c5@email.com");
  }

  public Candidate getCandidate1() {
    return candidate1;
  }

  public Candidate getCandidate2() {
    return candidate2;
  }

  public Candidate getCandidate3() {
    return candidate3;
  }

  public Candidate getCandidate4() {
    return candidate4;
  }

  public Candidate getCandidate5() {
    return candidate5;
  }
  
  public List<Candidate> getFiveMockCandidates() {
    return Arrays.asList(candidate1, candidate2, candidate3, candidate4, candidate5);
  }
}
