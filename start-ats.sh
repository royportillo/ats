#!/bin/bash

#
# This file is used to automatically start ATS daemon from within Jenkins build system.
# Usually, ATS will only be started if the build is successful.
#

java -Dserver.port=9000 -jar /var/lib/jenkins/workspace/ATS-0.1.0-SNAPSHOT/ats-core/target/ats-core-0.1.0-SNAPSHOT.jar &