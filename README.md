# Welcome to Application Tracking System (ATS)!

# Install

* OpenJDK 13
* Postgres 9.6+
* Maven 3.6.1
* Eclipse Cocoa 4.14 (With Maven support)
* Optionally, Eclipse plugin for HTML editor and SQL editor. 

# Eclipse Setup

* Use OpenJDK 13 in Installed JRE
* File -> Import -> Existing Maven Projects and point to `ats` your local git repository. Select the parent `pom.xml` module.
* Compile source and target is 13
* Import `ats/src/checkstyle/eclipse-java-google-style` in `Preferences -> Java -> Code Style -> Formatter`

# Running the project

##### Via command prompt

* Inside the `ats-core` folder, run the command `mvn spring-boot:run`

##### Using Eclipse

* Inside the `ats-core` project, under the `com.hrpo.ats` package, right click on `AtsApplication.java` then click Run As -> Java Application.


# Others

* Create a database name `ats` with username `postgres` and password `asdf` for development.
* Populate the database by running Ant target `load-sql` in `build-db.xml`

# Guidelines

* When doing a feature or bug fix, create your own branch using commands `git checkout -b <branch name>`.
* Convention on branch naming is `<author initial>/issue<number>`
* When creating a new API in DAO or service layer, create a JUnit for different scenarios.
* Before doing a `git push` remember to do a `git fetch` and `rebase` (if necessary) first.
* After commit/push, check if build is successful in bitbucket pipeline.
* Create Jenkins build and enter your branch name in the parameters. Officially test and validate your issue on that build.
* Submit pull request after official test in Jenkins build.


