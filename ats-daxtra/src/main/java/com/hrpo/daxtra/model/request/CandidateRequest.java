package com.hrpo.daxtra.model.request;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CandidateRequest implements Serializable {

  private static final long serialVersionUID = 2619762291759484857L;
  @JsonProperty("CandidateId")
  private String candidateId;
  @JsonProperty("Tags")
  private Tags tags;

  public CandidateRequest() {
    super();
  }

  public String getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(String candidateId) {
    this.candidateId = candidateId;
  }

  public Tags getTags() {
    return tags;
  }

  public void setTags(Tags tags) {
    this.tags = tags;
  }

  @Override
  public String toString() {
    return "Candidate [tags=" + tags + "]";
  }

}
