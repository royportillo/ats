package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import java.util.Optional;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StructuredOptions implements Serializable {
  
  private static final long serialVersionUID = -2243483563720559413L;
  
  @JsonProperty("FirstName")
  private String firstName;
  @JsonProperty("LastName")
  private String lastName;
  @JsonProperty("FullName")
  private String fullName;
  @JsonProperty("Contact")
  private Optional<Contact> contact = Optional.empty();
  @JsonProperty("User")
  private Optional<User> user = Optional.empty();

  public StructuredOptions() {
    super();
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Optional<Contact> getContact() {
    return contact;
  }

  public void setContact(Optional<Contact> contact) {
    this.contact = contact;
  }
  
  public Optional<User> getUser() {
    return user;
  }

  public void setUser(Optional<User> user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "StructuredOptions [firstName=" + firstName + ", lastName=" + lastName + ", fullName="
        + fullName + ", contact=" + contact + ", user=" + user + "]";
  }
}
