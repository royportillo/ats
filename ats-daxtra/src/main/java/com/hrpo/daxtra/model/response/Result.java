package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Result implements Serializable {
  
  private static final long serialVersionUID = -7814897686980907491L;
  
  @JsonProperty("CandidateId")
  private String candidateId;
  @JsonProperty("Score")
  private Double score;
  
  public Result() {
    super();
  }

  public String getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(String candidateId) {
    this.candidateId = candidateId;
  }

  public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  @Override
  public String toString() {
    return "Result [candidateId=" + candidateId + ", score=" + score + "]";
  }
  
}
