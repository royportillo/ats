package com.hrpo.daxtra.model.request;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DxRequest implements Serializable {

  private static final long serialVersionUID = 4550643047145321467L;

  @JsonProperty("Action")
  private String action;
  @JsonProperty("Options")
  private String options;
  @JsonProperty("Username")
  private String username;
  @JsonProperty("Password")
  private String password;
  @JsonProperty("Database")
  private String database;
  @JsonProperty("Query")
  private String query;
  @JsonProperty("Candidate")
  private CandidateRequest candidate;

  public DxRequest() {
    super();
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getOptions() {
    return options;
  }

  public void setOptions(String options) {
    this.options = options;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getDatabase() {
    return database;
  }

  public void setDatabase(String database) {
    this.database = database;
  }

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }


  public CandidateRequest getCandidate() {
    return candidate;
  }

  public void setCandidate(CandidateRequest candidate) {
    this.candidate = candidate;
  }


  @Override
  public String toString() {
    return "DxRequest [action=" + action + ", username=" + username + ", password=" + password
        + ", database=" + database + ", query=" + query + ", candidate=" + candidate + "]";
  }

}
