package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import java.util.Optional;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents the User node in Candidate API response.
 *
 */
public class User implements Serializable {

  private static final long serialVersionUID = 5678099913971665991L;
  
  //This contains the Source information of the Candidate
  @JsonProperty("Str2")
  private Optional<String> str2 = Optional.empty();
  
  public Optional<String> getStr2() {
    return str2;
  }

  public void setStr2(Optional<String> str2) {
    this.str2 = str2;
  }

  @Override
  public String toString() {
    return "User [Str2=" + str2 + "]";
  }
}
