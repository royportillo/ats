package com.hrpo.daxtra.model.request;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Tag implements Serializable {
  
  private static final long serialVersionUID = -3671698740576638671L;
  
  @JsonProperty("Name")
  private String name;
  @JsonProperty("Type")
  private String type;
  
  public Tag() {
    super();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "Tag [name=" + name + ", type=" + type + "]";
  }
  
}
