package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Status implements Serializable {
  
  private static final long serialVersionUID = -5986673303768109220L;
  
  @JsonProperty("Code")
  private String code;
  @JsonProperty("Description")
  private String description;
  @JsonProperty("Ref")
  private String ref;
  
  public Status() {
    super();
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }

  @Override
  public String toString() {
    return "Status [code=" + code + ", description=" + description + ", ref=" + ref + "]";
  }
}
