package com.hrpo.daxtra.model.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CandidateResponse implements Serializable {
  
  private static final long serialVersionUID = -2025967469862752948L;
  
  @JsonProperty("CandidateId")
  private String candidateId;
  @JsonProperty("ProfileType")
  private String rawCvFileType;
  @JsonProperty("Profile")
  private String rawCv;
  @JsonProperty("HRXML")
  private String hrXml;
  @JsonProperty("StructuredOptions")
  private StructuredOptions structuredOptions;

  public CandidateResponse() {
    super();
  }

  public String getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(String candidateId) {
    this.candidateId = candidateId;
  }
  
  public String getRawCvFileType() {
    return rawCvFileType;
  }

  public void setRawCvFileType(String rawCvFileType) {
    this.rawCvFileType = rawCvFileType;
  }

  public String getRawCv() {
    return rawCv;
  }

  public void setRawCv(String rawCv) {
    this.rawCv = rawCv;
  }

  public String getHrXml() {
    return hrXml;
  }

  public void setHrXml(String hrXml) {
    this.hrXml = hrXml;
  }

  public StructuredOptions getStructuredOptions() {
    return structuredOptions;
  }

  public void setStructuredOptions(StructuredOptions structuredOptions) {
    this.structuredOptions = structuredOptions;
  }

  @Override
  public String toString() {
    return "CandidateResponse [candidateId=" + candidateId + ", structuredOptions="
        + structuredOptions + "]";
  }
}
