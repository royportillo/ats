package com.hrpo.daxtra.dao;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import com.hrpo.daxtra.dto.Candidate;
import com.hrpo.daxtra.model.request.CandidateRequest;
import com.hrpo.daxtra.model.request.Tag;
import com.hrpo.daxtra.model.request.Tags;
import com.hrpo.daxtra.model.response.CandidateResponse;
import com.hrpo.daxtra.model.response.DxResponse;
import com.hrpo.daxtra.model.response.Result;
import com.hrpo.daxtra.model.response.StructuredOptions;

@Repository
public class DaxtraCandidateRestDao extends AbstractDaxtraRestDao implements DaxtraCandidateDao {

  Logger log = LoggerFactory.getLogger(DaxtraCandidateRestDao.class);

  // Remove candidate's resume info from the result.
  private static final String NO_HRXML_OPTIONS = "nohrxml";
  // Setting null to options results to default option which is with HRXML.
  private static final String WITH_HRXML_OPTIONS = null;
  private static final String WITH_CV_NO_HRXML_OPTIONS = "profile nohrxml";
  
  private static final String SEARCH_CANDIDATE = "search_candidates";
  private static final String GET_CANDIDATE = "get_candidate";

  public DaxtraCandidateRestDao() {
    super();
  }

  @Override
  public List<Candidate> findCandidateIdsByShortListName(String shortListName, String database) {

    final CandidateRequest candidate = new CandidateRequest();
    final Tags tags = new Tags();
    Tag tag = new Tag();
    tag.setName(shortListName);
    tag.setType("SLST");
    List<Tag> tagList = new ArrayList<>();
    tagList.add(tag);
    tags.setTags(tagList);

    candidate.setTags(tags);
    getDxRequest().setAction(SEARCH_CANDIDATE);
    getDxRequest().setCandidate(candidate);

    log.debug(getDxRequest().toString());

    ResponseEntity<DxResponse> response = sendExchange(database);

    List<Candidate> candidates = new ArrayList<>();
    for (Result r : response.getBody().getResults().getResultList()) {
      Candidate theCandidate = new Candidate();
      theCandidate.setCandidateId(r.getCandidateId());
      theCandidate.setScore(r.getScore());
      candidates.add(theCandidate);
    }

    log.debug(candidates.toString());

    return candidates;
  }

  @Override
  public Candidate findCandidateWithoutHrXmlByCandidateId(String candidateId, String database) {

    // set the option to not retrieve HRXML for performance reason.
    getDxRequest().setOptions(NO_HRXML_OPTIONS);
    
    return populateCandidateById(candidateId, database);
  }

  @Override
  public Candidate findCandidateWithHrXmlByCandidateId(String candidateId, String database) {
    
    getDxRequest().setOptions(WITH_HRXML_OPTIONS);
    
    return populateCandidateById(candidateId, database);
  }
  
  @Override
  public Candidate findCandidateWithCvByCandidateId(String candidateId, String database) {
    
    getDxRequest().setOptions(WITH_CV_NO_HRXML_OPTIONS);
    
    return populateCandidateById(candidateId, database);
  }
  
  private Candidate populateCandidateById(String candidateId, String database) {
    final CandidateRequest candidateReq = new CandidateRequest();
    
    getDxRequest().setAction(GET_CANDIDATE);
    
    candidateReq.setCandidateId(candidateId);
    getDxRequest().setCandidate(candidateReq);
    
    log.debug(getDxRequest().toString());
    
    ResponseEntity<DxResponse> response = sendExchange(database);
    CandidateResponse candidateRes = response.getBody().getCandidate();
    
    Candidate candidate = new Candidate();
    
    // optionally populated
    candidate.setRawCvFileType(candidateRes.getRawCvFileType());
    candidate.setRawCv(candidateRes.getRawCv());
    candidate.setHrXml(candidateRes.getHrXml());
    candidate.setCandidateId(candidateRes.getCandidateId());
    
    StructuredOptions structuredOptions = candidateRes.getStructuredOptions();
    candidate.setFirstName(structuredOptions.getFirstName());
    candidate.setLastName(structuredOptions.getLastName());
    candidate.setFullName(structuredOptions.getFullName());
    
    structuredOptions.getContact().ifPresent(c -> {
      candidate.setPhone(c.getPhone());
      candidate.setMobile(c.getMobile());
    });
    
    structuredOptions.getContact().ifPresent(c -> {
      candidate.setEmail(c.getEmail());
    });
    
    structuredOptions.getUser().ifPresent(u -> {
      u.getStr2().ifPresent(s -> {
        candidate.setSource(s);
      });
    });
    
    return candidate;
  }
}
