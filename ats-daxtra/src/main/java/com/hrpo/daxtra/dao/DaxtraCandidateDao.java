package com.hrpo.daxtra.dao;

import java.util.List;
import com.hrpo.daxtra.dto.Candidate;

public interface DaxtraCandidateDao {

  /**
   * Returns list of {@code Candidate} containing only Ids and score.
   * @see <a href="https://es-demo.daxtra.com/webservices/docs/index.html#search-candidates">
   *     search_candidates</a> API.
   * 
   * @param shortListName the short list name that contains list of candidates.
   * @param database the daxtra webservice database to get records from.
   * @return The list of {@code Candidate} containing Ids and score.
   */
  List<Candidate> findCandidateIdsByShortListName(String shortListName, String database);

  /**
   * Returns {@code Candidate} detail information based on {@code candidateId}.
   * @see <a href="https://es-demo.daxtra.com/webservices/docs/index.html#get-candidate">
   *     get_candidate</a> API.
   * 
   * @param candidateId the Id of the {@code Candidate} to be retrieved.
   * @param database the daxtra webservice database to get records from.
   * @return The {@code Candidate} that includes detailed information (but without HRXML).
   */
  Candidate findCandidateWithoutHrXmlByCandidateId(String candidateId, String database);
  
  /**
   * Returns {@code Candidate}'s detailed information with Base64 encoded HRXML
   *     based on {@code candidateId}.
   * @see <a href="https://es-demo.daxtra.com/webservices/docs/index.html#get-candidate">
   *     get_candidate</a> API.
   * 
   * @param candidateId the Id of the {@code Candidate} with HRXML to be retrieved.
   * @param database the daxtra webservice database to get records from.
   * @return The {@code Candidate} information that includes HRXML in Base64 encoded format.
   */
  Candidate findCandidateWithHrXmlByCandidateId(String candidateId, String database);
  
  /**
   * Returns {@code Candidate} detail information based on {@code candidateId}.
   * @see <a href="https://es-demo.daxtra.com/webservices/docs/index.html#get-candidate">
   *     get_candidate</a> API.
   * 
   * @param candidateId the Id of the {@code Candidate} to be retrieved.
   * @param database the daxtra webservice database to get records from.
   * @return The {@code Candidate} that includes detailed information with Base64-encoded
   *     raw CV (HRXML is excluded).
   */
  Candidate findCandidateWithCvByCandidateId(String candidateId, String database);
}
