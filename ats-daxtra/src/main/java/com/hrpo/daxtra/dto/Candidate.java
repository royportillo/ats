package com.hrpo.daxtra.dto;

import java.io.Serializable;

public class Candidate implements Serializable {

  private static final long serialVersionUID = 8144045591595244380L;
  
  private Integer id;
  private String candidateId;
  //For now use String type for source
  private String source;
  private Double score;
  private String firstName;
  private String lastName;
  private String fullName;
  private String phone;
  private String mobile;
  private String email;
  private String hrXml;
  private String rawCvFileType;
  private String rawCv;

  public Candidate() {
    super();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(String candidateId) {
    this.candidateId = candidateId;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getHrXml() {
    return hrXml;
  }

  public void setHrXml(String hrXml) {
    this.hrXml = hrXml;
  }
  
  public String getRawCvFileType() {
    return rawCvFileType;
  }

  public void setRawCvFileType(String rawCvMimeType) {
    this.rawCvFileType = rawCvMimeType;
  }

  public String getRawCv() {
    return rawCv;
  }

  public void setRawCv(String rawCv) {
    this.rawCv = rawCv;
  }

  @Override
  public String toString() {
    return "Candidate [id=" + id + ", candidateId=" + candidateId + ", score=" + score
        + ", firstName=" + firstName + ", lastName=" + lastName + ", fullName=" + fullName
        + ", phone=" + phone + ", email=" + email + "]";
  }
}
