package com.hrpo.daxtra.dao;

import org.junit.jupiter.api.BeforeAll;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;
import com.hrpo.daxtra.model.response.Status;

/**
 * An abstract test class that encapsulates reusable information for all DAXTRA module tests.
 */
abstract class AbstractDaoTest {

  private final String uri = "https://es-tok-101.daxtra.com/ws/dispatch";
  private final String database = "ws_24hrpo";
  
  private static final Status statusCode100 = new Status();
  
  @Mock
  private RestTemplate restTemplate;
  
  public AbstractDaoTest() {
    super();
  }
  
  @BeforeAll
  static void initAll() {
    statusCode100.setCode("100");
    statusCode100.setDescription("OK");
  }
  
  protected RestTemplate getRestTemplate() {
    return restTemplate;
  }
  
  protected String getEndpointUri() {
    return uri;
  }
  
  protected Status getStatusCode100() {
    return statusCode100;
  }
  
  protected String getDatabase() {
    return database;
  }
}
