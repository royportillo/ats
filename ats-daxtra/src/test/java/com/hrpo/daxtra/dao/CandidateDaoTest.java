package com.hrpo.daxtra.dao;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import com.hrpo.daxtra.DaxtraApiModuleConfiguration;
import com.hrpo.daxtra.dto.Candidate;
import com.hrpo.daxtra.model.response.CandidateResponse;
import com.hrpo.daxtra.model.response.DxResponse;
import com.hrpo.daxtra.model.response.Result;
import com.hrpo.daxtra.model.response.Results;
import com.hrpo.daxtra.model.response.StructuredOptions;

@SpringBootTest
@ContextConfiguration(classes = DaxtraApiModuleConfiguration.class)
class CandidateDaoTest extends AbstractCandidateDaoTest {
  
  public CandidateDaoTest() {
    super();
  }
  
  @Test
  @DisplayName("Test find candidate IDs by shortlist name")
  void testFindCandidateIdsByShortListName() {

    DxResponse response = new DxResponse();
    response.setStatus(getStatusCode100());

    Results results = new Results();
    Result result = new Result();
    result.setCandidateId("XC22846");
    result.setScore(100.0);

    List<Result> resultList = new ArrayList<>();
    resultList.add(result);
    results.setResultList(resultList);
    response.setResults(results);

    Mockito
    .when(getRestTemplate().exchange(eq(getEndpointUri()), 
        eq(HttpMethod.POST), any(HttpEntity.class), eq(DxResponse.class)))
    .thenReturn(new ResponseEntity<DxResponse>(response, HttpStatus.OK));

    List<Candidate> candidates = getCandidateDao().findCandidateIdsByShortListName("test MRE", 
        getDatabase());

    assertNotNull(candidates);
    assertEquals("XC22846", candidates.get(0).getCandidateId()); 
    assertEquals(100.0, candidates.get(0).getScore().doubleValue());
  }
  
  @Test
  @DisplayName("Test find candidate details without HRXML")
  void testFindCandidateWithoutHrXmlByCandidateId() {

    DxResponse response = new DxResponse();
    response.setStatus(getStatusCode100());
    
    CandidateResponse candidate = new CandidateResponse();
    candidate.setCandidateId("DUMMY-CANDIDATE-ID");
    
    candidate.setStructuredOptions(getStructuredOptions1TestData());
    response.setCandidate(candidate);
    
    Mockito
    .when(getRestTemplate().exchange(eq(getEndpointUri()), 
        eq(HttpMethod.POST), any(HttpEntity.class), eq(DxResponse.class)))
    .thenReturn(new ResponseEntity<DxResponse>(response, HttpStatus.OK));

    Candidate result = getCandidateDao()
        .findCandidateWithoutHrXmlByCandidateId("DUMMY-CANDIDATE-ID", getDatabase());
    assertNotNull(result);
    assertNull(result.getHrXml());
    assertEquals("DUMMY-CANDIDATE-ID", result.getCandidateId());
    assertEquals("Dummy Source", result.getSource());
    assertEquals("Dummy FirstName", result.getFirstName());
    assertEquals("Dummy LastName", result.getLastName());
    assertEquals("Dummy FullName", result.getFullName());
    assertEquals("+630123456789", result.getPhone());
    assertEquals("+630123456789", result.getMobile());
    assertEquals("test@gmail.com", result.getEmail());
  }
  
  @Test
  @DisplayName("Test find candidate details with HRXML")
  void testFindCandidateWithHrXmlByCandidateId() {

    DxResponse response = new DxResponse();
    response.setStatus(getStatusCode100());
    
    CandidateResponse candidate = new CandidateResponse();
    candidate.setCandidateId("DUMMY-CANDIDATE-ID");

    candidate.setHrXml("DummyBase64EncodedHRXML");
    candidate.setStructuredOptions(getStructuredOptions1TestData());
    response.setCandidate(candidate);
    
    Mockito
    .when(getRestTemplate().exchange(eq(getEndpointUri()), 
        eq(HttpMethod.POST), any(HttpEntity.class), eq(DxResponse.class)))
    .thenReturn(new ResponseEntity<DxResponse>(response, HttpStatus.OK));

    Candidate result = getCandidateDao()
        .findCandidateWithHrXmlByCandidateId("DUMMY-CANDIDATE-ID", getDatabase());
    assertNotNull(result);
    assertNotNull(result.getHrXml());
    assertAll("Candidate Details",
        () -> assertEquals("DummyBase64EncodedHRXML", result.getHrXml()),
        () -> assertEquals("DUMMY-CANDIDATE-ID", result.getCandidateId()),
        () -> assertEquals("Dummy Source", result.getSource()),
        () -> assertEquals("Dummy FirstName", result.getFirstName()),
        () -> assertEquals("Dummy LastName", result.getLastName()),
        () -> assertEquals("Dummy FullName", result.getFullName()),
        () -> assertEquals("+630123456789", result.getPhone()),
        () -> assertEquals("+630123456789", result.getMobile()),
        () -> assertEquals("test@gmail.com", result.getEmail()));
  }
  
  @Test
  @DisplayName("Test find candidate details without HRXML (Contact is optional)")
  void testFindCandidateWithoutHrXmlByCandidateId_NoContactDetails() {
    
    DxResponse response = new DxResponse();
    response.setStatus(getStatusCode100());
    
    CandidateResponse candidate = new CandidateResponse();
    candidate.setCandidateId("DUMMY-CANDIDATE-ID");
    
    StructuredOptions testDataWithNoContact = getStructuredOptions1TestData();
    
    // set contact to null
    testDataWithNoContact.setContact(Optional.ofNullable(null));
    
    candidate.setStructuredOptions(testDataWithNoContact);
    response.setCandidate(candidate);
    
    Mockito
    .when(getRestTemplate().exchange(eq(getEndpointUri()), 
        eq(HttpMethod.POST), any(HttpEntity.class), eq(DxResponse.class)))
    .thenReturn(new ResponseEntity<DxResponse>(response, HttpStatus.OK));
    
    Candidate result = getCandidateDao()
        .findCandidateWithoutHrXmlByCandidateId("DUMMY-CANDIDATE-ID", getDatabase());
    assertNotNull(result);
    assertNull(result.getHrXml());
    assertEquals("DUMMY-CANDIDATE-ID", result.getCandidateId());
    assertEquals("Dummy Source", result.getSource());
    assertEquals("Dummy FirstName", result.getFirstName());
    assertEquals("Dummy LastName", result.getLastName());
    assertEquals("Dummy FullName", result.getFullName());
    
    assertNull(result.getPhone());
    assertNull(result.getEmail());
  }
  
  @Test
  @DisplayName("Test find candidate details without HRXML (User is optional)")
  void testFindCandidateWithoutHrXmlByCandidateId_NoUserDetails() {
    
    DxResponse response = new DxResponse();
    response.setStatus(getStatusCode100());
    
    CandidateResponse candidate = new CandidateResponse();
    candidate.setCandidateId("DUMMY-CANDIDATE-ID");
    
    StructuredOptions testDataWithNoUser = getStructuredOptions1TestData();
    
    // set contact to null
    testDataWithNoUser.setUser(Optional.ofNullable(null));
    
    candidate.setStructuredOptions(testDataWithNoUser);
    response.setCandidate(candidate);
    
    Mockito
    .when(getRestTemplate().exchange(eq(getEndpointUri()), 
        eq(HttpMethod.POST), any(HttpEntity.class), eq(DxResponse.class)))
    .thenReturn(new ResponseEntity<DxResponse>(response, HttpStatus.OK));
    
    Candidate result = getCandidateDao()
        .findCandidateWithoutHrXmlByCandidateId("DUMMY-CANDIDATE-ID", getDatabase());
    assertNotNull(result);
    assertNull(result.getHrXml());
    assertEquals("DUMMY-CANDIDATE-ID", result.getCandidateId());
    assertEquals("Dummy FirstName", result.getFirstName());
    assertEquals("Dummy LastName", result.getLastName());
    assertEquals("Dummy FullName", result.getFullName());
    
    assertNull(result.getSource());
  }
  
  @Test
  @DisplayName("Test find candidate details with Raw CV")
  void testFindCandidateWithCvByCandidateId() {

    DxResponse response = new DxResponse();
    response.setStatus(getStatusCode100());
    
    CandidateResponse candidate = new CandidateResponse();
    candidate.setCandidateId("DUMMY-CANDIDATE-ID");

    candidate.setRawCv("DummyBase64EncodedCv");
    candidate.setRawCvFileType("dum;dummy/file-type");
    candidate.setStructuredOptions(getStructuredOptions1TestData());
    response.setCandidate(candidate);
    
    Mockito
    .when(getRestTemplate().exchange(eq(getEndpointUri()), 
        eq(HttpMethod.POST), any(HttpEntity.class), eq(DxResponse.class)))
    .thenReturn(new ResponseEntity<DxResponse>(response, HttpStatus.OK));

    Candidate result = getCandidateDao()
        .findCandidateWithCvByCandidateId("DUMMY-CANDIDATE-ID", getDatabase());
    assertNotNull(result);
    assertAll("Candidate Details",
        () -> assertEquals("DummyBase64EncodedCv", result.getRawCv()),
        () -> assertEquals("dum;dummy/file-type", result.getRawCvFileType()),
        () -> assertEquals("DUMMY-CANDIDATE-ID", result.getCandidateId()),
        () -> assertEquals("Dummy Source", result.getSource()),
        () -> assertEquals("Dummy FirstName", result.getFirstName()),
        () -> assertEquals("Dummy LastName", result.getLastName()),
        () -> assertEquals("Dummy FullName", result.getFullName()),
        () -> assertEquals("+630123456789", result.getPhone()),
        () -> assertEquals("+630123456789", result.getMobile()),
        () -> assertEquals("test@gmail.com", result.getEmail()));
  }
}
